<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Language" content="en-US" >
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<title>IB Deployer :: Dashboard</title>
<script type="text/javascript" src="/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.24.custom.min.js"></script>
<script type="text/javascript" src="/js/kendo.web.min.js"></script>
<script type="text/javascript" src="/js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="/js/jquery.scrollTo-1.4.2-min.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/date.js"></script>
<link href="/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/kendo/kendo.common.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/kendo/kendo.silver.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/smoothness/jquery-ui-1.8.21.custom.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/ib-brand/jquery-ui-1.8.21.custom.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/favicon.ico" rel="shortcut icon" >
<link href="/css/jquery.dataTables_themeroller.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/jquery.dataTables.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/colorbox.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/jcarousel/skin.css" media="screen" rel="stylesheet" type="text/css" >

    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->

	<link rel="stylesheet" href="/cache/04f29a58ed28df7f8bb958ea0b91af6d.css">
<script type="text/javascript" src="/cache/b66b1b4b597b3d75af6fada25faa2776.js"></script></head>
<body>

<div class="container">
    <!--START NAVIGATION  -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">
                    						                    IB Deployer
						                                    </a>
                <ul class="nav">
    <li class="active">
        <a href="/dashboard">Dashboard</a>
    </li>
    <li>
        <a href="/deploy">Deploy</a>
    </li>
    <li>
        <a href="/artifacts">Artifacts</a>
    </li>
    <li>
        <a href="/history">History</a>
    </li>
    <li id="dropdownAdmin" class="dropdown">
        <a href="#dropdownAdmin" data-toggle="dropdown" class="dropdown-toggle">Admin<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="/admin/Artifacts">Artifacts</a>
            </li>
            <li>
                <a href="/admin/permissions">Permissions</a>
            </li>
            <li>
                <a href="/admin/prune">Data Prune</a>
            </li>
            <li>
                <a href="/admin/reports">Reports</a>
            </li>
            <li>
                <a href="/monitor/status">Monitor Status</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="/help">Help</a>
    </li>
</ul>                                <ul id="auth_info" class="nav pull-right">
                    <li class="navbar-text">Welcome, {{user['first_name']}}</li>
                    <li class="divider-vertical"></li>
                    <li><a href="/auth/logout">Logout</a></li>                </ul>
            </div>
        </div>
    </div>
    <!-- END NAV -->

    
        <div class="page-header">
        <h1>Dashboard</h1>
    </div>

    <div class="row">
        <div class="span12">
            <h2 class="dashboard">Current Activity</h2>
        </div>
    </div>

    <div class="row">
        <div class="span12">
            <strong>Deployments</strong>&nbsp;&nbsp;<span id="deps_count" class="badge badge-info">0</span>
            <table id="current_deployments" class="table table-bordered table-striped table-condensed dashboard" style="margin-top:5px;">
                <tr>
                    <th>ID</th>
                    <th>Start Date/Time</th>
                    <th>Selections</th>
                    <th>Status</th>
                </tr>
            </table>
        </div>

    </div>

    <div class="row">
        <div class="span6">
            <strong>Artifacts</strong>&nbsp;&nbsp;<span id="artifacts_count" class="badge badge-info">0</span>
            <div class="dashboard-list-wrapper">
                <ul id="artifacts_deploying"></ul>
            </div>
        </div>

        <div class="span6">
            <strong>Servers</strong>&nbsp;&nbsp;<span id="servers_count" class="badge badge-info">0</span>
            <div class="dashboard-list-wrapper">
                <ul id="servers_deploying"></ul>
            </div>
        </div>
    </div>

    <!--
    <div class="grid_12" style="border: 1px solid #ccc; margin: 20px 10px;"></div>

    <div class="container_12">
        <div class="grid_12">
            <h2>Recent Deployments</h2>
            <table id="recent_deployments">
                <tr>
                    <th>ID</th>
                    <th>Date/Time</th>
                    <th>Selections</th>
                </tr>
            </table>
        </div>
    </div>
    -->
<script type="text/javascript">
    getCurrentDeployments();
    getDeployingArtifacts();
    getDeployingServers();
</script>
    <!-- START FOOTER -->
    <footer id="footer" class="footer">
        <a href="#" class="btn btn-mini"><i class="icon-arrow-up"></i></a>
    </footer>
    <!-- END FOOTER -->
</div>

</body>
</html>
