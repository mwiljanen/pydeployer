<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Language" content="en-US" >
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<title>IB Deployer :: Monitor :: Status</title>
<link href="/favicon.ico" rel="shortcut icon" >    </head>
    <body>
        <div>
            <code>
                <strong>Supervisor:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/><br/>

                                <strong>deployer_worker_00:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>deployer_worker_01:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>deployer_worker_02:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>deployer_worker_03:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>deployer_worker_04:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>deployer_worker_05:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>deployer_worker_06:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>deployer_worker_07:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>selenium_server:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_00:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_01:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_02:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_03:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_04:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_05:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_06:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_07:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_08:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_09:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_10:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_11:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_12:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_13:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_14:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_15:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_16:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_17:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_18:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_19:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_20:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_21:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_22:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_23:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_24:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_25:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_26:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_27:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_28:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_29:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_30:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>server_group_worker_31:</strong>
                                <span style="color:#090;">
                                RUNNING</span><br/>
                                <strong>xvfb:</strong>
                                <span style="color:#900;">
                                FATAL</span><br/>
                                <br/>

                <strong>Deployer database query test:</strong>
                                <span style="color:#090;">
                                OK</span><br/><br/>

                <strong>Nexus version retrieval test:</strong>
                                <span style="color:#090;">
                                OK</span><br/><br/>

                <strong>Hostinfo HTTP response test:</strong>
                                <span style="color:#090;">
                                OK</span><br/><br/>

                <strong>Zenoss API response test:</strong>
                                <span style="color:#090;">
                                OK</span><br/><br/>

                <strong>JIRA ping test:</strong>
                                <span style="color:#900;">
                                FAILURE</span>
            </code>

            <p>
                <code>
                    Total time for all checks in milleseconds (ms):<br/>
                    2446.49 complete
                </code>
            </p>

        </div>
    </body>
</html>
