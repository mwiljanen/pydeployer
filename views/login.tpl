<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Language" content="en-US" >
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<title>IB Deployer :: Login</title>
<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.24.custom.min.js"></script>
<link href="/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/common.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/favicon.ico" rel="shortcut icon" >
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div id="admin-wrapper" class="span3 offset4">
                    <!--START LOGO  -->
                    <div id="logo">
                        <h1><strong>IB</strong> Deployer</h1>

                    </div>
                    <!-- END LOGO -->
                    
<ul id="messages">
%if (invalid_login):
<li style="color: #c00;">Invalid credentials. Please try again.</li>	
%end
</ul>
    
<form id="login" enctype="application/x-www-form-urlencoded" class="well" style="background-color:#e5e5e5;" action="/login" method="post"><dl class="zend_form">
<dt id="username-label"><label for="username" class="required">Username:</label></dt>
<dd id="username-element">
<input type="text" name="username" id="username" value="" style="width: 150px;"></dd>
<dt id="password-label"><label for="password" class="required">Password:</label></dt>
<dd id="password-element">
<input type="password" name="password" id="password" value="" style="width: 150px;"></dd>

<input type="submit" name="submit" id="submit" value="Login" class="btn btn-primary" style="margin-top: 10px;"></dl></form>
<script type="text/javascript">
    // start with fresh localStorage
    localStorage.clear();
</script>                </div>
            </div>

        </div>
    </body>
</html>
