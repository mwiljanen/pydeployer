<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Language" content="en-US" >
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<title>IB Deployer :: Admin :: Data Prune</title>
<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.24.custom.min.js"></script>
<script type="text/javascript" src="/js/kendo.web.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="/js/admin/prune/index.js"></script>
<script type="text/javascript" src="/js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="/js/jquery.scrollTo-1.4.2-min.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/date.js"></script>
<link href="/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/kendo/kendo.common.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/kendo/kendo.silver.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/smoothness/jquery-ui-1.8.21.custom.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/ib-brand/jquery-ui-1.8.21.custom.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/favicon.ico" rel="shortcut icon" >
<link href="/css/jquery.dataTables_themeroller.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/jquery.dataTables.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/colorbox.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/jcarousel/skin.css" media="screen" rel="stylesheet" type="text/css" >

    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->

	<link rel="stylesheet" href="/cache/04f29a58ed28df7f8bb958ea0b91af6d.css">
<script type="text/javascript" src="/cache/b66b1b4b597b3d75af6fada25faa2776.js"></script></head>
<body>

<div class="container">
    <!--START NAVIGATION  -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">
                    						                    IB Deployer
						                                    </a>
                <ul class="nav">
    <li>
        <a href="/dashboard">Dashboard</a>
    </li>
    <li>
        <a href="/deploy">Deploy</a>
    </li>
    <li>
        <a href="/artifacts">Artifacts</a>
    </li>
    <li>
        <a href="/history">History</a>
    </li>
    <li class="dropdown" id="dropdownAdmin">
        <a href="#dropdownAdmin" data-toggle="dropdown" class="dropdown-toggle">Admin<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="/admin/Artifacts">Artifacts</a>
            </li>
            <li>
                <a href="/admin/permissions">Permissions</a>
            </li>
            <li class="active">
                <a href="/admin/prune">Data Prune</a>
            </li>
            <li>
                <a href="/admin/reports">Reports</a>
            </li>
            <li>
                <a href="/monitor/status">Monitor Status</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="/help">Help</a>
    </li>
</ul>                                <ul id="auth_info" class="nav pull-right">
                    <li class="navbar-text">Welcome, {{user['first_name']}}</li>
                    <li class="divider-vertical"></li>
                    <li><a href="/auth/logout">Logout</a></li>                </ul>
            </div>
        </div>
    </div>
    <!-- END NAV -->

            <div id="breadcrumb" class="row"><div class="span12"><div class="breadcrumb"><a href="#dropdownAdmin">Admin</a>&nbsp;&nbsp;/&nbsp;&nbsp;Data Prune</div></div></div>
        
    
<div class="page-header">
    <h1>Admin &#0187; Data Prune</h1>
</div>

<div class="row">
    <div class="span12">
        <div id="alerts"></div>

        <div id="accordion" class="accordion">

            <div class="accordion-group">
                <div class="accordion-heading">
                    <strong><a class="accordion-toggle" href="#collapseOne" data-parent="#accordion" data-toggle="collapse">Delete deployment logs and screenshots from a date/time period</a></strong>
                </div>
                <div id="collapseOne" class="accordion-body in collapse">
                    <div class="accordion-inner">
                        <form id="prune-by-dt-form" class="well form-inline" action="">

                            <label for="startDT">Start</label>
                            <input type="text" required data-required-msg="Required" name="startDT" id="startDT" class="datetimepicker"/>

                            <label for="endDT">End</label>
                            <input type="text" required data-required-msg="Required" name="endDT" id="endDT" class="datetimepicker"/>

                            <input id="prune-by-dt" class="btn btn-danger" type="button" value="Delete"/>
                            <input id="clear-dt-form" class="btn" type="button" value="Clear"/>

                        </form>
                    </div>
                </div>
            </div>

            <div class="accordion-group">
                <div class="accordion-heading">
                    <strong><a class="accordion-toggle" href="#collapseTwo" data-parent="#accordion" data-toggle="collapse">Delete a specific deployment's logs and screenshots</a></strong>
                </div>
                <div id="collapseTwo" class="accordion-body collapse">
                    <div class="accordion-inner">
                        <form id="prune-by-id-form" class="well form-inline" action="">

                            <label for="uniqueId">Unique ID</label>
                            <input type="text" required data-required-msg="Required" name="uniqueId" id="uniqueId"/>

                            <input id="prune-by-id" class="btn btn-danger" type="button" value="Delete"/>
                            <input id="clear-id-form" class="btn" type="button" value="Clear"/>

                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
    <!-- START FOOTER -->
    <footer id="footer" class="footer">
        <a href="#" class="btn btn-mini"><i class="icon-arrow-up"></i></a>
    </footer>
    <!-- END FOOTER -->
</div>

</body>
</html>
