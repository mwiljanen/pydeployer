<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Language" content="en-US" >
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<title>IB Deployer :: Deploy</title>
<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.8.24.custom.min.js"></script>
<script type="text/javascript" src="/js/kendo.web.min.js"></script>
<script type="text/javascript" src="/js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="/js/jquery.scrollTo-1.4.2-min.js"></script>
<script type="text/javascript" src="/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/js/date.js"></script>
<link href="/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/kendo/kendo.common.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/kendo/kendo.silver.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/smoothness/jquery-ui-1.8.21.custom.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/ib-brand/jquery-ui-1.8.21.custom.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/favicon.ico" rel="shortcut icon" >
<link href="/css/jquery.dataTables_themeroller.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/jquery.dataTables.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/colorbox.css" media="screen" rel="stylesheet" type="text/css" >
<link href="/css/jcarousel/skin.css" media="screen" rel="stylesheet" type="text/css" >

    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
            filter: none;
        }
    </style>
    <![endif]-->

	<link rel="stylesheet" href="/cache/04f29a58ed28df7f8bb958ea0b91af6d.css">
<script type="text/javascript" src="/cache/b66b1b4b597b3d75af6fada25faa2776.js"></script></head>
<body>

<div class="container">
    <!--START NAVIGATION  -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="brand" href="#">
                    						                    IB Deployer
						                                    </a>
                <ul class="nav">
    <li>
        <a href="/dashboard">Dashboard</a>
    </li>
    <li class="active">
        <a href="/deploy">Deploy</a>
    </li>
    <li>
        <a href="/artifacts">Artifacts</a>
    </li>
    <li>
        <a href="/history">History</a>
    </li>
    <li id="dropdownAdmin" class="dropdown">
        <a href="#dropdownAdmin" data-toggle="dropdown" class="dropdown-toggle">Admin<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="/admin/Artifacts">Artifacts</a>
            </li>
            <li>
                <a href="/admin/permissions">Permissions</a>
            </li>
            <li>
                <a href="/admin/prune">Data Prune</a>
            </li>
            <li>
                <a href="/admin/reports">Reports</a>
            </li>
            <li>
                <a href="/monitor/status">Monitor Status</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="/help">Help</a>
    </li>
</ul>                                <ul id="auth_info" class="nav pull-right">
                    <li class="navbar-text">Welcome, {{user['first_name']}}</li>
                    <li class="divider-vertical"></li>
                    <li><a href="/auth/logout">Logout</a></li>                </ul>
            </div>
        </div>
    </div>
    <!-- END NAV -->

    
        <div class="page-header">
        <h1>Deploy</h1>
    </div>

    <div class="row">
        <div class="span10">
            <div id="deployWell" class="well well-large">
                <form id="deployForm" action="/deployer/index" method="post" style="display: none;">

                    <div id="deployFormHeader">
                        <div class="span3" style="margin-left: 30px;">Artifact</div>
                        <div class="span2" style="margin-left: 5px;">Environment</div>
                        <div class="span2" style="margin-left: 38px;">Version</div>
                        <div class="span2" style="margin-left: 5px;">JIRA Issue Key</div>
                    </div>

                    <div id="artifactsToDeploy" style="clear: both;">

                    </div>

                    <button id="addArtifact" class="btn" type="button"><i class="icon-plus"></i> Add Artifact</button>

                    <div class="form-actions">
                        
    <!-- DEPLOYMENT OPTIONS -->
    <!--suppress HtmlFormInputWithoutLabel -->
<div id="deployment-options">Deployment Options&nbsp;&nbsp;<button id="dep-op-view" class="btn btn-mini" type="button"><i class="icon-circle-arrow-down"></i></button>&nbsp;&nbsp;<button id="dep-op-help" class="btn btn-mini btn-info" type="button"><i class="icon-question-sign icon-white"></i></button></div>
    <div id="deployment-options-wrapper" style="display: none;" class="well form-horizontal">
        <fieldset>
            <div class="control-group">
                <label class="control-label">Servers to exclude</label>
                <div class="controls">
                    <textarea rows="2" class="servers" id="serverExcludes" name="serverExcludes"
                           title="Server Excludes"
                           data-content="A comma-separated list of server (short) hostnames (i.e. envname01-datacenter01a,envname02-datacenter01a)"></textarea>
                    <span class="help-inline">A comma-separated list of server (short) hostnames</span>
                </div>
            </div>
            <div class="help-spacer"></div>
            <div class="control-group">
                <label id="options-label" class="control-label" title="Options"
                    data-content="Various options to manipulate the deployment process.  Hover over the checkboxes anytime for more information."><span id="options-label-span">Options</span></label>
                <div class="controls">
                    <label for="skipVersionChecks" class="checkbox inline">
                        <input class="deployer-option" id="skipVersionChecks" name="skipVersionChecks"
                               title="Skips post-deployment version verification. Will force deployment to all servers for selected artifacts." type="checkbox" value="1"
                            /> Skip Version Checks
                    </label>

                    <label for="skipLbOps" class="checkbox inline">
                        <input class="deployer-option" id="skipLbOps" name="skipLbOps"
                               title="Skips load balancer pool operations." type="checkbox" value="1"
                            /> Skip Load Balancer Operations
                    </label>

					<label for="skipDepDeploy" class="checkbox inline">
						<input class="deployer-option" id="skipDepDeploy" name="skipDepDeploy"
							   title="Skips the deployment of artifact dependencies." type="checkbox" value="1"
							/> Skip Artifact Dependency Deployment
					</label>

                    <label for="ignoreZenossProdState" class="checkbox inline">
                        <input class="deployer-option" id="ignoreZenossProdState" name="ignoreZenossProdState"
                               title="Ignore initial Zenoss production state; will allow deployment to servers regardless of Zenoss production state." type="checkbox" value="1"
                            /> Ignore Zenoss Production State
                    </label>

					<label for="resetZenossProdState" class="checkbox inline">
						<input class="deployer-option" id="resetZenossProdState" name="resetZenossProdState"
							   title="Ignore initial Zenoss production state AND reset Zenoss production states based on deployment environment" type="checkbox" value="1"
							/> Reset Zenoss Production State
					</label>

					<label for="ignoreArtifactDeployStatus" class="checkbox inline">
						<input class="deployer-option" id="ignoreArtifactDeployStatus" name="ignoreArtifactDeployStatus"
							   title="Ignore artifact deployment status; will allow concurrent deployments of the same artifact-environment combination." type="checkbox" value="1"
							/> Ignore Artifact Deployment Status
					</label>

					<label for="clearStatus" class="checkbox inline">
                        <input class="deployer-option" id="clearStatus" name="clearStatus"
                               title="Last deployment failed or aborted? This clears the deployment status for the selected artifacts and their servers." type="checkbox" value="1"
                            /> Clear Deploy Status
                    </label>

                    <label for="debug" class="checkbox inline">
                        <input class="deployer-option" id="debug" name="debug" type="checkbox" value="1"
                             title="Toggle debug logging"/> Debug
                    </label>

					<label for="aggressiveServiceMgmt" class="checkbox inline">
						<input class="deployer-option" id="aggressiveServiceMgmt" name="aggressiveServiceMgmt" type="checkbox" value="1"
														   title="Perform operations simultaneously on as many servers as possible (based on available workers).
							   As a safety precaution, you must have specific server excludes or includes selected on each artifact in order to use this option."/> Aggressive Mode
					</label>
                </div>
            </div>
            <div class="help-spacer"></div>
            <div id="manage-puppet-agent" class="control-group">
                <label id="manage-puppet-agent-label" class="control-label" title="Manage Puppet Agent"
                       data-content="On each server prior to deployment, the Puppet agent will be stopped, started, or restarted according to the selection made on this form.">Manage Puppet Agent</label>
                <div class="controls">
                    <label class="radio inline"><input id="puppetAgent0" name="puppetAgent" type="radio" value="none"
                        checked="checked"/>None</label>
                    <label class="radio inline"><input id="puppetAgent1" name="puppetAgent" type="radio" value="stop"
                        />Stop</label>
                    <label class="radio inline"><input id="puppetAgent2" name="puppetAgent" type="radio" value="start"
                        />Start</label>
                    <label class="radio inline"><input id="puppetAgent3" name="puppetAgent" type="radio" value="restart"
                        />Restart</label>
			    <p class="help-block"><strong>Waits 30 seconds for catalog run to complete, before proceeding with deployment</strong></p>
			</div>
		    </div>
		    <div class="help-spacer"></div>
		    <div id="manage-service" class="control-group">
			<label id="manage-service-label" class="control-label" title="Manage Service"
			    data-content="Skips deployment and will stop, start, or restart an artifact's service according to the selection made on this form.">Manage Service</label>
			<div class="controls">
			    <label class="radio inline"><input id="manageService0" name="manageService" type="radio" value="none"
				checked="checked"/>None</label>
                    <label class="radio inline"><input id="manageService1" name="manageService" type="radio" value="stop"
                        />Stop</label>
                    <label class="radio inline"><input id="manageService2" name="manageService" type="radio" value="start" class="has-tooltip"
						   title="Reset Zenoss Production State will be automatically checked when starting services from a stopped state."
                        />Start</label>
                    <label class="radio inline"><input id="manageService3" name="manageService" type="radio" value="restart"
                        />Restart</label>
                    <p class="help-block"><strong><em>Skips</em> deployment and manages an artifact's service instead</strong></p>
                </div>
            </div>
        </fieldset>
    </div>
    <!-- END DEPLOYMENT OPTIONS -->

<div id="deploy-buttons-wrapper" style="width: 144px;" class="well">
    <div class="well" style="text-align:center; background-color: #FFD7BF; margin: 0 auto; padding: 5px 0; width: 142px;">
        <span style="font-weight:bold; color:#000">SAFE MODE</span>
        <div id="run" class="ib-brand">
            <label for="run1">On</label><input id="run1" name="run" type="radio" value="0" checked="checked"/><label for="run2">Off</label><input id="run2" name="run" type="radio" value="1"/>
        </div>
    </div>

    <div id="submit-button-wrapper">
            <input class="btn btn-primary btn-large" id="submit" type="submit" style="width: 144px;" value="DEPLOY" />
    </div>
</div>

                        <div id="clear-form-wrapper">
                            <input id="clear-form" type="button" class="btn btn-large" value="Clear Form"/>
                        </div>
                    </div>
                </form>

                <div id="saveDeployment" class="control-group form-inline well">
                    <div class="controls">
                        <label for="deploymentNickname"><strong>Save Deployment</strong></label>
                        <div class="input-append">
                            <input type="text" id="deploymentNickname" placeholder="Nickname" class="span3"/>
                            <button type="button" id="share" title="Share with other users like you" class="btn" data-toggle="button"><i class="icon-gift"></i></button>
                            <button type="button" id="save" class="btn"><i class="icon-briefcase"></i> Save</button>
                        </div>
                        <a href="#savedDeployments" role="button" data-toggle="modal" id="manage" class="btn"><i class="icon-list-alt"></i> Manage</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="span2">
            <div id="currentActivity" class="well">
                <h4>Current Activity</h4>
                <strong>Artifacts</strong>&nbsp;&nbsp;<span id="artifacts_count" class="badge badge-info">0</span>
                <div class="list-wrapper">
                    <ul id="artifacts_deploying"></ul>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="savedDeployments" tabindex="-1" role="dialog" data-backdrop="true" aria-labelledby="sdModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="sdModalLabel">Saved Deployments <button id="refreshSaved" type="button" class="btn btn-small"><i class="icon-refresh"></i></button></h3>
        </div>
        <div class="modal-body">
            <table class="table table-condensed table-bordered table-striped table-hover">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th colspan="2">Deployment</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <script type="text/javascript">
		// initialize global variables
		var artifactCount = 0;
		var tabindex = 0;
		var artifacts = [];
		var artifactNames = [];
		var username;
		var roles;

		initDeployerCommon();
		initDeployIndex();
		getDeployingArtifacts();
    </script>
    <!-- START FOOTER -->
    <footer id="footer" class="footer">
        <a href="#" class="btn btn-mini"><i class="icon-arrow-up"></i></a>
    </footer>
    <!-- END FOOTER -->
</div>

</body>
</html>
