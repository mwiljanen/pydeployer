#!/usr/bin/env python
### Project:  IB Deployer v3.1.12
### Dev:  Matthew Wiljanen
### Date: 2015-01-29

### Current Libraries needed to run deployer

from bottle import request, debug, route, run, get, post, template, error, static_file, response, redirect, app, abort, view, template
from beaker.middleware import SessionMiddleware
import MySQLdb
import ldap
import datetime
import json
import pycurl
import cStringIO
from xml.dom import minidom
import re
import time
import math
import os
import pickle
import ast
import uuid
from calendar import monthrange
import XenAPI
import pprint
from utils import deployer_zenoss
### Load deployer config files


configs = {}

deployer_config_path = "/etc/ibconf/deployer/"

deployer_worker_path = "/opt/pydeployer/temp/services/"

configs['deployer'] = open(deployer_config_path + "deployer.ini" , "r").read()
configs['services'] = open(deployer_config_path + "services.ini" , "r").read()

### Stored queries pulled from old deployer version

query_dict = {
	'dash_currentdeps': """SELECT `processes`.*, `logs`.* FROM `processes`  INNER JOIN `logs` ON processes.uniqueId = logs.uniqueId WHERE (processes.type = 'client')""",
	'dash_servers': """SELECT `serversInDeployment`.* FROM `serversInDeployment`""",
	'dash_artifacts': """SELECT `artifacts`.*, `artifactEnvironments`.* FROM `artifacts` INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1)""",
}

### User session storage

session_options = {   
                      'session.type': 'file',
                      'session.data_dir': './session/',
                      'session.auto': True,
                  }

### Initialization of user session

app_middleware = SessionMiddleware(app(), session_options)
app_session = request.environ.get('beaker.session')

### Default MySQLdb connect list

db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

### AD / LDAP connection strings for User Authentication

LDAP_SERVER = "ldap://adproxy.sl.ibsys.com"
BIND_DN = "ibsys.com"
#USER_BASE = "ou=Users,ou=Development,ou=IB,dc=ibsys,dc=com"
USER_BASE = "ou=IB,dc=ibsys,dc=com"

### Sonatype Nexus project path URL for maven XML files

nexus_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/content/groups/public/com/ibsys/"

### Hostinfo URL used for testing, implemented internal MySQL hostinfo instead

hostinfo_url = "http://hostinfo-dal05.dal05.ib-util.com/hostinfo/csv/"

### User Stats stored from a successful login and used in page templates

user_stats = {'first_name':'Matthew', 'user_name':'mwiljanen' , 'full_name':'Wiljanen, Matthew', 'deployer_roles':[]}

### Deployer dictionary array

deployer_jobs = {}

### Deployer log path /var/log/ib/deployer

deployer_log_path = "/var/log/ib/deployer"

def logger(text_input="", debug_level="INFO", module="Deployer\Logger"):
	"""
	This is a logger function that outputs the level of INFO, DEBUG, and error.
	"""

	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	
	return '[%s (CDT)] deployer.%s [%s] - %s []\n' % (now, debug_level, module, text_input)



def get_config_values(tmp_config="", get_value=""):
	"""
	retrieve a config value from dictionary
	"""

	for tmp_line in tmp_config.split('\n'):
		if ( get_value in tmp_line ):
			return tmp_line.split(' = ')[1]


def uniqid():
	"""
	uniqid function derived from PHPs version
	"""

	return "%14x.%08x" % ( math.modf(time.time())[1]*10000000 + math.modf(time.time())[0] * 10000000, (math.modf(time.time())[0]-math.floor(math.modf(time.time())[0]))*1000000000 )



def validate_user(username="", password=""):
	'''
	this validates a user against IB's AD system
	'''

	try:
		user_exists = False
		ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, 0)
		ldap_connection = ldap.initialize(LDAP_SERVER)
		ldap_connection.simple_bind_s(username + "@" + BIND_DN, password.rstrip())
		results = ldap_connection.search_s(USER_BASE, ldap.SCOPE_SUBTREE, "(CN=*)")
		for dn, entry in results:
			if ( username in entry['sAMAccountName'][0] ):
				user_exists = True
				user_stats['user_name'] = entry['sAMAccountName'][0]			
				user_stats['full_name'] = entry['name'][0]
				user_stats['first_name'] = entry['givenName'][0]
				for member_entries in entry['memberOf']:
					if ('deployer' in member_entries):
						user_stats['deployer_roles'].append(member_entries.split(',')[0].split('CN=')[1].split('-')[1])
						
				print user_stats['deployer_roles']

				break

		return user_exists

	except ldap.LDAPError, e:
		#return 'Error connecting to LDAP server: ' + str(e) + '\n'
		return False


def get_nexus_versions(artifact_name, artifact_env):
	'''
	Added function to poll the nexus cache so it can will speed up the UI
	'''
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	get_nexus_query = """select nexus_metadata.versions from nexus_metadata inner join artifacts on nexus_metadata.artifactId = artifacts.artifactId where artifacts.name = '%s';""" % ( artifact_name )

	if ( curr.execute(get_nexus_query) ):
		nexus_meta_data = curr.fetchone()

		#print nexus_meta_data[0]

        	versions = []

		for item in nexus_meta_data[0].split('|'):
			if ( (artifact_env == 'qa' or artifact_env == 'prod' or artifact_env == 'stage') and 'SNAPSHOT' not in item):
				versions.append( item )
			elif ( artifact_env == 'dev' ):
				versions.append( item )

                return versions
def parse_server_data_2(tmp_server_data):
	"""
	function parses artifact serverData json string 
	"""
	
        srv_list = []
    
	server_data = tmp_server_data.replace(' ','')    
	
	if ( ',' in server_data ):
	        for srv_item in ''.join(server_data.split()).split(','):
        	        if ( '.eq.' in srv_item ):
                	        srv_ib_to_str = "{"
                        	for srv_sub_item in srv_item.split('|'):
                                	if ( '.eq.' in srv_sub_item ):
                                        	srv_ib_to_str += """'%s': '%s',""" % ( srv_sub_item.split('.eq.')[0], srv_sub_item.split('.eq.')[1])

	                        srv_ib_to_str += "}"
        	                srv_list.append(ast.literal_eval(srv_ib_to_str))

	else:
		print server_data
		srv_ib_to_list = []
                for srv_sub_item in server_data.split('|'):
                	if ( '.eq.' in srv_sub_item ):
                        	srv_ib_to_list.append( """'%s': '%s'""" % ( srv_sub_item.split('.eq.')[0], srv_sub_item.split('.eq.')[1]) )

                        
		srv_list.append(ast.literal_eval("{" + ','.join(srv_ib_to_list) + "}"))

        #print len(srv_list)   
        if ( len(srv_list) < 1 ):
                srv_list.append(ast.literal_eval("{ 'hostname': 'none' }"))

        return srv_list


def get_host_name(artifact_name, artifact_env):
	'''
	Pulls hostname from hostinfo cache based on artifact_name and artifact_env
	'''

        conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()
        server_hosts = []

	server_data_query = '''SELECT serverData FROM artifactEnvironments WHERE artifactName = '%s' and env = '%s' ;''' % ( artifact_name, artifact_env )

	if ( curr.execute(server_data_query) ):
		server_data = ''.join(curr.fetchone()[0])

	        blacklisted_subtypes = get_config_values(configs['deployer'], "skipSiteLoadingHosts")

        	for server_item in parse_server_data_2(server_data):

			server_query = []

			for server_item_part in server_item:
        	                server_query.append("""%s = '%s'""" % ( server_item_part, server_item[server_item_part] ) )

			ib_query = '''select hostname, ib_system_subtype from hostinfo where %s''' % ( ' AND '.join(server_query) )

	                if ( curr.execute(ib_query) ):
        	                for res_item in curr.fetchall():
                	                if (res_item[1] not in blacklisted_subtypes ):
						server_hosts.append( res_item[0] )

	return server_hosts



def template_header_output(nav_item='', subtitle=''):
	'''
	Default NavBar template html output, this could easily be a bottle template file, but left as heredoc so it is more dynamic in the code.
	'''
	
	template_selector = { 'select_dashboard': '',
		'select_deploy': '',
		'select_artifacts': '',
		'select_history': '',
		'select_admin': '',
		'select_help': '', 
		'title': 'IB Deployer 3.0 :: ' + nav_item.title() + ' :: ' + subtitle }


	template_selector[ 'select_' + nav_item ] = 'nav_item_sel'

	return '''
<html>
<head>
        <style> @import url(/css/deployer.css); </style>
	<title> %(title)s </title>
</head>
<body>

        <ul class="nav">

                <li class="nav_text">IB Deployer 3.0 Alpha</li>
                <a class="main_menu_link" href="/dashboard"><li class="nav_item %(select_dashboard)s">Dashboard</li></a>
                <div class="admin_menu %(select_deploy)s">Deploy vvv
                        <a class="main_menu_link" href="/deploy/artifact"><div class='admin_menu_item'>Deploy Artifact</div></a>
                        <a class="main_menu_link" href="/deploy/services"><div class='admin_menu_item'>Service Restart</div></a>
                        <a class="main_menu_link" href="/deploy/servers"><div class='admin_menu_item'>Provision Server</div></a>
                </div>
                <a class="main_menu_link" href="/artifacts"><li class="nav_item %(select_artifacts)s">Artifacts</li></a>
                <a class="main_menu_link" href="/history"><li class="nav_item %(select_history)s">History</li></a>
                <div class="admin_menu %(select_admin)s" >Admin vvv
                        <a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
                        <a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
                        <a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
                        <a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
                        <a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
                </div>

                <a class="main_menu_link" href="/help"><li class="nav_item %(select_help)s">Help</li></a>
                <li class="nav_spacer"></li>
        </ul>

	''' % template_selector



@route('/')
@route('/login')
def login():
	'''
	Outputs Login page
	'''

        return template('login', invalid_login=False)
	

@post('/login')
def do_login():
	'''
	Gets user's creds from previous form
	'''

	if ( request.forms.get("username") and request.forms.get("password") ):
		if ( validate_user(request.forms['username'], request.forms['password']) ):
			app_session = request.environ.get('beaker.session')
    			app_session['logged_in'] = True

			redirect('/dashboard')
		else:	
			return template('login', invalid_login=True)

@route('/auth/logout')
@route('/logout')
def logout():
	'''
	Logout page.
	'''

	app_session = request.environ.get('beaker.session')
	if app_session.get('logged_in'):
		app_session['logged_in'] = False
		user_stats = {'first_name':'', 'user_name':'' , 'full_name':''}
	redirect('/login')


@route('/dashboard')
def get_dashboard():
	'''
	This is the Deployer dashboard.
	'''

	#print template_header_output('dashboard')

	dashboard_template = { 'header': template_header_output('dashboard') }

	#header_output = template_header_output('dashboard')	


	return '''

%(header)s

	<div class="content">
		<h1>Dashboard</h1>
		<hr>
		<h2>Current Activity<h2>


		<iframe class="iframe_style" src="/dashboard/currentdeps" width="100%%" height="100%%" fraclass='dep_list'meborder=0></iframe>

	</div>
	</body>
</html>

	''' % dashboard_template

@route('/dashboard/currentdeps')
def get_dash_currentdeps():
	'''
	Dashboard IFrame populator to show current jobs in process.
	'''
	
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	deps_query = '''SELECT artifactEnvironments.currentUniqueId, artifactEnvironments.lastDeployedDate,  artifactEnvironments.artifactName  FROM `artifacts`  INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1);'''

	dep_count_query = '''SELECT DISTINCT currentUniqueId FROM artifactEnvironments WHERE isDeploying = 1;'''

	dep_count = 0

	if ( curr.execute(dep_count_query) ):	
		dep_count = len( curr.fetchall() )
	output_str = '''
	<head> <META http-equiv="refresh" content="5;URL=/dashboard/currentdeps"> </head>
	<style>  @import url(/css/deployer.css);  </style>
	'''

	output_str += '''

Deployments &nbsp; <span class="counter">&nbsp;%d&nbsp;</span>

<br>

<div class="div-table">
	<div class="div-table-row">
		<div class="div-table-head-col">Unique ID</div><div class="div-table-head-col">Start Date/Time	</div><div class="div-table-head-col">Selections</div><div class="div-table-head-col">Status</div>
	</div>

	''' % ( dep_count  )

	if ( curr.execute(deps_query) ):	
		cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }
		all_res = curr.fetchall()


		for res_row in all_res:
			#print  cur_deps_dict

			if ( len(cur_deps_dict['uniqueId']) == 0 ):
				cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }

				cur_deps_dict['uniqueId'] = res_row[0]
				cur_deps_dict['start_time'] = res_row[1]
				cur_deps_dict['selections'].append(res_row[2])
				cur_deps_dict['status'] = ''


			elif ( cur_deps_dict['uniqueId'] == res_row[0] ):
				cur_deps_dict['selections'].append(res_row[2])

			
			elif ( cur_deps_dict['uniqueId'] != res_row[0] and len(cur_deps_dict['uniqueId']) > 0):
			
				output_str += '''
		<div class="div-table-row">
			<div class="div-table-col">%s</div>
        	        <div class="div-table-col">%s</div>
        	        <div class="div-table-col">%s</div>
        	        <div class="div-table-col">Status</div>
		</div>
				''' % ( cur_deps_dict['uniqueId'], cur_deps_dict['start_time'], '<br>'.join(cur_deps_dict['selections']) )
				
				cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }

				cur_deps_dict['uniqueId'] = res_row[0]
				cur_deps_dict['start_time'] = res_row[1]
				cur_deps_dict['selections'].append(res_row[2])
				cur_deps_dict['status'] = ''


		output_str += '''
                <div class="div-table-row">
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">Status</div>
                </div>
                ''' % ( cur_deps_dict['uniqueId'], cur_deps_dict['start_time'], '<br>'.join(cur_deps_dict['selections']) )
			
	output_str += '''

</div>
	'''

	art_count_query = '''select artifactName, env from artifactEnvironments where isDeploying = 1 ORDER BY artifactName;'''

	art_count = 0

	art_cnt_output = []

	if ( curr.execute(art_count_query) ):	
		res = curr.fetchall()
		art_count = len( res )
		for art_item in res:
			art_cnt_output.append( "<li>" + art_item[0] + " : " + art_item[1].upper() + "</li>" ) 

	output_str += '''
	<br>	
	<div class="current_artifacts">
	Artifacts &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	''' % ( art_count, '\n'.join(art_cnt_output) )


	server_query = '''SELECT * FROM serversInDeployment;'''

	server_count = 0

	serv_cnt_output = []

	if ( curr.execute(server_query) ):	
		res = curr.fetchall()
		server_count = len( res )
		for serv_item in res:
			serv_cnt_output.append( "<li>" + serv_item[0] + "</li>" ) 

	output_str += '''
	

	<div class="current_servers">
	Servers &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	
	''' % ( server_count, '\n'.join( serv_cnt_output ) )


	return output_str


@route('/deploy/artifact')
def get_deploy():
	'''
	This is the Deploy Artifact page.
	'''

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	current_art_num = 0

	deploy_artifact_template = { 'header': template_header_output('deploy', 'Artifacts' ) , 'artifact_form': '' }

	if ( 'next_art_num' in request.query_string ):
		current_art_num = request.query['next_art_num']

	option_list_query = "select artifactName, env from artifactEnvironments ORDER BY env, artifactName;"

	current_select = 0

	artifact_list = []

	option_list = []

	version_list = []

	artifact_form = ""

	if ( 'artifact' in request.query_string ):
		art_update_cnt = 0
		for art_key in request.query.keys():
			if ( 'artifact[' in art_key ):
				artifact_list.append( request.query[ ('artifact[%d]' % art_update_cnt) ] )
				art_update_cnt += 1

	if ( 'working_artifact' in request.query_string and 'add_artifact' in request.query_string):
		server_include_list = []

		for include_item in request.query.keys():
			if ( 'include_' in include_item ):
				server_include_list.append(include_item.split('_')[1])
			
		artifact_list.append( request.query['working_artifact'] + ":" + request.query['version_to_deploy'] + ":" + request.query['jira_key'] + ":" + ','.join(server_include_list) )	


	if ( 'server_box' in request.query_string ):
		current_select = 1


	if ( 'deploy_button' in request.query_string ):
		if ( len(artifact_list) == 0 ):
			artifact_form += "<p style='color: red; font-size: xx-large;'>Error: No artifacts were selected!</p>"
		else:
			redirect( '/deploy/index?' + request.query_string )

	if ( 'remove_artifact_' in request.query_string ):
		rem_pos = request.query_string.find('remove_artifact_') + len("remove_artifact_")
		rem_art_pos = int(request.query_string[rem_pos:rem_pos+5].split('.')[0])
		del artifact_list[rem_art_pos]

	if( len(artifact_list) > 0 ):
		temp_update = {}
		art_cnt = 0
		for cur_art_item in artifact_list:
			#cur_art_item = request.query[ 'artifact[' + str( art_update_cnt ) + ']' ]
			temp_update['artifact_key'] = cur_art_item
			temp_update['curr_art_num'] = art_cnt
			temp_update['artifact_name'] = cur_art_item.split(':')[0]
			temp_update['artifact_env'] = cur_art_item.split(':')[1]
			temp_update['version'] = cur_art_item.split(':')[2]
			temp_update['jira_key'] = cur_art_item.split(':')[3]
			temp_update['servers_included'] = cur_art_item.split(':')[4]
			artifact_form += '''

		<div class="div-table-row">
			<div class="div-table-col-art"><input type='image' name='remove_artifact_%(curr_art_num)d' class='del_button' src='/images/blank.png'></a>%(artifact_name)s</div>
			<div class="div-table-col-env"> %(artifact_env)s </div>
			<div class="div-table-col"> %(version)s </div>
			<div class="div-table-col"> %(jira_key)s 
			<input type='hidden' name='artifact[%(curr_art_num)s]' value='%(artifact_key)s'></div>
		</div>

			''' % temp_update 

			art_cnt += 1


	if ( 'update_artifact' in request.query_string):
		current_select = 1 

	if( curr.execute( option_list_query ) ):
		for art_opt_item in curr.fetchall():
			option_list.append( '''<option>%s</option>''' % ( art_opt_item[0] + " : " + art_opt_item[1] ) )
	
	if ( current_select == 0 ):

		artifact_form += '''
	<div class="div-table-row">
		<div class="div-table-col-art"><select name='current_artifact_env' class='drop_down'>%s</select></div>
		<div class="div-table-col-env"><input type='submit' name='update_artifact' class='update_button' value='>'></div>
		<div class="div-table-col"></div>
		<div class="div-table-col"></div>
	</div>
		''' % ( ''.join(option_list)  )

	
	if ( current_select == 1 ):

		curr_art = ''.join(request.query['current_artifact_env']).split(':')[0].replace(' ','')
		curr_env = ''.join(request.query['current_artifact_env']).split(':')[1].replace(' ','')

		version_list.append( '''<option>%s</option>''' % ( "latest" ) )

		for ver_opt_item in get_nexus_versions(curr_art, curr_env):
			version_list.append( '''<option>%s</option>''' % ( ver_opt_item ) )
		
		form_update = {}
		form_update['artifact_name'] = curr_art
		form_update['artifact_env'] = curr_env
		form_update['next_art_num'] = current_art_num + 1
		form_update['versions'] = ''.join(version_list)
		form_update['working_artifact'] = curr_art + ":" + curr_env
		form_update['server_box'] = 'hide_server_box'
		
		server_count = 0
		
		form_update['servers'] = 'Servers Included List<br>(uncheck to exclude)<ul>'

		for server_item in get_host_name(curr_art, curr_env):
			
			form_update['servers'] += '''<li class='server_include_item'><input type='checkbox' name='include_%(server_item)s' id='server_id_%(server_item)s' checked>
			<label for='server_id_%(server_item)s'>%(server_item)s</label></li>''' % { 'server_item': server_item }				
		
			server_count += 1
		
		form_update['servers'] += '</ul>'
		

		artifact_form += '''

	<div class="div-table-row">
		<div class="div-table-col-art">%(artifact_name)s</div>
		<div class="div-table-col-env">%(artifact_env)s </div>
		<div class="div-table-col">
			<div class='show_info_button'>
				<div class='server_select_box'>%(servers)s</div>
			</div>
			<select name='version_to_deploy' class='version_drop_down'>%(versions)s</select>
		</div>
		<div class="div-table-col"><input type='text' name='jira_key'>
		<input type='hidden' name='working_artifact' value='%(working_artifact)s'>
		<input type='hidden' name='next_art_num' value='%(next_art_num)s'> 
		<input type='submit' name='add_artifact' value='Add Item'></div>
	</div>
		''' % form_update


	deploy_artifact_template['artifact_form'] = artifact_form

	return '''

%(header)s

<div class="content">
<h5>Deploy / Artifacts</h5>
<h1>Deploy > Artifacts</h1>
<hr>
	<div class="deploy_content">
		<div class="artifact_selections">
		<form method='GET' action='/deploy/artifact'>
		  <div class="div-table">
		    <div class="div-table-row">
		      <div class="div-table-head-col">Artifact</div><div class="div-table-head-col">Environment</div><div class="div-table-head-col">Version</div><div class="div-table-head-col">JIRA Issue Key</div>
		    </div>
			%(artifact_form)s
		<hr>
		
		<div class='deploy_opt_container'>
			<div class="deploy_opt_image"> 
			Deployment Options
	
			<input class="deploy_opt_image" id='deploy_btn' type='checkbox' > 
			<label for='deploy_btn' class='deploy_label_button'>&nbsp;&nbsp;?&nbsp;&nbsp;</label>

			<br>
			<br>
			<div class='deploy_options_box'>

			<div class='dep_opt_exluded_servers'>

				Servers to Exclude 
				<textarea cols='50' rows='10'></textarea>

			</div>

			<div class='total_dep_ops'>

			Options:

			<ul>
        	            <li class='depl_opts'><input name="skipVersionChecks" title="Skips post-deployment version verification. Will force deployment to all servers for selected artifacts." type="checkbox" value="1"/> Skip Version Checks</li>
			    <li class='depl_opts'>
                        	<input  id="skipLbOps" name="skipLbOps"
                               		title="Skips load balancer pool operations." type="checkbox" value="1"
	                            /> Skip Load Balancer Operations
			    </li>
			    <li class='depl_opts'>
				<input  id="skipDepDeploy" name="skipDepDeploy"
					   title="Skips the deployment of artifact dependencies." type="checkbox" value="1"
					/> Skip Artifact Dependency Deployment
			    </li>
			    <li class='depl_opts'>
                	        <input  id="ignoreZenossProdState" name="ignoreZenossProdState"
                        	       title="Ignore initial Zenoss production state; will allow deployment to servers regardless of Zenoss production state." type="checkbox" value="1"
	                            /> Ignore Zenoss Production State
			    </li>
			    <li class='depl_opts'>
				<input  id="resetZenossProdState" name="resetZenossProdState"
				   title="Ignore initial Zenoss production state AND reset Zenoss production states based on deployment environment" type="checkbox" value="1"
							/> Reset Zenoss Production State
			    </li>
			    <li class='depl_opts'>
				<input  id="ignoreArtifactDeployStatus" name="ignoreArtifactDeployStatus"
				   title="Ignore artifact deployment status; will allow concurrent deployments of the same artifact-environment combination." type="checkbox" value="1"
				/> Ignore Artifact Deployment Status
			    </li>
			    <li class='depl_opts'>
                	        <input  id="clearStatus" name="clearStatus"
                        	       title="Last deployment failed or aborted? This clears the deployment status for the selected artifacts and their servers." type="checkbox" value="1"
	                            /> Clear Deploy Status
        	            </li>

                	    <li class='depl_opts'>
	                        <input  id="debug" name="debug" type="checkbox" value="1"
        	                     title="Toggle debug logging" /> Debug
                	    </li>

				<br>
				<li class='depl_opts'>
				Manage Puppet Agent
				<br>
	                        <input  id="puppetAgent0" name="puppetAgent" type="radio" value='none' checked /><label for = 'puppetAgent0'>None</label>
	                        <input  id="puppetAgent1" name="puppetAgent" type="radio" value='stop'/><label for = 'puppetAgent1'>Stop</label>
	                        <input  id="puppetAgent2" name="puppetAgent" type="radio" value='start'/><label for = 'puppetAgent2'>Start</label>
	                        <input  id="puppetAgent3" name="puppetAgent" type="radio" value='restart'/><label for = 'puppetAgent3'>Restart</label>
	                        <input  id="puppetAgent4" name="puppetAgent" type="radio" value='force_restart'/><label for = 'puppetAgent4'>Force Restart</label>
			
				</li>
				
				<br>
				<li class='depl_opts'>
				Manage Service
				<br>
	                        <input  id="manageService0" name="manageService" type="radio" value='none' checked /><label for = 'manageService0'>None</label>
	                        <input  id="manageService1" name="manageService" type="radio" value='stop'/><label for = 'manageService1'>Stop</label>
	                        <input  id="manageService2" name="manageService" type="radio" value='start'/><label for = 'manageService2'>Start</label>
	                        <input  id="manageService3" name="manageService" type="radio" value='restart'/><label for = 'manageService3'>Restart</label>
				</li>
				</div>
			</div>

		</div>		
		
		</div>


		  </div>

		<hr>
		<div class='deploy_button_box'>
			<div class='safe_button_box'>
				<span style='padding-left: 15%%;'>SAFE MODE</span>
				<br>
				<input class="safe_button_chk" id='safe_mode_btn_on' name='run' type='radio' value='0' checked> 
				<label for='safe_mode_btn_on' class='safe_button_lbl'>On</label>
				<input class="safe_button_chk" id='safe_mode_btn_off' name='run' type='radio' value='1'> 
				<label for='safe_mode_btn_off' class='safe_button_lbl'>Off</label>

			</div>
			<div class='deploy_button'><span style='padding-left: 10%%; font-size: xx-large'>DEPLOY
			<input type='image' name='deploy_button' src='/images/blank.png' style='position: absolute; top: 0px; left: 0px; width: 100%%; height: 100%%;' /></span> </div>
		</div>
		
		</form>

		</div>

		<div class="current_activity">
			Current Activity
			<hr>
			<iframe class="iframe_style" src="/deploy/current_activity" width="100%%" height="90%%" frameborder=0></iframe>
		</div>
	</div>
</div>
</body>
</html>
	''' % deploy_artifact_template


@route('/deploy/add_artifact')
def add_artifact():
	redirect('/deploy?' + request.query_string)



@route('/deploy/services/restart')
def restart_services():

	restart_tasks_job = { 'tasks': dict(request.query), 'job_id': uuid.uuid4(), 'user_name': user_stats['user_name'] }

	pickle.dump( restart_tasks_job, open(deployer_worker_path + "service_restart_" + str(restart_tasks_job['job_id']) + ".job", "wb") )

	redirect('/deploy/service/watch/job_id/' + str(restart_tasks_job['job_id']) )


@route('/deploy/service/watch/job_id/<job_id>')
def watch_service_restart(job_id):

	watch_service_dict = { 'header': template_header_output('deploy', 'Service') ,'job_id': job_id }	


	return '''
	
%(header)s

<div class="content">
<h1>Service Restart Job ID: %(job_id)s </h1>
<hr>
		<iframe class="iframe_style" src="/deploy/service/watchlog/job_id/%(job_id)s#bottom" width="100%%" height="100%%" </iframe>
</div>

</body>
</html>	

	''' % watch_service_dict

@route('/deploy/service/watchlog/job_id/<job_id>')
def watch_service_current_log(job_id):
	watch_log_dict = {}

	try:
		watch_log_dict = { 'output' : open('/var/log/ib/deployer/deployer_service_' + job_id + '.log', 'r').read(), 'job_id': job_id }
	except:
 		watch_log_dict = { 'output' : '', 'job_id': job_id }

	return '''
<html>
	
	<head> <META http-equiv="refresh" content="2;URL=/deploy/service/watchlog/job_id/%(job_id)s"> </head>
	<body>
		<pre> %(output)s </pre>
		
	</body>
</html>
	''' % watch_log_dict



@route('/deploy/services')
def get_restart_services():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	service_restart_form = { 'header': template_header_output('deploy', 'Services') ,'hostnames': '', 'env_list': '', 'repo_list': '', 'domain_list': '', 'subtype_list': '', 
				'systype_list': '', 'select_all_puppet': '', 'select_all_tomcat': '', 'select_all_zenoss': '', 
				'partner_list': '', 'select_zenoss_mode': '', 'select_zenoss_maint_mode': '', 'maint_win_start': '' , 
				'maint_win_end': '', 'maint_win_error': '', 'set_cae_status': '', 'select_all_cae': '', 'set_tomcat_status': '', 'optional_header': '',
				'optional_columns': '' }


	if ( 'restart_button' in request.query_string ):
		redirect('/deploy/services/restart?' + request.query_string )

	filter_list = []

	zenoss_maint_states = [ 'Maintenance', 'Development', 'QA', 'Stage', 'Production' ]

	zenoss_maint_window_modes = [ 'None', 'Set', 'Delete' ]

	filter_str = ' WHERE hostname = "None" ;'

	if ( 'env_filter' in request.query_string ):
		if ( request.query['env_filter'] != 'None' and len( request.query['env_filter'] ) > 0 ):
			filter_list.append(""" ib_env = '%s'""" % request.query['env_filter'] )

	if ( 'partner_filter' in request.query_string ):
		if ( request.query['partner_filter'] != 'None' and len( request.query['partner_filter'] ) > 0 ):
			filter_list.append(""" ib_partner_group = '%s'""" % request.query['partner_filter'] )

	if ( 'domain_filter' in request.query_string ):
		if ( request.query['domain_filter'] != 'None' and len( request.query['domain_filter'] ) > 0 ):
			filter_list.append(""" domain = '%s'""" % request.query['domain_filter'] )

	if ( 'systype_filter' in request.query_string ):
		if ( request.query['systype_filter'] != 'None' and len( request.query['systype_filter'] ) > 0 ):
			filter_list.append(""" ib_system_type = '%s'""" % request.query['systype_filter'] )
			if ( request.query['systype_filter'] == 'mongo' ):
				service_restart_form['optional_header'] = 'Reindex Mongo Host'

	if ( 'subtype_filter' in request.query_string ):
		if ( request.query['subtype_filter'] != 'None' and len( request.query['subtype_filter'] ) > 0 ):
			filter_list.append(""" ib_system_subtype = '%s'""" % request.query['subtype_filter'] )

	if ( 'repo_filter' in request.query_string ):
		if ( request.query['repo_filter'] != 'None' and len( request.query['repo_filter'] ) > 0 ):
			filter_list.append(""" repository_host like '%%%s%%'""" % ( request.query['repo_filter'] ) )

	if ( 'select_all_puppet_hosts' in request.query_string ):
		if ( request.query['select_all_puppet_hosts'] == 'on' ):
			service_restart_form['select_all_puppet'] = 'checked'

	if ( 'select_all_tomcat_hosts' in request.query_string ):
		if ( request.query['select_all_tomcat_hosts'] == 'on' ):
			service_restart_form['select_all_tomcat'] = 'checked'

	if ( 'select_all_zenoss_hosts' in request.query_string ):
		if ( request.query['select_all_zenoss_hosts'] == 'on' ):
			service_restart_form['select_all_zenoss'] = 'checked'

	if ( 'select_all_cae_hosts' in request.query_string ):
		if ( request.query['select_all_cae_hosts'] == 'on' ):
			service_restart_form['select_all_cae'] = 'checked'

	if ( len(filter_list) ):
		if ( len(filter_list) == 1 ):
			filter_str = " WHERE " + filter_list[0]
		else: 
			filter_str = " WHERE " + ' AND '.join(filter_list)



	get_env_query = "select distinct hostinfo.ib_env from hostinfo;"

	if ( curr.execute( get_env_query ) ):
		service_restart_form['env_list'] = '''<select name = 'env_filter' ><option>None</option>'''

		for env_item in curr.fetchall():
			if ( 'env_filter' in request.query_string and env_item[0] in request.query_string and len( env_item[0] ) > 0 ):
				service_restart_form['env_list'] += '''<option selected>%s</option>''' % ( request.query['env_filter'] )
			elif ( len( env_item[0] ) > 0 ):
				service_restart_form['env_list'] += '''<option>%s</option>''' % ( env_item[0] )


		service_restart_form['env_list'] += '</select><br>'


	get_partner_query = "select distinct hostinfo.ib_partner_group from hostinfo;"

	if ( curr.execute( get_partner_query ) ):
		service_restart_form['partner_list'] = '''<select name = 'partner_filter' ><option>None</option>'''

		for partner_item in curr.fetchall():
			if ( 'partner_filter' in request.query_string and partner_item[0] in request.query_string and len( partner_item[0] ) > 0 ):
				service_restart_form['partner_list'] += '''<option selected>%s</option>''' % ( request.query['partner_filter'] )
			elif ( len( partner_item[0] ) > 0 ):
				service_restart_form['partner_list'] += '''<option>%s</option>''' % ( partner_item[0] )


		service_restart_form['partner_list'] += '</select><br>'

			

	get_repo_list_query = "select distinct hostinfo.repository_host from hostinfo;"

	if ( curr.execute( get_repo_list_query ) ):
		service_restart_form['repo_list'] = '''<select name = 'repo_filter' ><option>None</option>'''

		for repo_host in curr.fetchall():
			if ( 'repo_filter' in request.query_string and repo_host[0] in request.query_string and len( repo_host[0] ) > 0 ):
				service_restart_form['repo_list'] += '''<option selected>%s</option>''' % ( request.query['repo_filter'] )
			elif ( len( repo_host[0] ) > 0 ):
				service_restart_form['repo_list'] += '''<option>%s</option>''' % (repo_host[0])

		service_restart_form['repo_list'] += '</select><br>'



	get_systype_query = "select distinct hostinfo.ib_system_type from hostinfo;"

	if ( curr.execute( get_systype_query ) ):
		service_restart_form['systype_list'] = '''<select name = 'systype_filter' ><option>None</option>'''

		for systype_item in curr.fetchall():
			if ( 'systype_filter' in request.query_string and systype_item[0] in request.query_string and len( systype_item[0] ) > 0 ):
				service_restart_form['systype_list'] += '''<option selected>%s</option>''' % ( request.query['systype_filter'] )
			else:
				service_restart_form['systype_list'] += '''<option>%s</option>''' % ( systype_item[0] )


		service_restart_form['systype_list'] += '</select><br>'


	get_subtype_query = "select distinct hostinfo.ib_system_subtype from hostinfo;"

	if ( curr.execute( get_subtype_query ) ):
		service_restart_form['subtype_list'] = '''<select name = 'subtype_filter' ><option>None</option>'''

		for subtype_item in curr.fetchall():
			if ( 'subtype_filter' in request.query_string and subtype_item[0] in request.query_string and len( subtype_item[0] ) > 0 ):
				service_restart_form['subtype_list'] += '''<option selected>%s</option>''' % ( request.query['subtype_filter'] )
			elif ( len( subtype_item[0] ) > 0 ):
				service_restart_form['subtype_list'] += '''<option>%s</option>''' % ( subtype_item[0] )


		service_restart_form['subtype_list'] += '</select><br>'



	get_domain_query = "select distinct domain from hostinfo;"

	if ( curr.execute( get_domain_query ) ):
		service_restart_form['domain_list'] = '''<select name = 'domain_filter' ><option>None</option>'''

		for domain_item in curr.fetchall():
			if ( 'domain_filter' in request.query_string and domain_item[0] in request.query_string and len( domain_item[0] ) > 0):
				service_restart_form['domain_list'] += '''<option selected>%s</option>''' % ( request.query['domain_filter'] )
			elif ( len( domain_item[0] ) > 0 ):
				service_restart_form['domain_list'] += '''<option>%s</option>''' % ( domain_item[0] )

		service_restart_form['domain_list'] += '</select><br>'
		
	
	get_servers_query = "SELECT hostname FROM hostinfo %s ;" % ( filter_str )


	tmp_zenoss_state = ''

	service_restart_form['select_zenoss_mode'] += '''<select name = 'select_zenoss_maint_mode' ><option>None</option>'''
	for tmp_item in sorted(deployer_zenoss.zenoss_states.keys()):
		selected_item = ''
		if ( tmp_item in request.query_string and 'select_zenoss_maint_mode' in	request.query_string ):
			if ( request.query['select_zenoss_maint_mode'] == tmp_item ):
				selected_item = 'selected'
				tmp_zenoss_state = tmp_item
		service_restart_form['select_zenoss_mode'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item )

	service_restart_form['select_zenoss_mode'] += '''</select>'''

	


	if( curr.execute( get_servers_query ) ):
		for serv_item in curr.fetchall():
			tmp_dict = { 'tmp_host': serv_item[0], 
				'puppet_checked': service_restart_form['select_all_puppet'] ,
				'tomcat_checked': service_restart_form['select_all_tomcat'] ,
				'zenoss_checked': service_restart_form['select_all_zenoss'] ,
				'cae_checked': service_restart_form['select_all_cae'] ,
				'zenoss_state': tmp_zenoss_state, 'mongo_index': '' }

			if ( request.query['systype_filter'] == 'mongo' ):
				tmp_dict['mongo_index'] = '''<td><input type = 'checkbox' name = 'reindex_mongo_on_%s' ></td>''' % (serv_item[0])
			
			service_restart_form['hostnames'] += '''<tr><td>%(tmp_host)s</td>
								<td><input type = 'checkbox' name = 'restart_puppet_on_%(tmp_host)s' %(puppet_checked)s ></td> 
								<td><input type = 'checkbox' name = 'restart_tomcat_on_%(tmp_host)s' %(tomcat_checked)s ></td>
								<td><input type = 'checkbox' name = 'set_zenoss_on_%(tmp_host)s_to_%(zenoss_state)s' %(zenoss_checked)s ></td>
								<td><input type = 'checkbox' name = 'set_cae_status_on_%(tmp_host)s' %(cae_checked)s ></td>
								%(mongo_index)s
								</tr>''' % tmp_dict

	if ('set_zenoss_maint_window_start' in request.query_string and 'set_zenoss_maint_window_end' in request.query_string):
		service_restart_form['maint_win_start'] = request.query['set_zenoss_maint_window_start']
		service_restart_form['maint_win_end'] = request.query['set_zenoss_maint_window_end']
		if ( len(service_restart_form['maint_win_start']) > 0 and len( service_restart_form['maint_win_end'] ) > 0 ):
			try:
				datetime.datetime.strptime(service_restart_form['maint_win_start'], '%m/%d/%Y %H:%M')
				datetime.datetime.strptime(service_restart_form['maint_win_end'], '%m/%d/%Y %H:%M')
			except:
				service_restart_form['maint_win_error'] = "Not a valid date"
				service_restart_form['maint_win_start'] = ''
				service_restart_form['maint_win_end'] = ''

	service_restart_form['select_zenoss_maint_mode'] += '''Maintenance Window <select name = 'select_zenoss_maint_window' > '''
	for tmp_item in zenoss_maint_window_modes:
		selected_item = ''
		if ( tmp_item in request.query_string and 'select_zenoss_maint_window' in request.query_string ):
			if ( request.query['select_zenoss_maint_window'] == tmp_item ):
				selected_item = 'selected'
		service_restart_form['select_zenoss_maint_mode'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item )
	service_restart_form['select_zenoss_maint_mode'] += '''</select>'''

	service_restart_form['set_cae_status'] += '''Set CAE Status <select name = 'set_cae_status_mode' ><option>None</option><option>Down</option><option>Up</option></select>'''

	service_restart_form['set_tomcat_status'] += '''Set Tomcat Status <select name = 'set_tomcat_status_mode' ><option>None</option><option>Restart</option><option>Start</option><option>Stop</option></select>'''
	
	return '''

%(header)s

<div class="content">
<h5> Deploy / Service Restart </h5>
<h1>Deploy > Service Restart</h1>
<hr>
	<div class="deploy_content">
		<div class="artifact_selections">
			<form method='GET' action='/deploy/services'>
				  <div class="div-table">
		    			<div class="div-table-row">
		      		        </div>
			Environment:<br>
			%(env_list)s<br>
			System Types:<br>
			%(systype_list)s<br>
			System Subtypes:<br>
			%(subtype_list)s<br>
			Partner Groups:<br>
			%(partner_list)s<br>
			Repository Hosts:<br>
			%(repo_list)s<br>
			Domain List:
			%(domain_list)s<br>
		

			<span style='color: red ; font-weight: bold; '>%(maint_win_error)s</span><br>
			Set Zenoss Maint Window Start Time: ( ex: 10/22/2014 22:45 ) <input type='text' class='text_box_normal' name='set_zenoss_maint_window_start' value='%(maint_win_start)s' ><div class='maint_start_cal'></div><br>
			Set Zenoss Maint Window End Time: <input type='text' class='text_box_normal' name='set_zenoss_maint_window_end' value='%(maint_win_end)s' > <div class='maint_stop_cal'></div><br>

			<input type='submit' name='update_hosts' value = 'Update'> 
			
			<hr>
			<table><tr><th>Hostname</th><th>Puppet Restart</th><th>Tomcat Restart</th><th>Set Zenoss State</th><th>Set CAE Status</th><th>%(optional_header)s</tr>
				<tr><td></td><td>Select All: <input type='checkbox' name='select_all_puppet_hosts' %(select_all_puppet)s></td>
					<td>
					%(set_tomcat_status)s <br>
					Select All: <input type='checkbox' name='select_all_tomcat_hosts' %(select_all_tomcat)s></td>
					<td>
					Set Selected to %(select_zenoss_mode)s <br> 
					%(select_zenoss_maint_mode)s <br>
					Select All: <input type='checkbox' name='select_all_zenoss_hosts' %(select_all_zenoss)s> </td>
					<td>%(set_cae_status)s <br> Select All: <input type='checkbox' name='select_all_cae_hosts' %(select_all_cae)s> </td>
					<td>%(optional_columns)s</td>
				</tr>
			%(hostnames)s
			</table>
		<hr>
		<div class='deploy_button_box'>
				<div class='safe_button_box'>

					<span style='padding-left: 15%%;'>SAFE MODE</span>
					<br>
					<input class="safe_button_chk" id='safe_mode_btn_on' name='run' type='radio' value='0' checked> 
					<label for='safe_mode_btn_on' class='safe_button_lbl'>On</label>
					<input class="safe_button_chk" id='safe_mode_btn_off' name='run' type='radio' value='1'> 
					<label for='safe_mode_btn_off' class='safe_button_lbl'>Off</label>

				</div>
			<div class='deploy_button'><span style='padding-left: 5%%; font-size: xx-large'>RESTART
			<input type='image' name='restart_button' src='/images/blank.png' style='position: absolute; top: 0px; left: 0px; width: 100%%; height: 100%%;' /></span> </div>
		</div>


		<hr>

		
		</form>

		</div>

	</div>
</div>
</body>
</html>
	''' % ( service_restart_form )


@route('/deploy/servers')
def provision_servers():

	server_provision_dict = { 'header': template_header_output('deploy', 'Servers'), 'vm_host_output': '', 'vm_templates': '', 'vm_host_disk_params': '' }

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	host_envs = [ 'None', 'dev', 'qa', 'stage', 'prod' ]
	host_sub_envs = [ 'None', 'front', 'back', 'feeder', 'frontrls' ]
	
	host_vm_env = 'None'
	host_vm_sub_env = 'None'

	vm_cpus = range ( 2, 17 )


	server_provision_dict['vm_host_output'] += '''Select Xen Host Env: <select name = 'select_host_vm_env' onchange='submit()'> '''


	for tmp_item in host_envs:
		selected_item = ''
		if ( 'select_host_vm_env' in request.query_string ):
			if ( request.query['select_host_vm_env'] == tmp_item ):
				selected_item = 'selected'
		
		server_provision_dict['vm_host_output'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item  )

	server_provision_dict['vm_host_output'] += '''	</select><br> '''


	server_provision_dict['vm_host_output'] += '''Select Xen Group: <select name = 'select_host_vm_sub_env' onchange='submit()'> '''
	

	for tmp_item in host_sub_envs:
		selected_item = ''
		if ( 'select_host_vm_sub_env' in request.query_string ):
			if ( request.query['select_host_vm_sub_env'] == tmp_item ):
				selected_item = 'selected'
		
		server_provision_dict['vm_host_output'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item  )

	server_provision_dict['vm_host_output'] += '''	</select> <br>'''


	if ( 'select_host_vm_env' in request.query_string and 'select_host_vm_sub_env' in request.query_string):
		host_vm_env = request.query['select_host_vm_env']
		host_vm_sub_env = request.query['select_host_vm_sub_env']
		if ( host_vm_env == 'qa' ):
			host_vm_env = 'dev'

		get_host_vms_query = '''select concat( hostname, '.', domain) , memory_free 
		from hostinfo where LENGTH(xen_version) > 0 and hostname like '%%%s%%%s%%'; ''' % ( host_vm_env, host_vm_sub_env )

		

		server_provision_dict['vm_host_output'] += '''Select Xen FQ Hostname to Deploy to: <select name = 'select_host_vms' onchange='submit()'><option>None</option>'''

		if ( curr.execute(get_host_vms_query) ):
			for res_item in curr.fetchall():
				selected_item = ''
		                if ( 'select_host_vms' in request.query_string ):
                		        if ( res_item[0] in request.query['select_host_vms'] ):
                                		selected_item = 'selected'

				server_provision_dict['vm_host_output'] += '''<option value='%s|%s' %s>%s : %d MB free </option>''' % ( res_item[0],res_item[1], selected_item, res_item[0],int(res_item[1]) )
	
		server_provision_dict['vm_host_output'] += '''</select><br>'''


	if ( 'select_host_vms' in request.query_string ):
		if ( 'None' != request.query['select_host_vms'] ):

			xen_hostname = request.query['select_host_vms'].replace(' ','').split('|')[0]
			hostname_dc = xen_hostname.split('.')[1]

			ib_sys_type = ''
			ib_sys_subtype = ''
			ib_partner_group = ''



			try:
				xen_sess = XenAPI.Session( "https://%s" % ( xen_hostname ) )
                        	xen_sess.xenapi.login_with_password('root', 'Wio#1pt?')
                        	for vm_item in xen_sess.xenapi.VM.get_all():
					record = xen_sess.xenapi.VM.get_record(vm_item)
					if ( record["is_a_template"] and "Puppet" in record["name_label"]):
						pprint.pprint( record )
						server_provision_dict['vm_templates'] += '<option>' + record["name_label"] + '</option>'
				
				pool_item = xen_sess.xenapi.pool.get_all()[0]
				print pool_item

				default_sr = xen_sess.xenapi.pool.get_default_SR(pool_item)
				default_sr = xen_sess.xenapi.SR.get_record(default_sr)
				pprint.pprint( default_sr )

				xen_host_free_disk = int ( default_sr['physical_size'] ) - int (default_sr['physical_utilisation'] )
				print xen_host_free_disk
				
				#print "Choosing SR: %s (uuid %s)" % (default_sr['name_label'], default_sr['uuid'])
				hosts = xen_sess.xenapi.host.get_all()
                        	host_record = xen_sess.xenapi.host.get_record(hosts[0])
				pprint.pprint( host_record )

				#for vm_item in xen_sess.xenapi.VM.get_all():
				#	vm_record = xen_sess.xenapi.VM.get_record(vm_item)
				#	if ( vm_record['is_a_template'] ):
				#		pprint.pprint( vm_record )

			except:
				print "Cannot connect to host"

			server_provision_dict['vm_host_output'] += '''Select Puppet Template <select name = 'select_vm_template' > '''
			server_provision_dict['vm_host_output'] += server_provision_dict['vm_templates']
                        server_provision_dict['vm_host_output'] += '''  </select><br> '''


			server_provision_dict['vm_host_output'] += '''Select Number of VM vCPUs: <select name = 'select_host_vm_cpus' > '''

                        for tmp_item in vm_cpus:
                                server_provision_dict['vm_host_output'] += '''<option>%s</option>''' % ( tmp_item  )

                        server_provision_dict['vm_host_output'] += '''  </select><br> '''


			max_memory = request.query['select_host_vms'].replace(' ','').split('|')[1]
		
			memory_list = range(512, int(max_memory), 512)

			server_provision_dict['vm_host_output'] += '''	Select Maximum Memory in MB:<select name = 'select_host_vm_mem' > '''

			for tmp_item in memory_list:
				server_provision_dict['vm_host_output'] += '''<option>%s</option>''' % ( tmp_item  )

			server_provision_dict['vm_host_output'] += '''	</select> <br>'''
			
			get_system_type = '''SELECT DISTINCT ib_system_type FROM hostinfo;'''

			if ( curr.execute(get_system_type) ):
				server_provision_dict['vm_host_output'] += '''Select System Type: <select name = 'select_host_vm_ib_systype' > '''

				for tmp_item in curr.fetchall():
					selected_item = ''

					if ( 'select_host_vm_ib_systype' in request.query.keys() ):
						if ( request.query['select_host_vm_ib_systype'] == tmp_item[0] ):
							selected_item = 'selected'
							ib_sys_type = tmp_item[0]

					server_provision_dict['vm_host_output'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item[0]  )

				server_provision_dict['vm_host_output'] += '''</select><br>'''

			get_system_subtype = '''SELECT DISTINCT ib_system_subtype FROM hostinfo;'''

			if ( curr.execute(get_system_subtype) ):
				server_provision_dict['vm_host_output'] += '''Select System Subtype:  <select name = 'select_host_vm_ib_syssubtype' > '''

				for tmp_item in curr.fetchall():
					selected_item = ''
					if ( 'select_host_vm_ib_syssubtype' in request.query.keys() ):
						if ( request.query['select_host_vm_ib_syssubtype'] == tmp_item[0] ):
							selected_item = 'selected'
							ib_sys_subtype = tmp_item[0]
					
					server_provision_dict['vm_host_output'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item[0]  )

				server_provision_dict['vm_host_output'] += '''</select><br>'''


			get_datacenter = '''SELECT DISTINCT ib_datacenter FROM hostinfo;'''

			if ( curr.execute(get_datacenter) ):
				server_provision_dict['vm_host_output'] += '''Select Datacenter:  <select name = 'select_host_vm_ib_datacenter' > '''

				for tmp_item in curr.fetchall():
					if ( hostname_dc == tmp_item[0] ):
						server_provision_dict['vm_host_output'] += '''<option selected>%s</option>''' % ( tmp_item[0]  )
					else:
						server_provision_dict['vm_host_output'] += '''<option>%s</option>''' % ( tmp_item[0]  )

				server_provision_dict['vm_host_output'] += '''</select><br>'''


			get_partner_group = '''SELECT DISTINCT ib_partner_group FROM hostinfo;'''

			if ( curr.execute(get_partner_group) ):
				server_provision_dict['vm_host_output'] += '''Select Partner Group:  <select name = 'select_host_vm_ib_partner_group' > '''

				for tmp_item in curr.fetchall():
					selected_item = ''
					if ( 'select_host_vm_ib_partner_group' in request.query.keys() ):
						if ( request.query['select_host_vm_ib_partner_group'] == tmp_item[0] ):
							selected_item = 'selected'
							ib_partner_group = tmp_item[0]
										
					server_provision_dict['vm_host_output'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item[0]  )

				server_provision_dict['vm_host_output'] += '''</select><br>'''

			assumed_shortname = ''

			if ( ib_sys_type == 'cae' and ib_partner_group ):
				assumed_shortname = host_vm_env + ib_sys_type + 'frontend-' + ib_partner_group + '-' + hostname_dc
			elif ( ib_sys_type == 'cae' and ( 'unified' in ib_sys_subtype or 'userprofile' in ib_sys_subtype ) ):
				assumed_shortname = host_vm_env + ib_sys_type + 'frontend-' + ib_sys_subtype + '-' + hostname_dc
			else:
				assumed_shortname = host_vm_env + ib_sys_type + ib_sys_subtype + '-' + hostname_dc

			server_provision_dict['vm_host_output'] += '''Enter the VM Host shortname: <input type='text' name='vm_host_shortname' value='%s'><br>''' % ( assumed_shortname  )

			server_provision_dict['vm_host_output'] += '''Update: <input type='button' name='vm_host_update' value='Update' onclick='submit()'>'''
			
				



	return '''

%(header)s

<div class="content">
<h5>Deploy / Server</h5>
<h1>Deploy > Server</h1>
<hr>
	<div class="deploy_content">
		<div class="artifact_selections">
			<form method='GET' action='/deploy/servers'>
				  <div class="div-table">
		    			<div class="div-table-row">

					%(vm_host_output)s
		      		        </div>

		<hr>
		<div class='deploy_button_box'>
				<div class='safe_button_box'>

					<span style='padding-left: 15%%;'>SAFE MODE</span>
					<br>
					<input class="safe_button_chk" id='safe_mode_btn_on' name='run' type='radio' value='0' checked> 
					<label for='safe_mode_btn_on' class='safe_button_lbl'>On</label>
					<input class="safe_button_chk" id='safe_mode_btn_off' name='run' type='radio' value='1'> 
					<label for='safe_mode_btn_off' class='safe_button_lbl'>Off</label>

				</div>
			<div class='deploy_button'><span style='padding-left: 5%%; font-size: x-large'>CREATE VM
			<input type='image' name='create_vm_button' src='/images/blank.png' style='position: absolute; top: 0px; left: 0px; width: 100%%; height: 100%%;' /></span> </div>
		</div>


		<hr>

		
		</form>

		</div>

	</div>
</div>
</body>
</html>
	''' % ( server_provision_dict )


@route('/deploy/index')
def deploy_index():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

        artifacts_being_deployed = 0
        artifact_list = []
        pid = os.getpid()

        deployer_jobs['user'] = user_stats['user_name']
        deployer_jobs['uniqueId'] = uniqid()
        deployer_jobs['dateTime'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	deployer_jobs['artifacts'] = []


	for req_key in request.query.keys():
		if ( 'artifact[' in req_key ):
	                deployer_jobs['artifacts'].append(request.query['artifact[' + str(artifacts_being_deployed) + ']'] )
			artifacts_being_deployed += 1
		else:
			deployer_jobs[req_key] = request.query[req_key]

	
	curr.execute('''INSERT INTO processes VALUES ( 0, '%s', %s, '%s', NULL, %s, '%s');''' % ( deployer_jobs['uniqueId'], pid, 'client', 1, '' ) )
        conn.commit()

        pickle.dump( deployer_jobs, open(deployer_worker_path + "deployer_" + deployer_jobs['uniqueId'] + "_v3.job", "wb") )

	redirect('/deploy/watch/unique_id/' + deployer_jobs['uniqueId'] )


@route('/deploy/watch/unique_id/<unique_id>')
def watch_current_deployment(unique_id):

	watch_deploy_dict = { 'header': template_header_output('deploy', 'Artifacts'), 'unique_id': unique_id }	

	return '''
	
%(header)s

<div class="content">
<h1>Deploy ID: %(unique_id)s </h1>
<hr>
		<iframe class="iframe_style" src="/deploy/watchlog/unique_id/%(unique_id)s" width="100%%" height="100%%"></iframe>
</div>

</body>
</html>	

	''' % watch_deploy_dict

@route('/deploy/watchlog/unique_id/<unique_id>')
def watch_current_log(unique_id):
	watch_log_dict = {}

	try:
		watch_log_dict = { 'output' : open('/var/log/ib/deployer/deployer_' + unique_id + '.log', 'r').read(), 'unique_id': unique_id }
	except:
 		watch_log_dict = { 'output' : '', 'unique_id': unique_id }



	return '''
	<head> <META http-equiv="refresh" content="5;URL=/deploy/watchlog/unique_id/%(unique_id)s"> </head>
	<pre> %(output)s </pre>
	''' % watch_log_dict


@route('/deploy/current_activity')
def current_activity():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	art_count_query = '''select artifactName, env from artifactEnvironments where isDeploying = 1 ORDER BY artifactName;'''

	art_count = 0

	art_cnt_output = []

	if ( curr.execute(art_count_query) ):	
		res = curr.fetchall()
		art_count = len( res )
		for art_item in res:
			art_cnt_output.append( "<li class='dep_list'>" + art_item[0] + " : " + art_item[1].upper() + "</li>" ) 

	return '''
	<head> <META http-equiv="refresh" content="5;URL=/deploy/current_activity"> </head>
	<style> @import url(/css/deployer.css); </style>	
	<div class="current_artifacts">
	Artifacts &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	''' % ( art_count, '\n'.join(art_cnt_output) )


@route('/artifacts')
def show_artifacts():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	search_str = ''

	artifacts_template = { 'header': template_header_output('artifacts'), 'artifact_form': ''  }

	if ('artifact_search' in request.query_string):
		search_str = request.query['artifact_search']
		

	get_artifacts_query = '''SELECT artifactName, env, lastDeployedVersion, lastDeployedUser, lastDeployedDate FROM artifactEnvironments WHERE artifactName like '%%%s%%' ORDER BY artifactName;''' % search_str

	output_artifact = []
	
	if ( curr.execute(get_artifacts_query) ):
		artifact_result = curr.fetchall()
		alter_row = 0

		artifact_name = ''

		output_dict = { 'artifact': '', 'env': [], 'env_dev': '', 'env_qa': '', 'env_stage': '', 'env_prod': '' }

		for art_item in artifact_result:
			if ( output_dict['artifact'] != art_item[0] and len(art_item[0]) > 0):

				env_div_list = []
				for env_div_item in sorted(set(output_dict['env'])):
					env_div_list.append('''<div class='art_env_cell_%s' title='%s'>%s<span style="background: #3a87ad; font-size: smaller; color: white;">%s</span></div>''' % ( env_div_item,  output_dict['env_' + env_div_item] , env_div_item.upper(), output_dict['env_' + env_div_item + '_version'] ) )

				output_row = '''
			<div class="artifact_table_row">
			      <div class="artifact_table_col_name_%d">%s</div><div class="artifact_table_col_env_%d">%s</div>
			</div>
				''' % ( alter_row, output_dict['artifact'], alter_row,  '\n'.join(env_div_list) )

				if ( alter_row == 0 ):
					alter_row = 1
				else:
					alter_row = 0

				output_artifact.append( output_row  )

				output_dict['artifact'] = art_item[0]
				output_dict['env'] = []
				
				#output_dict['env'].append( art_item[1] )
				#output_dict['env_stats'].append ( str(art_item[2]) + str(art_item[3]) + str(art_item[4])  ) 

			if ( output_dict['artifact'] == art_item[0] ):
				output_dict['env'].append(art_item[1])
				if ( art_item[2] ):
					output_dict['env_' + art_item[1] ] = '''Last deployed: Version %s by %s on %s''' % (  art_item[2], art_item[3], art_item[4] )
				else:
					output_dict['env_' + art_item[1] ] = ''
				
				#output_dict['env_' + art_item[1] ] = str(art_item[2]) + str(art_item[3]) + str(art_item[4])
				output_dict['env_' + art_item[1] + '_version' ] = str(art_item[2])

	
		env_div_list = []
		for env_div_item in sorted(set(output_dict['env'])):
			env_div_list.append('''<div class='art_env_cell_%s' title='%s'>%s<span style="background: #3a87ad; font-size: smaller; color: white;">%s</span></div>''' % ( env_div_item,  output_dict['env_' + env_div_item] , env_div_item.upper(), output_dict['env_' + env_div_item + '_version'] ) )
		#for env_div_item in sorted(set(output_dict['env'])):
			#env_div_list.append('''<div class='art_env_cell_%s'>%s</div>''' % ( env_div_item, env_div_item.upper() ) )

		output_row = '''
			<div class="artifact_table_row">
			      <div class="artifact_table_col_name_%d">%s</div><div class="artifact_table_col_env_%d">%s</div>
			</div>
		''' % ( alter_row, output_dict['artifact'], alter_row,  '\n'.join(env_div_list) )

		if ( alter_row == 0 ):
			alter_row = 1
		else:
			alter_row = 0

		output_artifact.append( output_row  )

		
		#print str(output_dict)

	artifacts_template['artifact_form'] = '\n'.join(output_artifact)

	return '''<html>

	%(header)s

<div class="content">
<h1>Artifacts</h1>
<hr>
	<div class="artifact_content">
		    <div class="artifact_title_bar">
			<form  method='get' action='/artifacts'>
			  <input type='submit' name='artifact_search_submit' value='Search'>
			  <input type='text' name='artifact_search'>
			</form>
		    </div>
		   <div class="artifact_scroll">
		   
		       <div class="artifact_table">
		    	    <div class="artifact_table_row">
		                  <div class="artifact_table_col_head_name">Name</div><div class="artifact_table_col_head_env">Environment</div>
		            </div>
		            %(artifact_form)s
		   </div>
	        </div>
	</div>
</div>
</body>
</html>
	''' % artifacts_template

@route('/history')
def get_history():


	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	show_entries = [ '5', '10', '15', '20', '25', '50', '100', '200' ]

	history_input_dict = { 'header': template_header_output('history') }

	history_input_dict['show_history_log'] = ' show_logs'
	history_input_dict['show_history_art'] = ''

	if ( 'show_logs' in request.query_string ):
		history_input_dict['show_history_log'] = ' show_logs'
		history_input_dict['show_history_art'] = ''
	if ( 'show_arts' in request.query_string ):
		history_input_dict['show_history_log'] = ''
		history_input_dict['show_history_art'] = ' show_art'


	history_input_dict['current_entries'] = '10'

	if ( 'show_entries_dropdown' in request.query_string ):
		history_input_dict['current_entries'] = request.query['show_entries_dropdown']

	history_input_dict['show_entries_dropdown'] = ''
	for entry_item in show_entries:
		if ( entry_item == history_input_dict['current_entries'] ):
			history_input_dict['show_entries_dropdown'] += '<option selected>' + entry_item + '</option>'
		else:
			history_input_dict['show_entries_dropdown'] += '<option>' + entry_item + '</option>'


	history_by_log_query = '''SELECT SQL_CALC_FOUND_ROWS `uniqueId`, `datetime`, `artifactsSelected` FROM `logs` WHERE (isDryRun = 0) ORDER BY `datetime` desc LIMIT %s;''' % ( history_input_dict['current_entries'] ) 

	history_input_dict['log_history'] = ''

	if ( curr.execute(history_by_log_query) ):
		cur_results = curr.fetchall()
		col_counter = 0
		for history_item in cur_results:
			artifact_selections_output = ''
			if ( history_item[2] ):
				decoded = json.loads(history_item[2])
				#print decoded
				if ( type(decoded) == list ):
					for selected_item in decoded:
						if( 'name' in selected_item.keys() and 'env' in selected_item.keys() and 'versionToDeploy' in selected_item.keys() and 'jiraIssueKey' in selected_item.keys() ):
							artifact_selections_output += '%(name)s:%(env)s:%(versionToDeploy)s:%(jiraIssueKey)s <br>' % selected_item
				elif ( type(decoded) == dict ):
					for selected_item in decoded.keys():
						if( 'name' in decoded[selected_item].keys() and 'env' in  decoded[selected_item].keys() and 'versionToDeploy' in  decoded[selected_item].keys() and 'jiraIssueKey' in  decoded[selected_item].keys() ):
							artifact_selections_output += '%(name)s:%(env)s:%(versionToDeploy)s:%(jiraIssueKey)s <br>' % decoded[selected_item]
				
										
			
			log_hist_dict = { 'unique_id': history_item[0], 'date_time': history_item[1], 'artifact_selections': artifact_selections_output, 'alt': col_counter % 2 } 


			history_input_dict['log_history'] += '''
	    	    <div class="div-table-row">
        	          <div class="div-table-col-%(alt)s"><a href="/history/viewlog/uniqueId/%(unique_id)s">%(unique_id)s</a></div>
			  <div class="div-table-col-%(alt)s">%(date_time)s</div>
			  <div class="div-table-col-%(alt)s">%(artifact_selections)s</div>
	            </div>		
			''' % log_hist_dict
			col_counter += 1


	history_by_artifact_query = '''SELECT DISTINCT SQL_CALC_FOUND_ROWS serverDeployments.logUniqueId as logUniqueId, `artifactEnvironments`.`artifactName`, `artifactEnvironments`.`env`, `artifactEnvironments`.`id` AS `artifactEnvId`, `logs`.`datetime` FROM `serverDeployments`
 INNER JOIN `artifactEnvironments` ON serverDeployments.artifactEnvId = artifactEnvironments.id
 INNER JOIN `logs` ON serverDeployments.logUniqueId = logs.uniqueId WHERE (isDryRun = 0) ORDER BY `datetime` desc LIMIT %s''' % ( history_input_dict['current_entries'] )


	history_input_dict['artifact_history'] = ''

	if ( curr.execute(history_by_artifact_query) ):
		cur_results = curr.fetchall()
		col_counter = 0
		for art_hist_item in cur_results:
			art_hist_dict = { 'unique_id': art_hist_item[0], 'artifact': art_hist_item[1], 'env': art_hist_item[2] , 'date_time': art_hist_item[4], 'alt': col_counter % 2 }

			history_input_dict['artifact_history'] += '''
	    	    <div class="div-table-row">
        	          <div class="div-table-col-%(alt)s"><a href="/history/viewlog/uniqueId/%(unique_id)s">%(unique_id)s</a></div>
			  <div class="div-table-col-%(alt)s">%(artifact)s</div>
			  <div class="div-table-col-%(alt)s">%(env)s</div>
			  <div class="div-table-col-%(alt)s">%(date_time)s</div>
	            </div>		
			''' % art_hist_dict
			col_counter += 1


	return '''

%(header)s

<div class="content">
<h1>History</h1>
<hr>
	<div class="history_content">
		    <div class="artifact_title_bar">
			<form  method='get' action='/history'>
			  Show <select name = 'show_entries_dropdown' onchange='submit()'>%(show_entries_dropdown)s</select> entries
			  <input type='submit' name='history_search_submit' value='Search'>
			  <input type='text' name='history_search'>
			</form>
		</div>

		<div class = "history_tabs">

			<a href="/history?show_logs=true&show_entries_dropdown=%(current_entries)s">Show Logs</a>
			<a href="/history?show_arts=true&show_entries_dropdown=%(current_entries)s">Show Artifacts</a>
			   <div class="history_log_scroll%(show_history_log)s">
		       
		       		<div class="div-table">
			    	    <div class="div-table-row">
			                  <div class="div-table-head-col">Unique ID</div><div class="div-table-head-col">Date/Time</div><div class="div-table-head-col">Artifact Selections</div>
			            </div>
			    
				    %(log_history)s
				</div>
			   </div>
		   <div class="history_artifact_scroll%(show_history_art)s">
		       
		       <div class="div-table">
		    	    <div class="div-table-row">
		                  <div class="div-table-head-col">Unique ID</div>
				  <div class="div-table-head-col">Artifact</div>
				  <div class="div-table-head-col">Environment</div>
				  <div class="div-table-head-col">Date/Time</div>
		            </div>
			    
			    %(artifact_history)s
			</div>
		   </div>


		    </div>
	        </div>
	</div>

</div>
</body>
</html>
	''' % history_input_dict


@route('/history/viewlog/uniqueId/<unique_id>')
def get_history(unique_id):

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	viewlog_dict = { 'header': template_header_output('history'), 'unique_id': unique_id, 'logs': '' }

	viewlog_query = '''SELECT log FROM logs WHERE uniqueId = '%(unique_id)s'; ''' % viewlog_dict

	if ( curr.execute(viewlog_query) ):
		tmp_logs = curr.fetchone()[0]
		if ( tmp_logs ):
			viewlog_dict['logs'] = tmp_logs.replace('\r\n', '<br>').replace('\n', '<br>')

	return '''

%(header)s

<div class="content">
<h5> History / Deployment ID: %(unique_id)s </h5>
<h1>Deployment ID: %(unique_id)s</h1>
<hr>

	<div class = "viewlog_container">
		<pre class='log_output'>%(logs)s</pre>
	</div>

</div>

</body>
</html>

	''' % viewlog_dict

@route('/admin/artifacts')
def get_artifacts():

	artifact_focus = ''

	art_opt_focus = ''

	if ( 'artifact_focus' in request.query_string ):
		artifact_focus = request.query['artifact_focus']

	if ( 'art_opt_focus' in request.query_string ):
		art_opt_focus = request.query['art_opt_focus']

	admin_artifacts_dict = { 'header': template_header_output('admin', 'Artifacts') }

	admin_artifacts_dict['show_entries'] = '100'
	admin_artifacts_dict['artifact_entries'] = ''
	admin_artifacts_dict['artifact_focus'] = artifact_focus
	admin_artifacts_dict['art_opt_focus'] = art_opt_focus

	admin_artifact_query = {}

	admin_artifact_query['full'] = '''SELECT `artifacts`.* FROM `artifacts` ORDER BY `name` asc LIMIT %(show_entries)s''' % admin_artifacts_dict

	admin_artifact_query['env'] = '''SELECT env, isLoadBalanced, serverData  FROM artifactEnvironments WHERE (artifactName = '%(artifact_focus)s')''' % admin_artifacts_dict
	admin_artifact_query['other'] = '''SELECT mavenVersion, alwaysAllowSnapshots, notes FROM artifacts WHERE (name = '%(artifact_focus)s')''' % admin_artifacts_dict
	admin_artifact_query['deps'] = '''SELECT * FROM artifactDependencies WHERE (artifactName = '%(artifact_focus)s')''' % admin_artifacts_dict
	admin_artifact_query['sites'] = '''SELECT * FROM artifactSites WHERE (artifactName = '%(artifact_focus)s')''' % admin_artifacts_dict
	admin_artifact_query['proxies'] = '''SELECT * FROM artifactHaproxyProxies WHERE (artifactName = '%(artifact_focus)s')''' % admin_artifacts_dict



	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	if ( curr.execute(admin_artifact_query['full']) ):
		cur_color = 0
		res_output = curr.fetchall()
		for res_item in res_output:
			
			admin_list_dict = { 'name': res_item[1], 
			'server_type': res_item[2], 
			'group_id': res_item[3], 
			'artifact_id': res_item[4], 
			'artifact_type': res_item[5], 
			'deploy_name': res_item[7],
			'version_url_suffix': res_item[8],
			'status_url_suffix': res_item[9],
			'color': cur_color % 2,
			'show_table_options': 'hide_element', 
			'artifact_opt_output': '' }



			if ( artifact_focus == res_item[1] ):
				admin_list_dict['show_table_options'] = 'show_element'

			if ( len(art_opt_focus) > 0 ):
				if ( curr.execute(admin_artifact_query[art_opt_focus]) ):
					opt_results = curr.fetchall()
					for opt_item_res in opt_results:
						admin_opt_dict = { 'art_name': admin_list_dict['name'], 'env_name': opt_item_res[0], 'load_balanced': opt_item_res[1], 'server_data': opt_item_res[2] }
						admin_list_dict['artifact_opt_output'] += '''<div class="div-table-row">'''
						if ( art_opt_focus == 'env' ):

							if ( artifact_focus == admin_list_dict['name'] and 'edit' in  request.query_string ):
								if ( request.query['edit'] == 'env' ):
									admin_list_dict['artifact_opt_output'] += '''<div class="div-table-col">%(env_name)s</div></a>''' % admin_opt_dict
								if ( request.query['edit'] == 'server_data' ):
									admin_list_dict['artifact_opt_output'] += '''<div class="div-table-col"><input type='text' name='server_data_edit' value='%(server_data)s'></div>''' % admin_opt_dict
							if ( artifact_focus == admin_list_dict['name'] and 'load_balanced' in request.query_string and 'env_name' in request.query_string ):
								tmp_update_query = ''
								if (  request.query['load_balanced'] and request.query['env_name'] == admin_opt_dict['env_name'] ):
									if ( admin_opt_dict['load_balanced'] ):
										admin_opt_dict['load_balanced'] = 0
									else:
										admin_opt_dict['load_balanced'] = 1

									#tmp_dict('load_balanced': admin_opt_dict['load_balanced'], 'env': request.query )
									tmp_update_query = '''UPDATE artifactEnvironments SET isLoadBalanced = %(load_balanced)d WHERE artifactName = '%(art_name)s' AND env = '%(env_name)s' ;''' % admin_opt_dict
									print tmp_update_query
									curr.execute(tmp_update_query)
									conn.commit()
							
							admin_list_dict['artifact_opt_output'] += '''
						<a href='/admin/artifacts?artifact_focus=%(art_name)s&art_opt_focus=env&env_name=%(env_name)s&edit=env#%(art_name)s'><div class="div-table-col">%(env_name)s</div></a>
						<a href='/admin/artifacts?artifact_focus=%(art_name)s&art_opt_focus=env&env_name=%(env_name)s&load_balanced=%(load_balanced)d#%(art_name)s'><div class="div-table-col">%(load_balanced)s</div></a>
						<a href='/admin/artifacts?artifact_focus=%(art_name)s&art_opt_focus=env&env_name=%(env_name)s&server_data=selected&edit=server_data#%(art_name)s'><div class="div-table-col">%(server_data)s</div></a>
					</div>
							''' % admin_opt_dict
					

			admin_output_table = '''
		<div class="div-table-row">
				<div class="admin-div-table-col-%(color)s" id='%(name)s'>%(name)s</div>
	        	        <div class="admin-div-table-col-%(color)s">%(server_type)s</div>
        		        <div class="admin-div-table-col-%(color)s">%(group_id)s</div>
        		        <div class="admin-div-table-col-%(color)s"ActiveMQ-Monitor>%(artifact_id)s</div>
        	        	<div class="admin-div-table-col-%(color)s">%(artifact_type)s</div>
	        	        <div class="admin-div-table-col-%(color)s">%(deploy_name)s</div>
        		        <div class="admin-div-table-col-%(color)s">%(version_url_suffix)s</div>
        		        <div class="admin-div-table-col-%(color)s">%(status_url_suffix)s</div>
				
				<a href="/admin/artifacts?artifact_focus=%(name)s#%(name)s">Expand</a>
			
				<div class="div-table %(show_table_options)s">
					<div class="div-table-row">
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=env#%(name)s'><div class="admin-other-head-col">Environment</div></a>
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=other#%(name)s'><div class="admin-other-head-col">Other Details</div></a>
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=deps#%(name)s'><div class="admin-other-head-col">Dependencies</div></a>
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=sites#%(name)s'><div class="admin-other-head-col">Sites</div></a>
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=proxies#%(name)s'><div class="admin-other-head-col">Haproxy Proxies</div></a>
					</div>
					<div class="div-table-row">

					%(artifact_opt_output)s

					</div>
				</div>
		</div>
			
			''' % admin_list_dict

			admin_artifacts_dict['artifact_entries'] += admin_output_table

			cur_color += 1
	
	return '''
%(header)s

<div class="content">

<h5> Admin / Artifacts </h5>
<h1>Admin > Artifacts</h1>
<hr>
	<div class="admin_artifact_content">

	<div class="div-table">
		<div class="div-table-row">
			<div class="admin-div-table-head-col">Name</div>
        	        <div class="admin-div-table-head-col">Server Type</div>
        	        <div class="admin-div-table-head-col">Group ID</div>
        	        <div class="admin-div-table-head-col">Artifact ID</div>
        	        <div class="admin-div-table-head-col">Artifact Type</div>
        	        <div class="admin-div-table-head-col">Deploy Name</div>
        	        <div class="admin-div-table-head-col">Version URL Suffix</div>
        	        <div class="admin-div-table-head-col">Status URL Suffix</div>
		</div>
	%(artifact_entries)s
			
	</div>
	</div>
</div>

</body>
</html>
	''' % admin_artifacts_dict


@route('/admin/permissions')
def get_permissions():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	admin_perms_dict = { 'header': template_header_output('admin', 'Permissions'), 'user_perms' : '', 'group_perms' : '' }


	user_perms_query = '''SELECT `artifactEnvironments`.`artifactName`, `artifactEnvironments`.`env`, userPermissions.username, userPermissions.permission FROM `userPermissions`
			     INNER JOIN `artifactEnvironments` ON userPermissions.artifactEnvId = artifactEnvironments.id 
			     WHERE (userPermissions.artifactEnvId = artifactEnvironments.id) ORDER BY `artifactEnvId` asc LIMIT 10'''

	group_perms_query = '''SELECT artifactEnvironments.artifactName, artifactEnvironments.env, groupPermissions.groupDn, groupPermissions.permission  FROM `groupPermissions`
			INNER JOIN `artifactEnvironments` ON groupPermissions.artifactEnvId = artifactEnvironments.id 
			WHERE (groupPermissions.artifactEnvId = artifactEnvironments.id) ORDER BY `artifactEnvId` asc LIMIT 10'''

		

	if ( curr.execute( user_perms_query ) ):
		for res_item in curr.fetchall():
			admin_perms_dict['user_perms'] += '''
			<div class='div-table-row'>
				<div class='perms_name_col_0'>%s</div>
				<div class='perms_env_col_0'>%s</div>
				<div class='perms_username_col_0'>%s</div>
				<div class='perms_checked_col_0'>%s</div>
			</div>
			''' % ( res_item[0], res_item[1], res_item[2], res_item[3] )

	if ( curr.execute( group_perms_query ) ):
		for res_item in curr.fetchall():
			admin_perms_dict['group_perms'] += '''
			<div class='div-table-row'>
				<div class='perms_name_col_0'>%s</div>
				<div class='perms_env_col_0'>%s</div>
				<div class='perms_username_col_0'>%s</div>
				<div class='perms_checked_col_0'>%s</div>
			</div>
			''' % ( res_item[0], res_item[1], res_item[2], res_item[3] )

	return '''

%(header)s

<div class="content">

<h5> Admin / Permissions </h5>
<h1>Admin > Permissions</h1>
<hr>
	<div class="admin_artifact_content">

	<div class="div-table">
		<div class="div-table-row">
			<div class="perm_name_head_col">Artifact Name</div>
        	        <div class="perm_env_head_col">Environment</div>
        	        <div class="perm_username_head_col">Username</div>
        	        <div class="perm_check_head_col">Perm. Enabled</div>
        	        <div class="perm_delete_head_col"></div>
		</div>
			%(user_perms)s 
	</div>
	<div class="div-table">
		<div class="div-table-row">
			<div class="perm_name_head_col">Artifact Name</div>
        	        <div class="perm_env_head_col">Environment</div>
        	        <div class="perm_username_head_col">Group DN</div>
        	        <div class="perm_check_head_col">Perm. Enabled</div>
        	        <div class="perm_delete_head_col"></div>
		</div>
			%(group_perms)s
	</div>
	</div>

</div>

</body>
</html>
	''' % admin_perms_dict
	

@route('/admin/prune')
def get_prune():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()


	prune_output_dict = { 'header': template_header_output('admin', 'Data Prune'), 'prune_range' : '', 'cal_output': '', 'prune_output': '' }

	
	#SELECT `logs`.* FROM `logs` WHERE (datetime BETWEEN '2014-10-01 00:00:00' AND '2014-10-05 23:59:59')
	#get_prune_query = '''SELECT uniqueId FROM logs WHERE (datetime BETWEEN '%s' AND '%s')''' % ( time_start, time_end )
	
	if ( 'prune_data_submit' in request.query_string ):
		start_str = ''
		start_str += request.query['select_start_year']
		start_str += '-%02d' % ( datetime.datetime.strptime(request.query['select_start_month'], '%b').month )
		start_str += '-%02d' % ( int( request.query_string.split('selected_start_day_')[1].split('.')[0] ) )
		start_str += ' %02d:%02d:00' % ( int( request.query['select_start_hour'] ), int( request.query['select_start_minute'] ) )

		end_str = ''
		end_str += request.query['select_end_year']
		end_str += '-%02d' % ( datetime.datetime.strptime(request.query['select_end_month'], '%b').month )
		end_str += '-%02d' % ( int( request.query_string.split('selected_end_day_')[1].split('.')[0] ) )
		end_str += ' %02d:%02d:00' % ( int( request.query['select_end_hour'] ), int( request.query['select_end_minute'] ) )

		print start_str, end_str

		get_prune_query = '''SELECT uniqueId FROM logs WHERE (datetime BETWEEN '%s' AND '%s')''' % ( start_str, end_str )
		if ( curr.execute( get_prune_query ) ):
			prune_output_dict['prune_output'] += "The following deployment logs will be removed:<br>"
			for res_item in curr.fetchall():
				prune_output_dict['prune_output'] += res_item[0] + "<br>"
			delete_logs_query = '''DELETE FROM logs WHERE (datetime BETWEEN '%s' AND '%s')''' % ( start_str, end_str )
			curr.execute( delete_logs_query )
			conn.commit()
				
		else:
			prune_output_dict['prune_output'] += "No deployment logs found.<br>"


	months = [ '', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' ]
	years = range( 2004, 2025 )
	hours = range( 0, 24 )
	minutes = range( 0, 60)

	weekdays = [ 'S', 'M', 'T', 'W', 'Th', 'F', 'S' ]

	cur_start_month = None
	cur_start_year = None
	cur_start_day = None
	cur_start_hour = None
	cur_start_minute = None

	if ( 'select_start_year' in request.query_string and 'select_start_month' in request.query_string ):
		cur_start_month = datetime.datetime.strptime(request.query['select_start_month'], '%b').month
		cur_start_year = int(request.query['select_start_year'])
	else:
		cur_start_month = datetime.datetime.today().month 
		cur_start_year = datetime.datetime.today().year
		cur_start_day = datetime.datetime.today().day
		cur_start_hour = datetime.datetime.today().hour
		cur_start_minute = datetime.datetime.today().minute 
		

	days_this_month = monthrange(cur_start_year,cur_start_month)[1]
	#datetime.datetime( next_year, next_month, 1 ) - datetime.datetime( cur_start_year, cur_start_month, 1 )
		
	start_day = datetime.datetime( cur_start_year, cur_start_month, 1 ).weekday()
	
	prune_output_dict['cal_output'] += '''<div class='start_calendar'> Select Start Date: <br><select name='select_start_month' onchange='submit()'>'''
	for month_item in months:
		if ('select_start_month=' in request.query_string and request.query_string.split('select_start_month=')[1].split('&')[0] == month_item ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( month_item )
		elif ( months[ cur_start_month ] == month_item and 'select_start_month=' not in request.query_string ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( month_item )
		else:
			prune_output_dict['cal_output'] += '''<option>%s</option>''' % ( month_item )
			
	prune_output_dict['cal_output'] += '''</select>'''

	prune_output_dict['cal_output'] += '''<select name='select_start_year' onchange='submit()'>'''
	for year_item in years:
		if ('select_start_year=' in request.query_string and int(request.query_string.split('select_start_year=')[1].split('&')[0]) == year_item ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( year_item  )
		elif ( cur_start_year == int(year_item) and 'select_start_year=' not in request.query_string ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( year_item )
		else:
			prune_output_dict['cal_output'] += '''<option>%s</option>''' % ( year_item  )
	
	prune_output_dict['cal_output'] += '''</select>'''

	for day_item in range(0, 7):
		prune_output_dict['cal_output'] += '''<div class='cal_cell' style='top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item / 7 ) + 2 ) * 25, ( day_item  % 7 ) * 25 , weekdays[day_item] )
		
	for day_item in range(1, 42):
		if (day_item > start_day and day_item <= days_this_month + start_day ):
			if ( ( 'select_start_day_' in request.query_string ) and int( request.query_string.split('select_start_day_')[1].split('.')[0] ) == ( day_item - start_day ) ):
				prune_output_dict['cal_output'] += '''<div class='cal_cell' style='background-color: #bbb; top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item  / 7 ) + 3 ) * 25, ( day_item % 7 ) * 25 , '''<input type = 'image' src = '/images/blank.png' style='position: absolute; height: 20px; width: 20px;'name='select_start_day_%s' value='%s'>%s''' % ( str( day_item - start_day ), str( day_item - start_day ), str( day_item - start_day ) ) )
				prune_output_dict['cal_output'] += '''<input type='hidden' name='selected_start_day_%d.x'>''' % ( day_item - start_day )
			elif ( ( 'select_start_day_' not in request.query_string ) and ( 'selected_start_day_' in request.query_string ) and int( request.query_string.split('selected_start_day_')[1].split('.')[0] ) == ( day_item - start_day ) ):
				prune_output_dict['cal_output'] += '''<div class='cal_cell' style='background-color: #bbb; top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item  / 7 ) + 3 ) * 25, ( day_item % 7 ) * 25 , '''<input type = 'image' src = '/images/blank.png' style='position: absolute; height: 20px; width: 20px;'name='select_start_day_%s' value='%s'>%s''' % ( str( day_item - start_day ), str( day_item - start_day ), str( day_item - start_day ) ) )
				prune_output_dict['cal_output'] += '''<input type='hidden' name='selected_start_day_%d.x'>''' % ( day_item - start_day )
			elif ( cur_start_day == ( day_item - start_day ) ):
				prune_output_dict['cal_output'] += '''<div class='cal_cell' style='background-color: #bbb; top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item  / 7 ) + 3 ) * 25, ( day_item % 7 ) * 25 , '''<input type = 'image' src = '/images/blank.png' style='position: absolute; height: 20px; width: 20px;'name='select_start_day_%s' value='%s'>%s''' % ( str( day_item - start_day ), str( day_item - start_day ), str( day_item - start_day ) ) )
				prune_output_dict['cal_output'] += '''<input type='hidden' name='selected_start_day_%d.x'>''' % ( day_item - start_day )
			else: 
				prune_output_dict['cal_output'] += '''<div class='cal_cell' style='top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item  / 7 ) + 3 ) * 25, ( day_item % 7 ) * 25 , '''<input type = 'image' src = '/images/blank.png' style='position: absolute; height: 20px; width: 20px;'name='select_start_day_%s' value='%s'>%s''' % ( str( day_item - start_day ), str( day_item - start_day ), str( day_item - start_day ) ) )

	
	prune_output_dict['cal_output'] += '''<br><br><br><br><br><br><br><br><br><br>Time: <select name='select_start_hour' onchange='submit()'>'''
	for hour_item in hours:
		if ('select_start_hour=' in request.query_string and int(request.query_string.split('select_start_hour=')[1].split('&')[0]) == hour_item ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( hour_item  )
		elif ( cur_start_hour == int(hour_item) and 'select_start_hour=' not in request.query_string ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( hour_item )
		else:
			prune_output_dict['cal_output'] += '''<option>%s</option>''' % ( hour_item  )
	
	prune_output_dict['cal_output'] += '''</select>'''


	prune_output_dict['cal_output'] += ''':<select name='select_start_minute' onchange='submit()'>'''
	for minute_item in minutes:
		if ('select_start_minute=' in request.query_string and int(request.query_string.split('select_start_minute=')[1].split('&')[0]) == minute_item ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( minute_item  )
		elif ( cur_start_minute == int(minute_item) and 'select_start_minute=' not in request.query_string ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( minute_item )
		else:
			prune_output_dict['cal_output'] += '''<option>%s</option>''' % ( minute_item  )
	
	prune_output_dict['cal_output'] += '''</select></div>'''
	


	cur_end_month = None
	cur_end_year = None
	cur_end_day = None
	cur_end_hour = None
	cur_end_minute = None

	if ( 'select_end_year' in request.query_string and 'select_end_month' in request.query_string ):
		cur_end_month = datetime.datetime.strptime(request.query['select_end_month'], '%b').month
		cur_end_year = int(request.query['select_end_year'])
	else:
		cur_end_month = datetime.datetime.today().month 
		cur_end_year = datetime.datetime.today().year
		cur_end_day = datetime.datetime.today().day
		cur_end_hour = datetime.datetime.today().hour
		cur_end_minute = datetime.datetime.today().minute 
		

	days_this_month = monthrange(cur_end_year,cur_end_month)[1]
	#datetime.datetime( next_year, next_month, 1 ) - datetime.datetime( cur_end_year, cur_end_month, 1 )
		
	start_day = datetime.datetime( cur_end_year, cur_end_month, 1 ).weekday()
	
	prune_output_dict['cal_output'] += '''<div class='end_calendar'> Select End Date: <br><select name='select_end_month' onchange='submit()'>'''
	for month_item in months:
		if ('select_end_month=' in request.query_string and request.query_string.split('select_end_month=')[1].split('&')[0] == month_item ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( month_item )
		elif ( months[ cur_end_month ] == month_item and 'select_end_month=' not in request.query_string ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( month_item )
		else:
			prune_output_dict['cal_output'] += '''<option>%s</option>''' % ( month_item )
			
	prune_output_dict['cal_output'] += '''</select>'''

	prune_output_dict['cal_output'] += '''<select name='select_end_year' onchange='submit()'>'''
	for year_item in years:
		if ('select_end_year=' in request.query_string and int(request.query_string.split('select_end_year=')[1].split('&')[0]) == year_item ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( year_item  )
		elif ( cur_end_year == int(year_item) and 'select_end_year=' not in request.query_string ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( year_item )
		else:
			prune_output_dict['cal_output'] += '''<option>%s</option>''' % ( year_item  )
	
	prune_output_dict['cal_output'] += '''</select>'''

	for day_item in range(0, 7):
		prune_output_dict['cal_output'] += '''<div class='cal_cell' style='top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item / 7 ) + 2 ) * 25, ( day_item  % 7 ) * 25 , weekdays[day_item] )
		
	for day_item in range(1, 42):
		if (day_item > start_day and day_item <= days_this_month + start_day ):
			if ( ( 'select_end_day_' in request.query_string ) and int( request.query_string.split('select_end_day_')[1].split('.')[0] ) == ( day_item - start_day ) ):
					prune_output_dict['cal_output'] += '''<div class='cal_cell' style='background-color: #bbb; top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item  / 7 ) + 3 ) * 25, ( day_item % 7 ) * 25 , '''<input type = 'image' src = '/images/blank.png' style='position: absolute; height: 20px; width: 20px;'name='select_end_day_%s' value='%s'>%s''' % ( str( day_item - start_day ), str( day_item - start_day ), str( day_item - start_day ) ) )
					prune_output_dict['cal_output'] += '''<input type='hidden' name='selected_end_day_%d.x'>''' % ( day_item - start_day )
			elif ( ( 'select_end_day_' not in request.query_string ) and ( 'selected_end_day_' in request.query_string ) and int( request.query_string.split('selected_end_day_')[1].split('.')[0] ) == ( day_item - start_day ) ):
					prune_output_dict['cal_output'] += '''<div class='cal_cell' style='background-color: #bbb; top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item  / 7 ) + 3 ) * 25, ( day_item % 7 ) * 25 , '''<input type = 'image' src = '/images/blank.png' style='position: absolute; height: 20px; width: 20px;'name='select_end_day_%s' value='%s'>%s''' % ( str( day_item - start_day ), str( day_item - start_day ), str( day_item - start_day ) ) )
					prune_output_dict['cal_output'] += '''<input type='hidden' name='selected_end_day_%d.x'>''' % ( day_item - start_day )
			elif ( cur_start_day == ( day_item - start_day ) ):
					prune_output_dict['cal_output'] += '''<div class='cal_cell' style='background-color: #bbb; top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item  / 7 ) + 3 ) * 25, ( day_item % 7 ) * 25 , '''<input type = 'image' src = '/images/blank.png' style='position: absolute; height: 20px; width: 20px;'name='select_end_day_%s' value='%s'>%s''' % ( str( day_item - start_day ), str( day_item - start_day ), str( day_item - start_day ) ) )
					prune_output_dict['cal_output'] += '''<input type='hidden' name='selected_end_day_%d.x'>''' % ( day_item - start_day )
			else: 
				prune_output_dict['cal_output'] += '''<div class='cal_cell' style='top: %dpx; left: %dpx;'>%s</div>''' % ( ( ( day_item  / 7 ) + 3 ) * 25, ( day_item % 7 ) * 25 , '''<input type = 'image' src = '/images/blank.png' style='position: absolute; height: 20px; width: 20px;'name='select_end_day_%s' value='%s'>%s''' % ( str( day_item - start_day ), str( day_item - start_day ), str( day_item - start_day ) ) )

	
	prune_output_dict['cal_output'] += '''<br><br><br><br><br><br><br><br><br><br>Time: <select name='select_end_hour' onchange='submit()'>'''
	for hour_item in hours:
		if ('select_end_hour=' in request.query_string and int(request.query_string.split('select_end_hour=')[1].split('&')[0]) == hour_item ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( hour_item  )
		elif ( cur_end_hour == int(hour_item) and 'select_end_hour=' not in request.query_string ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( hour_item )
		else:
			prune_output_dict['cal_output'] += '''<option>%s</option>''' % ( hour_item  )
	
	prune_output_dict['cal_output'] += '''</select>'''


	prune_output_dict['cal_output'] += ''':<select name='select_end_minute' onchange='submit()'>'''
	for minute_item in minutes:
		if ('select_end_minute=' in request.query_string and int(request.query_string.split('select_end_minute=')[1].split('&')[0]) == minute_item ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( minute_item  )
		elif ( cur_end_minute == int(minute_item) and 'select_end_minute=' not in request.query_string ):
			prune_output_dict['cal_output'] += '''<option selected>%s</option>''' % ( minute_item )
		else:
			prune_output_dict['cal_output'] += '''<option>%s</option>''' % ( minute_item  )
	
	prune_output_dict['cal_output'] += '''</select></div>'''
	
	return '''
%(header)s

<div class="content">

<h5> Admin / Prune Data </h5>
<h1>Admin > Prune Data</h1>
<hr>
	<div class="prune_artifact_content">

			%(prune_range)s 

			<form method='get' action='/admin/prune'>
				%(cal_output)s
				<input type='submit' name='prune_data_submit' value='Prune Data'>

			</form>
			<div class="prune_display">
			%(prune_output)s
			</div>

	</div>

</div>

</body>
</html>
	''' % prune_output_dict
	

@route('/admin/reports')
def admin_reports():

	admin_reports_template = { 'header': template_header_output('admin', 'Reports') }

	return '''

%(header)s

<div class="content">
<h5> Admin / Reports </h5>
<h1>Admin > Reports</h1>
<hr>
	<div class="help_content">
				

	</div>
</div>
</body>
</html>
	''' % admin_reports_template


@route('/admin/monitor')
def admin_monitor():

	monitor_output = { 'header': template_header_output('admin', 'Monitor Services'), 'monitor_str' :''}

	if ( os.popen('''ps -Aef | grep python | grep deployer_artifact_worker.py | grep -v grep''').read() ):
		monitor_output['monitor_str'] += "Deployer Artifact Worker: RUNNING<br><br>"
	else:
		monitor_output['monitor_str'] += "Deployer Artifact Worker: STOPPED<br><br>"

	if ( os.popen('''ps -Aef | grep python | grep deployer_service_worker.py | grep -v grep''').read() ):
		monitor_output['monitor_str'] += "Deployer Restart Service: RUNNING<br><br>"
	else:
		monitor_output['monitor_str'] += "Deployer Restart Service: STOPPED<br><br>"

	if ( os.popen('''ps -Aef | grep python | grep deployer_server_provisioner.py | grep -v grep''').read() ):
		monitor_output['monitor_str'] += "Deployer Server Provisioner: RUNNING<br><br>"
	else:
		monitor_output['monitor_str'] += "Deployer Server Provisioner: STOPPED<br><br>"

	return '''

%(header)s

<div class="content">
<h5> Admin / Monitor </h5>
<h1>Admin > Monitor</h1>
<hr>

%(monitor_str)s

</div>
</body>
</html>
	''' % ( monitor_output )


@route('/help')
def deployer_help():

	help_template = { 'header': template_header_output('help') }
	
	return '''

%(header)s

<div class="content">
<h1>Help</h1>
<hr>
	<div class="help_content">
		
<h2>Working in an RFC ticket?</h2>

    <ol>
    <li>Assign the RFC to 'sysadmin.jira'</li>
    <li>Create a new comment on the RFC with one or more of the following:
	<ul>
        <li>A description of your issue</li>
        <li>A screen shot of the Deployer page showing the notification(s) you received</li>
        <li>Click the 'Show Log' button and copy the entire log, or at least the line containing the "unique id for this instance"</li>
	</ul>
	</li>
    </ol>
<h2>Otherwise...</h2>

Please open a ticket here: <a href="http://jira.ibsys.com/browse/ONECD">http://jira.ibsys.com/browse/ONECD</a>

	</div>
</div>
</body>
</html>
	''' % help_template


# Static Routes
@get('/js/<filename:re:.*\.js>')
def javascript(filename):
	return static_file(filename, root='public/js')

@get('/css/<filename:re:.*\.css>')
def stylesheets(filename):
	return static_file(filename, root='public/css')

@get('/css/ib-brand/images/<filename:re:.*\.(png|jpg|gif|jpeg|ico)>')
def ibcustom(filename):
	return static_file(filename, root='public/css/ib-brand/images/')

@get('/css/smoothness/images/<filename:re:.*\.(png|jpg|gif|jpeg|ico)>')
def ibcustom(filename):
	return static_file(filename, root='public/css/ib-brand/images/')

@get('/images/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
	return static_file(filename, root='public/images')

@get('/bootstrap/img/<filename:re:.*\.(png|jpg|gif|jpeg)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/img')

@get('/bootstrap/js/<filename:re:.*\.(js)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/js')

@get('/bootstrap/css/<filename:re:.*\.(css)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/css')

@get('/cache/<filename:re:.*\.(js|css)>')
def cache(filename):
	return static_file(filename, root='public/cache')

@get('/screenshots/<unique_id>/<filename>')
def screenshots(unique_id, filename):
	return static_file(filename, root='public/screenshots/' + unique_id )

@error(404)
def error404(error):
	return "Oops!<br>Unable to find page<br>"



debug(True)
run(app=app_middleware, host='0.0.0.0', port=18082, reloader=True)
