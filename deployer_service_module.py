### Project:  IB Deployer Operations Module v3.1.12
### Dev:  Matthew Wiljanen
### Date: 2015-01-30

### Current Libraries needed to run deployer

from bottle import request, debug, route, run, get, post, template, error, static_file, response, redirect, app, abort, view, template
from beaker.middleware import SessionMiddleware
import MySQLdb
import ldap
import datetime
import json
import pycurl
import cStringIO
from xml.dom import minidom
import re
import time
import math
import os
import pickle
import ast
import uuid
from calendar import monthrange
import XenAPI
import pprint
from utils import deployer_zenoss
### Load deployer config files


configs = {}

deployer_config_path = "/etc/ibconf/deployer/"

deployer_worker_path = "/opt/pydeployer/temp/services/"

configs['deployer'] = open(deployer_config_path + "deployer.ini" , "r").read()
configs['services'] = open(deployer_config_path + "services.ini" , "r").read()

### Stored queries pulled from old deployer version

query_dict = {
        'dash_currentdeps': """SELECT `processes`.*, `logs`.* FROM `processes`  INNER JOIN `logs` ON processes.uniqueId = logs.uniqueId WHERE (processes.type = 'client')""",
        'dash_servers': """SELECT `serversInDeployment`.* FROM `serversInDeployment`""",
        'dash_artifacts': """SELECT `artifacts`.*, `artifactEnvironments`.* FROM `artifacts` INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1)""",
}

### User session storage

session_options = {   
                      'session.type': 'file',
                      'session.data_dir': './session/',
                      'session.auto': True,
                  }

user_stats = {'first_name':'', 'user_name':'' , 'full_name':'', 'deployer_roles':[]}

### Initialization of user session

app_middleware = SessionMiddleware(app(), session_options)
app_session = request.environ.get('beaker.session')

### Default MySQLdb connect list

db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

def template_header_output(nav_item='', subtitle=''):
	'''
	Default NavBar template html output, this could easily be a bottle template file, but left as heredoc so it is more dynamic in the code.
	'''
	
	template_selector = { 'select_dashboard': '',
		'select_deploy': '',
		'select_artifacts': '',
		'select_history': '',
		'select_admin': '',
		'select_help': '', 
		'title': 'IB Deployer 3.0 :: ' + nav_item.title() + ' :: ' + subtitle }


	template_selector[ 'select_' + nav_item ] = 'nav_item_sel'

	return '''
<html>
<head>
        <style> @import url(/css/deployer.css); </style>
	<title> %(title)s </title>
</head>
<body>

        <ul class="nav">

                <li class="nav_text">IB Deployer 2.5</li>
                <a class="main_menu_link" href="/dashboard"><li class="nav_item %(select_dashboard)s">Dashboard</li></a>
                <a class="main_menu_link" href="/deploy"><li class="nav_item %(select_dashboard)s">Deploy</li></a>
                <a class="main_menu_link" href="/operations"><li class="nav_item %(select_dashboard)s">Operations</li></a>
                <a class="main_menu_link" href="/artifacts"><li class="nav_item %(select_artifacts)s">Artifacts</li></a>
                <a class="main_menu_link" href="/history"><li class="nav_item %(select_history)s">History</li></a>
                <div class="admin_menu %(select_admin)s" >Admin vvv
                        <a class="main_menu_link" href="/admin/Artifacts"><div class='admin_menu_item'>Artifacts</div></a>
                        <a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
                        <a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
                        <a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
                        <a class="main_menu_link" href="/monitor/status"><div class='admin_menu_item'>Monitor Status</div></a>
                </div>

                <a class="main_menu_link" href="/help"><li class="nav_item %(select_help)s">Help</li></a>
                <a class="main_menu_link" href="/auth/logout"><li class="nav_item %(select_help)s">Log Out</li></a>
                <li class="nav_spacer"></li>
        </ul>

	''' % template_selector





@route('/operations/restart')
def restart_services():

	restart_tasks_job = { 'tasks': dict(request.query), 'job_id': uuid.uuid4(), 'user_name': user_stats['user_name'] }

	pickle.dump( restart_tasks_job, open(deployer_worker_path + "service_restart_" + str(restart_tasks_job['job_id']) + ".job", "wb") )

	redirect('/operations/watch/job_id/' + str(restart_tasks_job['job_id']) )


@route('/operations/watch/job_id/<job_id>')
def watch_service_restart(job_id):

	watch_service_dict = { 'header': template_header_output('deploy', 'Service') ,'job_id': job_id }	


	return '''
	
%(header)s

<div class="content">
<h1>Service Restart Job ID: %(job_id)s </h1>
<hr>
		<iframe class="iframe_style" src="/operations/watchlog/job_id/%(job_id)s#bottom" width="100%%" height="100%%" </iframe>
</div>

</body>
</html>	

	''' % watch_service_dict

@route('/operations/watchlog/job_id/<job_id>')
def watch_service_current_log(job_id):
	watch_log_dict = {}

	try:
		watch_log_dict = { 'output' : open('/var/log/ib/deployer/deployer_service_' + job_id + '.log', 'r').read(), 'job_id': job_id }
	except:
 		watch_log_dict = { 'output' : '', 'job_id': job_id }

	return '''
<html>
	
	<head> <META http-equiv="refresh" content="2;URL=/operations/watchlog/job_id/%(job_id)s"> </head>
	<body>
		<pre> %(output)s </pre>
		
	</body>
</html>
	''' % watch_log_dict



@route('/operations')
def get_restart_services():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	service_restart_form = { 'header': template_header_output('deploy', 'Services') ,'hostnames': '', 'env_list': '', 'repo_list': '', 'domain_list': '', 'subtype_list': '', 
				'systype_list': '', 'select_all_puppet': '', 'select_all_tomcat': '', 'select_all_zenoss': '', 
				'partner_list': '', 'select_zenoss_mode': '', 'select_zenoss_maint_mode': '', 'maint_win_start': '' , 
				'maint_win_end': '', 'maint_win_error': '', 'set_cae_status': '', 'select_all_cae': '', 'set_tomcat_status': '', 'optional_header': '',
				'optional_columns': '' }


	if ( 'restart_button' in request.query_string ):
		redirect('/deploy/services/restart?' + request.query_string )

	filter_list = []

	zenoss_maint_states = [ 'Maintenance', 'Development', 'QA', 'Stage', 'Production' ]

	zenoss_maint_window_modes = [ 'None', 'Set', 'Delete' ]

	filter_str = ' WHERE hostname = "None" ;'

	if ( 'env_filter' in request.query_string ):
		if ( request.query['env_filter'] != 'None' and len( request.query['env_filter'] ) > 0 ):
			filter_list.append(""" ib_env = '%s'""" % request.query['env_filter'] )

	if ( 'partner_filter' in request.query_string ):
		if ( request.query['partner_filter'] != 'None' and len( request.query['partner_filter'] ) > 0 ):
			filter_list.append(""" ib_partner_group = '%s'""" % request.query['partner_filter'] )

	if ( 'domain_filter' in request.query_string ):
		if ( request.query['domain_filter'] != 'None' and len( request.query['domain_filter'] ) > 0 ):
			filter_list.append(""" domain = '%s'""" % request.query['domain_filter'] )

	if ( 'systype_filter' in request.query_string ):
		if ( request.query['systype_filter'] != 'None' and len( request.query['systype_filter'] ) > 0 ):
			filter_list.append(""" ib_system_type = '%s'""" % request.query['systype_filter'] )
			if ( request.query['systype_filter'] == 'mongo' ):
				service_restart_form['optional_header'] = 'Reindex Mongo Host'

	if ( 'subtype_filter' in request.query_string ):
		if ( request.query['subtype_filter'] != 'None' and len( request.query['subtype_filter'] ) > 0 ):
			filter_list.append(""" ib_system_subtype = '%s'""" % request.query['subtype_filter'] )

	if ( 'repo_filter' in request.query_string ):
		if ( request.query['repo_filter'] != 'None' and len( request.query['repo_filter'] ) > 0 ):
			filter_list.append(""" repository_host like '%%%s%%'""" % ( request.query['repo_filter'] ) )

	if ( 'select_all_puppet_hosts' in request.query_string ):
		if ( request.query['select_all_puppet_hosts'] == 'on' ):
			service_restart_form['select_all_puppet'] = 'checked'

	if ( 'select_all_tomcat_hosts' in request.query_string ):
		if ( request.query['select_all_tomcat_hosts'] == 'on' ):
			service_restart_form['select_all_tomcat'] = 'checked'

	if ( 'select_all_zenoss_hosts' in request.query_string ):
		if ( request.query['select_all_zenoss_hosts'] == 'on' ):
			service_restart_form['select_all_zenoss'] = 'checked'

	if ( 'select_all_cae_hosts' in request.query_string ):
		if ( request.query['select_all_cae_hosts'] == 'on' ):
			service_restart_form['select_all_cae'] = 'checked'

	if ( len(filter_list) ):
		if ( len(filter_list) == 1 ):
			filter_str = " WHERE " + filter_list[0]
		else: 
			filter_str = " WHERE " + ' AND '.join(filter_list)



	get_env_query = "select distinct hostinfo.ib_env from hostinfo;"

	if ( curr.execute( get_env_query ) ):
		service_restart_form['env_list'] = '''<select name = 'env_filter' ><option>None</option>'''

		for env_item in curr.fetchall():
			if ( 'env_filter' in request.query_string and env_item[0] in request.query_string and len( env_item[0] ) > 0 ):
				service_restart_form['env_list'] += '''<option selected>%s</option>''' % ( request.query['env_filter'] )
			elif ( len( env_item[0] ) > 0 ):
				service_restart_form['env_list'] += '''<option>%s</option>''' % ( env_item[0] )


		service_restart_form['env_list'] += '</select><br>'


	get_partner_query = "select distinct hostinfo.ib_partner_group from hostinfo;"

	if ( curr.execute( get_partner_query ) ):
		service_restart_form['partner_list'] = '''<select name = 'partner_filter' ><option>None</option>'''

		for partner_item in curr.fetchall():
			if ( 'partner_filter' in request.query_string and partner_item[0] in request.query_string and len( partner_item[0] ) > 0 ):
				service_restart_form['partner_list'] += '''<option selected>%s</option>''' % ( request.query['partner_filter'] )
			elif ( len( partner_item[0] ) > 0 ):
				service_restart_form['partner_list'] += '''<option>%s</option>''' % ( partner_item[0] )


		service_restart_form['partner_list'] += '</select><br>'

			

	get_repo_list_query = "select distinct hostinfo.repository_host from hostinfo;"

	if ( curr.execute( get_repo_list_query ) ):
		service_restart_form['repo_list'] = '''<select name = 'repo_filter' ><option>None</option>'''

		for repo_host in curr.fetchall():
			if ( 'repo_filter' in request.query_string and repo_host[0] in request.query_string and len( repo_host[0] ) > 0 ):
				service_restart_form['repo_list'] += '''<option selected>%s</option>''' % ( request.query['repo_filter'] )
			elif ( len( repo_host[0] ) > 0 ):
				service_restart_form['repo_list'] += '''<option>%s</option>''' % (repo_host[0])

		service_restart_form['repo_list'] += '</select><br>'



	get_systype_query = "select distinct hostinfo.ib_system_type from hostinfo;"

	if ( curr.execute( get_systype_query ) ):
		service_restart_form['systype_list'] = '''<select name = 'systype_filter' ><option>None</option>'''

		for systype_item in curr.fetchall():
			if ( 'systype_filter' in request.query_string and systype_item[0] in request.query_string and len( systype_item[0] ) > 0 ):
				service_restart_form['systype_list'] += '''<option selected>%s</option>''' % ( request.query['systype_filter'] )
			else:
				service_restart_form['systype_list'] += '''<option>%s</option>''' % ( systype_item[0] )


		service_restart_form['systype_list'] += '</select><br>'


	get_subtype_query = "select distinct hostinfo.ib_system_subtype from hostinfo;"

	if ( curr.execute( get_subtype_query ) ):
		service_restart_form['subtype_list'] = '''<select name = 'subtype_filter' ><option>None</option>'''

		for subtype_item in curr.fetchall():
			if ( 'subtype_filter' in request.query_string and subtype_item[0] in request.query_string and len( subtype_item[0] ) > 0 ):
				service_restart_form['subtype_list'] += '''<option selected>%s</option>''' % ( request.query['subtype_filter'] )
			elif ( len( subtype_item[0] ) > 0 ):
				service_restart_form['subtype_list'] += '''<option>%s</option>''' % ( subtype_item[0] )


		service_restart_form['subtype_list'] += '</select><br>'



	get_domain_query = "select distinct domain from hostinfo;"

	if ( curr.execute( get_domain_query ) ):
		service_restart_form['domain_list'] = '''<select name = 'domain_filter' ><option>None</option>'''

		for domain_item in curr.fetchall():
			if ( 'domain_filter' in request.query_string and domain_item[0] in request.query_string and len( domain_item[0] ) > 0):
				service_restart_form['domain_list'] += '''<option selected>%s</option>''' % ( request.query['domain_filter'] )
			elif ( len( domain_item[0] ) > 0 ):
				service_restart_form['domain_list'] += '''<option>%s</option>''' % ( domain_item[0] )

		service_restart_form['domain_list'] += '</select><br>'
		
	
	get_servers_query = "SELECT hostname FROM hostinfo %s ;" % ( filter_str )


	tmp_zenoss_state = ''

	service_restart_form['select_zenoss_mode'] += '''<select name = 'select_zenoss_maint_mode' ><option>None</option>'''
	for tmp_item in sorted(deployer_zenoss.zenoss_states.keys()):
		selected_item = ''
		if ( tmp_item in request.query_string and 'select_zenoss_maint_mode' in	request.query_string ):
			if ( request.query['select_zenoss_maint_mode'] == tmp_item ):
				selected_item = 'selected'
				tmp_zenoss_state = tmp_item
		service_restart_form['select_zenoss_mode'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item )

	service_restart_form['select_zenoss_mode'] += '''</select>'''

	


	if( curr.execute( get_servers_query ) ):
		for serv_item in curr.fetchall():
			tmp_dict = { 'tmp_host': serv_item[0], 
				'puppet_checked': service_restart_form['select_all_puppet'] ,
				'tomcat_checked': service_restart_form['select_all_tomcat'] ,
				'zenoss_checked': service_restart_form['select_all_zenoss'] ,
				'cae_checked': service_restart_form['select_all_cae'] ,
				'zenoss_state': tmp_zenoss_state, 'mongo_index': '' }

			if ( request.query['systype_filter'] == 'mongo' ):
				tmp_dict['mongo_index'] = '''<td><input type = 'checkbox' name = 'reindex_mongo_on_%s' ></td>''' % (serv_item[0])
			
			service_restart_form['hostnames'] += '''<tr><td>%(tmp_host)s</td>
								<td><input type = 'checkbox' name = 'restart_puppet_on_%(tmp_host)s' %(puppet_checked)s ></td> 
								<td><input type = 'checkbox' name = 'restart_tomcat_on_%(tmp_host)s' %(tomcat_checked)s ></td>
								<td><input type = 'checkbox' name = 'set_zenoss_on_%(tmp_host)s_to_%(zenoss_state)s' %(zenoss_checked)s ></td>
								<td><input type = 'checkbox' name = 'set_cae_status_on_%(tmp_host)s' %(cae_checked)s ></td>
								%(mongo_index)s
								</tr>''' % tmp_dict

	if ('set_zenoss_maint_window_start' in request.query_string and 'set_zenoss_maint_window_end' in request.query_string):
		service_restart_form['maint_win_start'] = request.query['set_zenoss_maint_window_start']
		service_restart_form['maint_win_end'] = request.query['set_zenoss_maint_window_end']
		if ( len(service_restart_form['maint_win_start']) > 0 and len( service_restart_form['maint_win_end'] ) > 0 ):
			try:
				datetime.datetime.strptime(service_restart_form['maint_win_start'], '%m/%d/%Y %H:%M')
				datetime.datetime.strptime(service_restart_form['maint_win_end'], '%m/%d/%Y %H:%M')
			except:
				service_restart_form['maint_win_error'] = "Not a valid date"
				service_restart_form['maint_win_start'] = ''
				service_restart_form['maint_win_end'] = ''

	service_restart_form['select_zenoss_maint_mode'] += '''Maintenance Window <select name = 'select_zenoss_maint_window' > '''
	for tmp_item in zenoss_maint_window_modes:
		selected_item = ''
		if ( tmp_item in request.query_string and 'select_zenoss_maint_window' in request.query_string ):
			if ( request.query['select_zenoss_maint_window'] == tmp_item ):
				selected_item = 'selected'
		service_restart_form['select_zenoss_maint_mode'] += '''<option %s>%s</option>''' % ( selected_item, tmp_item )
	service_restart_form['select_zenoss_maint_mode'] += '''</select>'''

	service_restart_form['set_cae_status'] += '''Set CAE Status <select name = 'set_cae_status_mode' ><option>None</option><option>Down</option><option>Up</option></select>'''

	service_restart_form['set_tomcat_status'] += '''Set Tomcat Status <select name = 'set_tomcat_status_mode' ><option>None</option><option>Restart</option><option>Start</option><option>Stop</option></select>'''
	
	return '''

%(header)s

<div class="content">
<h5> Deploy / Service Restart </h5>
<h1>Deploy > Service Restart</h1>
<hr>
	<div class="deploy_content">
		<div class="artifact_selections">
			<form method='GET' action='/deploy/services'>
				  <div class="div-table">
		    			<div class="div-table-row">
		      		        </div>
			Environment:<br>
			%(env_list)s<br>
			System Types:<br>
			%(systype_list)s<br>
			System Subtypes:<br>
			%(subtype_list)s<br>
			Partner Groups:<br>
			%(partner_list)s<br>
			Repository Hosts:<br>
			%(repo_list)s<br>
			Domain List:
			%(domain_list)s<br>
		

			<span style='color: red ; font-weight: bold; '>%(maint_win_error)s</span><br>
			Set Zenoss Maint Window Start Time: ( ex: 10/22/2014 22:45 ) <input type='text' class='text_box_normal' name='set_zenoss_maint_window_start' value='%(maint_win_start)s' ><div class='maint_start_cal'></div><br>
			Set Zenoss Maint Window End Time: <input type='text' class='text_box_normal' name='set_zenoss_maint_window_end' value='%(maint_win_end)s' > <div class='maint_stop_cal'></div><br>

			<input type='submit' name='update_hosts' value = 'Update'> 
			
			<hr>
			<table><tr><th>Hostname</th><th>Puppet Restart</th><th>Tomcat Restart</th><th>Set Zenoss State</th><th>Set CAE Status</th><th>%(optional_header)s</tr>
				<tr><td></td><td>Select All: <input type='checkbox' name='select_all_puppet_hosts' %(select_all_puppet)s></td>
					<td>
					%(set_tomcat_status)s <br>
					Select All: <input type='checkbox' name='select_all_tomcat_hosts' %(select_all_tomcat)s></td>
					<td>
					Set Selected to %(select_zenoss_mode)s <br> 
					%(select_zenoss_maint_mode)s <br>
					Select All: <input type='checkbox' name='select_all_zenoss_hosts' %(select_all_zenoss)s> </td>
					<td>%(set_cae_status)s <br> Select All: <input type='checkbox' name='select_all_cae_hosts' %(select_all_cae)s> </td>
					<td>%(optional_columns)s</td>
				</tr>
			%(hostnames)s
			</table>
		<hr>
		<div class='deploy_button_box'>
				<div class='safe_button_box'>

					<span style='padding-left: 15%%;'>SAFE MODE</span>
					<br>
					<input class="safe_button_chk" id='safe_mode_btn_on' name='run' type='radio' value='0' checked> 
					<label for='safe_mode_btn_on' class='safe_button_lbl'>On</label>
					<input class="safe_button_chk" id='safe_mode_btn_off' name='run' type='radio' value='1'> 
					<label for='safe_mode_btn_off' class='safe_button_lbl'>Off</label>

				</div>
			<div class='deploy_button'><span style='padding-left: 5%%; font-size: xx-large'>RESTART
			<input type='image' name='restart_button' src='/images/blank.png' style='position: absolute; top: 0px; left: 0px; width: 100%%; height: 100%%;' /></span> </div>
		</div>


		<hr>

		
		</form>

		</div>

	</div>
</div>
</body>
</html>
	''' % ( service_restart_form )

@get('/css/<filename:re:.*\.css>')
def stylesheets(filename):
	return static_file(filename, root="css")

@get('/images/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
        return static_file(filename, root='images')


run(host='0.0.0.0', port=18083, reloader=True)
