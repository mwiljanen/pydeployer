#!/usr/bin/env python

import MySQLdb
import pycurl
import cStringIO
from xml.dom import minidom
import datetime

#nexus_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/content/groups/public/com/ibsys/"

nexus_cache_log="/var/log/ib/deployer/nexus_cache.log"

fd = open(nexus_cache_log, 'a')

fd.write("Starting to cache Nexus Artifacts at " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\n" )

db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
curr = conn.cursor()

get_deploy_name_query = """SELECT DISTINCT `artifactId`,`groupId` FROM `artifacts`"""

curr.execute(get_deploy_name_query)
all_artifacts = curr.fetchall()

artifact_count = 0

for artifact_id in all_artifacts:
	try:
		artifact_count += 1
		c = pycurl.Curl()
		buf = cStringIO.StringIO()

		nexus_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/content/groups/public/" + artifact_id[1].replace('.', '/') + "/"


		print nexus_url  + artifact_id[0] + "/maven-metadata.xml"

		c.setopt(c.URL, nexus_url + artifact_id[0] + "/maven-metadata.xml")
		c.setopt(c.WRITEFUNCTION, buf.write)
		c.perform()

		try:
			xmldoc = minidom.parseString(buf.getvalue())
	
			item_list = xmldoc.getElementsByTagName('version')
			latest = xmldoc.getElementsByTagName('latest')
			last_updated =  xmldoc.getElementsByTagName('lastUpdated')

			versions = []
			for item in reversed(item_list):
				versions.append( item.childNodes[0].nodeValue )
		
			nexus_item_query = """REPLACE INTO nexus_metadata VALUES ( '%s', '%s', '%s', '%s', '%s', '' )""" % ( artifact_id[1], artifact_id[0], latest[0].firstChild.nodeValue, "|".join(versions), last_updated[0].firstChild.nodeValue )

			curr.execute(nexus_item_query)
			conn.commit()
		except:
			print "Something went wrong!"

	except MySQLdb.Error:
		print "Something went wrong!"


fd.write("Updated " + str(artifact_count) + " in Nexus Artifact cache.\n" )
fd.write("Completed caching of Nexus Artifacts at " + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "\n" )

fd.close()
