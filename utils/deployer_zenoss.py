#!/usr/bin/env python

from zenoss import Zenoss
import pycurl
import datetime
import time
import cStringIO

zenoss_states = { 'Maintenance': 300, 'Test': 400, 'Development': 500, 'QA': 600, 'Staging': 800, 'ProdTest': 900, 'Utility': 950, 'Production': 1000  }

def set_state(hostname="", state=""):
        zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
        zenoss.set_prod_state(hostname, zenoss_states[state] )

def get_state(hostname=""):
        zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
        return zenoss.find_device(hostname)['productionState']

def get_stats(hostname=""):
        zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
        return zenoss.find_device(hostname)

def set_maint_window(hostname="", username="", start_time="", end_time="", comment=""):
	host_stats = get_stats(hostname)

	start_datestamp = datetime.datetime.strptime(start_time, '%m/%d/%Y %H:%M')

	end_datestamp = datetime.datetime.strptime(end_time, '%m/%d/%Y %H:%M')

	duration = (end_datestamp - start_datestamp)

	c = pycurl.Curl()
	
	timestamp = time.strftime("%Y%m%d%H%M%S",time.localtime(time.time()))

	maint_name = username + "_" + timestamp + '_' + host_stats['name']


	#target_url = 'http://zenoss1-dal05.dal05.sl.ibsys.com/zport/dmd/Devices/Server/Linux/IB%20-%20Deployer/devices/devdeployer01-dal05.dal05.ib-util.com/devicedetail'
	add_maint_url = 'http://zenoss1-dal05.dal05.sl.ibsys.com' + host_stats['uid'].replace(' ', '%20') + '/manage_addMaintenanceWindow?newId=' + maint_name

	#c.setopt(pycurl.FOLLOWLOCATION, 1)
	c.setopt(pycurl.URL, add_maint_url)
	c.setopt(pycurl.USERPWD, "json:J50nJ50n")
	#c.setopt(c.VERBOSE, True)

	c.perform()
	
	set_maint_time_url = 'http://zenoss1-dal05.dal05.sl.ibsys.com' + host_stats['uid'].replace(' ', '%20') + '/maintenanceWindows/' + maint_name + '/manage_editMaintenanceWindow' + start_datestamp.strftime('?startDate=%m/%d/%Y&startHours=%H&startMinutes=%M') + '&durationDays=' + str(duration.days) + '&durationHours=' + str(int(duration.seconds / 3600)) + '&durationMinutes=' + str(int(duration.seconds - (duration.seconds / 3600) * 3600) / 60)

	#print set_maint_time_url
	#c.setopt(pycurl.FOLLOWLOCATION, 1)
	c.setopt(pycurl.URL, set_maint_time_url)
	c.setopt(pycurl.USERPWD, "json:J50nJ50n")
	#c.setopt(c.VERBOSE, True)

	c.perform()

	return "Set Maintenance Window with name: " + maint_name + " starting at " + str(start_datestamp) + " and ending at " + str(end_datestamp)


def get_maint_windows(hostname=""):
	host_stats = get_stats(hostname)


	c = pycurl.Curl()
	buff = cStringIO.StringIO()

	get_maint_win_url = 'http://zenoss1-dal05.dal05.sl.ibsys.com' + host_stats['uid'].replace(' ', '%20') + '/getMaintenanceWindows'
	c.setopt(pycurl.URL, get_maint_win_url)
	c.setopt(pycurl.USERPWD, "json:J50nJ50n")
	c.setopt(pycurl.WRITEFUNCTION, buff.write)

	c.perform()

	maint_list = []

	for maint_item in buff.getvalue().split(','):
		if ( 'MaintenanceWindow at ' in maint_item ):
			maint_list.append(maint_item.split('MaintenanceWindow at ')[1].split('>')[0])

	return maint_list

def delete_maint_windows(hostname=""):
	del_output = ''

	host_stats = get_stats(hostname)
	c = pycurl.Curl()

	for maint_item in get_maint_windows(hostname):
		del_output += "Deleting maint window: " + maint_item.split('/')[-1] + ".\n"
		del_maint_win_url = 'http://zenoss1-dal05.dal05.sl.ibsys.com' + host_stats['uid'].replace(' ', '%20') + '/manage_deleteMaintenanceWindow?maintenanceIds=' + maint_item.split('/')[-1]
		c.setopt(pycurl.URL, del_maint_win_url)
		c.setopt(pycurl.USERPWD, "json:J50nJ50n")
		c.perform()
		
	return del_output


		
