import pycurl
import cStringIO

class happy_proxy:
	def __init__(self, target_lb='', target_backend='', target_host=''):
		self.target_lb = target_lb
		self.target_backend = target_backend
		self.target_host = target_host


	def set_status( self, status, target_lb='', target_backend='', target_host='' ):
		num_backends = 0

		self.target_lb = target_lb
		self.target_backend = target_backend
		self.target_host = target_host


		self.conn = pycurl.Curl()
		self.buff = cStringIO.StringIO()

		self.conn.setopt(self.conn.URL, "http://" + self.target_lb + ":8000/haproxy")
		self.conn.setopt(self.conn.USERPWD, "haproxy:stats")
		
		self.conn.setopt(self.conn.WRITEFUNCTION, self.buff.write)
		self.conn.perform()

		backend_filter = '''<tr class="titre"><th class="pxname" width="10%"><a name="'''
		form_filter = '''<form action="/haproxy" method="post"><table class="tbl" width="100%">'''

		status_changed = False

		for line_get in self.buff.getvalue().split('\n'):
			if ( backend_filter in line_get ):

				backend_name = line_get[(line_get.find(backend_filter) + len(backend_filter) ):].split('''"''')[0]
				if (backend_name == self.target_backend):

					perform_task = '''s=%s&action=%s&b=%%23%d''' % ( self.target_host, status, num_backends + 1 )

					#print "Host " + self.target_host + " on LB " + backend_name + " is now " + status + "d"

					self.conn.setopt(self.conn.POSTFIELDS, perform_task)
					#self.conn.setopt(self.conn.WRITEFUNCTION, buf.write)
					self.conn.perform()
			
					status_changed = True
					
					break
				num_backends += 1

		return status_changed


	def get_status( self ):


		self.conn = pycurl.Curl()
		self.buff = cStringIO.StringIO()

		self.conn.setopt(self.conn.URL, "http://" + self.target_lb + ":8000/haproxy")
		self.conn.setopt(self.conn.USERPWD, "haproxy:stats")


		self.conn.setopt(self.conn.WRITEFUNCTION, self.buff.write)
		self.conn.perform()


		for line_get in self.buff.getvalue().split('\n'):
		        if( self.target_backend in line_get and self.target_host in line_get):
                		status_output = ""
				status = line_get.split('''<tr class="''')[1][0:7]
				if ( status == 'active3' ):
					status_output = "UP"
				if ( status == 'active2' ):
					status_output = "UP going DOWN"
				if ( status == 'active1' ):
					status_output = "DOWN going UP"
				if ( status == 'active0' ):
					status_output = "DOWN"
				if ( status == 'maintai' ):
					status_output = "MAINT"

				return status_output

	def find_front_end ( self, hostname='' ):
		self.conn = pycurl.Curl()
		self.buff = cStringIO.StringIO()

		self.conn.setopt(self.conn.URL, "http://" + self.target_lb + ":8000/haproxy")
		self.conn.setopt(self.conn.USERPWD, "haproxy:stats")

		self.conn.setopt(self.conn.WRITEFUNCTION, self.buff.write)
		self.conn.perform()

		for line_get in self.buff.getvalue().split('\n'):
			if( hostname in line_get):
				
				print line_get.split('''/%s"></a>''' % hostname )[0].split('''<a name="''')[1]

	def set_all_status ( self, status, target_lb, hostname='' ):
		self.conn = pycurl.Curl()
		self.buff = cStringIO.StringIO()

		self.conn.setopt(self.conn.URL, "http://" + target_lb + ":8000/haproxy")
		self.conn.setopt(self.conn.USERPWD, "haproxy:stats")

		self.conn.setopt(self.conn.WRITEFUNCTION, self.buff.write)
		self.conn.perform()
		
		lb_output = ''
		for line_get in self.buff.getvalue().split('\n'):
			if( hostname in line_get):
				tmp_backend = line_get.split('''/%s"></a>''' % hostname )[0].split('''<a name="''')[1]
				lb_output += status.capitalize() +"d host " + hostname + " with " + tmp_backend + " backend on " + target_lb + " load balancer.\n"
				self.set_status(status, target_lb, target_backend=tmp_backend, target_host=hostname )

		return lb_output
