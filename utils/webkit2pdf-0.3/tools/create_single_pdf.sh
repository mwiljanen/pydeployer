#!/bin/bash

if [ "$1" = "" ]; then
	echo usage: $0 path/to/pdfs output.pdf
	exit 1
fi
if [ ! -d "$1" ]; then
	echo $1 does not exist or is not a directory
	exit 1
fi
if [ "$2" = "" ]; then
	echo usage: $0 path/to/pdfs output.pdf
	exit 1
fi

cd "$1"
rm -rf create-pages
mkdir create-pages

#make single pages
for i in *.pdf; do pdftk $i burst output create-pages/$i.%02d.pdf; done

#remove empty pages
rm `find create-pages/ -size -1500c`

#create single file
cd - >/dev/null

pdftk "$1"/create-pages/*pdf cat output $2
