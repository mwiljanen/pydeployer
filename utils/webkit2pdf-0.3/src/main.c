/*
 * Webkit2pdf - Copyright (c) 2008-2009 Colin Leroy <colin@colino.net>
 * A batch exporter of arbitrary web pages to PDF.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA
 *
 */

#include <stdlib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <webkit/webkitwebview.h>
#include <webkit/webkitwebframe.h>
#include <webkit/webkitnetworkrequest.h>
#include <webkit/webkitwebsettings.h>
#include <poppler.h>
#define CUSTOM_PAPER_FILENAME ".gtk-custom-papers"

GtkPageSetup *page_setup = NULL;
GtkPrintSettings *print_settings = NULL;
GtkWidget *window = NULL;
GtkWidget *webview = NULL;
GtkToolButton *page_setup_btn = NULL;
GtkToolButton *browse_btn = NULL;
GtkToolButton *format_btn = NULL;
GtkToolButton *print_btn = NULL;
GtkToolButton *export_btn = NULL;
GtkToolButton *quit_btn = NULL;
GtkWidget *statusbar = NULL;
GtkWidget *label_paper_size, *label_output_dir, *label_file_format;
gchar **urls;
gint cur_url = 0;
gint total_urls = 0;
gchar *output_dir = NULL;
gchar *output_file_format = NULL;
GtkWidget *pdf;
PopplerDocument *pdf_doc = NULL;
gboolean is_export = FALSE;
gboolean gui_shown = FALSE;
const gchar *papersize = NULL;
gboolean enable_scripts = TRUE;
gboolean print_backgrounds = FALSE;

#ifndef POPPLER_WITH_GDK
#include <goo/gtypes.h>

static void load_done(WebKitWebFrame *frame, WebKitLoadStatus status);

static void
copy_cairo_surface_to_pixbuf (cairo_surface_t *surface,
			      GdkPixbuf       *pixbuf)
{
  int cairo_width, cairo_height, cairo_rowstride;
  unsigned char *pixbuf_data, *dst, *cairo_data;
  int pixbuf_rowstride, pixbuf_n_channels;
  unsigned int *src;
  int x, y;

  cairo_width = cairo_image_surface_get_width (surface);
  cairo_height = cairo_image_surface_get_height (surface);
  cairo_rowstride = cairo_image_surface_get_stride (surface);
  cairo_data = cairo_image_surface_get_data (surface);

  pixbuf_data = gdk_pixbuf_get_pixels (pixbuf);
  pixbuf_rowstride = gdk_pixbuf_get_rowstride (pixbuf);
  pixbuf_n_channels = gdk_pixbuf_get_n_channels (pixbuf);

  if (cairo_width > gdk_pixbuf_get_width (pixbuf))
    cairo_width = gdk_pixbuf_get_width (pixbuf);
  if (cairo_height > gdk_pixbuf_get_height (pixbuf))
    cairo_height = gdk_pixbuf_get_height (pixbuf);
  for (y = 0; y < cairo_height; y++)
    {
      src = (unsigned int *) (cairo_data + y * cairo_rowstride);
      dst = pixbuf_data + y * pixbuf_rowstride;
      for (x = 0; x < cairo_width; x++) 
	{
	  dst[0] = (*src >> 16) & 0xff;
	  dst[1] = (*src >> 8) & 0xff; 
	  dst[2] = (*src >> 0) & 0xff;
	  if (pixbuf_n_channels == 4)
	      dst[3] = (*src >> 24) & 0xff;
	  dst += pixbuf_n_channels;
	  src++;
	}
    }
}

static void
_poppler_page_render_to_pixbuf (PopplerPage *page,
				int src_x, int src_y,
				int src_width, int src_height,
				double scale,
				int rotation,
				GBool printing,
				GdkPixbuf *pixbuf)
{
  cairo_t *cr;
  cairo_surface_t *surface;

  surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32,
					src_width, src_height);
  cr = cairo_create (surface);
  cairo_save (cr);
  switch (rotation) {
  case 90:
	  cairo_translate (cr, src_x + src_width, -src_y);
	  break;
  case 180:
	  cairo_translate (cr, src_x + src_width, src_y + src_height);
	  break;
  case 270:
	  cairo_translate (cr, -src_x, src_y + src_height);
	  break;
  default:
	  cairo_translate (cr, -src_x, -src_y);
  }

  if (scale != 1.0)
	  cairo_scale (cr, scale, scale);

  if (rotation != 0)
	  cairo_rotate (cr, rotation * G_PI / 180.0);

  if (printing)
	  poppler_page_render_for_printing (page, cr);
  else
	  poppler_page_render (page, cr);
  cairo_restore (cr);

  cairo_set_operator (cr, CAIRO_OPERATOR_DEST_OVER);
  cairo_set_source_rgb (cr, 1., 1., 1.);
  cairo_paint (cr);

  cairo_destroy (cr);

  copy_cairo_surface_to_pixbuf (surface, pixbuf);
  cairo_surface_destroy (surface);
}

/**
 * poppler_page_render_to_pixbuf:
 * @page: the page to render from
 * @src_x: x coordinate of upper left corner  
 * @src_y: y coordinate of upper left corner  
 * @src_width: width of rectangle to render  
 * @src_height: height of rectangle to render
 * @scale: scale specified as pixels per point
 * @rotation: rotate the document by the specified degree
 * @pixbuf: pixbuf to render into
 *
 * First scale the document to match the specified pixels per point,
 * then render the rectangle given by the upper left corner at
 * (src_x, src_y) and src_width and src_height.
 * This function is for rendering a page that will be displayed.
 * If you want to render a page that will be printed use
 * poppler_page_render_to_pixbuf_for_printing() instead
 *
 * Deprecated: 0.16
 **/
void
poppler_page_render_to_pixbuf (PopplerPage *page,
			       int src_x, int src_y,
			       int src_width, int src_height,
			       double scale,
			       int rotation,
			       GdkPixbuf *pixbuf)
{
  g_return_if_fail (POPPLER_IS_PAGE (page));
  g_return_if_fail (scale > 0.0);
  g_return_if_fail (pixbuf != NULL);

  _poppler_page_render_to_pixbuf (page, src_x, src_y,
				  src_width, src_height,
				  scale, rotation,
				  gFalse,
				  pixbuf);
}
#endif

static WebKitNavigationResponse navigation_requested(WebKitWebView *view, 
		WebKitWebFrame *frame, 
		WebKitNetworkRequest *netreq, 
		gpointer data)
{
	return WEBKIT_NAVIGATION_RESPONSE_ACCEPT;
}

static void load_status_changed(GObject *object, GParamSpec *pspec, gpointer data)
{
	WebKitWebView *view = WEBKIT_WEB_VIEW(object);
	WebKitLoadStatus status = webkit_web_view_get_load_status(view);

	if (status == WEBKIT_LOAD_FINISHED || status == WEBKIT_LOAD_FAILED)
		load_done(webkit_web_view_get_main_frame(view), status);
}

static void load_resource (WebKitWebView     *web_view,
                           WebKitWebFrame    *web_frame,
                           WebKitWebResource *web_resource,
                           gpointer           user_data)
{
	if(!gui_shown) {
		printf(".");
		fflush(stdout);
	}
}

static void load_done(WebKitWebFrame *frame, WebKitLoadStatus status)
{
	gchar *format = g_strdup_printf(output_file_format,cur_url);
	gchar *filename = g_strconcat(output_dir?output_dir:g_get_current_dir(),
				G_DIR_SEPARATOR_S,format, NULL);
	gchar *uri;
	gchar *text;
	GError *error = NULL;
	PopplerPage *page = NULL;
	GdkPixbuf *pb;

	if(!gui_shown)
		printf("\n");

	if (status == WEBKIT_LOAD_FINISHED && urls && urls[cur_url]) {
		GtkPrintOperation *operation = gtk_print_operation_new();
		if (print_settings)
			gtk_print_operation_set_print_settings(operation, print_settings);
		gtk_statusbar_pop(GTK_STATUSBAR(statusbar), 
			gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "status"));
		text = g_strdup_printf(_("(%d/%d) Printing %s to %s..."), cur_url+1, total_urls, urls[cur_url], filename);
		gtk_statusbar_push(GTK_STATUSBAR(statusbar), 
			gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "status"),
			text);
		if (!gui_shown)
			printf("  %s\n", text);
		g_free(text);
		/* set the zoom back to 1 before printing */
		webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(webview), 1.0);
		if (is_export) {
			/* we want to write to PDF without showing any dialog to the user */
			gtk_print_operation_set_export_filename(operation, filename);
			webkit_web_frame_print_full (frame, operation, GTK_PRINT_OPERATION_ACTION_EXPORT, &error);
		} else {
			/* we want to print interactively so the user can change settings,
			 * and either print to printer or to PDF according to the user's
			 * choice */
			webkit_web_frame_print_full(frame, operation, GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG, &error);
			if (!error) {
				g_object_unref(print_settings);
				print_settings = gtk_print_operation_get_print_settings(operation);
			}
		}
		g_object_unref(operation);
		/* unzoom for the preview */
		webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(webview), 0.4);

		if (error) {
			/* printing failed for some reason */
			printf("error: %s\n", error->message);
			g_error_free(error);
			error = NULL;
			return;
		}

		if (is_export && gui_shown) {
			if (pdf_doc)
				g_object_unref(pdf_doc);
			uri = g_filename_to_uri(filename, NULL, &error);
			if (error) {
				printf("error: %s\n", error->message);
				g_error_free(error);
				error = NULL;
			}
			pdf_doc = poppler_document_new_from_file(uri, NULL, &error);
			g_free(uri);
			if (!error) {
				gdouble width, height;
				GtkAllocation *allocation;
				double xratio, yratio, zoom=1.0;
				page = poppler_document_get_page(pdf_doc, 0);

				poppler_page_get_size(page, &width, &height);
				allocation = &(pdf->allocation);
				xratio = allocation->width / width;
				yratio = allocation->height / height;
				if (xratio >= yratio) {
					zoom = yratio;
				} else {
					zoom = xratio;
				}
				pb = gdk_pixbuf_new(GDK_COLORSPACE_RGB, 
					FALSE, 8, 
					(int)(width * zoom), 
					(int)(height * zoom));	
				poppler_page_render_to_pixbuf(page, 0, 0, 
					(int)(width * zoom), 
					(int)(height * zoom), 
					zoom, 0, pb);

				gtk_image_set_from_pixbuf(GTK_IMAGE(pdf), pb);
				g_object_unref(G_OBJECT(pb));
				g_object_unref(G_OBJECT(page));
			} else {
				g_error_free(error);
			}
		}
		
	} else if (status == WEBKIT_LOAD_FAILED)
		printf("  Error\n");

	g_free(filename);
	cur_url++;
	
	gtk_statusbar_pop(GTK_STATUSBAR(statusbar), 
		gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "status"));
	if (urls && urls[cur_url]) {
		g_strstrip(urls[cur_url]);
		if (*urls[cur_url]) {
			text = g_strdup_printf(_("(%d/%d) Loading %s..."), cur_url+1, total_urls, urls[cur_url]);
			gtk_statusbar_push(GTK_STATUSBAR(statusbar), 
				gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "status"),
				text);
			if (!gui_shown) {
				printf(" %s", text);
				fflush(stdout);
			}
			g_free(text);
			webkit_web_view_open(WEBKIT_WEB_VIEW(webview), urls[cur_url]);
			return;
		}
	}
	gtk_widget_set_sensitive(GTK_WIDGET(print_btn), TRUE);	
	gtk_widget_set_sensitive(GTK_WIDGET(export_btn), TRUE);	
	gtk_widget_set_sensitive(GTK_WIDGET(page_setup_btn), TRUE);
	gtk_widget_set_sensitive(GTK_WIDGET(browse_btn), TRUE);	
	gtk_widget_set_sensitive(GTK_WIDGET(format_btn), TRUE);	
	g_strfreev(urls);
	urls = NULL;
	text = g_strdup_printf(_("All done."));
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), 
		gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "status"),
		text);
	if (!gui_shown) {
		printf("%s\n", text);
		gtk_main_quit();
	}
	g_free(text);
}

static void update_labels(void)
{
	GtkPaperSize *size = gtk_page_setup_get_paper_size(page_setup);
	gchar *tmp;
	
	tmp = g_strdup_printf(_("Paper size: %s"), gtk_paper_size_get_name(size));
	gtk_label_set_text(GTK_LABEL(label_paper_size), tmp);
	g_free(tmp);

	tmp = g_strdup_printf(_("Output dir: %s"), output_dir?output_dir:".");
	gtk_label_set_text(GTK_LABEL(label_output_dir), tmp);
	g_free(tmp);

	tmp = g_strdup_printf(_("Name format: %s"), output_file_format);
	gtk_label_set_text(GTK_LABEL(label_file_format), tmp);
	g_free(tmp);
}

static void browse_cb(GtkWidget *widget, gpointer data)
{
	GtkWidget *chooser;
	chooser = gtk_file_chooser_dialog_new
				(_("Choose output directory"), NULL, 
				GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
				GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
				GTK_STOCK_OPEN, GTK_RESPONSE_ACCEPT, 
				NULL);
	if (output_dir)
		gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(chooser), output_dir);

	if (gtk_dialog_run (GTK_DIALOG (chooser)) == GTK_RESPONSE_ACCEPT) {
		GSList *slist = gtk_file_chooser_get_filenames (GTK_FILE_CHOOSER (chooser));
		if (slist) {
			g_free(output_dir);
			output_dir = g_strdup(slist->data);
		}
		g_slist_free(slist);
	}
	gtk_widget_destroy (chooser);
	printf("saving to %s\n", output_dir);
	update_labels();
}

static gchar *get_string(const char *text1, const char *text2, const gchar *value)
{
	GtkWidget *dialog = NULL;
	GtkWidget *input = NULL;
	GtkWidget *label = NULL;
	GtkWidget *hbox1 = NULL;
	GtkWidget *hbox2 = NULL;

	gchar *result = FALSE;

	dialog = gtk_dialog_new_with_buttons(text1,
			NULL, GTK_DIALOG_MODAL, GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);

	input = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(input), value);
	hbox1 = gtk_hbox_new(FALSE,6);
	hbox2 = gtk_hbox_new(FALSE,6);

	label = gtk_image_new_from_stock(GTK_STOCK_DIALOG_QUESTION, GTK_ICON_SIZE_DIALOG);
	gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, 0);
	label = gtk_label_new(text1);
	gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox1, FALSE, FALSE, 0);
	label = gtk_label_new(text2);
	gtk_box_pack_end(GTK_BOX(hbox2), input, FALSE, FALSE, 0);
	gtk_box_pack_end(GTK_BOX(hbox2), label, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(GTK_DIALOG(dialog)->vbox), hbox2, FALSE, FALSE, 0);

	gtk_widget_show_all(GTK_DIALOG(dialog)->vbox);
	gtk_dialog_run(GTK_DIALOG(dialog));

	result = gtk_editable_get_chars(GTK_EDITABLE(input), 0, -1);

	gtk_widget_destroy(dialog);

	return result;
}

static gboolean check_format(const gchar *format)
{
	const gchar *percent;
	
	percent = strchr(format, '%');
	
	if (!percent)
		return FALSE;
	
	if (percent != strrchr(format, '%'))
		return FALSE;
	
	while (*percent != '\0') {
		percent++;
		if (*percent == '\0')
			return FALSE;
		else if (*percent >= '0' && *percent <= '9')
			continue;
		else if (*percent == 'd')
			return TRUE;
		else
			return FALSE;
	}
	return FALSE;
}

static void format_cb(GtkWidget *widget, gpointer data)
{
	gchar *new_format = get_string(_("Name format"), 
				_("Please enter a new format for output filenames."),
				output_file_format);
	if (new_format && check_format(new_format)) {
		g_free(output_file_format);
		output_file_format = new_format;
		update_labels();
	}
}

static void page_setup_cb(GtkWidget   *widget, gpointer data)
{
	if (!page_setup)
		page_setup = gtk_page_setup_new();
	if (!print_settings)
		print_settings = gtk_print_settings_new();
	
	page_setup = gtk_print_run_page_setup_dialog(GTK_WINDOW(window), page_setup, print_settings);
	printf("got page setup (%s)\n", gtk_paper_size_get_name(
  		gtk_page_setup_get_paper_size(page_setup)));
	gtk_print_settings_set_paper_size(print_settings, gtk_page_setup_get_paper_size(page_setup));
	update_labels();
}

static void do_print(GtkTextView *textview)
{
	GtkTextBuffer *buffer = GTK_TEXT_BUFFER(
			gtk_text_view_get_buffer(textview));
	GtkTextIter start, end;
	gchar *fetch;
	gchar *text = NULL;
	gchar **count = NULL;

	gtk_statusbar_pop(GTK_STATUSBAR(statusbar), 
		gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "status"));


	gtk_text_buffer_get_start_iter(buffer, &start);
	gtk_text_buffer_get_end_iter(buffer, &end);
	
	fetch = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
	urls = g_strsplit(fetch, "\n", -1);
	
	count = urls;
	total_urls = 0;
	while (*count) { 
		total_urls++; 
		count++; 
	}

	g_free(fetch);
	
	cur_url = 0;

	if (urls == NULL || urls[cur_url] == NULL)
		return;

	gtk_widget_set_sensitive(GTK_WIDGET(print_btn), FALSE);	
	gtk_widget_set_sensitive(GTK_WIDGET(export_btn), FALSE);	
	gtk_widget_set_sensitive(GTK_WIDGET(page_setup_btn), FALSE);	
	gtk_widget_set_sensitive(GTK_WIDGET(browse_btn), FALSE);	
	gtk_widget_set_sensitive(GTK_WIDGET(format_btn), FALSE);	

	text = g_strdup_printf(_("(%d/%d) Loading %s..."), cur_url+1, total_urls, urls[cur_url]);
	gtk_statusbar_push(GTK_STATUSBAR(statusbar), 
		gtk_statusbar_get_context_id(GTK_STATUSBAR(statusbar), "status"),
		text);
	if (!gui_shown) {
		printf(" %s", text);
		fflush(stdout);
	}
	g_free(text);
	webkit_web_view_open(WEBKIT_WEB_VIEW(webview), urls[cur_url]);
}

static void export_cb(GtkWidget   *widget, gpointer data)
{
	is_export = TRUE;
	do_print(GTK_TEXT_VIEW(data));
}

static void print_cb(GtkWidget   *widget, gpointer data)
{
	is_export = FALSE;
	do_print(GTK_TEXT_VIEW(data));
}

static void quit_cb(GtkWidget   *widget, gpointer data)
{
	gtk_main_quit();
}

static void print_paper_sizes(void)
{
	GList *list = gtk_paper_size_get_paper_sizes(TRUE);
	GList *cur;
	for (cur = list; cur; cur = cur->next) {
		GtkPaperSize *size = (GtkPaperSize *)cur->data;
		printf("  %s\n", gtk_paper_size_get_name(size));
	}
	g_list_free(list);
}

static void load_size(void)
{
	GList *list = gtk_paper_size_get_paper_sizes(TRUE);
	GList *cur;
	GtkPaperSize *size = NULL;
	for (cur = list; cur; cur = cur->next) {
		size = (GtkPaperSize *)cur->data;
		if (!strcmp(gtk_paper_size_get_name(size), papersize)) {
			break;
		} else {
			size = NULL;
		}
	}
	if (size) {
		page_setup = gtk_page_setup_new();
		gtk_page_setup_set_paper_size(page_setup, size);
	} else {
		printf("no paper matches '%s'\n", papersize);
		exit(0);
	}
	printf ("Using paper %s (%.02fx%.02f)\n", gtk_paper_size_get_name(size),
			gtk_page_setup_get_paper_height(page_setup, GTK_UNIT_MM),
			gtk_page_setup_get_paper_width(page_setup, GTK_UNIT_MM));

	if (!print_settings)
		print_settings = gtk_print_settings_new();
	
	gtk_print_settings_set_paper_size(print_settings, size);

}

static void usage(void)
{
	GtkPaperSize *size = gtk_page_setup_get_paper_size(page_setup);
	printf("\n");
	printf("Usage: webkit2pdf [options] [URLs]\n");
	printf("\n");
	printf("Options:\n");
	printf("	-h 				this help\n");
	printf("	--help 				this help\n");
	printf("	-s help				list available paper sizes\n");
	printf("	-o output_dir			set output dir 		(default: .)\n");
	printf("	-f format			set name format 	(default: %%04d.pdf)\n");
	printf("	-s size				use paper size 		(default: %s)\n", 
		gtk_paper_size_get_name(size));
	printf("	-j				disable Javascript 	(default: enable)\n");
	printf("	-b				print background colors	(default: don't print)\n");
	printf("\n");
	printf("No options and no URLs: Use GUI\n");
}

void apply_web_view_settings(void) {
	WebKitWebSettings *settings = webkit_web_view_get_settings(WEBKIT_WEB_VIEW(webview));
	g_object_set (settings,"print-backgrounds", print_backgrounds, NULL);
	g_object_set (settings,"enable-plugins", FALSE, NULL);
	g_object_set (settings,"enable-scripts", enable_scripts, NULL);
}

int main(int argc, char *argv[])
{
	GtkWidget * hbox = NULL;
	GtkWidget * vbox_text = NULL;
	GtkWidget * vbox_view = NULL;
	GtkWidget * vbox_pdf = NULL;
	GtkWidget * vbox = NULL;
	GtkTextView *text = NULL;
	GtkWidget *toolbar = NULL;
	GtkWidget *label;
	GtkWidget *scrolledwin;

	bindtextdomain(PACKAGE, LOCALEDIR );
        bind_textdomain_codeset (PACKAGE, "UTF-8");
	textdomain(PACKAGE);

#if !GLIB_CHECK_VERSION(2,32,0)
	g_thread_init(NULL);
#endif

	gtk_init(&argc, &argv);
	window =gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request(GTK_WIDGET(window), 800, 600);
	webview =webkit_web_view_new();
	hbox = gtk_hbox_new(TRUE, 6);
	vbox = gtk_vbox_new(FALSE, 6);
	vbox_text = gtk_vbox_new(FALSE, 6);
	vbox_view = gtk_vbox_new(FALSE, 6);
	vbox_pdf = gtk_vbox_new(FALSE, 6);
	text = GTK_TEXT_VIEW(gtk_text_view_new());
	toolbar = gtk_toolbar_new();
	
	gtk_toolbar_set_style(GTK_TOOLBAR(toolbar), GTK_TOOLBAR_BOTH);
	page_setup = gtk_page_setup_new();

	label = gtk_label_new(_("URLs to print:"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);

	page_setup_btn = GTK_TOOL_BUTTON(gtk_tool_button_new_from_stock(GTK_STOCK_PAGE_SETUP));
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(page_setup_btn), -1);
	g_signal_connect (G_OBJECT(page_setup_btn), "clicked", G_CALLBACK(page_setup_cb), NULL);

	browse_btn = GTK_TOOL_BUTTON(gtk_tool_button_new_from_stock(GTK_STOCK_OPEN));
	gtk_tool_button_set_label(browse_btn, _("Output dir"));
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(browse_btn), -1);
	g_signal_connect (G_OBJECT(browse_btn), "clicked", G_CALLBACK(browse_cb), NULL);

	format_btn = GTK_TOOL_BUTTON(gtk_tool_button_new_from_stock(GTK_STOCK_DND_MULTIPLE));
	gtk_tool_button_set_label(format_btn, _("Name format"));
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(format_btn), -1);
	g_signal_connect (G_OBJECT(format_btn), "clicked", G_CALLBACK(format_cb), NULL);

	export_btn = GTK_TOOL_BUTTON(gtk_tool_button_new_from_stock(GTK_STOCK_PRINT));
	gtk_tool_button_set_label(export_btn, _("Export PDF"));
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(export_btn), -1);
	g_signal_connect (G_OBJECT(export_btn), "clicked", G_CALLBACK(export_cb), text);

	print_btn = GTK_TOOL_BUTTON(gtk_tool_button_new_from_stock(GTK_STOCK_PRINT));
	gtk_tool_button_set_label(print_btn, _("Print"));
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(print_btn), -1);
	g_signal_connect (G_OBJECT(print_btn), "clicked", G_CALLBACK(print_cb), text);

	quit_btn = GTK_TOOL_BUTTON(gtk_tool_button_new_from_stock(GTK_STOCK_QUIT));
	gtk_toolbar_insert(GTK_TOOLBAR(toolbar), GTK_TOOL_ITEM(quit_btn), -1);
	g_signal_connect (G_OBJECT(quit_btn), "clicked", G_CALLBACK(quit_cb), NULL);

	gtk_container_add(GTK_CONTAINER(window), vbox);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 2);
	gtk_box_pack_start(GTK_BOX(vbox), toolbar, FALSE, FALSE, 2);
	gtk_box_pack_start(GTK_BOX(vbox_text), label, FALSE, FALSE, 2);
	scrolledwin = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrolledwin),
				       GTK_POLICY_AUTOMATIC,
				       GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type(GTK_SCROLLED_WINDOW(scrolledwin),
					    GTK_SHADOW_IN);
	gtk_container_add(GTK_CONTAINER(scrolledwin), GTK_WIDGET(text));
	gtk_box_pack_start(GTK_BOX(vbox_text), GTK_WIDGET(scrolledwin), TRUE, TRUE, 2);
	gtk_box_pack_start(GTK_BOX(hbox), vbox_text, TRUE, TRUE, 2);
	
	
	label = gtk_label_new(_("URL preview:"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_box_pack_start(GTK_BOX(vbox_view), label, FALSE, FALSE, 2);
	gtk_box_pack_start(GTK_BOX(vbox_view), GTK_WIDGET(webview), TRUE, TRUE, 2);
	gtk_box_pack_start(GTK_BOX(hbox), vbox_view, TRUE, TRUE, 2);
	
	pdf = gtk_image_new();
	label = gtk_label_new(_("Export preview:"));
	gtk_misc_set_alignment(GTK_MISC(label), 0, 0.5);
	gtk_box_pack_start(GTK_BOX(vbox_pdf), label, FALSE, FALSE, 2);
	gtk_box_pack_start(GTK_BOX(vbox_pdf), GTK_WIDGET(pdf), TRUE, TRUE, 2);
	
	label_paper_size = gtk_label_new((""));
	gtk_misc_set_alignment(GTK_MISC(label_paper_size), 0, 0.5);
	
	label_output_dir = gtk_label_new((""));
	gtk_misc_set_alignment(GTK_MISC(label_output_dir), 0, 0.5);
	
	label_file_format = gtk_label_new((":"));
	gtk_misc_set_alignment(GTK_MISC(label_file_format), 0, 0.5);
	
	gtk_box_pack_start(GTK_BOX(vbox_pdf), label_paper_size, FALSE, FALSE, 2);
	gtk_box_pack_start(GTK_BOX(vbox_pdf), label_output_dir, FALSE, FALSE, 2);
	gtk_box_pack_start(GTK_BOX(vbox_pdf), label_file_format, FALSE, FALSE, 2);
	
	gtk_box_pack_start(GTK_BOX(hbox), vbox_pdf, TRUE, TRUE, 2);
	
	statusbar = gtk_statusbar_new();
	gtk_box_pack_start(GTK_BOX(vbox), statusbar, FALSE, FALSE, 2);
	
	webkit_web_view_set_zoom_level(WEBKIT_WEB_VIEW(webview), 0.4);

	g_object_set (webview,"full-content-zoom", TRUE, NULL);

	g_signal_connect(webview, "navigation-requested",
					 G_CALLBACK(navigation_requested),
					 (gpointer *) NULL);
	g_signal_connect(window, "delete-event",
					 G_CALLBACK(quit_cb),
					 (gpointer *) NULL);

	g_signal_connect(webview, "resource-load-finished", G_CALLBACK(load_resource),NULL);
	g_signal_connect (webview, "notify::load-status",
		G_CALLBACK (load_status_changed), NULL);


	output_file_format = g_strdup("%04d.pdf");
	if (argc < 2) {
		/* apply web view default settings */
		apply_web_view_settings();
		gtk_widget_show_all(window);
		update_labels();
		gui_shown = TRUE;
	} else {
		gchar *url = NULL;
		int i = 1;
		gboolean has_url = FALSE;
		gtk_widget_realize(window);
		for (; i < argc; i++) {
			if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
				usage();
				exit(0);
			} else if (!strcmp(argv[i], "-s") && i+1 < argc) {
				papersize = argv[i+1];
				if (!strcmp(papersize, "help")) {
					print_paper_sizes();
					exit(0);
				}
				i++;
			} else if (!strcmp(argv[i], "-o") && i+1 < argc) {
				output_dir = g_strdup(argv[i+1]);
				i++;
			} else if (!strcmp(argv[i], "-f") && i+1 < argc) {
				g_free(output_file_format);
				output_file_format = g_strdup(argv[i+1]);
				if (!check_format(output_file_format)) {
					printf("Invalid format '%s'.\nFormat must contain exactly one %%d (with optional flags).\n",
						output_file_format);
					exit(1);
				}
				i++;
			} else if (!strcmp(argv[i], "-j")) {
				enable_scripts = FALSE;
			} else if (!strcmp(argv[i], "-b")) {
				print_backgrounds = TRUE;
			} else {
				gchar *tmp = g_strconcat(url?url:"", url?"\n":"",argv[i], NULL);
				g_free(url);
				url = tmp;
				has_url = TRUE;
				gtk_text_buffer_set_text(GTK_TEXT_BUFFER(
					gtk_text_view_get_buffer(GTK_TEXT_VIEW(
						text))), url, -1);
			}
		}
		if (papersize) {
			load_size();
		}
		if (!has_url) {
			gtk_widget_show_all(window);
			update_labels();
			gui_shown = TRUE;
		} else {
			is_export = TRUE;
			do_print(GTK_TEXT_VIEW(text));
		}

		/* set webkit view settings after reading command-line options */
		apply_web_view_settings();
	}
	gtk_main();	
	exit(0);
}
