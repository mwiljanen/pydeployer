import multiprocessing
import random

def worker(i):
    for cnt in range(0,16000000):
	temp = 0 #print "Process " + str(i) + "working " + str(cnt)
    print i,'done'


if __name__ == "__main__":

    procs = []
    for i in range(4):
        t = multiprocessing.Process(target = worker, args=(i,))
        procs.append(t)
        t.start()


    for t_item in procs:
        t_item.join()


    print 'All the processes have finished.'
