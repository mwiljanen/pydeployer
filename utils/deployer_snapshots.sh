#!/bin/bash

export DISPLAY=:99.0

if [ -n "$(pidof Xvfb)" ]
then
	wkhtmltopdf http://www.kxly.mmm.beta.ib-prod.com -B 0 -L 0 -R 0 -T 0 --page-height 3000mm --page-width 250mm --zoom 2.0 - |  \
	convert -density 150 pdf:- -background white -splice 0x1  -background black -splice 0x1           -trim  +repage -chop 0x1 -adaptive-resize 1024x  test_out.png

else
	Xvfb :99 -screen 1 1024x768x16 &
	#Xvfb :99 -screen 0 800x600x16 &
fi
