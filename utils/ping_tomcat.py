#!/usr/bin/env python

import pycurl
import cStringIO

def ping_tomcat(hostname):

        try:
                conn = pycurl.Curl()
                buff = cStringIO.StringIO()
                conn.setopt(conn.URL, "http://" + hostname + ":8080")
                conn.setopt(conn.WRITEFUNCTION, buff.write)
                conn.setopt(conn.TIMEOUT, 5)
                conn.perform()
                return True

        except: 
                return False


if ( ping_tomcat('qacaebeta01-dal05b.dal05b.ib-qa.com') ):
	print "Host up"

