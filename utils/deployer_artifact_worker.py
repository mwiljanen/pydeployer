#!/usr/bin/env python
'''
Application: Deployer Worker Service
Version 2.6.67
Name: Matthew Wiljanen
Date: 2014-09-02

Description:  Based off of Gearman version of Deployer, but this uses a simple loop to check for tasks inside the
processes table in the deployer database.  If one exists with a status of 1 it means it is a new job with job file
ready for processing.  Keeping all tasks in python on a single executing thread but making multiprocess worker threads
as they come in.
'''

import MySQLdb
import pycurl
import cStringIO
from xml.dom import minidom
import datetime
import time
import pickle
import os
import multiprocessing
import random
from zenoss import Zenoss
import ast
import re
import paramiko
import deployer_zenoss as zenoss
from happy_proxy import happy_proxy


db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

deployer_worker_path = "/tmp/deployer/"

deployer_log_path = "/var/log/ib/deployer"

deployer_config_path = "/etc/ibconf/deployer/"

nexus_rest_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/service/local/repo_groups/public/content/"

configs = {}

configs['deployer'] = open(deployer_config_path + "deployer.ini" , "r").read()
configs['services'] = open(deployer_config_path + "services.ini" , "r").read()



zenoss_states = { 'Maintenance': 300, 'Test': 400, 'Development': 500, 'QA': 600, 'Staging': 800, 'ProdTest': 900, 'Production': 1000  }

jobs = {}


def remote_command(fqdn, r_command=""):
	sshconf = {}
	sshconf.update( get_config_values(configs['services'], "ssh.username") )
	sshconf.update( get_config_values(configs['services'], "ssh.publicKey") )
	sshconf.update( get_config_values(configs['services'], "ssh.privateKey") )

	#print sshconf

	try:
		ssh = paramiko.SSHClient()
		paramiko.util.log_to_file("ssh.log")
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(hostname=fqdn, username=sshconf['ssh.username'], key_filename=sshconf['ssh.privateKey'])
		#print r_command
		ssh.exec_command(r_command)[1].read()
		ssh.close()
	except:
		print "SSH Failed to login to " + fqdn


def get_latest_version(artifact_name, artifact_env):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	get_nexus_query = """select nexus_metadata.* from nexus_metadata inner join artifacts on nexus_metadata.artifactId = artifacts.artifactId where artifacts.name = '%s';""" % ( artifact_name )
	curr.execute(get_nexus_query)
	nexus_meta_data = curr.fetchone()
	versions = []
	for item in nexus_meta_data[3].split('|'):
		if ( (artifact_env == 'qa' or artifact_env == 'prod' or artifact_env == 'stage') and 'SNAPSHOT' not in item):
			versions.append( item )
		elif ( artifact_env == 'dev' ):
			versions.append( item )
	return list(versions)[0]
	


def set_zenoss_state(hostname="", state=""):
	zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
	zenoss.set_prod_state(hostname, zenoss_states[state] )

def get_zenoss_state(hostname=""):
	zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
	return zenoss.find_device(hostname)['productionState']

def get_config_values(tmp_config="", get_value=""):
	values = {}

        for tmp_line in tmp_config.split('\n'):
                if ( get_value in tmp_line and tmp_line[0] != ';' ):
			values[tmp_line.split(' = ')[0]] = tmp_line.split(' = ')[1].replace('"','')

	return values


def parse_server_data_2(server_data):
	srv_list = []
	
	for srv_item in ''.join(server_data.split()).split(','):
		if ( '.eq.' in srv_item ):
	        	srv_ib_to_str = "{"
		        for srv_sub_item in srv_item.split('|'):
				if ( '.eq.' in srv_sub_item ):
		        		srv_ib_to_str += """'%s': '%s',""" % ( srv_sub_item.split('.eq.')[0], srv_sub_item.split('.eq.')[1])

		        srv_ib_to_str += "}"
        		srv_list.append(ast.literal_eval(srv_ib_to_str))

	#print len(srv_list)
	if ( len(srv_list) < 1 ):
		srv_list.append(ast.literal_eval("{ 'hostname': 'none' }"))

	return srv_list


def parse_server_data(server_data):
	server_maps = []
	for env_line in server_data.split(','):
		for ib_env_line in env_line.split('|'):
			server_maps.append( { ib_env_line.split('.eq.')[0]: ib_env_line.split('.eq.')[1] } )

	return server_maps

def get_fqdn(server_data):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()
	server_hosts = []

        server_query = []

	blacklisted_subtypes = get_config_values(configs['deployer'], "skipSiteLoadingHosts")
        for server_item in parse_server_data_2(server_data):
                #print server_item
                
                for server_item_part in server_item:
                        server_query.append("""%s = '%s'""" % ( server_item_part, server_item[server_item_part] ) )
        
                #print ' AND '.join(server_query)

		ib_query = """select hostname,domain from hostinfo where %s""" % ( ' AND '.join(server_query) ) 

		#print ib_query

		curr.execute(ib_query)
	
		for res_item in curr.fetchall():
			server_hosts.append( res_item[0] + "." + res_item[1] )

	return server_hosts


def get_hostinfo(server_data):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()
	server_hosts = []

	blacklisted_subtypes = get_config_values(configs['deployer'], "skipSiteLoadingHosts")

	for server_item in parse_server_data_2(server_data):

		#print server_item
		
		server_query = []

		for server_item_part in server_item:
			server_query.append("""%s = '%s'""" % ( server_item_part, server_item[server_item_part] ) )
	
		#print ' AND '.join(server_query)

		ib_query = """select hostname,ib_datacenter,ib_env,ib_dc,ib_dc_room,ib_partner_group,ib_system_subtype,ib_system_type,domain from hostinfo where %s""" % ( ' AND '.join(server_query) ) 

		#print ib_query

		if ( curr.execute(ib_query) ):
			for res_item in curr.fetchall():
				if (res_item[6] not in blacklisted_subtypes ):
					server_hosts.append( { 'hostname': res_item[0],
					'ib_datacenter': res_item[1],
					'ib_env': res_item[2],
					'ib_dc': res_item[3],
					'ib_dc_room': res_item[4],
					'ib_partner_group': res_item[5],
					'ib_system_subtype': res_item[6],
					'ib_system_type': res_item[7],
					'fqdn':  res_item[0] + "." + res_item[8] } )

	return server_hosts

def get_fqdn_from_hostname(hostname):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()
        server_hosts = []

	

def logger(text_input="", debug_level="INFO", module=""):
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        return '[%s (CDT)] deployer.%s %s - %s []\n' % (now, debug_level, module, text_input)



def worker(unique_id, job_config):

	print datetime.datetime.now()
	## All Deployer Logic will happen here

	### Get connection to MySQL

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	### Set current job_log_filename path based on uniqueId

	job_log_filename = '''%s/deployer_%s.log''' % ( deployer_log_path, job_config['uniqueId'] )
	#job_log_filename = '''%s/deployer_%s.log''' % ( deployer_log_path, 'test' )
	
	### Open logging file for unique_id

	fd = open(job_log_filename, 'w')
	fd.write( logger( os.getpid() ) )
	fd.write( logger( "Unique ID: " + job_config['uniqueId']) )
	fd.write( logger( "User: " + job_config['user']) )

	### Output Job Stats to log

	fd.write( logger( "Current job options:" + str(job_config), "DEBUG" ) )

	chart_output = """The following deployment selections were made:
+---------+-------------+----------------+------------+
| Name | Environment | Version | JIRA Issue |
+---------+-------------+----------------+------------+\n"""
	for artifact_row in job_config['artifacts']:
		chart_output += "| %s | %s | %s | %s |\n" % ( artifact_row.split(':')[0], artifact_row.split(':')[1], artifact_row.split(':')[2],artifact_row.split(':')[3] )

	chart_output += """+---------+-------------+----------------+------------+\n"""
	fd.write( logger( chart_output ) )

	### Warn if user selected SAFE MODE

	if (job_config['run'] == "0"):
		fd.write( logger( '''### "SAFE MODE" ENABLED. No manipulative SSH or network monitoring functions will be executed. ###''', "NOTICE" ) )
	fd.write( logger( "Preparing artifacts for deployment..." ) )

	### Declare deployment_items list to hold all important information regarding current deploy job
	
	deployment_items = []

	### Start preparations for deployment of artifacts

	print job_config

	for artifact_row_item in job_config['artifacts']:

		print artifact_row_item

		artifact_item = { 'name': artifact_row_item.split(':')[0], 'env': artifact_row_item.split(':')[1], 'versionToDeploy': artifact_row_item.split(':')[2], 'jira_task': artifact_row_item.split(':')[3] }


		print '''Retrieving database record for '%s', '%s' environment''' % (artifact_item['name'], artifact_item['env'])
		fd.write( logger( '''Retrieving database record for '%s', '%s' environment''' % (artifact_item['name'], artifact_item['env']) ) )

		### Get other artifact information to populate in deployment_items disctionary
		curr.execute("""SELECT `artifacts`.`artifactId`, `artifactEnvironments`.`serverData`, `artifactEnvironments`.`isLoadBalanced`, 
		`artifacts`.`artifactType`, `artifactEnvironments`.`isDeploying`, `artifacts`.`groupId`
		FROM `artifacts` INNER JOIN `artifactEnvironments` 
		ON artifacts.name = artifactEnvironments.artifactName WHERE (artifacts.name = artifactEnvironments.artifactName) 
		AND (artifacts.name = '%s' and artifactEnvironments.env = '%s')""" % ( (artifact_item['name'] , artifact_item['env'] ) ) )
		
		res = curr.fetchone()

		### Populate other data for this artifact
		
		if ( 'latest' in artifact_item['versionToDeploy'] ):
			artifact_item['versionToDeploy'] = get_latest_version(artifact_item['name'], artifact_item['env'])
		artifact_item['id'] = res[0]
		artifact_item['serverData'] = res[1]
		artifact_item['isLoadBalanced'] = res[2]
		artifact_item['artifactType'] = res[3]		
		artifact_item['version'] = artifact_item['versionToDeploy'] #get_latest_version(artifact_item['name'], artifact_item['env'])
		artifact_item['isDeploying'] = res[4]
		artifact_item['groupId'] = res[5]

		### Artifact dependancy check

		check_dependants_query = """SELECT dependency FROM artifactDependencies WHERE artifactName = '%s' and env = '%s'; """ % ( (artifact_item['name'] , artifact_item['env'] ) )

		### If a dependancy exists add it to the artifact list to be processed as well.
		if ( curr.execute(check_dependants_query) and 'skipDepDeploy' not in job_config ):
			print '''[%s] has dependencies that will be deployed before it ''' % ( artifact_item['name'] )
			fd.write( logger( '''[%s] has dependencies that will be deployed before it ''' % ( artifact_item['name'] ), "NOTICE" ) )
			for dependant_artifact in curr.fetchall():
		
				deps_artifact = ''
				get_nexus_version = '''select nexus_metadata.latest from nexus_metadata inner join artifacts on nexus_metadata.artifactId = artifacts.artifactId where artifacts.name = '%s';''' % ( dependant_artifact[0] )
				if ( curr.execute(get_nexus_version) ):
					deps_artifact += dependant_artifact[0] + ':' + artifact_item['env'] + ':' + artifact_item['versionToDeploy'] + ':' + artifact_item['jira_task']


				#deps_artifact['versionToDeploy'] = artifact_item['versionToDeploy']
				#curr.fetchone()[0]
				#deps_artifact['env'] = artifact_item['env']
				
				job_config['artifacts'].append(deps_artifact)
				
		### Prep servers needed for deployment
		
		print '''Retrieving servers for [%s@%s]...''' % (artifact_item['name'], artifact_item['env'])
		fd.write( logger( '''Retrieving servers for [%s@%s]...''' % (artifact_item['name'], artifact_item['env']) ) )

		artifact_item['servers'] = []
		

		### Loop through all server names and check if they are load balanced	
		for host_item in get_hostinfo( ''.join(artifact_item['serverData'].split()) ):
		#for host_item in get_hostinfo( ''.join(artifact_item['serverData'].split()) ):
			host_item['prodState'] = "" #zenoss.get_state(host_item['hostname'])

			#### Re-enable for Zenoss Prod state saving.
			#host_item['prodState'] = zenoss.get_state(host_item['hostname'])
						
			fd.write( logger( "Getting Zenoss device for " + host_item['fqdn'] + " which is in " + host_item['prodState'], "DEBUG" ) )

			lbSystemSubtypeMap = get_config_values(configs['services'], "hostinfo.lbSystemSubtypeMap")

			host_item['lb_server_map'] = []
			host_item['haproxy_targets'] = []

			### Check if servers are load balanced and if they are add the load balancer server to host_item['ib_system_subtype']

			if( host_item['ib_system_subtype'] and artifact_item['isLoadBalanced'] ):
				fd.write( logger( "Getting load balancers for " + host_item['fqdn'] + " from hostinfo." , "DEBUG" ) )


				lb_item = ""

				haproxy_query = '''SELECT proxyName FROM artifactHaproxyProxies WHERE artifactName = "%s" ; ''' % ( artifact_item['name'] )


				if ( curr.execute(haproxy_query) ):
					for proxy_list in curr.fetchall():

						host_item['haproxy_targets'].append(proxy_list[0])

				fd.write( logger( "Getting haproxy targets for " + host_item['fqdn'] + "." , "DEBUG" ) )
				

				for system_type_item in lbSystemSubtypeMap:

					if ( len(system_type_item.split('.')) == 4 ):
						if ( host_item['ib_system_subtype'] == system_type_item.split('.')[3] and host_item['ib_system_type'] == system_type_item.split('.')[2] ):
							lb_item = lbSystemSubtypeMap[system_type_item]
							break

						elif ( system_type_item.split('.')[3] == '*' and host_item['ib_system_subtype'] != system_type_item.split('.')[3] and host_item['ib_system_type'] == system_type_item.split('.')[2] ):
							lb_item = "front"
							break

					if ( len(system_type_item.split('.')) == 3 ):
						if (system_type_item.split('.')[2] in host_item['hostname'] ):
							lb_item = lbSystemSubtypeMap[system_type_item]
							break

				#print get_fqdn( "ib_system_type.eq.balancer|ib_env.eq.%s|ib_dc.eq.%s|ib_system_subtype.eq.%s" % ( host_item['ib_env'], host_item['ib_dc'], lb_item ) )

				for lb_list_item in get_fqdn( "ib_system_type.eq.balancer|ib_env.eq.%s|ib_dc.eq.%s|ib_system_subtype.eq.%s" % ( host_item['ib_env'], host_item['ib_dc'], lb_item ) ):
					host_item['lb_server_map'].append(lb_list_item)
			

			fd.write( logger( "Load Balance server map for " + host_item['fqdn'] + "\n" + str(host_item['lb_server_map']) , "DEBUG" ) )

			#remote_command(host_item['fqdn'], "ps -Aef | grep tomcat7")

			artifact_item['servers'].append(host_item)



		print '''Retrieving [%s] (%s) metadata from Nexus...''' % ( artifact_item['id'], artifact_item['artifactType'])
		fd.write( logger('''Retrieving [%s] (%s) metadata from Nexus...''' % ( artifact_item['id'], artifact_item['artifactType']) ) )

		### Check current nexus snapshots needed for this deployment especially for dev servers.

		nexus_api_url = nexus_rest_url + artifact_item['groupId'].replace('.', '/') + "/" + artifact_item['id'] + "/" + artifact_item['version'] + "/"

		#print nexus_api_url

		c = pycurl.Curl()
		buf = cStringIO.StringIO()
		
	        c.setopt(c.URL, nexus_api_url)
        	c.setopt(c.WRITEFUNCTION, buf.write)
	        c.perform()

		xmldoc = minidom.parseString(buf.getvalue())
		
		#content_list = xmldoc.getElementsByTagName('contents')

		### Populate deployment data for each artifact

		artifact_item['deployment_data'] = {}

		for content_item in xmldoc.getElementsByTagName('content-item'):
			text_item = content_item.getElementsByTagName('text')
			url_item = content_item.getElementsByTagName('resourceURI')

			search_template = "" 
			if ('wro' in  artifact_item['artifactType']):
				search_template = '''%s.jar$''' % artifact_item['artifactType']

			if ('war' in artifact_item['artifactType']):
				search_template = '''.%s$''' % artifact_item['artifactType']
				
			if (re.findall(search_template, text_item[0].firstChild.nodeValue)):
			#if ( '.war' in text_item[0].firstChild.nodeValue):
				artifact_item['deployment_data']['filename'] = text_item[0].firstChild.nodeValue
				artifact_item['deployment_data']['resourceURI'] = url_item[0].firstChild.nodeValue


		#print artifact_item['deployment_data']

		#print nexus_api_url
		#print buf.getvalue()

		print '''Verifying version [%s] (%s)...''' % (artifact_item['id'], artifact_item['artifactType'])
		fd.write( logger( '''Verifying version [%s] (%s)...''' % (artifact_item['id'], artifact_item['artifactType']) ) )

		print '''Found [%s] version %s in Maven metadata''' % (artifact_item['id'], artifact_item['version'])
		fd.write( logger( '''Found [%s] version %s in Maven metadata''' % (artifact_item['name'], artifact_item['version']) ) )

		print '''Retrieving sites for [%s]...''' % ( artifact_item['name'] )
		fd.write( logger( '''Retrieving sites for [%s]...''' % ( artifact_item['name'] ) ) )

		artifact_item['sites'] = []
		get_sites_query = '''SELECT `artifactSites`.* FROM `artifactSites` WHERE (artifactName = '%s') AND (isEnabled = 1) ORDER BY `priority` ASC''' % ( artifact_item['name'] )

		if curr.execute(get_sites_query):
			for get_site_item in curr.fetchall():
				artifact_item['sites'].append(get_site_item)

		print '''Found %s for sites(s) for [%s]''' % ( str(len(artifact_item['sites'])), artifact_item['name'] )
		fd.write( logger( '''Found %s for sites(s) for [%s]''' % ( str(len(artifact_item['sites'])), artifact_item['name'] ) ) )


		deployment_items.append( artifact_item )



	#### PREP completed now ready for deployment

	#### Check to see if all servers are off of MAINT mode so deployment can commence and check to see if an artifact is also being deployed.

	servers_to_deploy = {}

	for cur_deploy_artifact in reversed(deployment_items):
		artifact_tmp = []
		if ( cur_deploy_artifact['isDeploying'] ):
			print '''[%s] is already deploying to the '%s' environment; Skipping...''' % ( cur_deploy_artifact['name'], artifact_item['env'] )
			fd.write( logger( '''[%s] is already deploying to the '%s' environment; Skipping...''' % ( cur_deploy_artifact['name'], artifact_item['env'] ), "CRITICAL" ) )
			print "Removing " + cur_deploy_artifact['fqdn'] + " since it is deploying."

		else:

			##No artifacts are in deployment so this gets the lastest zenoss mode.
			if ( False ):  ### Set UI flag here for Monitoring and resetting
				hosts_in_maint = False
				for check_zenoss_state in cur_deploy_artifact['servers']:
					if ( 'Maintenance' in zenoss.get_state( check_zenoss_state['hostname'] ) ):
						print '''Waiting for host %s to leave Maintenace so deployment can continue...''' % ( check_zenoss_state['fqdn'] )
						hosts_in_maint = True
						break
			
				while hosts_in_maint:
					hosts_in_maint = False
					for check_zenoss_state in cur_deploy_artifact['servers']:
						if ( 'Maintenance' in zenoss.get_state( check_zenoss_state['hostname'] ) ):
							print '''Waiting for host %s to leave Maintenace so deployment can continue...'''  % ( check_zenoss_state['fqdn'] )
							hosts_in_maint = True
							break	


			#print "Ready to deploy to " + str(cur_deploy_artifact['servers'])

			#### Invert deployment with the servers as lead of group.
		

			for server_deploy_item in cur_deploy_artifact['servers']:
				#print server_deploy_item['ib_partner_group']
				if ( server_deploy_item['fqdn'] in servers_to_deploy.keys() ):
					servers_to_deploy[server_deploy_item['fqdn']].append(cur_deploy_artifact)
				else:
					servers_to_deploy.update( { server_deploy_item['fqdn']: [ cur_deploy_artifact ] } )

	fd.write( logger( '''Deploying to these servers: [%s]''' % ( str(servers_to_deploy) ), "DEBUG" ) )
	fd.close()

	server_procs = []
	for server_item in servers_to_deploy.keys():
		#print deploy_to_item['id'], deploy_to_item['artifactType']
		print "Deploying to server " + server_item
		
		deploy_to_server(unique_id, server_item, servers_to_deploy)

		#server_deployment_job = multiprocessing.Process(target = deploy_to_server, args=( unique_id, server_item, servers_to_deploy ) )
		#server_procs.append(server_deployment_job)
		#server_deployment_job.start()
	
	#for server_proc_item in server_procs:
		#server_proc_item.join()


	print "Completed Job ID: " + unique_id

	job_log_filename = '''%s/deployer_%s.log''' % ( deployer_log_path, unique_id )

	fd = open(job_log_filename, 'a')

	fd.write( logger( "Completed Job ID: " + unique_id ) )	

	fd.close()

	os.remove("/tmp/deployer/deployer_" + unique_id + "_v3.job")
	remove_process_query = "DELETE FROM processes WHERE uniqueId ='%s'" % (unique_id)
	curr.execute(remove_process_query)
	conn.commit()	

	print datetime.datetime.now()


def deploy_to_server(unique_id, server_name, servers_deploy_data):
	job_log_filename = '''%s/deployer_%s.log''' % ( deployer_log_path, unique_id )

	fd = open(job_log_filename, 'a')

	fd.write( logger( '''Starting deployment to server %s with unique_id of %s''' % (server_name, unique_id ) ) )	

	parent_command_batch = []
	child_command_batch = []

	nexus_conf = {}
	nexus_conf.update( get_config_values(configs['services'], "nexus.username") )
	nexus_conf.update( get_config_values(configs['services'], "nexus.password") )

	### Get command file output from associated dmap file

	for deploy_to_item in servers_deploy_data[server_name]:
		print server_name


		command_map = ""

		if ( 'importer' in deploy_to_item['id'] ):
			command_map = open("modules/commands/importer.dmap", "r").read().rstrip().split('\n')
			
		elif ( 'webapp' in deploy_to_item['id'] and 'war' in deploy_to_item['artifactType'] ):
			command_map = open("modules/commands/cae-webapp.dmap", "r").read().rstrip().split('\n')

		#if ( 'webapp' in deploy_to_item['id'] and 'war' in deploy_to_item['artifactType'] and 'base' in deploy_to_item['id'] ):
		#	command_map = open("modules/commands/cae-base-webapp.dmap", "r").read().rstrip().split('\n')

		elif ( 'webapp' in deploy_to_item['id'] and 'wro' in deploy_to_item['artifactType'] ):
			command_map = open("modules/commands/cae-wro.dmap", "r").read().rstrip().split('\n')

		elif ( 'webscan' in deploy_to_item['id'] and 'war' in deploy_to_item['artifactType'] ):
			command_map = open("modules/commands/webscan.dmap", "r").read().rstrip().split('\n')

		else:
			command_map = open("modules/commands/cae-webapp.dmap", "r").read().rstrip().split('\n')


		for command in command_map:

			if ('[parent]' in command):
				parent_command_batch.append(command.split('[parent]=')[1])

			elif ('[child_deploy]=' in command):
				deploy_command = command.split('[child_deploy]=')[1]
				tmp_command = ""
				
				if ( 'base' in deploy_to_item['id'] ):
					tmp_command = deploy_command + " --user " + nexus_conf["nexus.username"] +  " --password " + nexus_conf["nexus.password"] + " " + deploy_to_item['deployment_data']['resourceURI'] + " -O ib-base." + deploy_to_item['artifactType']
				else:
					tmp_command = deploy_command + " --user " + nexus_conf["nexus.username"] +  " --password " + nexus_conf["nexus.password"] + " " + deploy_to_item['deployment_data']['resourceURI'] + " -O " + deploy_to_item['id'].split('-')[0] + "." + deploy_to_item['artifactType']

				#print tmp_command				
				
				child_command_batch.append(tmp_command)


			elif ('[child_task]=' in command):
				deploy_command = command.split('[child_task]=')[1]

				tmp_command = ""
				
				deploy_id = ""

				if ( 'base' in deploy_to_item['id'] ):
					deploy_id = "ib-base"
				else:
					deploy_id = deploy_to_item['id'].split('-')[0]

				if ( "[nexus-url]"  in deploy_command ):

					nexus_command = "wget -q --no-check-certificate --user " + nexus_conf["nexus.username"] +  " --password " + nexus_conf["nexus.password"] + " " + deploy_to_item['deployment_data']['resourceURI'] + " -O " + deploy_to_item['id'] + "." + deploy_to_item['artifactType'] + ".jar"

					tmp_command = deploy_command.replace( "[nexus-url]", nexus_command ).replace("[partner-id]", deploy_id).replace("[version]", deploy_to_item['version'] ).replace('[nexus-filename]', deploy_to_item['deployment_data']['filename'])

				else:

					tmp_command = deploy_command.replace("[partner-id]", deploy_id).replace("[version]", deploy_to_item['version'] ).replace('[nexus-filename]', deploy_to_item['deployment_data']['filename'])

				#print tmp_command				
				child_command_batch.append(tmp_command)

			elif ('[child_clean]=' in command):
				tmp_command = command.split('[child_clean]=')[1] + deploy_to_item['id']
				child_command_batch.append(tmp_command)

			else:
				parent_command_batch.append(command)	
	
	header_command = []
	footer_command = []

	
	fd.write( logger( '''Setting Zenoss and Haproxy states to MAINTENANCE for server: %s''' % (server_name) ) )	

	for deploy_to_item in servers_deploy_data[server_name]:

		print "Setting up LB settings for server " + server_name
		for lb_item in deploy_to_item['servers']:
			if ( lb_item['fqdn'] == server_name ):
				zenoss.set_state(lb_item['hostname'], 'Maintenance')
				for lb_server in lb_item['lb_server_map']:
					for ha_item in lb_item['haproxy_targets']:
						ha_c = happy_proxy()
						ha_c.set_status('disable', lb_server, ha_item, lb_item['hostname'])
		

	if( len ( parent_command_batch ) ):
		header_command = parent_command_batch[0:parent_command_batch.index('[parent_to_child]')]
		parent_command_batch.reverse()
		footer_command = parent_command_batch[0:parent_command_batch.index('[parent_to_child]')]
		footer_command.reverse()

	print "Running commands on " + server_name
	
	for command_item in header_command:
		print '''Running %s ...  %s@%s on %s for uid %s''' % ( command_item, deploy_to_item['id'], deploy_to_item['artifactType'], server_name, unique_id )
		fd.write( logger( '''Running %s ...  %s@%s on %s for uid %s''' % ( command_item, deploy_to_item['id'], deploy_to_item['artifactType'], server_name, unique_id ) ) )	
		remote_command(server_name, command_item )

	child_proc = []

	for command_item in child_command_batch:
		print '''Running %s ...  %s@%s on %s for uid %s''' % ( command_item, deploy_to_item['id'], deploy_to_item['artifactType'], server_name, unique_id )
		fd.write( logger( '''Running %s ...  %s@%s on %s for uid %s''' % ( command_item, deploy_to_item['id'], deploy_to_item['artifactType'], server_name, unique_id ) ) )	
		remote_command(server_name, command_item )


		#print "Running " + command_item + " ... " + "{ " + server_name
		#child_remote_task = multiprocessing.Process(target = multi_remote_child, args=( server_name, command_item ) )
		#child_proc.append(child_remote_task)
		#child_remote_task.start()

	#for child_item in child_proc:
	#	child_item.join()
	
	for command_item in footer_command:
		print '''Running %s ...  %s@%s on %s for uid %s''' % ( command_item, deploy_to_item['id'], deploy_to_item['artifactType'], server_name, unique_id )
		fd.write( logger( '''Running %s ...  %s@%s on %s for uid %s''' % ( command_item, deploy_to_item['id'], deploy_to_item['artifactType'], server_name, unique_id ) ) )	
		remote_command(server_name, command_item )


	for deploy_to_item in servers_deploy_data[server_name]:

		print "Setting up LB settings for server " + server_name

		for lb_item in deploy_to_item['servers']:
			if ( lb_item['fqdn'] == server_name ):
				zenoss_dict = { 'dev': 'Development', 'qa': 'QA', 'prod':'Production', 'stage':'Stage' }
				zenoss.set_state( lb_item['hostname'], zenoss_dict[lb_item['ib_env']] )
				for lb_server in lb_item['lb_server_map']:
					for ha_item in lb_item['haproxy_targets']:
						ha_c = happy_proxy()
						ha_c.set_status('enable', lb_server, ha_item, lb_item['hostname'])

	fd.write( logger( '''Resetting Zenoss and Haproxy to previous states for server: %s''' % (server_name) ) )	
			

	print "Completed deployment to server " + server_name

	fd.close()

		#print parent_command_batch[0:parent_command_batch.index('[parent_to_child]')] , parent_command_batch[parent_command_batch.index('[parent_to_child]')+1:]
			

def multi_remote_child(server_name, command):
	print "Running " + command + " on " + server_name
	remote_command(server_name, command )


while True:
	
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()
	
	process_query = "SELECT uniqueId FROM processes WHERE status = 1 LIMIT 1;"

	curr.execute(process_query)

	job_unique_id = curr.fetchone()

	if ( job_unique_id ):
		try:
			fd = open("/tmp/deployer/deployer_" + job_unique_id[0] + "_v3.job", "rb")
			jobs = pickle.load( fd )
			fd.close()
			print "Found Job file for " + job_unique_id[0] + " and will start processing."
			process_query = "UPDATE processes SET status = 2 WHERE  uniqueId = '%s'" % ( job_unique_id[0] )
			curr.execute(process_query)
			conn.commit()
			print "Processing Job ID: " + job_unique_id[0]
			j = multiprocessing.Process(target = worker, args=(job_unique_id[0],jobs))
			j.start()

	
		except IOError:
			print "Job file for ID: " + job_unique_id[0] + " does not exist, removing from process list."
			remove_process_query = "DELETE FROM processes WHERE uniqueId ='%s'" % (job_unique_id[0])
			curr.execute(remove_process_query)
			conn.commit()
			

	#print "Test"
	time.sleep(0.25)	
