#!/usr/bin/env python
'''
Application: Deployer Service Restart
Version 3.0.1
Name: Matthew Wiljanen
Date: 2014-10-09
'''

import MySQLdb
import pycurl
import cStringIO
from xml.dom import minidom
import datetime
import time
import pickle
import os
import multiprocessing
import random
from zenoss import Zenoss
import ast
import re
import paramiko
import deployer_zenoss as zenoss
from happy_proxy import happy_proxy


db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

deployer_worker_path = "/tmp/deployer/"

deployer_log_path = "/var/log/ib/deployer"

deployer_config_path = "/etc/ibconf/deployer/"

nexus_rest_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/service/local/repo_groups/public/content/"

configs = {}

configs['deployer'] = open(deployer_config_path + "deployer.ini" , "r").read()
configs['services'] = open(deployer_config_path + "services.ini" , "r").read()

zenoss_states = { 'Maintenance': 300, 'Test': 400, 'Development': 500, 'QA': 600, 'Staging': 800, 'ProdTest': 900, 'Production': 1000  }
zenoss_short_name = { 'maint': 'Maintenance', 'dev': 'Development', 'qa': 'QA', 'stage': 'Staging', 'prod': 'Production' }

jobs = {}

def remote_command(fqdn, r_command=""):
	sshconf = {}
	sshconf.update( get_config_values(configs['services'], "ssh.username") )
	sshconf.update( get_config_values(configs['services'], "ssh.publicKey") )
	sshconf.update( get_config_values(configs['services'], "ssh.privateKey") )

	try:
		ssh = paramiko.SSHClient()
		paramiko.util.log_to_file("ssh.log")
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(hostname=fqdn, username=sshconf['ssh.username'], key_filename=sshconf['ssh.privateKey'])
		return ssh.exec_command(r_command)[1].read()
	except:
		return ''
		
		#print "SSH Failed to login to " + fqdn

def set_zenoss_state(hostname="", state=""):
	zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
	zenoss.set_prod_state(hostname, zenoss_states[state] )

def get_zenoss_state(hostname=""):
	zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
	return zenoss.find_device(hostname)['productionState']

def get_config_values(tmp_config="", get_value=""):
	values = {}

        for tmp_line in tmp_config.split('\n'):
                if ( get_value in tmp_line and tmp_line[0] != ';' ):
			values[tmp_line.split(' = ')[0]] = tmp_line.split(' = ')[1].replace('"','')

	return values


def ping_tomcat(hostname):
	
	try:
		conn = pycurl.Curl()
        	buff = cStringIO.StringIO()
	        conn.setopt(conn.URL, "http://" + hostname + ":8080")
        	conn.setopt(conn.WRITEFUNCTION, buff.write)
		conn.setopt(conn.TIMEOUT, 5)
        	conn.perform()
		return True

	except:
		return False

def get_fqdn_from_hostname(hostname):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	hostname_query = '''select concat(hostname, '.', domain) from hostinfo where hostname = '%s' ;''' % ( hostname ) 

	if ( curr.execute( hostname_query ) ):
		return curr.fetchone()[0]


def logger(text_input="", debug_level="INFO", module=""):
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        return '[%s (CDT)] deployer.%s %s - %s []\n' % (now, debug_level, module, text_input)


def service_worker(job_config):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	print datetime.datetime.now()

	job_log_filename = '''%s/deployer_service_%s.log''' % ( deployer_log_path, job_config['job_id'] )
	

	for task_item in job_config['tasks']:
		if ( 'restart_puppet_on' in task_item ):
			server_name = get_fqdn_from_hostname(task_item.split('restart_puppet_on_')[1])
			print "Restarting puppet on " + server_name
			remote_command(server_name, 'sudo /etc/init.d/puppet restart')
			time.sleep(1)
			wait_counter = 12

			while ( len ( remote_command(server_name, 'sudo tail -n1 /var/log/daemon.log | grep "Finished catalog run"').rstrip() ) == 0 and wait_counter > 0):
				print "Waiting 5 seconds for puppet to finish loading"
				wait_counter -= 1
				time.sleep(5)

			
			print "Puppet reloaded on " + server_name

		if ( 'restart_tomcat_on' in task_item ):
			server_name = get_fqdn_from_hostname(task_item.split('restart_tomcat_on_')[1])
			hostname = task_item.split('restart_tomcat_on_')[1]
			host_env = ''
			

			if ('dev' in hostname):			
				host_env = 'dev'

			if ('stage' in hostname):			
				host_env = 'stage'

			if ('qa' in hostname):			
				host_env = 'qa'

			if ('prod' in hostname):			
				host_env = 'prod'

			ha = happy_proxy()
			
			lb_list = []
			lb_list_query = '''select concat(hostname, '.', domain) from hostinfo where ib_system_type = 'balancer' and ib_env = '%s';''' % ( host_env  )
			if ( curr.execute (lb_list_query) ):
				for res_item in curr.fetchall():
					lb_list.append(res_item[0])

			print "Setting " + server_name + " to maintenance."

			set_zenoss_state(hostname, zenoss_short_name['maint'])


			print "Disabling load balancer for " + server_name
			for lb_list_item in lb_list:
				ha.set_all_status( 'disable', lb_list_item, hostname )					
				
			remote_command(server_name, 'touch /tmp/CAE_DOWN')

			print "Restarting tomcat on " + server_name
			remote_command(server_name, 'sudo /etc/init.d/tomcat7 stop')
			while ( len ( remote_command(server_name, 'ps -ewwo pid,args | grep [t]omcat | grep java').rstrip() ) != 0 ):
				print "Waiting 3 seconds for Tomcat to stop..."
				time.sleep(3)

			remote_command(server_name, 'sudo /etc/init.d/tomcat7 start')

			while ( ping_tomcat( server_name ) == False ):
				print "Waiting for tomcat to restart..."
				time.sleep(5)

			print "Tomcat restarted ..."

			remote_command(server_name, 'rm /tmp/CAE_DOWN')
	
			print "Enabling load balancer for " + server_name
			for lb_list_item in lb_list:
				ha.set_all_status( 'enable', lb_list_item, hostname )					

			print "Setting " + server_name + " to " + zenoss_short_name[host_env]  + "."

			set_zenoss_state(hostname, zenoss_short_name[host_env])

		if ( 'set_zenoss_on' in task_item ):
			server_name = get_fqdn_from_hostname(task_item.split('set_zenoss_on_')[1])
			hostname = task_item.split('set_zenoss_on_')[1]
			host_env = ''
			
			print server_name, hostname

			if ('dev' in hostname):			
				host_env = 'dev'

			if ('stage' in hostname):			
				host_env = 'stage'

			if ('qa' in hostname):			
				host_env = 'qa'

			if ('prod' in hostname):			
				host_env = 'prod'

			ha = happy_proxy()
			

			lb_list = []
			lb_list_query = '''select concat(hostname, '.', domain) from hostinfo where ib_system_type = 'balancer' and ib_env = '%s';''' % ( host_env  )
			if ( curr.execute (lb_list_query) ):
				for res_item in curr.fetchall():
					lb_list.append(res_item[0])

			print "Setting " + server_name + " to maintenance."

			set_zenoss_state(hostname, zenoss_short_name['maint'])

			print "Disabling load balancer for " + server_name
			for lb_list_item in lb_list:
				ha.set_all_status( 'disable', lb_list_item, hostname )					

			print "Enabling load balancer for " + server_name
			for lb_list_item in lb_list:
				ha.set_all_status( 'enable', lb_list_item, hostname )					
			

			print "Setting " + server_name + " to " + zenoss_short_name[host_env]  + "."

			set_zenoss_state(hostname, zenoss_short_name[host_env])

			
while True:
	
	try:
		for job_item in os.listdir("/tmp/deployer/services/"):
			jobs = pickle.load( open("/tmp/deployer/services/" + job_item, "rb") )

			print jobs
			print "Found Job file for " + job_item + " and will start processing."

			os.remove("/tmp/deployer/services/" + job_item)
			service_worker(jobs)
			#j = multiprocessing.Process( target = service_worker, args=( jobs ) )
			#j.start()
	
	except IOError:
		print "Another error occurred!"

	#print "Test"
	time.sleep(1)	
