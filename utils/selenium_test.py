#!/usr/bin/env python

from pyvirtualdisplay import Display
from selenium import webdriver
import sys
display = Display(visible=0, size=(800, 600))
display.start()

# now Firefox will run in a virtual display. 
# you will not see the browser.
browser = webdriver.Firefox()
browser.get(sys.argv[1])
#print browser.title

browser.get_screenshot_as_file( browser.title.replace(' ','_') + '.png' )
browser.quit()

display.stop()
