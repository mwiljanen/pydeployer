#!/usr/bin/env python
'''
Application: Deployer Service Restart
Version 3.0.1
Name: Matthew Wiljanen
Date: 2014-10-09
'''

import MySQLdb
import pycurl
import cStringIO
from xml.dom import minidom
import datetime
import time
import pickle
import os
import multiprocessing
import random
from zenoss import Zenoss
import ast
import re
import paramiko
import deployer_zenoss as zenoss
from happy_proxy import happy_proxy


db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

deployer_worker_path = "/tmp/deployer/"

deployer_log_path = "/var/log/ib/deployer"

deployer_config_path = "/etc/ibconf/deployer/"

nexus_rest_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/service/local/repo_groups/public/content/"

configs = {}

configs['deployer'] = open(deployer_config_path + "deployer.ini" , "r").read()
configs['services'] = open(deployer_config_path + "services.ini" , "r").read()

zenoss_states = { 'Maintenance': 300, 'Test': 400, 'Development': 500, 'QA': 600, 'Staging': 800, 'ProdTest': 900, 'Utility': 950, 'Production': 1000  }
zenoss_short_name = { 'maint': 'Maintenance', 'dev': 'Development', 'qa': 'QA', 'stage': 'Staging', 'prod': 'Production' }

jobs = {}

def remote_command(fqdn, r_command=""):
	sshconf = {}
	sshconf.update( get_config_values(configs['services'], "ssh.username") )
	sshconf.update( get_config_values(configs['services'], "ssh.publicKey") )
	sshconf.update( get_config_values(configs['services'], "ssh.privateKey") )

	try:
		ssh = paramiko.SSHClient()
		paramiko.util.log_to_file("ssh.log")
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(timeout=60, hostname=fqdn, username=sshconf['ssh.username'], key_filename=sshconf['ssh.privateKey'])
		return ssh.exec_command(r_command)[1].read()
	except:
		return ''

def super_remote_command(fqdn, r_command=""):
	sshconf = {}
	sshconf.update( get_config_values(configs['services'], "ssh.username") )
	sshconf.update( get_config_values(configs['services'], "ssh.publicKey") )
	sshconf.update( get_config_values(configs['services'], "ssh.privateKey") )

	try:
		ssh = paramiko.SSHClient()
		paramiko.util.log_to_file("superssh.log")
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		ssh.connect(timeout=60, hostname=fqdn, username='coresvcs', key_filename=sshconf['ssh.privateKey'])
		return ssh.exec_command(r_command)[1].read()
	except:
		print "SSH Failed to login to " + fqdn
		return ''
		

def set_zenoss_state(hostname="", state=""):
	zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
	zenoss.set_prod_state(hostname, zenoss_states[state] )

def get_zenoss_state(hostname=""):
	zenoss = Zenoss('http://zenoss1-dal05.dal05.sl.ibsys.com', 'json', 'J50nJ50n')
	return zenoss.find_device(hostname)['productionState']

def get_config_values(tmp_config="", get_value=""):
	values = {}

        for tmp_line in tmp_config.split('\n'):
                if ( get_value in tmp_line and tmp_line[0] != ';' ):
			values[tmp_line.split(' = ')[0]] = tmp_line.split(' = ')[1].replace('"','')

	return values


def ping_tomcat(hostname):
	
	try:
		conn = pycurl.Curl()
        	buff = cStringIO.StringIO()
	        conn.setopt(conn.URL, "http://" + hostname + ":8080")
        	conn.setopt(conn.WRITEFUNCTION, buff.write)
		conn.setopt(conn.TIMEOUT, 5)
        	conn.perform()
		return True

	except:
		return False

def get_fqdn_from_hostname(hostname):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	hostname_query = '''select concat(hostname, '.', domain) from hostinfo where hostname = '%s' ;''' % ( hostname ) 

	if ( curr.execute( hostname_query ) ):
		return curr.fetchone()[0]


def logger(file_name="", text_input="", debug_level="INFO", module=""):
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	print '[%s (CDT)] %s %s - %s []\n' % (now, debug_level, module, text_input)
        open(file_name, 'a').write( '[%s (CDT)] %s %s - %s []\n' % (now, debug_level, module, text_input) )


def puppet_restart_task(job_log_filename, task_item):
	server_name = get_fqdn_from_hostname(task_item.split('restart_puppet_on_')[1])
	logger(job_log_filename, "Restarting puppet on " + server_name)
	
	super_remote_command(server_name, '''sudo puppet agent -t''')

	wait_counter = 12

	super_remote_command(server_name, 'sudo tail -n10 /var/log/daemon.log | grep "Finished catalog run"')

	while ( len ( super_remote_command(server_name, 'sudo tail -n10 /var/log/daemon.log | grep "Finished catalog run"').rstrip() ) == 0 and wait_counter > 0):
		logger(job_log_filename, "Waiting 5 seconds for puppet to finish loading on " + server_name )
		wait_counter -= 1
		time.sleep(5)

			
	logger(job_log_filename, "Puppet reloaded on " + server_name )


def service_worker(job_config):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	print datetime.datetime.now()

	job_log_filename = '''%s/deployer_service_%s.log''' % ( deployer_log_path, job_config['job_id'] )
	
	print str(sorted(job_config['tasks']))


	puppet_task_proc = []
	print job_config['tasks'].keys()	

	for task_item in sorted(job_config['tasks'].keys()):
		if ( 'restart_puppet_on' in task_item ):

			puppet_task = multiprocessing.Process( target = puppet_restart_task, args=( job_log_filename, task_item ) )
			puppet_task.start()
			puppet_task_proc.append(puppet_task)

		if ( 'set_cae_status_on' in task_item ):
			server_name = get_fqdn_from_hostname(task_item.split('set_cae_status_on_')[1])
			status_mode = job_config['tasks']['set_cae_status_mode']

			if ( status_mode.lower() == 'down' ):
				logger(job_log_filename, "Touching /tmp/CAE_DOWN on " + server_name)
				remote_command(server_name, 'touch /tmp/CAE_DOWN')
			if ( status_mode.lower() == 'up' ):
				logger(job_log_filename, "Removing /tmp/CAE_DOWN on " + server_name)
				remote_command(server_name, 'rm /tmp/CAE_DOWN')
				

		if ( 'restart_tomcat_on' in task_item ):
			server_name = get_fqdn_from_hostname(task_item.split('restart_tomcat_on_')[1])
			hostname = task_item.split('restart_tomcat_on_')[1]

			set_tomcat_status = "Restart"
			if ( 'set_tomcat_status_mode' in job_config['tasks'].keys() ):
				set_tomcat_status = job_config['tasks']['set_tomcat_status_mode']

			host_env = ''

			
			if ('dev' in hostname):			
				host_env = 'dev'

			if ('stage' in hostname):			
				host_env = 'stage'

			if ('qa' in hostname):			
				host_env = 'qa'

			if ('prod' in hostname):			
				host_env = 'prod'


			#print server_name, hostname, host_env
			if ( set_tomcat_status == "Start" ):
				logger(job_log_filename, "Starting tomcat on " + server_name )

				remote_command(server_name, 'sudo /etc/init.d/tomcat7 start')

				wait_counter = 600

				while ( ping_tomcat( server_name ) == False and wait_counter > 0 ):
					logger(job_log_filename, "Waiting 10 seconds for tomcat to restart..." )
					time.sleep(5)
					wait_counter -= 1

				logger(job_log_filename, "Tomcat restarted ..." )

				logger(job_log_filename, "Deleting /tmp/CAE_DOWN")

				remote_command(server_name, 'rm /tmp/CAE_DOWN')

				lb_list = []
				lb_list_query = '''select concat(hostname, '.', domain) from hostinfo where ib_system_type = 'balancer' and ib_env = '%s';''' % ( host_env  )
				if ( curr.execute (lb_list_query) ):
					for res_item in curr.fetchall():
						lb_list.append(res_item[0])

	
				logger(job_log_filename,  "Enabling load balancer for " + server_name )
				for lb_list_item in lb_list:
					logger(job_log_filename, ha.set_all_status( 'enable', lb_list_item, hostname ) )
					logger(job_log_filename, "Setting " + server_name + " to " + zenoss_short_name[host_env]  + "." )

				set_zenoss_state(hostname, zenoss_short_name[host_env])

				logger(job_log_filename, "Started tomcat on " + server_name + "." )

			if ( set_tomcat_status == "Stop" ):

				ha = happy_proxy()
			
				lb_list = []
				lb_list_query = '''select concat(hostname, '.', domain) from hostinfo where ib_system_type = 'balancer' and ib_env = '%s';''' % ( host_env  )
				if ( curr.execute (lb_list_query) ):
					for res_item in curr.fetchall():
						lb_list.append(res_item[0])

				logger(job_log_filename, "Setting " + server_name + " to maintenance." )

				set_zenoss_state(hostname, zenoss_short_name['maint'])


				logger(job_log_filename, "Disabling load balancer for " + server_name )
				for lb_list_item in lb_list:
					logger(job_log_filename, ha.set_all_status( 'disable', lb_list_item, hostname )	)
			
				logger(job_log_filename, "Touching /tmp/CAE_DOWN")
				remote_command(server_name, 'touch /tmp/CAE_DOWN')

				logger(job_log_filename, "Stopping tomcat on " + server_name )

				remote_command(server_name, 'sudo /etc/init.d/tomcat7 stop')
				while ( len ( remote_command(server_name, 'ps -ewwo pid,args | grep [t]omcat | grep java').rstrip() ) != 0 ):
					logger(job_log_filename, "Waiting 3 seconds for Tomcat to stop..." )
					time.sleep(3)

				logger(job_log_filename, "Stopped tomcat on " + server_name + "." )

			if ( set_tomcat_status == "Restart" ):

				ha = happy_proxy()
			
				lb_list = []
				lb_list_query = '''select concat(hostname, '.', domain) from hostinfo where ib_system_type = 'balancer' and ib_env = '%s';''' % ( host_env  )
				if ( curr.execute (lb_list_query) ):
					for res_item in curr.fetchall():
						lb_list.append(res_item[0])

				logger(job_log_filename, "Setting " + server_name + " to maintenance." )

				set_zenoss_state(hostname, zenoss_short_name['maint'])


				logger(job_log_filename, "Disabling load balancer for " + server_name )
				for lb_list_item in lb_list:
					logger(job_log_filename, ha.set_all_status( 'disable', lb_list_item, hostname )	)
			
				logger(job_log_filename, "Touching /tmp/CAE_DOWN")
				remote_command(server_name, 'touch /tmp/CAE_DOWN')

				logger(job_log_filename, "Stopping tomcat on " + server_name )

				remote_command(server_name, 'sudo /etc/init.d/tomcat7 stop')
				while ( len ( remote_command(server_name, 'ps -ewwo pid,args | grep [t]omcat | grep java').rstrip() ) != 0 ):
					logger(job_log_filename, "Waiting 3 seconds for Tomcat to stop..." )
					time.sleep(3)

				logger(job_log_filename, "Clearing war folders in webapps folder on " + server_name )
				remote_command(server_name, 'cd /var/lib/tomcat7/webapps; for i in `find . -mindepth 1 -maxdepth 1 -type d -not -name "ROOT"`; do if [ ! "$i" == "." ]; then sudo -u tomcat7 rm -rf $i; fi; done')

				logger(job_log_filename, "Clearing tomcat cache on server " + server_name )
				remote_command(server_name, 'sudo -u tomcat7 rm -rf /var/cache/tomcat7/Catalina/localhost/*')
			

				logger(job_log_filename, "Starting tomcat on " + server_name )

				remote_command(server_name, 'sudo /etc/init.d/tomcat7 start')

				wait_counter = 600

				while ( ping_tomcat( server_name ) == False and wait_counter > 0 ):
					logger(job_log_filename, "Waiting 10 seconds for tomcat to restart..." )
					time.sleep(5)
					wait_counter -= 1

				logger(job_log_filename, "Tomcat restarted ..." )

				logger(job_log_filename, "Deleting /tmp/CAE_DOWN")

				remote_command(server_name, 'rm /tmp/CAE_DOWN')
	
				logger(job_log_filename,  "Enabling load balancer for " + server_name )
				for lb_list_item in lb_list:
					logger(job_log_filename, ha.set_all_status( 'enable', lb_list_item, hostname ) )
					logger(job_log_filename, "Setting " + server_name + " to " + zenoss_short_name[host_env]  + "." )

				set_zenoss_state(hostname, zenoss_short_name[host_env])

				logger(job_log_filename, "Completed tomcat restart on " + server_name + "." )
			

		if ( 'set_zenoss_on' in task_item ):
			server_name = get_fqdn_from_hostname(task_item.split('_')[3])
			hostname = task_item.split('_')[3]
			zenoss_state = job_config['tasks']['select_zenoss_maint_mode']
			set_zenoss_window_state = job_config['tasks']['select_zenoss_maint_window']

			host_env = ''
			
			if ('dev' in hostname):			
				host_env = 'dev'

			if ('stage' in hostname):			
				host_env = 'stage'

			if ('qa' in hostname):			
				host_env = 'qa'

			if ('prod' in hostname):			
				host_env = 'prod'

			ha = happy_proxy()

			if (set_zenoss_window_state == "Delete"):
				logger( job_log_filename, zenoss.delete_maint_windows(hostname) )

			if (set_zenoss_window_state == "Set"):
				if ( 'set_zenoss_maint_window_start' in job_config['tasks'].keys() and 'set_zenoss_maint_window_end' in job_config['tasks'].keys() ):
					if ( len(job_config['tasks']['set_zenoss_maint_window_start']) > 0 and len(job_config['tasks']['set_zenoss_maint_window_end']) > 0 ):
						logger(job_log_filename, zenoss.set_maint_window(hostname, job_config['user_name'], job_config['tasks']['set_zenoss_maint_window_start'], job_config['tasks']['set_zenoss_maint_window_end'] ) )
		

			if (zenoss_state == "Reset" and not len(set_zenoss_window_state) ):
				lb_list = []
				lb_list_query = '''select concat(hostname, '.', domain) from hostinfo where ib_system_type = 'balancer' and ib_env = '%s';''' % ( host_env  )
				if ( curr.execute (lb_list_query) ):
					for res_item in curr.fetchall():
						lb_list.append(res_item[0])

				logger(job_log_filename,  "Setting " + server_name + " to maintenance." )

				set_zenoss_state(hostname, zenoss_short_name['maint'])

				logger(job_log_filename, "Disabling load balancer for " + server_name )
				for lb_list_item in lb_list:
					logger(job_log_filename, ha.set_all_status( 'disable', lb_list_item, hostname )	)

				logger(job_log_filename, "Enabling load balancer for " + server_name )
				for lb_list_item in lb_list:
					logger(job_log_filename, ha.set_all_status( 'enable', lb_list_item, hostname ) )	
			
				logger(job_log_filename, "Setting " + server_name + " to " + zenoss_short_name[host_env]  + "." )

				set_zenoss_state(hostname, zenoss_short_name[host_env])

				logger(job_log_filename, "Completed zenoss reset on " + server_name + "." )
			
			elif (zenoss_state == "Maintenance" and not len(set_zenoss_window_state) ):

				logger(job_log_filename,  "Setting " + server_name + " to maintenance." )
				lb_list = []
				lb_list_query = '''select concat(hostname, '.', domain) from hostinfo where ib_system_type = 'balancer' and ib_env = '%s';''' % ( host_env  )
				if ( curr.execute (lb_list_query) ):
					for res_item in curr.fetchall():
						lb_list.append(res_item[0])
	
					set_zenoss_state(hostname, zenoss_state)

				logger(job_log_filename, "Disabling load balancer for " + server_name )
				for lb_list_item in lb_list:
					logger(job_log_filename, ha.set_all_status( 'disable', lb_list_item, hostname )	)

			else:
				lb_list = []
				lb_list_query = '''select concat(hostname, '.', domain) from hostinfo where ib_system_type = 'balancer' and ib_env = '%s';''' % ( host_env  )
				if ( curr.execute (lb_list_query) ):
					for res_item in curr.fetchall():
						lb_list.append(res_item[0])

				logger(job_log_filename, "Enabling load balancer for " + server_name )
				for lb_list_item in lb_list:
					logger(job_log_filename, ha.set_all_status( 'enable', lb_list_item, hostname ) )	

				logger(job_log_filename,  "Setting " + server_name + " to " + zenoss_state  + "." )

				set_zenoss_state(hostname, zenoss_state)


		if ( 'reindex_mongo_on_' in task_item ):
			hostname = task_item.split('_')[3]
			server_name = get_fqdn_from_hostname(hostname)

			host_env = ''
			
			if ('dev' in hostname):			
				host_env = 'dev'

			if ('stage' in hostname):			
				host_env = 'stage'

			if ('qa' in hostname):			
				host_env = 'qa'

			if ('prod' in hostname):			
				host_env = 'prod'

			logger(job_log_filename, "Setting Mongo server " + server_name + " to Maintenance in Zenoss.")

			set_zenoss_state(hostname, zenoss_short_name['maint'])

			logger(job_log_filename, "Reindexing Mongo on Server " + server_name)

			logger(job_log_filename, "Dropping Mongo server " + server_name + " to secondary.")
	
			super_remote_command(server_name, '''sudo mongo --eval "rs.stepDown(60);"''')

			logger(job_log_filename, "Sleeping for 60 seconds for the switch to secondary.")

			time.sleep(60)			

			logger(job_log_filename, "Stopping Mongo server " + server_name)
			super_remote_command(server_name, '''sudo service mongodb stop''')
			
			mongo_counter = 12 

			while ( super_remote_command(server_name, 'pgrep mongod').rstrip() and mongo_counter > 0 ):
				logger(job_log_filename, "Waiting 5 seconds for Mongo to stop on " + server_name )
				time.sleep(5)
				mongo_counter -= 1
				if (mongo_counter == 0):
					logger(job_log_filename, "Waited 60 seconds and mongod won't die killing it! " + server_name )
					super_remote_command(server_name, '''sudo kill -9 $(pidof mongod)''')
					
			
			logger(job_log_filename, "Mongo stopped removing old indexes on " + server_name )
			
			super_remote_command(server_name, '''sudo rm -rf /var/lib/mongodb/* /var/log/mongodb/*''')
						
			time.sleep(5)

			logger(job_log_filename, "Restarting Mongo from " + server_name )
			super_remote_command(server_name, '''sudo service mongodb start''')

			
			while ( len ( super_remote_command(server_name, '''sudo tail -n 1000 /var/log/mongodb/mongodb.log | grep -Ev '(connection accept|end connection)' | grep 'replSet initial sync done'; ''').rstrip() ) == 0 ):
				logger(job_log_filename, "Mongo has indexed " + super_remote_command(server_name, '''echo -e "$(cd /var/lib/mongodb && du -s ) KB and $(find /var/lib/mongodb | wc -l) files"''') + server_name + " and reloading in 30 seconds." )
				time.sleep(30)
			
			logger(job_log_filename, "Mongo reindexing complete on " + server_name )

			logger(job_log_filename, "Setting Mongo server " + server_name + " back to " + zenoss_short_name[host_env] + " in Zenoss.")

			set_zenoss_state(hostname, zenoss_short_name[host_env])

			#super_remote_command(server_name, reindex_command)

		if ( 'content_model_update_on_' in task_item ):
			hostname = task_item.split('_')[3]
			server_name = get_fqdn_from_hostname(hostname)

			host_env = ''
			
			if ('dev' in hostname):			
				host_env = 'dev'

			if ('stage' in hostname):			
				host_env = 'stage'

			if ('qa' in hostname):			
				host_env = 'qa'

			if ('prod' in hostname):			
				host_env = 'prod'
		
			
	
	for pup_item in puppet_task_proc:
        	pup_item.join()


	logger(job_log_filename, "Job ID " + str(job_config['job_id']) + " completed." )


			
while True:
		

	try:
		
		for job_item in os.listdir("/opt/pydeployer/temp/services/"):
			jobs = pickle.load( open("/opt/pydeployer/temp/services/" + job_item, "rb") )

			print jobs
			print "Found Job file for " + job_item + " and will start processing."

			#service_worker(jobs)

			j = multiprocessing.Process( target = service_worker, args=( jobs, ) )
			j.start()

			os.remove("/opt/pydeployer/temp/services/" + job_item)
	
	except IOError:
		print "Another error occurred!"

	#print "Test"
	time.sleep(1)	
