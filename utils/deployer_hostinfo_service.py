#!/usr/bin/env python

import MySQLdb
import pycurl
import csv
from StringIO import StringIO

hostinfo_url="http://hostinfo-dal05.dal05.ib-util.com/hostinfo/csv/"

db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
curr = conn.cursor()

buf = StringIO()
c = pycurl.Curl()
c.setopt(c.URL, hostinfo_url)
c.setopt(c.WRITEFUNCTION, buf.write)

c.perform()

reader = csv.reader(StringIO(buf.getvalue()),quotechar='"')


rownum = 0
for row in reader:
    if rownum == 0:
        header = row
    else:
  
	query = """REPLACE INTO hostinfo VALUES ( "%s" );""" % ('","'.join(row) )

	curr.execute( query )
	conn.commit()

    rownum += 1

c.close()


