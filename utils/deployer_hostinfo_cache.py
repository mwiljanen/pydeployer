#!/usr/bin/env python

import MySQLdb
import pycurl
import csv
from StringIO import StringIO

hostinfo_url="http://hostinfo-dal05.dal05.ib-util.com/hostinfo/csv/"

db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
curr = conn.cursor()

empty_table = False
mysql_header_cnt = 0
try:
	if ( curr.execute('select * from hostinfo') ):
		mysql_header_cnt = len(curr.description)
		empty_table = False
except:
	pass



buf = StringIO()
c = pycurl.Curl()
c.setopt(c.URL, hostinfo_url)
c.setopt(c.WRITEFUNCTION, buf.write)

c.perform()

reader = csv.reader(StringIO(buf.getvalue()),quotechar='"')

c.close()

rownum = 0

for row in reader:
    if ( len(row) != mysql_header_cnt and not empty_table):
	#print "Different column amounts CSV: " + str(len(row)) + ", MySQL: " + str( mysql_header_cnt  )
	curr.execute('drop table if exists hostinfo ;')
	conn.commit()
	empty_table = True	

    if rownum == 0:
        header = row

	if (empty_table):
		header_list = []

		hostname_dup_fix = True
		for col_item in header:
			if ( 'hostname' in col_item and hostname_dup_fix ):
				header_list.append(" " + col_item.replace('-','_')[0:199] + "_2 VARCHAR(200) DEFAULT NULL ")
				hostname_dup_fix = False

			elif ( 'note' in col_item or 'interfaces' in col_item ):
				header_list.append(" " + col_item.replace('-','_')[0:199] + " VARCHAR(200) DEFAULT NULL ")

			elif ( 'applications' in col_item ):
				header_list.append(" " + col_item.replace('-','_') + " TEXT DEFAULT NULL ")

			else:
				header_list.append(" " + col_item.replace('-','_')[0:199] + " VARCHAR(200) DEFAULT NULL ")

		create_table_query = '''create table `hostinfo` ( %s ,
UNIQUE KEY `hostname` (`hostname`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1; ''' % ',\n'.join(header_list)
		#print create_table_query
		curr.execute( create_table_query )
		conn.commit()
    else:

	try:
		query = """REPLACE INTO hostinfo VALUES ( "%s" );""" % ('","'.join(row) )

		curr.execute( query )
		conn.commit()
	except:
		pass
    rownum += 1



