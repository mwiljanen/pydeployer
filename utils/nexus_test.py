#!/usr/bin/env python

import pycurl
import cStringIO
from xml.dom import minidom

from datetime import datetime, timedelta

import MySQLdb

import sys


def ping_artifact(hostname, artifact):
        try:
                conn = pycurl.Curl()
                buff = cStringIO.StringIO()
                conn.setopt(conn.URL, "http://" + hostname + ":8080/" + artifact + "/monitor/version/xml")
                conn.setopt(conn.WRITEFUNCTION, buff.write)
                conn.setopt(conn.TIMEOUT, 5)
                conn.perform()
		if ( conn.getinfo(pycurl.HTTP_CODE) == 200):
	                return True
		else:
			return False
        except:
                return False


db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

nexus_root_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/service/local/repo_groups/public/content/"

artifact_branch_list = [
'com.ibsys.ibp2',
'com.ibs',
'com.ibsys.common',
'com.ibsys.ens',
'com.ibsys.dashboard',
'com.ibsys.rest',
'com.ibsys.video',
'com.ibsys.importer',
'com.ibsys.toolshed',
'com.ibsys.app',
'com.ibsys.webscan',
'com.ibsys' ]

conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
curr = conn.cursor()

artifact_str = sys.argv[1]

artifact_list = []
artifact_dict = { 'artifact_id': '', 'env': '', 'jira': '', 'version': '' }

server_deploy_list = []


for art_str_item in artifact_str.split(','):
	print art_str_item
	artifact_dict['artifact_id'] = art_str_item.split(':')[0]
	artifact_dict['env'] = art_str_item.split(':')[1]
	artifact_dict['version'] = art_str_item.split(':')[2]
	artifact_dict['jira'] = art_str_item.split(':')[3]

	deploy_servers = []
	shortname = '' 

	if ( 'cae' in artifact_dict['artifact_id'] ):
		shortname = artifact_dict['artifact_id'].split('-')[0]
		get_artifact_query = '''select concat( hostname, '.', domain ) from hostinfo where ib_system_type = 'cae' and ib_env = '%s';''' % ( artifact_dict['env'] )

		if ( curr.execute( get_artifact_query ) ):
			for res_item in curr.fetchall():
				if ( ping_artifact( res_item[0], shortname ) ):
					print "Found artifact " + shortname + " and adding it for deployment to server " + res_item[0]
					deploy_servers.append( res_item[0] )

	for artifact_group_item in artifact_branch_list:

		c = pycurl.Curl()
		buf = cStringIO.StringIO()

		c.setopt(c.URL, nexus_root_url + artifact_group_item.replace('.', '/') + '/' )
		c.setopt(c.WRITEFUNCTION, buf.write)
		c.perform()

		xmldoc = minidom.parseString(buf.getvalue())

		for content_item in xmldoc.getElementsByTagName('content-item'):
			artifact_nexus_name = content_item.getElementsByTagName('text')[0].firstChild.nodeValue
			if ( shortname in artifact_nexus_name ):
				print content_item.getElementsByTagName('text')[0].firstChild.nodeValue
				tmp_date = content_item.getElementsByTagName('lastModified')[0].firstChild.nodeValue.split('.')[0]
				if ( datetime.strptime(tmp_date, "%Y-%m-%d %H:%M:%S") > ( datetime.today() - timedelta(days=365) ) ):
					print "URL: " + content_item.getElementsByTagName('resourceURI')[0].firstChild.nodeValue
				#print "Relative Path: " + content_item.getElementsByTagName('relativePath')[0].firstChild.nodeValue





#content_item

#for last_mod_item in xmldoc.getElementsByTagName('lastModified'):
	#print datetime(last_mod_item.firstChild.nodeValue) - datetime.now()
	#tmp_date = last_mod_item.firstChild.nodeValue.split('.')[0]
	#if ( datetime.strptime(tmp_date, "%Y-%m-%d %H:%M:%S") > ( datetime.today() - timedelta(days=90) ) ):
		#print tmp_date


#print datetime.today().day


#print buf.getvalue()

