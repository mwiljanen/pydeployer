#!/usr/bin/env python
import pycurl
import cStringIO

class haproxy_client:
	def connect(self, target_lb, target_backend, target_host):
		self.target_lb = target_lb
		self.target_backend = target_backend
		self.target_host = target_host

		self.conn = pycurl.Curl()
		self.buff = cStringIO.StringIO()

		self.conn.setopt(self.conn.URL, "http://" + self.target_lb + ":8000/haproxy")
		self.conn.setopt(self.conn.USERPWD, "haproxy:stats")


	def set_status( self, status ):
		num_backends = 0
		
		self.conn.setopt(self.conn.WRITEFUNCTION, self.buff.write)
		self.conn.perform()

		backend_filter = '''<tr class="titre"><th class="pxname" width="10%"><a name="'''
		form_filter = '''<form action="/haproxy" method="post"><table class="tbl" width="100%">'''

		status_changed = False

		for line_get in self.buff.getvalue().split('\n'):
			if ( backend_filter in line_get ):

				backend_name = line_get[(line_get.find(backend_filter) + len(backend_filter) ):].split('''"''')[0]
				if (backend_name == self.target_backend):

					perform_task = '''s=%s&action=%s&b=%%23%d''' % ( self.target_host, status, num_backends + 1 )

					#print "Host " + self.target_host + " on LB " + backend_name + " is now " + status + "d"

					self.conn.setopt(self.conn.POSTFIELDS, perform_task)
					#self.conn.setopt(self.conn.WRITEFUNCTION, buf.write)
					self.conn.perform()
			
					status_changed = True
					
					break
				num_backends += 1

		return status_changed


	def get_status( self ):

		self.conn.setopt(self.conn.WRITEFUNCTION, self.buff.write)
		self.conn.perform()

		for line_get in self.buff.getvalue().split('\n'):
		        if( self.target_backend in line_get and self.target_host in line_get):
                		status_output = ""
				status = line_get.split('''<tr class="''')[1][0:7]
				if ( status == 'active3' ):
					status_output = "UP"
				if ( status == 'active2' ):
					status_output = "UP going DOWN"
				if ( status == 'active1' ):
					status_output = "DOWN going UP"
				if ( status == 'active0' ):
					status_output = "DOWN"
				if ( status == 'maintai' ):
					status_output = "MAINT"

				return status_output

