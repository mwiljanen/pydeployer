CREATE TABLE `service_restart_job` (
  `id` int(16) unsigned NOT NULL AUTO_INCREMENT,
  `uniqueId` char(64) NOT NULL,
  `jobId` char(63) NOT NULL,
  `artifactEnvId` int(8) unsigned NOT NULL,
  `status` tinyint(1) unsigned DEFAULT NULL,
  `job_config` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `artifactJobs` (`jobId`,`artifactEnvId`),
  KEY `artifactEnvId` (`artifactEnvId`),
  CONSTRAINT `jobs_ibfk_1` FOREIGN KEY (`artifactEnvId`) REFERENCES `artifactEnvironments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22868 DEFAULT CHARSET=utf8
