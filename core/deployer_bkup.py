#!/usr/bin/env python
### Project:  IB Deployer v3.0.1
### Dev:  Matthew Wiljanen
### Date: 2014-09-16


### Current Libraries needed to run deployer

from bottle import request, debug, route, run, get, post, template, error, static_file, response, redirect, app, abort, view, template
from beaker.middleware import SessionMiddleware
import MySQLdb
import ldap
import datetime
import json
import pycurl
import cStringIO
from xml.dom import minidom
import re
import time
import math
import os
import gearman
import pickle


### Load deployer config files


configs = {}

deployer_config_path = "/etc/ibconf/deployer/"

deployer_worker_path = "/tmp/deployer/"

configs['deployer'] = open(deployer_config_path + "deployer.ini" , "r").read()
configs['services'] = open(deployer_config_path + "services.ini" , "r").read()

### Stored queries pulled from old deployer version

query_dict = {
	'dash_currentdeps': """SELECT `processes`.*, `logs`.* FROM `processes`  INNER JOIN `logs` ON processes.uniqueId = logs.uniqueId WHERE (processes.type = 'client')""",
	'dash_servers': """SELECT `serversInDeployment`.* FROM `serversInDeployment`""",
	'dash_artifacts': """SELECT `artifacts`.*, `artifactEnvironments`.* FROM `artifacts` INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1)""",
}

### User session storage

session_options = {   
                      'session.type': 'file',
                      'session.data_dir': './session/',
                      'session.auto': True,
                  }



### Initialization of user session

app_middleware = SessionMiddleware(app(), session_options)
app_session = request.environ.get('beaker.session')

### Default MySQLdb connect list

db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

### AD / LDAP connection strings for User Authentication

LDAP_SERVER = "ldap://adproxy.sl.ibsys.com"
BIND_DN = "ibsys.com"
#USER_BASE = "ou=Users,ou=Development,ou=IB,dc=ibsys,dc=com"
USER_BASE = "ou=IB,dc=ibsys,dc=com"

### Sonatype Nexus project path URL for maven XML files

nexus_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/content/groups/public/com/ibsys/"

### Hostinfo URL used for testing, implemented internal MySQL hostinfo instead

hostinfo_url = "http://hostinfo-dal05.dal05.ib-util.com/hostinfo/csv/"

### User Stats stored from a successful login and used in page templates

user_stats = {'first_name':'Matthew', 'user_name':'mwiljanen' , 'full_name':'Wiljanen, Matthew', 'deployer_roles':[]}

### Deployer dictionary array

deployer_jobs = {}

### Deployer log path /var/log/ib/deployer

deployer_log_path = "/var/log/ib/deployer"

def logger(text_input="", debug_level="INFO", module="Deployer\Logger"):
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	
	return '[%s (CDT)] deployer.%s [%s] - %s []\n' % (now, debug_level, module, text_input)


#retrieve a config value from dictionary

def get_config_values(tmp_config="", get_value=""):
	for tmp_line in tmp_config.split('\n'):
		if ( get_value in tmp_line ):
			return tmp_line.split(' = ')[1]

### uniqid function derived from PHPs version

def uniqid():
	return "%14x.%08x" % ( math.modf(time.time())[1]*10000000 + math.modf(time.time())[0] * 10000000, (math.modf(time.time())[0]-math.floor(math.modf(time.time())[0]))*1000000000 )


def remove_values_from_list(the_list, val):
	while val in the_list:
		the_list.remove(val)
	return the_list

### this validates a user against IB's AD system

def validate_user(username="", password=""):
	try:
		user_exists = False
		ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, 0)
		ldap_connection = ldap.initialize(LDAP_SERVER)
		ldap_connection.simple_bind_s(username + "@" + BIND_DN, password.rstrip())
		results = ldap_connection.search_s(USER_BASE, ldap.SCOPE_SUBTREE, "(CN=*)")
		for dn, entry in results:
			if ( username in entry['sAMAccountName'][0] ):
				user_exists = True
				user_stats['user_name'] = entry['sAMAccountName'][0]			
				user_stats['full_name'] = entry['name'][0]
				user_stats['first_name'] = entry['givenName'][0]
				for member_entries in entry['memberOf']:
					if ('deployer' in member_entries):
						user_stats['deployer_roles'].append(member_entries.split(',')[0].split('CN=')[1].split('-')[1])
						
				print user_stats['deployer_roles']

				break

		return user_exists

	except ldap.LDAPError, e:
		#return 'Error connecting to LDAP server: ' + str(e) + '\n'
		return False



def get_nexus_versions(artifact_name, artifact_env):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	get_nexus_query = """select nexus_metadata.versions from nexus_metadata inner join artifacts on nexus_metadata.artifactId = artifacts.artifactId where artifacts.name = '%s';""" % ( artifact_name )

	if ( curr.execute(get_nexus_query) ):
		nexus_meta_data = curr.fetchone()

		#print nexus_meta_data[0]

        	versions = []

		for item in nexus_meta_data[0].split('|'):
			if ( (artifact_env == 'qa' or artifact_env == 'prod' or artifact_env == 'stage') and 'SNAPSHOT' not in item):
				versions.append( item )
			elif ( artifact_env == 'dev' ):
				versions.append( item )

                return versions


### This pulls a list of artifacts from the database


@route('/')
@route('/login')
def login():
        return template('login', invalid_login=False)
	

@post('/login')
def do_login():
	if ( request.forms.get("username") and request.forms.get("password") ):
		if ( validate_user(request.forms['username'], request.forms['password']) ):
			app_session = request.environ.get('beaker.session')
    			app_session['logged_in'] = True

			redirect('/dashboard')
		else:	
			return template('login', invalid_login=True)

@route('/auth/logout')
@route('/logout')
def logout():
	app_session = request.environ.get('beaker.session')
	if app_session.get('logged_in'):
		app_session['logged_in'] = False
		user_stats = {'first_name':'', 'user_name':'' , 'full_name':''}
	redirect('/login')


@route('/dashboard')
def get_dashboard():
	return '''
	<style> @import url(/css/deployer.css); </style>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a href="/dashboard"><li class="nav_item_sel">Dashboard</li></a>
		<a href="/deploy"><li class="nav_item">Deploy</li></a>
		<a href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a href="/history"><li class="nav_item">History</li></a>
		<li class="nav_item">Admin vvv</li>
		<a href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

	<div class="content">
		<h1>Dashboard</h1>
		<hr>
		<h2>Current Activity<h2>


		<iframe src="/dashboard/currentdeps" width="100%" height="100%" fraclass='dep_list'meborder=0></iframe>

	</div>
	'''

@route('/dashboard/currentdeps')
def get_dash_currentdeps():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	deps_query = '''SELECT artifactEnvironments.currentUniqueId, artifactEnvironments.lastDeployedDate,  artifactEnvironments.artifactName  FROM `artifacts`  INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1);'''

	dep_count_query = '''SELECT DISTINCT currentUniqueId FROM artifactEnvironments WHERE isDeploying = 1;'''

	dep_count = 0

	if ( curr.execute(dep_count_query) ):	
		dep_count = len( curr.fetchall() )
	output_str = '''
	<head> <META http-equiv="refresh" content="5;URL=/dashboard/currentdeps"> </head>
	<style>  @import url(/css/deployer.css);  </style>
	'''

	output_str += '''

Deployments &nbsp; <span class="counter">&nbsp;%d&nbsp;</span>

<br>

<div class="div-table">
	<div class="div-table-row">
		<div class="div-table-head-col">Unique ID</div><div class="div-table-head-col">Start Date/Time	</div><div class="div-table-head-col">Selections</div><div class="div-table-head-col">Status</div>
	</div>

	''' % ( dep_count  )

	if ( curr.execute(deps_query) ):	
		cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }
		all_res = curr.fetchall()


		for res_row in all_res:
			#print  cur_deps_dict

			if ( len(cur_deps_dict['uniqueId']) == 0 ):
				cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }

				cur_deps_dict['uniqueId'] = res_row[0]
				cur_deps_dict['start_time'] = res_row[1]
				cur_deps_dict['selections'].append(res_row[2])
				cur_deps_dict['status'] = ''


			elif ( cur_deps_dict['uniqueId'] == res_row[0] ):
				cur_deps_dict['selections'].append(res_row[2])

			
			elif ( cur_deps_dict['uniqueId'] != res_row[0] and len(cur_deps_dict['uniqueId']) > 0):
			
				output_str += '''
		<div class="div-table-row">
			<div class="div-table-col">%s</div>
        	        <div class="div-table-col">%s</div>
        	        <div class="div-table-col">%s</div>
        	        <div class="div-table-col">Status</div>
		</div>
				''' % ( cur_deps_dict['uniqueId'], cur_deps_dict['start_time'], '<br>'.join(cur_deps_dict['selections']) )
				
				cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }

				cur_deps_dict['uniqueId'] = res_row[0]
				cur_deps_dict['start_time'] = res_row[1]
				cur_deps_dict['selections'].append(res_row[2])
				cur_deps_dict['status'] = ''


		output_str += '''
                <div class="div-table-row">
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">Status</div>
                </div>
                ''' % ( cur_deps_dict['uniqueId'], cur_deps_dict['start_time'], '<br>'.join(cur_deps_dict['selections']) )
			
	output_str += '''

</div>
	'''

	art_count_query = '''select artifactName, env from artifactEnvironments where isDeploying = 1 ORDER BY artifactName;'''

	art_count = 0

	art_cnt_output = []

	if ( curr.execute(art_count_query) ):	
		res = curr.fetchall()
		art_count = len( res )
		for art_item in res:
			art_cnt_output.append( "<li>" + art_item[0] + " : " + art_item[1].upper() + "</li>" ) 

	output_str += '''
	<br>	
	<div class="current_artifacts">
	Artifacts &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	''' % ( art_count, '\n'.join(art_cnt_output) )


	server_query = '''SELECT * FROM serversInDeployment;'''

	server_count = 0

	serv_cnt_output = []

	if ( curr.execute(server_query) ):	
		res = curr.fetchall()
		server_count = len( res )
		for serv_item in res:
			serv_cnt_output.append( "<li>" + serv_item[0] + "</li>" ) 

	output_str += '''
	

	<div class="current_servers">
	Servers &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	
	''' % ( server_count, '\n'.join( serv_cnt_output ) )


	return output_str


@route('/deploy')
def get_deploy():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()


	current_art_num = 0

	if ( 'next_art_num' in request.query_string ):
		current_art_num = request.query['next_art_num']

	option_list_query = "select artifactName, env from artifactEnvironments ORDER BY env, artifactName;"

	current_select = 0

	artifact_list = []

	option_list = []

	version_list = []

	artifact_form = ""



	if ( 'artifact' in request.query_string ):
		art_update_cnt = 0
		for art_key in request.query.keys():
			if ( 'artifact[' in art_key ):
				artifact_list.append( request.query[ ('artifact[%d]' % art_update_cnt) ] )
				art_update_cnt += 1

	if ( 'working_artifact' in request.query_string and 'add_artifact' in request.query_string):
		artifact_list.append( request.query['working_artifact'] + ":" + request.query['version_to_deploy'] + ":" + request.query['jira_key'])	


	if ( 'deploy_button' in request.query_string ):
		if ( len(artifact_list) == 0 ):
			artifact_form += "<p style='color: red; font-size: xx-large;'>Error: No artifacts were selected!</p>"
		else:
			redirect( '/deploy/index?' + request.query_string )

	if ( 'remove_artifact_' in request.query_string ):
		rem_pos = request.query_string.find('remove_artifact_') + len("remove_artifact_")
		rem_art_pos = int(request.query_string[rem_pos:rem_pos+5].split('.')[0])
		del artifact_list[rem_art_pos]

	if( len(artifact_list) > 0 ):
		temp_update = {}
		art_cnt = 0
		for cur_art_item in artifact_list:
			#cur_art_item = request.query[ 'artifact[' + str( art_update_cnt ) + ']' ]
			temp_update['artifact_key'] = cur_art_item
			temp_update['curr_art_num'] = art_cnt
			temp_update['artifact_name'] = cur_art_item.split(':')[0]
			temp_update['artifact_env'] = cur_art_item.split(':')[1]
			temp_update['version'] = cur_art_item.split(':')[2]
			temp_update['jira_key'] = cur_art_item.split(':')[3]

			artifact_form += '''

		<div class="div-table-row">
			<div class="div-table-col-art"><input type='image' name='remove_artifact_%(curr_art_num)d' class='del_button' src='/images/blank.png'></a>%(artifact_name)s</div>
			<div class="div-table-col-env"> %(artifact_env)s </div>
			<div class="div-table-col"> %(version)s </div>
			<div class="div-table-col"> %(jira_key)s 
			<input type='hidden' name='artifact[%(curr_art_num)s]' value='%(artifact_key)s'></div>
		</div>

			''' % temp_update 

			art_cnt += 1


	if ( 'update_artifact' in request.query_string ):
		current_select = 1 

	if( curr.execute( option_list_query ) ):
		for art_opt_item in curr.fetchall():
			option_list.append( '''<option>%s</option>''' % ( art_opt_item[0] + " : " + art_opt_item[1] ) )
	
	if ( current_select == 0 ):


		artifact_form += '''
	<div class="div-table-row">
		<div class="div-table-col-art"><select name='current_artifact_env' class='drop_down'>%s</select></div>
		<div class="div-table-col-env"><input type='submit' name='update_artifact' class='update_button' value='>'></div>
		<div class="div-table-col"></div>
		<div class="div-table-col"></div>
	</div>
		''' % ( ''.join(option_list)  )

	
	if ( current_select == 1 ):


		curr_art = ''.join(request.query['current_artifact_env']).split(':')[0].replace(' ','')
		curr_env = ''.join(request.query['current_artifact_env']).split(':')[1].replace(' ','')

		version_list.append( '''<option>%s</option>''' % ( "latest" ) )

		for ver_opt_item in get_nexus_versions(curr_art, curr_env):
			version_list.append( '''<option>%s</option>''' % ( ver_opt_item ) )
		
		form_update = {}
		form_update['artifact_name'] = curr_art
		form_update['artifact_env'] = curr_env
		form_update['next_art_num'] = current_art_num + 1
		form_update['versions'] = ''.join(version_list)
		form_update['working_artifact'] = curr_art + ":" + curr_env

		artifact_form += '''

	<div class="div-table-row">
		<div class="div-table-col-art">%(artifact_name)s</div>
		<div class="div-table-col-env">%(artifact_env)s </div>
		<div class="div-table-col"><select name='version_to_deploy' class='drop_down'>%(versions)s</select></div>
		<div class="div-table-col"><input type='text' name='jira_key'>
		<input type='hidden' name='working_artifact' value='%(working_artifact)s'>
		<input type='hidden' name='next_art_num' value='%(next_art_num)s'>

		<input type='submit' name='add_artifact' value='Add Item'></div>
	</div>
		''' % form_update


	return '''

<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a href="/deploy"><li class="nav_item_sel">Deploy</li></a>
		<a href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a href="/history"><li class="nav_item">History</li></a>
		<li class="nav_item">Admin vvv</li>
		<a href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h1>Deploy</h1>
<hr>
	<div class="deploy_content">
		<div class="artifact_selections">
		<form method='GET' action='/deploy'>
		  <div class="div-table">
		    <div class="div-table-row">
		      <div class="div-table-head-col">Artifact</div><div class="div-table-head-col">Environment</div><div class="div-table-head-col">Version</div><div class="div-table-head-col">JIRA Issue Key</div>
		    </div>
			%s
		<hr>
		
		<div class='deploy_opt_container'>
			<div class="deploy_opt_image"> 
			Deployment Options
	
			<input class="deploy_opt_image" id='deploy_btn' type='checkbox' > 
			<label for='deploy_btn' class='deploy_label_button'>&nbsp;&nbsp;?&nbsp;&nbsp;</label>

			<br>
			<br>
			<div class='deploy_options_box'>

			<div class='dep_opt_exluded_servers'>

				Servers to Exclude 
				<textarea cols='50' rows='10'></textarea>

			</div>

			<div class='total_dep_ops'>

			Options:

			<ul>
        	            <li class='depl_opts'><input name="skipVersionChecks" title="Skips post-deployment version verification. Will force deployment to all servers for selected artifacts." type="checkbox" value="1"/> Skip Version Checks</li>
			    <li class='depl_opts'>
                        	<input  id="skipLbOps" name="skipLbOps"
                               		title="Skips load balancer pool operations." type="checkbox" value="1"
	                            /> Skip Load Balancer Operations
			    </li>
			    <li class='depl_opts'>
				<input  id="skipDepDeploy" name="skipDepDeploy"
					   title="Skips the deployment of artifact dependencies." type="checkbox" value="1"
					/> Skip Artifact Dependency Deployment
			    </li>
			    <li class='depl_opts'>
                	        <input  id="ignoreZenossProdState" name="ignoreZenossProdState"
                        	       title="Ignore initial Zenoss production state; will allow deployment to servers regardless of Zenoss production state." type="checkbox" value="1"
	                            /> Ignore Zenoss Production State
			    </li>
			    <li class='depl_opts'>
				<input  id="resetZenossProdState" name="resetZenossProdState"
				   title="Ignore initial Zenoss production state AND reset Zenoss production states based on deployment environment" type="checkbox" value="1"
							/> Reset Zenoss Production State
			    </li>
			    <li class='depl_opts'>
				<input  id="ignoreArtifactDeployStatus" name="ignoreArtifactDeployStatus"
				   title="Ignore artifact deployment status; will allow concurrent deployments of the same artifact-environment combination." type="checkbox" value="1"
				/> Ignore Artifact Deployment Status
			    </li>
			    <li class='depl_opts'>
                	        <input  id="clearStatus" name="clearStatus"
                        	       title="Last deployment failed or aborted? This clears the deployment status for the selected artifacts and their servers." type="checkbox" value="1"
	                            /> Clear Deploy Status
        	            </li>

                	    <li class='depl_opts'>
	                        <input  id="debug" name="debug" type="checkbox" value="1"
        	                     title="Toggle debug logging" /> Debug
                	    </li>

				<br>
				<li class='depl_opts'>
				Manage Puppet Agent
				<br>
	                        <input  id="puppetAgent0" name="puppetAgent" type="radio" value="1" value='none'/><label for = 'puppetAgent0'>None</label>
	                        <input  id="puppetAgent1" name="puppetAgent" type="radio" value="1" value='stop'/><label for = 'puppetAgent1'>Stop</label>
	                        <input  id="puppetAgent2" name="puppetAgent" type="radio" value="1" value='start'/><label for = 'puppetAgent2'>Start</label>
	                        <input  id="puppetAgent3" name="puppetAgent" type="radio" value="1" value='restart'/><label for = 'puppetAgent3'>Restart</label>
			
				</li>
				
				<br>
				<li class='depl_opts'>
				Manage Service
				<br>
	                        <input  id="manageService0" name="manageService" type="radio" value="1" value='none'/><label for = 'manageService0'>None</label>
	                        <input  id="manageService1" name="manageService" type="radio" value="1" value='stop'/><label for = 'manageService1'>Stop</label>
	                        <input  id="manageService2" name="manageService" type="radio" value="1" value='start'/><label for = 'manageService2'>Start</label>
	                        <input  id="manageService3" name="manageService" type="radio" value="1" value='restart'/><label for = 'manageService3'>Restart</label>
				</li>
				</div>
			</div>

		</div>		
		
		</div>


		  </div>

		<hr>
		<div class='deploy_button_box'>
			<div class='safe_button_box'>
				<span style='padding-left: 15%%;'>SAFE MODE</span>
				<br>
				<input class="safe_button_chk" id='safe_mode_btn_on' name='safe_mode_run' type='radio' value='on' checked> 
				<label for='safe_mode_btn_on' class='safe_button_lbl'>On</label>
				<input class="safe_button_chk" id='safe_mode_btn_off' name='safe_mode_run' type='radio' value='off'> 
				<label for='safe_mode_btn_off' class='safe_button_lbl'>Off</label>

			</div>
			<div class='deploy_button'><span style='padding-left: 10%%; font-size: xx-large'>DEPLOY
			<input type='image' name='deploy_button' src='/images/blank.png' style='position: absolute; top: 0px; left: 0px; width: 100%%; height: 100%%;' /></span> </div>
		</div>
		
		</form>

		</div>

		<div class="current_activity">
			Current Activity
			<hr>
			<iframe src="/deploy/current_activity" width="100%%" height="100%%" frameborder=0></iframe>
		</div>
	</div>
</div>
</body>
</html>
	''' % ( artifact_form )


@route('/deploy/add_artifact')
def add_artifact():
	redirect('/deploy?' + request.query_string)

@route('/deploy/index')
def deploy_index():
	option_str = ''

	for key_item in request.query.keys():
		print key_item
		option_str += key_item + " = " + request.query[ key_item ] + "<br>"

	return option_str

@route('/deploy/current_activity')
def current_activity():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	art_count_query = '''select artifactName, env from artifactEnvironments where isDeploying = 1 ORDER BY artifactName;'''

	art_count = 0

	art_cnt_output = []

	if ( curr.execute(art_count_query) ):	
		res = curr.fetchall()
		art_count = len( res )
		for art_item in res:
			art_cnt_output.append( "<li class='dep_list'>" + art_item[0] + " : " + art_item[1].upper() + "</li>" ) 

	return '''
	<head> <META http-equiv="refresh" content="5;URL=/deploy/current_activity"> </head>
	<style> @import url(/css/deployer.css); </style>	
	<div class="current_artifacts">
	Artifacts &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	''' % ( art_count, '\n'.join(art_cnt_output) )


@route('/artifacts')
def show_artifacts():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	search_str = ''

	if ('artifact_search' in request.query_string):
		search_str = request.query['artifact_search']
		

	get_artifacts_query = '''SELECT artifactName, env, lastDeployedVersion, lastDeployedUser, lastDeployedDate FROM artifactEnvironments WHERE artifactName like '%%%s%%' ORDER BY artifactName;''' % search_str

	output_artifact = []
	
	if ( curr.execute(get_artifacts_query) ):
		artifact_result = curr.fetchall()
		alter_row = 0

		artifact_name = ''

		output_dict = { 'artifact': '', 'env': [], 'env_dev': '', 'env_qa': '', 'env_stage': '', 'env_prod': '' }

		for art_item in artifact_result:
			if ( output_dict['artifact'] != art_item[0] and len(art_item[0]) > 0):

				env_div_list = []
				for env_div_item in sorted(set(output_dict['env'])):
					env_div_list.append('''<div class='art_env_cell_%s' title='%s'>%s<span style="background: #3a87ad; font-size: smaller; color: white;">%s</span></div>''' % ( env_div_item,  output_dict['env_' + env_div_item] , env_div_item.upper(), output_dict['env_' + env_div_item + '_version'] ) )

				output_row = '''
			<div class="artifact_table_row">
			      <div class="artifact_table_col_name_%d">%s</div><div class="artifact_table_col_env_%d">%s</div>
			</div>
				''' % ( alter_row, output_dict['artifact'], alter_row,  '\n'.join(env_div_list) )

				if ( alter_row == 0 ):
					alter_row = 1
				else:
					alter_row = 0

				output_artifact.append( output_row  )

				output_dict['artifact'] = art_item[0]
				output_dict['env'] = []
				
				#output_dict['env'].append( art_item[1] )
				#output_dict['env_stats'].append ( str(art_item[2]) + str(art_item[3]) + str(art_item[4])  ) 

			if ( output_dict['artifact'] == art_item[0] ):
				output_dict['env'].append(art_item[1])
				if ( art_item[2] ):
					output_dict['env_' + art_item[1] ] = '''Last deployed: Version %s by %s on %s''' % (  art_item[2], art_item[3], art_item[4] )
				else:
					output_dict['env_' + art_item[1] ] = ''
				
				#output_dict['env_' + art_item[1] ] = str(art_item[2]) + str(art_item[3]) + str(art_item[4])
				output_dict['env_' + art_item[1] + '_version' ] = str(art_item[2])

	
		env_div_list = []
		for env_div_item in sorted(set(output_dict['env'])):
			env_div_list.append('''<div class='art_env_cell_%s' title='%s'>%s<span style="background: #3a87ad; font-size: smaller; color: white;">%s</span></div>''' % ( env_div_item,  output_dict['env_' + env_div_item] , env_div_item.upper(), output_dict['env_' + env_div_item + '_version'] ) )
		#for env_div_item in sorted(set(output_dict['env'])):
			#env_div_list.append('''<div class='art_env_cell_%s'>%s</div>''' % ( env_div_item, env_div_item.upper() ) )

		output_row = '''
			<div class="artifact_table_row">
			      <div class="artifact_table_col_name_%d">%s</div><div class="artifact_table_col_env_%d">%s</div>
			</div>
		''' % ( alter_row, output_dict['artifact'], alter_row,  '\n'.join(env_div_list) )

		if ( alter_row == 0 ):
			alter_row = 1
		else:
			alter_row = 0

		output_artifact.append( output_row  )

		
		#print str(output_dict)

	return '''<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a href="/deploy"><li class="nav_item">Deploy</li></a>
		<a href="/artifacts"><li class="nav_item_sel">Artifacts</li></a>
		<a href="/history"><li class="nav_item">History</li></a>
		<li class="nav_item">Admin vvv</li>
		<a href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h1>Artifacts</h1>
<hr>
	<div class="artifact_content">
		    <div class="artifact_title_bar">
			<form  method='get' action='/artifacts'>
			  <input type='submit' name='artifact_search_submit' value='Search'>
			  <input type='text' name='artifact_search'>
			</form>
		    </div>
		   <div class="artifact_scroll">
		   
		       <div class="artifact_table">
		    	    <div class="artifact_table_row">
		                  <div class="artifact_table_col_head_name">Name</div><div class="artifact_table_col_head_env">Environment</div>
		            </div>
		            %s
		   </div>
	        </div>
	</div>
</body>
</html>
	''' % ( '\n'.join(output_artifact) )


@route('/history')
def get_history():

	return '''<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a href="/deploy"><li class="nav_item">Deploy</li></a>
		<a href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a href="/history"><li class="nav_item_sel">History</li></a>
		<li class="nav_item">Admin vvv</li>
		<a href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h1>History</h1>
<hr>
	<div class="artifact_content">
		    <div class="artifact_title_bar">
			<form  method='get' action='/artifacts'>
			  <input type='submit' name='artifact_search_submit' value='Search'>
			  <input type='text' name='artifact_search'>
			</form>
		    </div>
		   <div class="artifact_scroll">
		   
		       <div class="artifact_table">
		    	    <div class="artifact_table_row">
		                  <div class="artifact_table_col_head_name">Name</div><div class="artifact_table_col_head_env">Environment</div>
		            </div>
		   </div>
	        </div>
	</div>
</body>
</html>
	'''



# Static Routes
@get('/js/<filename:re:.*\.js>')
def javascript(filename):
	return static_file(filename, root='public/js')

@get('/css/<filename:re:.*\.css>')
def stylesheets(filename):
	return static_file(filename, root='public/css')

@get('/css/ib-brand/images/<filename:re:.*\.(png|jpg|gif|jpeg|ico)>')
def ibcustom(filename):
	return static_file(filename, root='public/css/ib-brand/images/')

@get('/css/smoothness/images/<filename:re:.*\.(png|jpg|gif|jpeg|ico)>')
def ibcustom(filename):
	return static_file(filename, root='public/css/ib-brand/images/')

@get('/images/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
	return static_file(filename, root='public/images')

@get('/bootstrap/img/<filename:re:.*\.(png|jpg|gif|jpeg)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/img')

@get('/bootstrap/js/<filename:re:.*\.(js)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/js')

@get('/bootstrap/css/<filename:re:.*\.(css)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/css')

@get('/cache/<filename:re:.*\.(js|css)>')
def cache(filename):
	return static_file(filename, root='public/cache')

@get('/screenshots/<unique_id>/<filename>')
def screenshots(unique_id, filename):
	return static_file(filename, root='public/screenshots/' + unique_id )

@error(404)
def error404(error):
	return "Oops!<br>Unable to find page<br>"



debug(True)
run(app=app_middleware, host='0.0.0.0', port=18082, reloader=True)
