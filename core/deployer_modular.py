#!/usr/bin/env python
### Project:  IB Deployer v3.0.0.1
### Dev:  Matthew Wiljanen
### Date: 2014-10-02

### Current Libraries needed to run deployer

from bottle import request, debug, route, run, get, post, template, error, static_file, response, redirect, app, abort, view, template
from beaker.middleware import SessionMiddleware
import MySQLdb
import ldap
import datetime
import json
import pycurl
import cStringIO
from xml.dom import minidom
import re
import time
import math
import os
import gearman
import pickle
import ast
from deployer import dashboard
### Load deployer config files


configs = {}

deployer_config_path = "/etc/ibconf/deployer/"

deployer_worker_path = "/tmp/deployer/"

configs['deployer'] = open(deployer_config_path + "deployer.ini" , "r").read()
configs['services'] = open(deployer_config_path + "services.ini" , "r").read()

### Stored queries pulled from old deployer version

query_dict = {
	'dash_currentdeps': """SELECT `processes`.*, `logs`.* FROM `processes`  INNER JOIN `logs` ON processes.uniqueId = logs.uniqueId WHERE (processes.type = 'client')""",
	'dash_servers': """SELECT `serversInDeployment`.* FROM `serversInDeployment`""",
	'dash_artifacts': """SELECT `artifacts`.*, `artifactEnvironments`.* FROM `artifacts` INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1)""",
}

### User session storage

session_options = {   
                      'session.type': 'file',
                      'session.data_dir': './session/',
                      'session.auto': True,
                  }



### Initialization of user session

app_middleware = SessionMiddleware(app(), session_options)
app_session = request.environ.get('beaker.session')

### Default MySQLdb connect list

db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

### AD / LDAP connection strings for User Authentication

LDAP_SERVER = "ldap://adproxy.sl.ibsys.com"
BIND_DN = "ibsys.com"
#USER_BASE = "ou=Users,ou=Development,ou=IB,dc=ibsys,dc=com"
USER_BASE = "ou=IB,dc=ibsys,dc=com"

### Sonatype Nexus project path URL for maven XML files

nexus_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/content/groups/public/com/ibsys/"

### Hostinfo URL used for testing, implemented internal MySQL hostinfo instead

hostinfo_url = "http://hostinfo-dal05.dal05.ib-util.com/hostinfo/csv/"

### User Stats stored from a successful login and used in page templates

user_stats = {'first_name':'Matthew', 'user_name':'mwiljanen' , 'full_name':'Wiljanen, Matthew', 'deployer_roles':[]}

### Deployer dictionary array

deployer_jobs = {}

### Deployer log path /var/log/ib/deployer

deployer_log_path = "/var/log/ib/deployer"

def logger(text_input="", debug_level="INFO", module="Deployer\Logger"):
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	
	return '[%s (CDT)] deployer.%s [%s] - %s []\n' % (now, debug_level, module, text_input)


#retrieve a config value from dictionary

def get_config_values(tmp_config="", get_value=""):
	for tmp_line in tmp_config.split('\n'):
		if ( get_value in tmp_line ):
			return tmp_line.split(' = ')[1]

### uniqid function derived from PHPs version

def uniqid():
	return "%14x.%08x" % ( math.modf(time.time())[1]*10000000 + math.modf(time.time())[0] * 10000000, (math.modf(time.time())[0]-math.floor(math.modf(time.time())[0]))*1000000000 )


def remove_values_from_list(the_list, val):
	while val in the_list:
		the_list.remove(val)
	return the_list

### this validates a user against IB's AD system

def validate_user(username="", password=""):
	try:
		user_exists = False
		ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, 0)
		ldap_connection = ldap.initialize(LDAP_SERVER)
		ldap_connection.simple_bind_s(username + "@" + BIND_DN, password.rstrip())
		results = ldap_connection.search_s(USER_BASE, ldap.SCOPE_SUBTREE, "(CN=*)")
		for dn, entry in results:
			if ( username in entry['sAMAccountName'][0] ):
				user_exists = True
				user_stats['user_name'] = entry['sAMAccountName'][0]			
				user_stats['full_name'] = entry['name'][0]
				user_stats['first_name'] = entry['givenName'][0]
				for member_entries in entry['memberOf']:
					if ('deployer' in member_entries):
						user_stats['deployer_roles'].append(member_entries.split(',')[0].split('CN=')[1].split('-')[1])
						
				print user_stats['deployer_roles']

				break

		return user_exists

	except ldap.LDAPError, e:
		#return 'Error connecting to LDAP server: ' + str(e) + '\n'
		return False



def get_nexus_versions(artifact_name, artifact_env):
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	get_nexus_query = """select nexus_metadata.versions from nexus_metadata inner join artifacts on nexus_metadata.artifactId = artifacts.artifactId where artifacts.name = '%s';""" % ( artifact_name )

	if ( curr.execute(get_nexus_query) ):
		nexus_meta_data = curr.fetchone()

		#print nexus_meta_data[0]

        	versions = []

		for item in nexus_meta_data[0].split('|'):
			if ( (artifact_env == 'qa' or artifact_env == 'prod' or artifact_env == 'stage') and 'SNAPSHOT' not in item):
				versions.append( item )
			elif ( artifact_env == 'dev' ):
				versions.append( item )

                return versions

def parse_server_data_2(server_data):
        srv_list = []
        
        for srv_item in ''.join(server_data.split()).split(','):
                if ( '.eq.' in srv_item ):
                        srv_ib_to_str = "{"
                        for srv_sub_item in srv_item.split('|'):
                                if ( '.eq.' in srv_sub_item ):
                                        srv_ib_to_str += """'%s': '%s',""" % ( srv_sub_item.split('.eq.')[0], srv_sub_item.split('.eq.')[1])

                        srv_ib_to_str += "}"
                        srv_list.append(ast.literal_eval(srv_ib_to_str))

        #print len(srv_list)   
        if ( len(srv_list) < 1 ):
                srv_list.append(ast.literal_eval("{ 'hostname': 'none' }"))

        return srv_list


def get_host_name(artifact_name, artifact_env):
        conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()
        server_hosts = []

	server_data_query = '''SELECT serverData FROM artifactEnvironments WHERE artifactName = '%s' and env = '%s' ;''' % ( artifact_name, artifact_env )

	if ( curr.execute(server_data_query) ):
		server_data = ''.join(curr.fetchone()[0])

	        blacklisted_subtypes = get_config_values(configs['deployer'], "skipSiteLoadingHosts")

        	for server_item in parse_server_data_2(server_data):

			server_query = []

			for server_item_part in server_item:
        	                server_query.append("""%s = '%s'""" % ( server_item_part, server_item[server_item_part] ) )

			ib_query = '''select hostname, ib_system_subtype from hostinfo where %s''' % ( ' AND '.join(server_query) )

	                if ( curr.execute(ib_query) ):
        	                for res_item in curr.fetchall():
                	                if (res_item[1] not in blacklisted_subtypes ):
						server_hosts.append( res_item[0] )

	return server_hosts



### This pulls a list of artifacts from the database


@route('/')
@route('/login')
def login():
        return template('login', invalid_login=False)
	

@post('/login')
def do_login():
	if ( request.forms.get("username") and request.forms.get("password") ):
		if ( validate_user(request.forms['username'], request.forms['password']) ):
			app_session = request.environ.get('beaker.session')
    			app_session['logged_in'] = True

			redirect('/dashboard')
		else:	
			return template('login', invalid_login=True)

@route('/auth/logout')
@route('/logout')
def logout():
	app_session = request.environ.get('beaker.session')
	if app_session.get('logged_in'):
		app_session['logged_in'] = False
		user_stats = {'first_name':'', 'user_name':'' , 'full_name':''}
	redirect('/login')


@route('/deploy')
def get_deploy():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	current_art_num = 0

	if ( 'next_art_num' in request.query_string ):
		current_art_num = request.query['next_art_num']

	option_list_query = "select artifactName, env from artifactEnvironments ORDER BY env, artifactName;"

	current_select = 0

	artifact_list = []

	option_list = []

	version_list = []

	artifact_form = ""

	if ( 'artifact' in request.query_string ):
		art_update_cnt = 0
		for art_key in request.query.keys():
			if ( 'artifact[' in art_key ):
				artifact_list.append( request.query[ ('artifact[%d]' % art_update_cnt) ] )
				art_update_cnt += 1

	if ( 'working_artifact' in request.query_string and 'add_artifact' in request.query_string):
		server_include_list = []

		for include_item in request.query.keys():
			if ( 'include_' in include_item ):
				server_include_list.append(include_item.split('_')[1])
			
		artifact_list.append( request.query['working_artifact'] + ":" + request.query['version_to_deploy'] + ":" + request.query['jira_key'] + ":" + ','.join(server_include_list) )	


	if ( 'server_box' in request.query_string ):
		current_select = 1


	if ( 'deploy_button' in request.query_string ):
		if ( len(artifact_list) == 0 ):
			artifact_form += "<p style='color: red; font-size: xx-large;'>Error: No artifacts were selected!</p>"
		else:
			redirect( '/deploy/index?' + request.query_string )

	if ( 'remove_artifact_' in request.query_string ):
		rem_pos = request.query_string.find('remove_artifact_') + len("remove_artifact_")
		rem_art_pos = int(request.query_string[rem_pos:rem_pos+5].split('.')[0])
		del artifact_list[rem_art_pos]

	if( len(artifact_list) > 0 ):
		temp_update = {}
		art_cnt = 0
		for cur_art_item in artifact_list:
			#cur_art_item = request.query[ 'artifact[' + str( art_update_cnt ) + ']' ]
			temp_update['artifact_key'] = cur_art_item
			temp_update['curr_art_num'] = art_cnt
			temp_update['artifact_name'] = cur_art_item.split(':')[0]
			temp_update['artifact_env'] = cur_art_item.split(':')[1]
			temp_update['version'] = cur_art_item.split(':')[2]
			temp_update['jira_key'] = cur_art_item.split(':')[3]
			temp_update['servers_included'] = cur_art_item.split(':')[4]
			artifact_form += '''

		<div class="div-table-row">
			<div class="div-table-col-art"><input type='image' name='remove_artifact_%(curr_art_num)d' class='del_button' src='/images/blank.png'></a>%(artifact_name)s</div>
			<div class="div-table-col-env"> %(artifact_env)s </div>
			<div class="div-table-col"> %(version)s </div>
			<div class="div-table-col"> %(jira_key)s 
			<input type='hidden' name='artifact[%(curr_art_num)s]' value='%(artifact_key)s'></div>
		</div>

			''' % temp_update 

			art_cnt += 1


	if ( 'update_artifact' in request.query_string):
		current_select = 1 

	if( curr.execute( option_list_query ) ):
		for art_opt_item in curr.fetchall():
			option_list.append( '''<option>%s</option>''' % ( art_opt_item[0] + " : " + art_opt_item[1] ) )
	
	if ( current_select == 0 ):

		artifact_form += '''
	<div class="div-table-row">
		<div class="div-table-col-art"><select name='current_artifact_env' class='drop_down'>%s</select></div>
		<div class="div-table-col-env"><input type='submit' name='update_artifact' class='update_button' value='>'></div>
		<div class="div-table-col"></div>
		<div class="div-table-col"></div>
	</div>
		''' % ( ''.join(option_list)  )

	
	if ( current_select == 1 ):

		curr_art = ''.join(request.query['current_artifact_env']).split(':')[0].replace(' ','')
		curr_env = ''.join(request.query['current_artifact_env']).split(':')[1].replace(' ','')

		version_list.append( '''<option>%s</option>''' % ( "latest" ) )

		for ver_opt_item in get_nexus_versions(curr_art, curr_env):
			version_list.append( '''<option>%s</option>''' % ( ver_opt_item ) )
		
		form_update = {}
		form_update['artifact_name'] = curr_art
		form_update['artifact_env'] = curr_env
		form_update['next_art_num'] = current_art_num + 1
		form_update['versions'] = ''.join(version_list)
		form_update['working_artifact'] = curr_art + ":" + curr_env
		form_update['server_box'] = 'hide_server_box'
		
		server_count = 0
		
		form_update['servers'] = 'Servers Included List<br>(uncheck to exclude)<ul>'

		for server_item in get_host_name(curr_art, curr_env):
			
			form_update['servers'] += '''<li class='server_include_item'><input type='checkbox' name='include_%(server_item)s' id='server_id_%(server_item)s' checked>
			<label for='server_id_%(server_item)s'>%(server_item)s</label></li>''' % { 'server_item': server_item }				
		
			server_count += 1
		
		form_update['servers'] += '</ul>'
		

		artifact_form += '''

	<div class="div-table-row">
		<div class="div-table-col-art">%(artifact_name)s</div>
		<div class="div-table-col-env">%(artifact_env)s </div>
		<div class="div-table-col">
			<div class='show_info_button'>
				<div class='server_select_box'>%(servers)s</div>
			</div>
			<select name='version_to_deploy' class='version_drop_down'>%(versions)s</select>
		</div>
		<div class="div-table-col"><input type='text' name='jira_key'>
		<input type='hidden' name='working_artifact' value='%(working_artifact)s'>
		<input type='hidden' name='next_art_num' value='%(next_art_num)s'> 
		<input type='submit' name='add_artifact' value='Add Item'></div>
	</div>
		''' % form_update


	return '''

<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a class="main_menu_link" href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a class="main_menu_link" href="/deploy"><li class="nav_item_sel">Deploy</li></a>
		<a class="main_menu_link" href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a class="main_menu_link" href="/history"><li class="nav_item">History</li></a>
		<div class="admin_menu">Admin vvv
			<a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
			<a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
			<a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
			<a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
			<a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
		</div>
		<a class="main_menu_link" href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h1>Deploy Artifacts</h1>
<hr>
	<div class="deploy_content">
		<div class="artifact_selections">
		<form method='GET' action='/deploy'>
		  <div class="div-table">
		    <div class="div-table-row">
		      <div class="div-table-head-col">Artifact</div><div class="div-table-head-col">Environment</div><div class="div-table-head-col">Version</div><div class="div-table-head-col">JIRA Issue Key</div>
		    </div>
			%s
		<hr>
		
		<div class='deploy_opt_container'>
			<div class="deploy_opt_image"> 
			Deployment Options
	
			<input class="deploy_opt_image" id='deploy_btn' type='checkbox' > 
			<label for='deploy_btn' class='deploy_label_button'>&nbsp;&nbsp;?&nbsp;&nbsp;</label>

			<br>
			<br>
			<div class='deploy_options_box'>

			<div class='dep_opt_exluded_servers'>

				Servers to Exclude 
				<textarea cols='50' rows='10'></textarea>

			</div>

			<div class='total_dep_ops'>

			Options:

			<ul>
        	            <li class='depl_opts'><input name="skipVersionChecks" title="Skips post-deployment version verification. Will force deployment to all servers for selected artifacts." type="checkbox" value="1"/> Skip Version Checks</li>
			    <li class='depl_opts'>
                        	<input  id="skipLbOps" name="skipLbOps"
                               		title="Skips load balancer pool operations." type="checkbox" value="1"
	                            /> Skip Load Balancer Operations
			    </li>
			    <li class='depl_opts'>
				<input  id="skipDepDeploy" name="skipDepDeploy"
					   title="Skips the deployment of artifact dependencies." type="checkbox" value="1"
					/> Skip Artifact Dependency Deployment
			    </li>
			    <li class='depl_opts'>
                	        <input  id="ignoreZenossProdState" name="ignoreZenossProdState"
                        	       title="Ignore initial Zenoss production state; will allow deployment to servers regardless of Zenoss production state." type="checkbox" value="1"
	                            /> Ignore Zenoss Production State
			    </li>
			    <li class='depl_opts'>
				<input  id="resetZenossProdState" name="resetZenossProdState"
				   title="Ignore initial Zenoss production state AND reset Zenoss production states based on deployment environment" type="checkbox" value="1"
							/> Reset Zenoss Production State
			    </li>
			    <li class='depl_opts'>
				<input  id="ignoreArtifactDeployStatus" name="ignoreArtifactDeployStatus"
				   title="Ignore artifact deployment status; will allow concurrent deployments of the same artifact-environment combination." type="checkbox" value="1"
				/> Ignore Artifact Deployment Status
			    </li>
			    <li class='depl_opts'>
                	        <input  id="clearStatus" name="clearStatus"
                        	       title="Last deployment failed or aborted? This clears the deployment status for the selected artifacts and their servers." type="checkbox" value="1"
	                            /> Clear Deploy Status
        	            </li>

                	    <li class='depl_opts'>
	                        <input  id="debug" name="debug" type="checkbox" value="1"
        	                     title="Toggle debug logging" /> Debug
                	    </li>

				<br>
				<li class='depl_opts'>
				Manage Puppet Agent
				<br>
	                        <input  id="puppetAgent0" name="puppetAgent" type="radio" value='none' checked /><label for = 'puppetAgent0'>None</label>
	                        <input  id="puppetAgent1" name="puppetAgent" type="radio" value='stop'/><label for = 'puppetAgent1'>Stop</label>
	                        <input  id="puppetAgent2" name="puppetAgent" type="radio" value='start'/><label for = 'puppetAgent2'>Start</label>
	                        <input  id="puppetAgent3" name="puppetAgent" type="radio" value='restart'/><label for = 'puppetAgent3'>Restart</label>
	                        <input  id="puppetAgent4" name="puppetAgent" type="radio" value='force_restart'/><label for = 'puppetAgent4'>Force Restart</label>
			
				</li>
				
				<br>
				<li class='depl_opts'>
				Manage Service
				<br>
	                        <input  id="manageService0" name="manageService" type="radio" value='none' checked /><label for = 'manageService0'>None</label>
	                        <input  id="manageService1" name="manageService" type="radio" value='stop'/><label for = 'manageService1'>Stop</label>
	                        <input  id="manageService2" name="manageService" type="radio" value='start'/><label for = 'manageService2'>Start</label>
	                        <input  id="manageService3" name="manageService" type="radio" value='restart'/><label for = 'manageService3'>Restart</label>
				</li>
				</div>
			</div>

		</div>		
		
		</div>


		  </div>

		<hr>
		<div class='deploy_button_box'>
			<div class='safe_button_box'>
				<span style='padding-left: 15%%;'>SAFE MODE</span>
				<br>
				<input class="safe_button_chk" id='safe_mode_btn_on' name='run' type='radio' value='0' checked> 
				<label for='safe_mode_btn_on' class='safe_button_lbl'>On</label>
				<input class="safe_button_chk" id='safe_mode_btn_off' name='run' type='radio' value='1'> 
				<label for='safe_mode_btn_off' class='safe_button_lbl'>Off</label>

			</div>
			<div class='deploy_button'><span style='padding-left: 10%%; font-size: xx-large'>DEPLOY
			<input type='image' name='deploy_button' src='/images/blank.png' style='position: absolute; top: 0px; left: 0px; width: 100%%; height: 100%%;' /></span> </div>
		</div>
		
		</form>

		</div>

		<div class="current_activity">
			Current Activity
			<hr>
			<iframe src="/deploy/current_activity" width="100%%" height="90%%" frameborder=0></iframe>
		</div>
	</div>
</div>
</body>
</html>
	''' % ( artifact_form )


@route('/deploy/add_artifact')
def add_artifact():
	redirect('/deploy?' + request.query_string)

@route('/deploy/index')
def deploy_index():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

        artifacts_being_deployed = 0
        artifact_list = []
        pid = os.getpid()

        deployer_jobs['user'] = user_stats['user_name']
        deployer_jobs['uniqueId'] = uniqid()
        deployer_jobs['dateTime'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	deployer_jobs['artifacts'] = []


	print deployer_jobs

	for req_key in request.query.keys():
		if ( 'artifact[' in req_key ):
	                deployer_jobs['artifacts'].append(request.query['artifact[' + str(artifacts_being_deployed) + ']'] )
			artifacts_being_deployed += 1
		else:
			deployer_jobs[req_key] = request.query[req_key]
			print request.query[req_key]

	print  deployer_jobs
	
	curr.execute('''INSERT INTO processes VALUES ( 0, '%s', %s, '%s', NULL, %s, '%s');''' % ( deployer_jobs['uniqueId'], pid, 'client', 1, '' ) )
        conn.commit()

        pickle.dump( deployer_jobs, open(deployer_worker_path + "deployer_" + deployer_jobs['uniqueId'] + "_v3.job", "wb") )

	redirect('/deploy/watch/unique_id/' + deployer_jobs['uniqueId'] )


@route('/deploy/watch/unique_id/<unique_id>')
def watch_current_deployment(unique_id):

	watch_deploy_dict = { 'unique_id': unique_id }	
	
	

	return '''
	
<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a class="main_menu_link" href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a class="main_menu_link" href="/deploy"><li class="nav_item_sel">Deploy</li></a>
		<a class="main_menu_link" href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a class="main_menu_link" href="/history"><li class="nav_item">History</li></a>
		<div class="admin_menu">Admin vvv
			<a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
			<a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
			<a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
			<a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
			<a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
		</div>
		<a class="main_menu_link" href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h1>Deploy ID: %(unique_id)s </h1>
<hr>
		<iframe src="/deploy/watchlog/unique_id/%(unique_id)s" width="100%%" height="100%%"></iframe>
</div>

</body>
</html>	

	''' % watch_deploy_dict

@route('/deploy/watchlog/unique_id/<unique_id>')
def watch_current_log(unique_id):
	watch_log_dict = {}

	try:
		watch_log_dict = { 'output' : open('/var/log/ib/deployer/deployer_' + unique_id + '.log', 'r').read(), 'unique_id': unique_id }
	except:
 		watch_log_dict = { 'output' : '', 'unique_id': unique_id }



	return '''
	<head> <META http-equiv="refresh" content="5;URL=/deploy/watchlog/unique_id/%(unique_id)s"> </head>
	<pre> %(output)s </pre>
	''' % watch_log_dict


@route('/deploy/current_activity')
def current_activity():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	art_count_query = '''select artifactName, env from artifactEnvironments where isDeploying = 1 ORDER BY artifactName;'''

	art_count = 0

	art_cnt_output = []

	if ( curr.execute(art_count_query) ):	
		res = curr.fetchall()
		art_count = len( res )
		for art_item in res:
			art_cnt_output.append( "<li class='dep_list'>" + art_item[0] + " : " + art_item[1].upper() + "</li>" ) 

	return '''
	<head> <META http-equiv="refresh" content="5;URL=/deploy/current_activity"> </head>
	<style> @import url(/css/deployer.css); </style>	
	<div class="current_artifacts">
	Artifacts &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	''' % ( art_count, '\n'.join(art_cnt_output) )


@route('/artifacts')
def show_artifacts():

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	search_str = ''

	if ('artifact_search' in request.query_string):
		search_str = request.query['artifact_search']
		

	get_artifacts_query = '''SELECT artifactName, env, lastDeployedVersion, lastDeployedUser, lastDeployedDate FROM artifactEnvironments WHERE artifactName like '%%%s%%' ORDER BY artifactName;''' % search_str

	output_artifact = []
	
	if ( curr.execute(get_artifacts_query) ):
		artifact_result = curr.fetchall()
		alter_row = 0

		artifact_name = ''

		output_dict = { 'artifact': '', 'env': [], 'env_dev': '', 'env_qa': '', 'env_stage': '', 'env_prod': '' }

		for art_item in artifact_result:
			if ( output_dict['artifact'] != art_item[0] and len(art_item[0]) > 0):

				env_div_list = []
				for env_div_item in sorted(set(output_dict['env'])):
					env_div_list.append('''<div class='art_env_cell_%s' title='%s'>%s<span style="background: #3a87ad; font-size: smaller; color: white;">%s</span></div>''' % ( env_div_item,  output_dict['env_' + env_div_item] , env_div_item.upper(), output_dict['env_' + env_div_item + '_version'] ) )

				output_row = '''
			<div class="artifact_table_row">
			      <div class="artifact_table_col_name_%d">%s</div><div class="artifact_table_col_env_%d">%s</div>
			</div>
				''' % ( alter_row, output_dict['artifact'], alter_row,  '\n'.join(env_div_list) )

				if ( alter_row == 0 ):
					alter_row = 1
				else:
					alter_row = 0

				output_artifact.append( output_row  )

				output_dict['artifact'] = art_item[0]
				output_dict['env'] = []
				
				#output_dict['env'].append( art_item[1] )
				#output_dict['env_stats'].append ( str(art_item[2]) + str(art_item[3]) + str(art_item[4])  ) 

			if ( output_dict['artifact'] == art_item[0] ):
				output_dict['env'].append(art_item[1])
				if ( art_item[2] ):
					output_dict['env_' + art_item[1] ] = '''Last deployed: Version %s by %s on %s''' % (  art_item[2], art_item[3], art_item[4] )
				else:
					output_dict['env_' + art_item[1] ] = ''
				
				#output_dict['env_' + art_item[1] ] = str(art_item[2]) + str(art_item[3]) + str(art_item[4])
				output_dict['env_' + art_item[1] + '_version' ] = str(art_item[2])

	
		env_div_list = []
		for env_div_item in sorted(set(output_dict['env'])):
			env_div_list.append('''<div class='art_env_cell_%s' title='%s'>%s<span style="background: #3a87ad; font-size: smaller; color: white;">%s</span></div>''' % ( env_div_item,  output_dict['env_' + env_div_item] , env_div_item.upper(), output_dict['env_' + env_div_item + '_version'] ) )
		#for env_div_item in sorted(set(output_dict['env'])):
			#env_div_list.append('''<div class='art_env_cell_%s'>%s</div>''' % ( env_div_item, env_div_item.upper() ) )

		output_row = '''
			<div class="artifact_table_row">
			      <div class="artifact_table_col_name_%d">%s</div><div class="artifact_table_col_env_%d">%s</div>
			</div>
		''' % ( alter_row, output_dict['artifact'], alter_row,  '\n'.join(env_div_list) )

		if ( alter_row == 0 ):
			alter_row = 1
		else:
			alter_row = 0

		output_artifact.append( output_row  )

		
		#print str(output_dict)

	return '''<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a class="main_menu_link" href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a class="main_menu_link" href="/deploy"><li class="nav_item">Deploy</li></a>
		<a class="main_menu_link" href="/artifacts"><li class="nav_item_sel">Artifacts</li></a>
		<a class="main_menu_link" href="/history"><li class="nav_item">History</li></a>
		<div class="admin_menu">Admin vvv
			<a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
			<a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
			<a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
			<a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
			<a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
		</div>

		<a class="main_menu_link" href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h1>Artifacts</h1>
<hr>
	<div class="artifact_content">
		    <div class="artifact_title_bar">
			<form  method='get' action='/artifacts'>
			  <input type='submit' name='artifact_search_submit' value='Search'>
			  <input type='text' name='artifact_search'>
			</form>
		    </div>
		   <div class="artifact_scroll">
		   
		       <div class="artifact_table">
		    	    <div class="artifact_table_row">
		                  <div class="artifact_table_col_head_name">Name</div><div class="artifact_table_col_head_env">Environment</div>
		            </div>
		            %s
		   </div>
	        </div>
	</div>
</div>
</body>
</html>
	''' % ( '\n'.join(output_artifact) )


@route('/history')
def get_history():


	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	show_entries = [ '5', '10', '15', '20', '25', '50', '100', '200' ]

	history_input_dict = {}

	history_input_dict['show_history_log'] = ' show_logs'
	history_input_dict['show_history_art'] = ''

	if ( 'show_logs' in request.query_string ):
		history_input_dict['show_history_log'] = ' show_logs'
		history_input_dict['show_history_art'] = ''
	if ( 'show_arts' in request.query_string ):
		history_input_dict['show_history_log'] = ''
		history_input_dict['show_history_art'] = ' show_art'


	history_input_dict['current_entries'] = '10'

	if ( 'show_entries_dropdown' in request.query_string ):
		history_input_dict['current_entries'] = request.query['show_entries_dropdown']

	history_input_dict['show_entries_dropdown'] = ''
	for entry_item in show_entries:
		if ( entry_item == history_input_dict['current_entries'] ):
			history_input_dict['show_entries_dropdown'] += '<option selected>' + entry_item + '</option>'
		else:
			history_input_dict['show_entries_dropdown'] += '<option>' + entry_item + '</option>'


	history_by_log_query = '''SELECT SQL_CALC_FOUND_ROWS `uniqueId`, `datetime`, `artifactsSelected` FROM `logs` WHERE (isDryRun = 0) ORDER BY `datetime` desc LIMIT %s;''' % ( history_input_dict['current_entries'] ) 

	history_input_dict['log_history'] = ''

	if ( curr.execute(history_by_log_query) ):
		cur_results = curr.fetchall()
		col_counter = 0
		for history_item in cur_results:
			artifact_selections_output = ''
			if ( history_item[2] ):
				decoded = json.loads(history_item[2])
				#print decoded
				if ( type(decoded) == list ):
					for selected_item in decoded:
						if( 'name' in selected_item.keys() and 'env' in selected_item.keys() and 'versionToDeploy' in selected_item.keys() and 'jiraIssueKey' in selected_item.keys() ):
							artifact_selections_output += '%(name)s:%(env)s:%(versionToDeploy)s:%(jiraIssueKey)s <br>' % selected_item
				elif ( type(decoded) == dict ):
					for selected_item in decoded.keys():
						if( 'name' in decoded[selected_item].keys() and 'env' in  decoded[selected_item].keys() and 'versionToDeploy' in  decoded[selected_item].keys() and 'jiraIssueKey' in  decoded[selected_item].keys() ):
							artifact_selections_output += '%(name)s:%(env)s:%(versionToDeploy)s:%(jiraIssueKey)s <br>' % decoded[selected_item]
				
										
			
			log_hist_dict = { 'unique_id': history_item[0], 'date_time': history_item[1], 'artifact_selections': artifact_selections_output, 'alt': col_counter % 2 } 


			history_input_dict['log_history'] += '''
	    	    <div class="div-table-row">
        	          <div class="div-table-col-%(alt)s"><a href="/history/viewlog/uniqueId/%(unique_id)s">%(unique_id)s</a></div>
			  <div class="div-table-col-%(alt)s">%(date_time)s</div>
			  <div class="div-table-col-%(alt)s">%(artifact_selections)s</div>
	            </div>		
			''' % log_hist_dict
			col_counter += 1


	history_by_artifact_query = '''SELECT DISTINCT SQL_CALC_FOUND_ROWS serverDeployments.logUniqueId as logUniqueId, `artifactEnvironments`.`artifactName`, `artifactEnvironments`.`env`, `artifactEnvironments`.`id` AS `artifactEnvId`, `logs`.`datetime` FROM `serverDeployments`
 INNER JOIN `artifactEnvironments` ON serverDeployments.artifactEnvId = artifactEnvironments.id
 INNER JOIN `logs` ON serverDeployments.logUniqueId = logs.uniqueId WHERE (isDryRun = 0) ORDER BY `datetime` desc LIMIT %s''' % ( history_input_dict['current_entries'] )


	history_input_dict['artifact_history'] = ''

	if ( curr.execute(history_by_artifact_query) ):
		cur_results = curr.fetchall()
		col_counter = 0
		for art_hist_item in cur_results:
			art_hist_dict = { 'unique_id': art_hist_item[0], 'artifact': art_hist_item[1], 'env': art_hist_item[2] , 'date_time': art_hist_item[4], 'alt': col_counter % 2 }

			history_input_dict['artifact_history'] += '''
	    	    <div class="div-table-row">
        	          <div class="div-table-col-%(alt)s"><a href="/history/viewlog/uniqueId/%(unique_id)s">%(unique_id)s</a></div>
			  <div class="div-table-col-%(alt)s">%(artifact)s</div>
			  <div class="div-table-col-%(alt)s">%(env)s</div>
			  <div class="div-table-col-%(alt)s">%(date_time)s</div>
	            </div>		
			''' % art_hist_dict
			col_counter += 1


	return '''<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a class="main_menu_link" href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a class="main_menu_link" href="/deploy"><li class="nav_item">Deploy</li></a>
		<a class="main_menu_link" href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a class="main_menu_link" href="/history"><li class="nav_item_sel">History</li></a>
		<div class="admin_menu">Admin vvv
			<a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
			<a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
			<a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
			<a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
			<a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
		</div>

		<a class="main_menu_link" href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h1>History</h1>
<hr>
	<div class="history_content">
		    <div class="artifact_title_bar">
			<form  method='get' action='/history'>
			  Show <select name = 'show_entries_dropdown' onchange='submit()'>%(show_entries_dropdown)s</select> entries
			  <input type='submit' name='history_search_submit' value='Search'>
			  <input type='text' name='history_search'>
			</form>
		</div>

		<div class = "history_tabs">

			<a href="/history?show_logs=true&show_entries_dropdown=%(current_entries)s">Show Logs</a>
			<a href="/history?show_arts=true&show_entries_dropdown=%(current_entries)s">Show Artifacts</a>
			   <div class="history_log_scroll%(show_history_log)s">
		       
		       		<div class="div-table">
			    	    <div class="div-table-row">
			                  <div class="div-table-head-col">Unique ID</div><div class="div-table-head-col">Date/Time</div><div class="div-table-head-col">Artifact Selections</div>
			            </div>
			    
				    %(log_history)s
				</div>
			   </div>
		   <div class="history_artifact_scroll%(show_history_art)s">
		       
		       <div class="div-table">
		    	    <div class="div-table-row">
		                  <div class="div-table-head-col">Unique ID</div>
				  <div class="div-table-head-col">Artifact</div>
				  <div class="div-table-head-col">Environment</div>
				  <div class="div-table-head-col">Date/Time</div>
		            </div>
			    
			    %(artifact_history)s
			</div>
		   </div>


		    </div>
	        </div>
	</div>

</div>
</body>
</html>
	''' % history_input_dict


@route('/history/viewlog/uniqueId/<unique_id>')
def get_history(unique_id):

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	viewlog_dict = { 'unique_id': unique_id, 'logs': '' }

	viewlog_query = '''SELECT log FROM logs WHERE uniqueId = '%(unique_id)s'; ''' % viewlog_dict

	if ( curr.execute(viewlog_query) ):
		tmp_logs = curr.fetchone()[0]
		if ( tmp_logs ):
			viewlog_dict['logs'] = tmp_logs.replace('\r\n', '<br>').replace('\n', '<br>')

	return '''<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a class="main_menu_link" href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a class="main_menu_link" href="/deploy"><li class="nav_item">Deploy</li></a>
		<a class="main_menu_link" href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a class="main_menu_link" href="/history"><li class="nav_item_sel">History</li></a>
		<div class="admin_menu">Admin vvv
			<a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
			<a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
			<a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
			<a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
			<a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
		</div>
		<a class="main_menu_link" href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h5> History / Deployment ID: %(unique_id)s </h5>
<h1>Deployment ID: %(unique_id)s</h1>
<hr>

	<div class = "viewlog_container">
		<pre class='log_output'>%(logs)s</pre>
	</div>

</div>

</body>
</html>

	''' % viewlog_dict

@route('/admin/artifacts')
def get_artifacts():

	artifact_focus = ''

	art_opt_focus = ''

	if ( 'artifact_focus' in request.query_string ):
		artifact_focus = request.query['artifact_focus']

	if ( 'art_opt_focus' in request.query_string ):
		art_opt_focus = request.query['art_opt_focus']

	admin_artifacts_dict = {}

	admin_artifacts_dict['show_entries'] = '100'
	admin_artifacts_dict['artifact_entries'] = ''
	admin_artifacts_dict['artifact_focus'] = artifact_focus
	admin_artifacts_dict['art_opt_focus'] = art_opt_focus

	admin_artifact_query = {}

	admin_artifact_query['full'] = '''SELECT `artifacts`.* FROM `artifacts` ORDER BY `name` asc LIMIT %(show_entries)s''' % admin_artifacts_dict

	admin_artifact_query['env'] = '''SELECT env, isLoadBalanced, serverData  FROM artifactEnvironments WHERE (artifactName = '%(artifact_focus)s')''' % admin_artifacts_dict
	admin_artifact_query['other'] = '''SELECT mavenVersion, alwaysAllowSnapshots, notes FROM artifacts WHERE (name = '%(artifact_focus)s')''' % admin_artifacts_dict
	admin_artifact_query['deps'] = '''SELECT * FROM artifactDependencies WHERE (artifactName = '%(artifact_focus)s')''' % admin_artifacts_dict
	admin_artifact_query['sites'] = '''SELECT * FROM artifactSites WHERE (artifactName = '%(artifact_focus)s')''' % admin_artifacts_dict
	admin_artifact_query['proxies'] = '''SELECT * FROM artifactHaproxyProxies WHERE (artifactName = '%(artifact_focus)s')''' % admin_artifacts_dict



	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	if ( curr.execute(admin_artifact_query['full']) ):
		cur_color = 0
		res_output = curr.fetchall()
		for res_item in res_output:
			
			admin_list_dict = { 'name': res_item[1], 
			'server_type': res_item[2], 
			'group_id': res_item[3], 
			'artifact_id': res_item[4], 
			'artifact_type': res_item[5], 
			'deploy_name': res_item[7],
			'version_url_suffix': res_item[8],
			'status_url_suffix': res_item[9],
			'color': cur_color % 2,
			'show_table_options': 'hide_element', 
			'artifact_opt_output': '' }



			if ( artifact_focus == res_item[1] ):
				admin_list_dict['show_table_options'] = 'show_element'

			if ( len(art_opt_focus) > 0 ):
				if ( curr.execute(admin_artifact_query[art_opt_focus]) ):
					opt_results = curr.fetchall()
					for opt_item_res in opt_results:
						admin_opt_dict = { 'art_name': admin_list_dict['name'], 'env_name': opt_item_res[0], 'load_balanced': opt_item_res[1], 'server_data': opt_item_res[2] }
						admin_list_dict['artifact_opt_output'] += '''<div class="div-table-row">'''
						if ( art_opt_focus == 'env' ):

							if ( artifact_focus == admin_list_dict['name'] and 'edit' in  request.query_string ):
								if ( request.query['edit'] == 'env' ):
									admin_list_dict['artifact_opt_output'] += '''<div class="div-table-col">%(env_name)s</div></a>''' % admin_opt_dict
								if ( request.query['edit'] == 'server_data' ):
									admin_list_dict['artifact_opt_output'] += '''<div class="div-table-col"><input type='text' name='server_data_edit' value='%(server_data)s'></div>''' % admin_opt_dict
							if ( artifact_focus == admin_list_dict['name'] and 'load_balanced' in request.query_string and 'env_name' in request.query_string ):
								tmp_update_query = ''
								if (  request.query['load_balanced'] and request.query['env_name'] == admin_opt_dict['env_name'] ):
									if ( admin_opt_dict['load_balanced'] ):
										admin_opt_dict['load_balanced'] = 0
									else:
										admin_opt_dict['load_balanced'] = 1

									#tmp_dict('load_balanced': admin_opt_dict['load_balanced'], 'env': request.query )
									tmp_update_query = '''UPDATE artifactEnvironments SET isLoadBalanced = %(load_balanced)d WHERE artifactName = '%(art_name)s' AND env = '%(env_name)s' ;''' % admin_opt_dict
									print tmp_update_query
									curr.execute(tmp_update_query)
									conn.commit()
							
							admin_list_dict['artifact_opt_output'] += '''
						<a href='/admin/artifacts?artifact_focus=%(art_name)s&art_opt_focus=env&env_name=%(env_name)s&edit=env#%(art_name)s'><div class="div-table-col">%(env_name)s</div></a>
						<a href='/admin/artifacts?artifact_focus=%(art_name)s&art_opt_focus=env&env_name=%(env_name)s&load_balanced=%(load_balanced)d#%(art_name)s'><div class="div-table-col">%(load_balanced)s</div></a>
						<a href='/admin/artifacts?artifact_focus=%(art_name)s&art_opt_focus=env&env_name=%(env_name)s&server_data=selected&edit=server_data#%(art_name)s'><div class="div-table-col">%(server_data)s</div></a>
					</div>
							''' % admin_opt_dict
					

			admin_output_table = '''
		<div class="div-table-row">
				<div class="admin-div-table-col-%(color)s" id='%(name)s'>%(name)s</div>
	        	        <div class="admin-div-table-col-%(color)s">%(server_type)s</div>
        		        <div class="admin-div-table-col-%(color)s">%(group_id)s</div>
        		        <div class="admin-div-table-col-%(color)s"ActiveMQ-Monitor>%(artifact_id)s</div>
        	        	<div class="admin-div-table-col-%(color)s">%(artifact_type)s</div>
	        	        <div class="admin-div-table-col-%(color)s">%(deploy_name)s</div>
        		        <div class="admin-div-table-col-%(color)s">%(version_url_suffix)s</div>
        		        <div class="admin-div-table-col-%(color)s">%(status_url_suffix)s</div>
				
				<a href="/admin/artifacts?artifact_focus=%(name)s#%(name)s">Expand</a>
			
				<div class="div-table %(show_table_options)s">
					<div class="div-table-row">
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=env#%(name)s'><div class="admin-other-head-col">Environment</div></a>
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=other#%(name)s'><div class="admin-other-head-col">Other Details</div></a>
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=deps#%(name)s'><div class="admin-other-head-col">Dependencies</div></a>
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=sites#%(name)s'><div class="admin-other-head-col">Sites</div></a>
						<a href='/admin/artifacts?artifact_focus=%(name)s&art_opt_focus=proxies#%(name)s'><div class="admin-other-head-col">Haproxy Proxies</div></a>
					</div>
					<div class="div-table-row">

					%(artifact_opt_output)s

					</div>
				</div>
		</div>
			
			''' % admin_list_dict

			admin_artifacts_dict['artifact_entries'] += admin_output_table

			cur_color += 1
	
	return '''
<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a class="main_menu_link" href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a class="main_menu_link" href="/deploy"><li class="nav_item">Deploy</li></a>
		<a class="main_menu_link" href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a class="main_menu_link" href="/history"><li class="nav_item">History</li></a>
		<div class="admin_menu nav_item_sel">Admin vvv
			<a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
			<a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
			<a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
			<a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
			<a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
		</div>

		<a class="main_menu_link" href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>
<div class="content">

<h5> Admin / Artifacts </h5>
<h1>Admin > Artifacts</h1>
<hr>
	<div class="admin_artifact_content">

	<div class="div-table">
		<div class="div-table-row">
			<div class="admin-div-table-head-col">Name</div>
        	        <div class="admin-div-table-head-col">Server Type</div>
        	        <div class="admin-div-table-head-col">Group ID</div>
        	        <div class="admin-div-table-head-col">Artifact ID</div>
        	        <div class="admin-div-table-head-col">Artifact Type</div>
        	        <div class="admin-div-table-head-col">Deploy Name</div>
        	        <div class="admin-div-table-head-col">Version URL Suffix</div>
        	        <div class="admin-div-table-head-col">Status URL Suffix</div>
		</div>
	%(artifact_entries)s
			
	</div>
	</div>
</div>

</body>
</html>
	''' % admin_artifacts_dict




@route('/help')
def deployer_help():

	return '''
<html>
<head>
	<style> @import url(/css/deployer.css); </style>
</head>
<body>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a class="main_menu_link" href="/dashboard"><li class="nav_item">Dashboard</li></a>
		<a class="main_menu_link" href="/deploy"><li class="nav_item">Deploy</li></a>
		<a class="main_menu_link" href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a class="main_menu_link" href="/history"><li class="nav_item">History</li></a>
		<div class="admin_menu">Admin vvv
			<a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
			<a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
			<a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
			<a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
			<a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
		</div>

		<a class="main_menu_link" href="/help"><li class="nav_item_sel">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

<div class="content">
<h1>Help</h1>
<hr>
	<div class="help_content">
		
<h2>Working in an RFC ticket?</h2>

    <ol>
    <li>Assign the RFC to 'sysadmin.jira'</li>
    <li>Create a new comment on the RFC with one or more of the following:
	<ul>
        <li>A description of your issue</li>
        <li>A screen shot of the Deployer page showing the notification(s) you received</li>
        <li>Click the 'Show Log' button and copy the entire log, or at least the line containing the "unique id for this instance"</li>
	</ul>
	</li>
    </ol>
<h2>Otherwise...</h2>

Please open a ticket here: <a href="http://jira.ibsys.com/browse/ONECD">http://jira.ibsys.com/browse/ONECD</a>



	</div>
</div>
</body>
</html>
	''' 





# Static Routes
@get('/js/<filename:re:.*\.js>')
def javascript(filename):
	return static_file(filename, root='public/js')

@get('/css/<filename:re:.*\.css>')
def stylesheets(filename):
	return static_file(filename, root='public/css')

@get('/css/ib-brand/images/<filename:re:.*\.(png|jpg|gif|jpeg|ico)>')
def ibcustom(filename):
	return static_file(filename, root='public/css/ib-brand/images/')

@get('/css/smoothness/images/<filename:re:.*\.(png|jpg|gif|jpeg|ico)>')
def ibcustom(filename):
	return static_file(filename, root='public/css/ib-brand/images/')

@get('/images/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
	return static_file(filename, root='public/images')

@get('/bootstrap/img/<filename:re:.*\.(png|jpg|gif|jpeg)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/img')

@get('/bootstrap/js/<filename:re:.*\.(js)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/js')

@get('/bootstrap/css/<filename:re:.*\.(css)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/css')

@get('/cache/<filename:re:.*\.(js|css)>')
def cache(filename):
	return static_file(filename, root='public/cache')

@get('/screenshots/<unique_id>/<filename>')
def screenshots(unique_id, filename):
	return static_file(filename, root='public/screenshots/' + unique_id )

@error(404)
def error404(error):
	return "Oops!<br>Unable to find page<br>"



debug(True)
run(app=app_middleware, host='0.0.0.0', port=18082, reloader=True)
