@route('/dashboard')
def get_dashboard():
	return '''
	<style> @import url(/css/deployer.css); </style>

	<ul class="nav">
		<li class="nav_text">IB Deployer 3.0 Alpha</li>
		<a class="main_menu_link" href="/dashboard"><li class="nav_item_sel">Dashboard</li></a>
		<a class="main_menu_link" href="/deploy"><li class="nav_item">Deploy</li></a>
		<a class="main_menu_link" href="/artifacts"><li class="nav_item">Artifacts</li></a>
		<a class="main_menu_link" href="/history"><li class="nav_item">History</li></a>
		<div class="admin_menu">Admin vvv
			<a class="main_menu_link" href="/admin/artifacts"><div class='admin_menu_item'>Artifacts</div></a>
			<a class="main_menu_link" href="/admin/permissions"><div class='admin_menu_item'>Permissions</div></a>
			<a class="main_menu_link" href="/admin/prune"><div class='admin_menu_item'>Data Prune</div></a>
			<a class="main_menu_link" href="/admin/reports"><div class='admin_menu_item'>Reports</div></a>
			<a class="main_menu_link" href="/admin/monitor"><div class='admin_menu_item'>Monitor Status</div></a>
		</div>
		<a class="main_menu_link" href="/help"><li class="nav_item">Help</li></a>
		<li class="nav_spacer"></li>
	</ul>

	<div class="content">
		<h1>Dashboard</h1>
		<hr>
		<h2>Current Activity<h2>


		<iframe src="/dashboard/currentdeps" width="100%" height="100%" fraclass='dep_list'meborder=0></iframe>

	</div>
	'''

@route('/dashboard/currentdeps')
def get_dash_currentdeps():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
        curr = conn.cursor()

	deps_query = '''SELECT artifactEnvironments.currentUniqueId, artifactEnvironments.lastDeployedDate,  artifactEnvironments.artifactName  FROM `artifacts`  INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1);'''

	dep_count_query = '''SELECT DISTINCT currentUniqueId FROM artifactEnvironments WHERE isDeploying = 1;'''

	dep_count = 0

	if ( curr.execute(dep_count_query) ):	
		dep_count = len( curr.fetchall() )
	output_str = '''
	<head> <META http-equiv="refresh" content="5;URL=/dashboard/currentdeps"> </head>
	<style>  @import url(/css/deployer.css);  </style>
	'''

	output_str += '''

Deployments &nbsp; <span class="counter">&nbsp;%d&nbsp;</span>

<br>

<div class="div-table">
	<div class="div-table-row">
		<div class="div-table-head-col">Unique ID</div><div class="div-table-head-col">Start Date/Time	</div><div class="div-table-head-col">Selections</div><div class="div-table-head-col">Status</div>
	</div>

	''' % ( dep_count  )

	if ( curr.execute(deps_query) ):	
		cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }
		all_res = curr.fetchall()


		for res_row in all_res:
			#print  cur_deps_dict

			if ( len(cur_deps_dict['uniqueId']) == 0 ):
				cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }

				cur_deps_dict['uniqueId'] = res_row[0]
				cur_deps_dict['start_time'] = res_row[1]
				cur_deps_dict['selections'].append(res_row[2])
				cur_deps_dict['status'] = ''


			elif ( cur_deps_dict['uniqueId'] == res_row[0] ):
				cur_deps_dict['selections'].append(res_row[2])

			
			elif ( cur_deps_dict['uniqueId'] != res_row[0] and len(cur_deps_dict['uniqueId']) > 0):
			
				output_str += '''
		<div class="div-table-row">
			<div class="div-table-col">%s</div>
        	        <div class="div-table-col">%s</div>
        	        <div class="div-table-col">%s</div>
        	        <div class="div-table-col">Status</div>
		</div>
				''' % ( cur_deps_dict['uniqueId'], cur_deps_dict['start_time'], '<br>'.join(cur_deps_dict['selections']) )
				
				cur_deps_dict = { 'uniqueId': '', 'start_time': '', 'selections': [], 'status': '' }

				cur_deps_dict['uniqueId'] = res_row[0]
				cur_deps_dict['start_time'] = res_row[1]
				cur_deps_dict['selections'].append(res_row[2])
				cur_deps_dict['status'] = ''


		output_str += '''
                <div class="div-table-row">
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">%s</div>
                        <div class="div-table-col">Status</div>
                </div>
                ''' % ( cur_deps_dict['uniqueId'], cur_deps_dict['start_time'], '<br>'.join(cur_deps_dict['selections']) )
			
	output_str += '''

</div>
	'''

	art_count_query = '''select artifactName, env from artifactEnvironments where isDeploying = 1 ORDER BY artifactName;'''

	art_count = 0

	art_cnt_output = []

	if ( curr.execute(art_count_query) ):	
		res = curr.fetchall()
		art_count = len( res )
		for art_item in res:
			art_cnt_output.append( "<li>" + art_item[0] + " : " + art_item[1].upper() + "</li>" ) 

	output_str += '''
	<br>	
	<div class="current_artifacts">
	Artifacts &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	''' % ( art_count, '\n'.join(art_cnt_output) )


	server_query = '''SELECT * FROM serversInDeployment;'''

	server_count = 0

	serv_cnt_output = []

	if ( curr.execute(server_query) ):	
		res = curr.fetchall()
		server_count = len( res )
		for serv_item in res:
			serv_cnt_output.append( "<li>" + serv_item[0] + "</li>" ) 

	output_str += '''
	

	<div class="current_servers">
	Servers &nbsp;&nbsp;<span class="counter">&nbsp;%d&nbsp;</span>	
	<ul>
		%s
	</ul>
	</div>
	
	''' % ( server_count, '\n'.join( serv_cnt_output ) )


	return output_str


