import gearman
import time
import os

gm_worker = gearman.GearmanWorker(['localhost:4730'])

#  define method to handled 'reverse' work
def task_listener_reverse(gearman_worker, gearman_job):
	print 'reporting status'
	time.sleep(5)
	return gearman_job.data[::-1]

def doDeployerInit(gearman_worker, gearman_job):
	print 'Reporting Job'
	return gearman_job.data

gm_worker.set_client_id('your_worker_client_id_name')
gm_worker.register_task('initDeployer', doDeployerInit)

gm_worker.work()

