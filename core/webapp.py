#!/usr/bin/env python
### Project:  IB Deployer v2.0.32
### Dev:  Matthew Wiljanen
### Date: 2014-07-16


### Current Libraries needed to run deployer

from bottle import request, debug, route, run, get, post, template, error, static_file, response, redirect, app, abort, view, template
from beaker.middleware import SessionMiddleware
import MySQLdb
import ldap
import datetime
import json
import pycurl
import cStringIO
from xml.dom import minidom
import re
import time
import math
import os
import gearman
import pickle


### Load deployer config files


configs = {}

deployer_config_path = "/etc/ibconf/deployer/"

deployer_worker_path = "/tmp/deployer/"

configs['deployer'] = open(deployer_config_path + "deployer.ini" , "r").read()
configs['services'] = open(deployer_config_path + "services.ini" , "r").read()

### Stored queries pulled from old deployer version

query_dict = {
	'dash_currentdeps': """SELECT `processes`.*, `logs`.* FROM `processes`  INNER JOIN `logs` ON processes.uniqueId = logs.uniqueId WHERE (processes.type = 'client')""",
	'dash_servers': """SELECT `serversInDeployment`.* FROM `serversInDeployment`""",
	'dash_artifacts': """SELECT `artifacts`.*, `artifactEnvironments`.* FROM `artifacts` INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1)""",
}

### User session storage

session_options = {   
                      'session.type': 'file',
                      'session.data_dir': './session/',
                      'session.auto': True,
                  }



### Initialization of user session

app_middleware = SessionMiddleware(app(), session_options)
app_session = request.environ.get('beaker.session')

### Default MySQLdb connect list

db_connect=['localhost', 'deployer', 'd3pl0y3r', 'deployer']

### AD / LDAP connection strings for User Authentication

LDAP_SERVER = "ldap://adproxy.sl.ibsys.com"
BIND_DN = "ibsys.com"
#USER_BASE = "ou=Users,ou=Development,ou=IB,dc=ibsys,dc=com"
USER_BASE = "ou=IB,dc=ibsys,dc=com"

### Sonatype Nexus project path URL for maven XML files

nexus_url = "https://bamboo:b%40mb00@cm-nexus.ibsys.com/nexus/content/groups/public/com/ibsys/"

### Hostinfo URL used for testing, implemented internal MySQL hostinfo instead

hostinfo_url = "http://hostinfo-dal05.dal05.ib-util.com/hostinfo/csv/"

### User Stats stored from a successful login and used in page templates

user_stats = {'first_name':'Matthew', 'user_name':'mwiljanen' , 'full_name':'Wiljanen, Matthew', 'deployer_roles':[]}

### Deployer dictionary array

deployer_jobs = {}

### Deployer log path /var/log/ib/deployer

deployer_log_path = "/var/log/ib/deployer"

def logger(text_input="", debug_level="INFO", module="Deployer\Logger"):
	now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	
	return '[%s (CDT)] deployer.%s [%s] - %s []\n' % (now, debug_level, module, text_input)


#retrieve a config value from dictionary

def get_config_values(tmp_config="", get_value=""):
	for tmp_line in tmp_config.split('\n'):
		if ( get_value in tmp_line ):
			return tmp_line.split(' = ')[1]

### uniqid function derived from PHPs version

def uniqid():
	return "%14x.%08x" % ( math.modf(time.time())[1]*10000000 + math.modf(time.time())[0] * 10000000, (math.modf(time.time())[0]-math.floor(math.modf(time.time())[0]))*1000000000 )


def remove_values_from_list(the_list, val):
	while val in the_list:
		the_list.remove(val)
	return the_list

### this validates a user against IB's AD system

def validate_user(username="", password=""):
	try:
		user_exists = False
		ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, 0)
		ldap_connection = ldap.initialize(LDAP_SERVER)
		ldap_connection.simple_bind_s(username + "@" + BIND_DN, password.rstrip())
		results = ldap_connection.search_s(USER_BASE, ldap.SCOPE_SUBTREE, "(CN=*)")
		for dn, entry in results:
			if ( username in entry['sAMAccountName'][0] ):
				user_exists = True
				user_stats['user_name'] = entry['sAMAccountName'][0]			
				user_stats['full_name'] = entry['name'][0]
				user_stats['first_name'] = entry['givenName'][0]
				for member_entries in entry['memberOf']:
					if ('deployer' in member_entries):
						user_stats['deployer_roles'].append(member_entries.split(',')[0].split('CN=')[1].split('-')[1])
						
				print user_stats['deployer_roles']

				break

		return user_exists

	except ldap.LDAPError, e:
		#return 'Error connecting to LDAP server: ' + str(e) + '\n'
		return False


### This pulls a list of artifacts from the database

def get_artifact_list():
	artifact_list = []
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()
	
        artifact_names_query = """SELECT DISTINCT name FROM artifacts ORDER BY name"""
        curr.execute(artifact_names_query)
        for item in sorted(curr.fetchall()):
		artifact_list.append(item[0])
	
	return artifact_list



def get_artifact_details_by_env_json(artifact_name=""):
	
	artifact_details_query = """SELECT `artifacts`.*, `artifactEnvironments`.`id` AS `artifactEnvId`, `artifactEnvironments`.`env`, `artifactEnvironments`.`isLoadBalanced`, `artifactEnvironments`.`serverData`, 
	`artifactEnvironments`.`lastDeployedDate`, `artifactEnvironments`.`lastDeployedVersion`, `artifactEnvironments`.`lastDeployedUser`, `artifactEnvironments`.`isDeploying`, `artifactEnvironments`.`currentUser`, 
	`artifactEnvironments`.`currentUniqueId` FROM `artifacts`  INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifacts.name = artifactEnvironments.artifactName) AND 
	(artifacts.name = '%(artifact_name)s') AND (artifactEnvironments.env = 'dev');""" % locals()

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()
	
        curr.execute(artifact_details_query)
	
	return curr.fetchone()[0:]

def get_artifacts():
	artifact_env_query = """SELECT artifactName, env FROM artifactEnvironments ORDER BY artifactName"""

	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()
	curr.execute(artifact_env_query)

	results = {}

	unfiltered_results = curr.fetchall()

	cur_artifact = ""
	cur_env = ""
	env_list = []
	for a, b in list(unfiltered_results):
		if (cur_artifact != a):
			#first item in both lists
			if ( len(cur_artifact) > 0 ):
				results[cur_artifact] = dict( name=cur_artifact, envs=env_list )
			env_list = []
			cur_artifact = a
			cur_env = b
			env_list.append(b)
		elif (cur_env != b):
			cur_env = b
			env_list.append(b)
			#print cur_artifact, env_list
	return results



###Webapp Logic starts at this point

###This portion handles the dashboard portion of the deployer
	
@route('/dashboard')
@route('/dashboard/<get_path>')
@route('/dashboard/<get_path>/')
def dashboard(get_path=""):
	app_session = request.environ.get('beaker.session')
	
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0 ):
		conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
		curr = conn.cursor()
	
		if(get_path == "currentdeps"):
			curr.execute(query_dict['dash_currentdeps'])
			unfiltered_results = curr.fetchall()
			results = []
			counter = 0
			field_names = curr.description
			for c_item in unfiltered_results:
				#print str(c_item[8]).replace('"', '\\"').replace('[','{').replace(']','}')
				
				dash_dict_deps = dict()
				dash_dict_deps["id"] = str(c_item[0]).replace('None', 'null')
				dash_dict_deps["uniqueId"] = str(c_item[1]).replace('None', 'null')
				dash_dict_deps["pid"] = str(c_item[2]).replace('None', 'null')
				dash_dict_deps["type"] = str(c_item[3]).replace('None', 'null')
				dash_dict_deps["batchServers"] = str(c_item[4]).replace('None', 'null')
				dash_dict_deps["datetime"] = str(c_item[7]).replace('None', 'null')
				dash_dict_deps["artifactsSelected"] = str(c_item[8]).replace('None', 'null').replace('"', '\\"')
				dash_dict_deps["log"] = str(c_item[9]).replace('None', 'null').replace('\r', '\\r').replace('\n', '\\n').replace('"', '\\"')
				dash_dict_deps["idDryRun"] = str(c_item[10]).replace('None', 'null')
				dash_dict_deps["lastProgressBarValue"] = str(c_item[11]).replace('None', 'null')
				dash_dict_deps["lastProgressBarMessage"] = str(c_item[12]).replace('None', 'null')
				dash_dict_deps["user"] = str(c_item[13]).replace('None', 'null')
				dash_dict_deps["params"] = str(c_item[14]).replace('None', 'null').replace('"', '\\"').replace('[','{').replace(']','}')

				results.append( '''{"id":"%s","uniqueId":"%s","pid":"%s","type":"%s","batchServers":"%s","datetime":"%s","artifactsSelected":"%s","log":"%s","isDryRun":"%s","lastProgressBarValue":"%s","lastProgressBarMessage":"%s","user":"%s","params":"%s"}''' % (
				dash_dict_deps["id"],
				dash_dict_deps["uniqueId"],
				dash_dict_deps["pid"],
				dash_dict_deps["type"],
				dash_dict_deps["batchServers"],
				dash_dict_deps["datetime"],
				dash_dict_deps["artifactsSelected"],
				dash_dict_deps["log"],
				dash_dict_deps["idDryRun"],
				dash_dict_deps["lastProgressBarValue"],
				dash_dict_deps["lastProgressBarMessage"],
				dash_dict_deps["user"],
				dash_dict_deps["params"] ) )


			response.content_type = 'application/json, text/javascript'
 			
			return "[" + ",".join(results) + "]"

		if(get_path == "artifacts_old"):
			curr.execute(query_dict['dash_artifacts'])
			unfiltered_results = curr.fetchall()
			results = []
			field_names = curr.description
			for c_item in unfiltered_results:
				
				dash_dict_deps = dict()
				dash_dict_deps["id"] = str(c_item[12]).replace('None', 'null')
				dash_dict_deps["name"] = str(c_item[1]).replace('None', 'null')
				dash_dict_deps["serverType"] = str(c_item[2]).replace('None', 'null')
				dash_dict_deps["groupId"] = str(c_item[3]).replace('None', 'null')
				dash_dict_deps["artifactId"] = str(c_item[4]).replace('None', 'null')
				dash_dict_deps["artifactType"] = str(c_item[5]).replace('None', 'null')
				dash_dict_deps["mavenVersion"] = str(c_item[6]).replace('None', 'null')
				dash_dict_deps["deployName"] = str(c_item[7]).replace('None', 'null')
				dash_dict_deps["versionUrlSuffix"] = str(c_item[8]).replace('None', 'null')
				dash_dict_deps["statusUrlSuffix"] = str(c_item[9]).replace('None', 'null')
				dash_dict_deps["alwaysAllowSnapshots"] = str(c_item[10]).replace('None', 'null')
				dash_dict_deps["notes"] = str(c_item[11]).replace('None', 'null')
				dash_dict_deps["artifactName"] = str(c_item[13]).replace('None', 'null')
				dash_dict_deps["env"] = str(c_item[14]).replace('None', 'null')
				dash_dict_deps["isLoadBalanced"] = str(c_item[15]).replace('None', 'null')
				dash_dict_deps["serverData"] = str(c_item[16]).replace('None', 'null')
				dash_dict_deps["lastDeployedDate"] = str(c_item[17]).replace('None', 'null')
				dash_dict_deps["lastDeployedVersion"] = str(c_item[18]).replace('None', 'null')
				dash_dict_deps["isDeploying"] = str(c_item[19]).replace('None', 'null')
				dash_dict_deps["lastDeployedUser"] = str(c_item[20]).replace('None', 'null')
				dash_dict_deps["currentUser"] = str(c_item[21]).replace('None', 'null')
				dash_dict_deps["currentUniqueId"] = str(c_item[22]).replace('None', 'null')


				results.append( '''{"id":"%s","name":"%s","serverType":"%s","groupId":"%s","artifactId":"%s","artifactType":"%s","mavenVersion":"%s","deployName":"%s","versionUrlSuffix":"%s","statusUrlSuffix":"%s","alwaysAllowSnapshots":"%s","notes":"%s","artifactName":"%s","env":"%s","isLoadBalanced":"%s","serverData":"%s","lastDeployedDate":"%s","lastDeployedVersion":"%s","isDeploying":"%s","lastDeployedUser":"%s","currentUser":"%s","currentUniqueId":"%s"}''' % (
				dash_dict_deps["id"],
				dash_dict_deps["name"],
				dash_dict_deps["serverType"],
				dash_dict_deps["groupId"],
				dash_dict_deps["artifactId"],
				dash_dict_deps["artifactType"],
				dash_dict_deps["mavenVersion"],
				dash_dict_deps["deployName"],
				dash_dict_deps["versionUrlSuffix"],
				dash_dict_deps["statusUrlSuffix"],
				dash_dict_deps["alwaysAllowSnapshots"],
				dash_dict_deps["notes"],
				dash_dict_deps["artifactName"],
				dash_dict_deps["env"],
				dash_dict_deps["isLoadBalanced"],
				dash_dict_deps["serverData"],
				dash_dict_deps["lastDeployedDate"],
				dash_dict_deps["lastDeployedVersion"],
				dash_dict_deps["isDeploying"],
				dash_dict_deps["lastDeployedUser"],
				dash_dict_deps["currentUser"],
				dash_dict_deps["currentUniqueId"] ) )


			response.content_type = 'application/json, text/javascript'
 			
			return "[" + ",".join(results) + "]"

		if(get_path == "servers"):
			curr.execute(query_dict['dash_servers'])
			unfiltered_results = curr.fetchall()
			results = []

			for r_item in unfiltered_results:
				results.append('''{"hostname":"%s"}''' % ( r_item[0] ) )

			response.content_type = 'application/json, text/javascript'

			return "[" + ",".join(results) + "]"

	 	return template('dashboard', user=user_stats)
	redirect('/login')

@route('/')
@route('/login')
def login():
        return template('login', invalid_login=False)
	

@post('/login')
def do_login():
	if ( request.forms.get("username") and request.forms.get("password") ):
		if ( validate_user(request.forms['username'], request.forms['password']) ):
			app_session = request.environ.get('beaker.session')
    			app_session['logged_in'] = True

			redirect('/dashboard')
		else:	
			return template('login', invalid_login=True)

@route('/auth/logout')
@route('/logout')
def logout():
	app_session = request.environ.get('beaker.session')
	if app_session.get('logged_in'):
		app_session['logged_in'] = False
		user_stats = {'first_name':'', 'user_name':'' , 'full_name':''}
	redirect('/login')


@post('/deployer/index')
def deployer_index():
	app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
		conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
		curr = conn.cursor()

		artifacts_being_deployed = 0
		artifact_list = []
		pid = os.getpid()
		
		deployer_jobs['user'] = user_stats['user_name']
		deployer_jobs['uniqueId'] = uniqid()
		deployer_jobs['dateTime'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
		
		for req_key in request.forms.keys():
			#print req_key, request.forms[req_key]
			if ( 'artifacts[' in req_key ):
				artifacts_being_deployed += 1
				artifact_list.append(req_key.split('[')[1].split(']')[0])
			else:
				deployer_jobs[req_key] = request.form[req_key]
		
		deployer_jobs['artifacts'] = []

		for artifact_cnt in sorted(set(artifact_list)):
			deployer_jobs['artifacts'].append({ 'name': request.forms['artifacts[' + str(artifact_cnt) + '][name]' ],
							    'env': request.forms['artifacts[' + str(artifact_cnt) + '][env]' ], 
							    'versionToDeploy': request.forms['artifacts[' + str(artifact_cnt) + '][versionToDeploy]' ], 
							    'jiraIssueKey': request.forms['artifacts[' + str(artifact_cnt) + '][jiraIssueKey]' ],
							    'serverIncludes': request.forms.getall('artifacts[' + str(artifact_cnt) + '][serverIncludes][]'),
							    'serverExcludes': request.forms.getall('artifacts[' + str(artifact_cnt) + '][serverExcludes][]') })

		print deployer_jobs

		curr.execute('''INSERT INTO processes VALUES ( 0, '%s', %s, '%s', NULL, %s, '%s');''' % ( deployer_jobs['uniqueId'], pid, 'client', 1, '' ) )
		conn.commit()				
		
		pickle.dump( deployer_jobs, open(deployer_worker_path + "deployer_" + deployer_jobs['uniqueId'] + ".job", "wb") )


@route('/deployer/logtail/uniqueId/<unique_id>')
def watch(unique_id):
        app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
		job_log_filename = '''%s/deployer_%s.log''' % ( deployer_log_path, unique_id )
		fd = open(job_log_filename, 'r')
		input = fd.readlines()
		return ''.join(input).replace('\n','\n<br>')
		#input[len(input)-1:]
		
	redirect('/login')



@route('/deployer/watch/uniqueId/<unique_id>')
def watch(unique_id):
        app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
			conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
			curr = conn.cursor()

			watch_query = """SELECT `logs`.* FROM `logs` WHERE (uniqueId = '%s') LIMIT 1""" % ( unique_id )
			curr.execute(artifact_names_query)
			

	redirect('/login')




@route('/deploy')
@route('/deploy/<get_path>')
@post('/deploy/<get_path>')
def deploy(get_path=""):
	app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):

		print uniqid()
		environ_query = """SELECT DISTINCT env FROM artifactEnvironments ORDER BY env"""


		artifact_name_query = """SELECT  artifactName FROM artifactEnvironments ORDER BY artifactName"""

		if ( "get-artifacts" in get_path ):
			return get_artifacts()

		elif ( "get-saved-deployments" in get_path ):

			conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
			curr = conn.cursor()

			results = ""
		
			query = """SELECT `userSavedDeployments`.* FROM `userSavedDeployments` WHERE (username = 'mwiljanen' OR share = 1)"""

			curr.execute(query)

			unfiltered_results = curr.fetchall()

			field_names = curr.description
			for sa_id, sa_username, sa_deploymentNickname, sa_deploymentSelections, sa_share, sa_roles in unfiltered_results:
				results += '''[{"id":"%(sa_id)s","username":"%(sa_username)s","deploymentNickname":"%(sa_deploymentNickname)s","deploymentSelections":"%(sa_deploymentSelections)s","share":"%(sa_share)s","roles":"%(sa_roles)s"]''' % locals()
				

			response.content_type = 'text/html,application/json'
 
			return results

		elif ( "index" in get_path ):

			return "True"

		elif ( "get-username" in get_path ):
			response.content_type = 'application/json'
			return '''{"username":"%s"}''' % ( user_stats['user_name'] )

		elif ( "get-roles" in get_path ):
			response.content_type = 'application/json'
			return str(user_stats['deployer_roles'])

		else:
			conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
			curr = conn.cursor()

			artifact_names_query = """SELECT DISTINCT name FROM artifacts"""
			curr.execute(artifact_names_query)

			deployable_artifacts="<select>"
			for item in curr.fetchall():
				deployable_artifacts += "<option>" + item[0] + "</option>\n"
			deployable_artifacts +="</select>"

			curr.execute("""SELECT distinct env FROM artifactEnvironments""")

			deploy_environs="<select>"

	        	for item in curr.fetchall():
        	        	deploy_environs += "<option>" + item[0] + "</option>\n"
		        deploy_environs +="</select>"


			curr.execute("""SELECT `artifacts`.*, `artifactEnvironments`.* FROM `artifacts` INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifactEnvironments.isDeploying = 1)""")

			artifacts=[]
			for artifact_item in curr.fetchall():
				artifacts.append(artifact_item)

			return template('deploy', user=user_stats)

	redirect('/login')

@route('/history/viewlog/uniqueId/<unique_id>')
def history_viewlog(unique_id):
        app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
			conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
			curr = conn.cursor()

			view_history_query = """SELECT `logs`.* FROM `logs` WHERE (uniqueId = '%s') LIMIT 1""" % ( unique_id  )
		
			curr.execute(view_history_query)
			results = curr.fetchone()

			return template('viewlog', unique_id=str(results[1]), logs=str(results[4]), user=user_stats)

	redirect('/login')

@get('/artifacts/get-nexus-versions')
@get('/artifacts/get-nexus-versions/')
def get_nexus_versions():
	if( request.query.get("artifactName") and request.query.get("artifactEnv") ):
		conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
		curr = conn.cursor()

		artifact_env = request.query["artifactEnv"]

		get_nexus_query = """select nexus_metadata.* from nexus_metadata inner join artifacts on nexus_metadata.artifactId = artifacts.artifactId where artifacts.name = '%s';""" % ( request.query["artifactName"] )
		curr.execute(get_nexus_query)
		nexus_meta_data = curr.fetchone()

		versions = [] 
				
		for item in nexus_meta_data[3].split('|'):
			if ( (artifact_env == 'qa' or artifact_env == 'prod' or artifact_env == 'stage') and 'SNAPSHOT' not in item):
				versions.append('''"%s"''' % ( item ) )
			elif ( artifact_env == 'dev' ):
				versions.append('''"%s"''' % ( item ) )

		response.content_type = 'application/json'
		return "[" + ",".join(versions) + "]"

@get('/artifacts/view')
@get('/artifacts/view/')
def get_artifact_view():
	artifact_env_query = """SELECT `artifacts`.*, `artifactEnvironments`.`id` AS `artifactEnvId`, `artifactEnvironments`.`env`, `artifactEnvironments`.`isLoadBalanced`, `artifactEnvironments`.`serverData`, `artifactEnvironments`.`lastDeployedDate`, `artifactEnvironments`.`lastDeployedVersion`, `artifactEnvironments`.`lastDeployedUser`, `artifactEnvironments`.`isDeploying`, `artifactEnvironments`.`currentUser`, `artifactEnvironments`.`currentUniqueId` FROM `artifacts`
	INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifacts.name = artifactEnvironments.artifactName) AND (artifacts.name = '%s') ORDER BY Field(artifactEnvironments.env, 'dev','qa','stage','prod') """ % ( request.query["name"] )
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	curr.execute(artifact_env_query)

	artifact_fields = curr.description

	#print str(artifact_fields), len(artifact_fields)

	unfiltered_results = curr.fetchall()

	#print str(unfiltered_results[0][13])

	artifact_env_id = { 'qa': {}, 'dev': {}, 'prod': {}, 'stage': {}}

	
	current_envs = []
	for env_ctr_item in unfiltered_results:
		current_envs.append(env_ctr_item[13])

	#print current_envs

	server_data = []

	for item_line in unfiltered_results:
		server_data.append(''.join(item_line[15].split()).rstrip())
		artifact_env_id[item_line[13]]['isLoadBalanced'] = str(item_line[14])
		artifact_env_id[item_line[13]]['isDeploying'] = str(item_line[19])
		artifact_env_id[item_line[13]]['lastDeployedDate'] = str(item_line[16])
		artifact_env_id[item_line[13]]['lastDeployedUser'] = str(item_line[18])
		artifact_env_id[item_line[13]]['lastDeployedVersion'] = str(item_line[17])
							
	#print str(artifact_env_id.keys())

			
	#print configs['deployer']
	#print configs['deployer'].find("skipSiteLoadingHostsRegexp")
			
	blacklisted_subtypes = get_config_values(configs['deployer'], "skipSiteLoadingHosts")

	default_servers = ""
	servers = []
	for server_line in server_data:
		for env_line in server_line.split(','):
			ib_env_query = ""
			for ib_env_line in env_line.split('|'):
				if ( "ib_system_type" in ib_env_line ):
					ib_env_query += """ib_system_type = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
				elif ( "ib_system_subtype" in ib_env_line ):
					ib_env_query += """ AND ib_system_subtype = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
				elif ( "ib_partner_group" in ib_env_line):
					ib_env_query += """ AND ib_partner_group = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
				elif ( "ib_env" in ib_env_line ):
					ib_env_query += """ AND ib_env = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
				elif ( "hostname" in ib_env_line):
					ib_env_query += """hostname = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
				else:
					ib_env_query += ""
					#print ib_env_query
			curr.execute("""select hostname,ib_datacenter,ib_env,ib_dc,ib_dc_room,ib_partner_group,ib_system_subtype,ib_system_type from hostinfo where %s""" % ( ib_env_query ) )
			for srv_host in curr.fetchall():
				if (srv_host[6] not in blacklisted_subtypes ):
					server_hosts = {}
					server_hosts["_config"] = {}
					server_hosts["dataCenter"] = srv_host[1]
					server_hosts["dc"] = srv_host[3]
					server_hosts["dcRoom"] = srv_host[4]
					server_hosts["domain"] = srv_host[1] + "ib-" + srv_host[2] + ".com"
					server_hosts["env"] = srv_host[2]
					server_hosts["fqdn"] = srv_host[0] + "." + srv_host[1] + ".ib-" + srv_host[2] + ".com"
					server_hosts["hostname"] = srv_host[0]
					server_hosts["isLoadBalanced"] = artifact_env_id[srv_host[2]]['isLoadBalanced']
					server_hosts["lastDeploymentStatus"] = "null"
					server_hosts["lastDeploymentVersion"] = artifact_env_id[srv_host[2]]['lastDeployedVersion']
					server_hosts["loadBalancers"] = []
					server_hosts["monitoringEnabled"] = "true"
					server_hosts["partnerGroup"] = srv_host[5]
					server_hosts["productionState"] = "null"
					server_hosts["sshEnabled"] = "true"
					server_hosts["systemSubtype"] = srv_host[6]
					server_hosts["systemType"] = srv_host[7]
					servers.append(server_hosts)													
			
	default_servers = '''"defaultServers":{'''
	for def_serv in servers:
		if (def_serv["env"] == "prod"):
			default_servers += '''"%s":%s,''' % (def_serv["fqdn"], json.dumps(def_serv))
	default_servers = default_servers[:-1]
	default_servers += "},"
	env_counters = [0,0,0,0]

	env_server = {}

	for art_env_cnt in current_envs:
			
		artifact_env_id[art_env_cnt]['servers'] = '''"%s":{"currentUniqueId":"%s","currentUser":"%s","isDeploying":"%s","isLoadBalanced":"%s","lastDeployedDate":"%s","lastDeployedUser":"%s","lastDeployedVersion":"%s","servers":{''' % ( art_env_cnt, "",user_stats['user_name'], artifact_env_id[art_env_cnt]['isDeploying'],artifact_env_id[art_env_cnt]['isLoadBalanced'],artifact_env_id[art_env_cnt]['lastDeployedDate'],artifact_env_id[art_env_cnt]['lastDeployedUser'],artifact_env_id[art_env_cnt]['lastDeployedVersion'] )

		for env_serv in servers:
			if(env_serv["env"] == 'dev' and art_env_cnt == 'dev'):
				artifact_env_id[art_env_cnt]['servers'] += '''"%s":%s,''' % (env_serv["hostname"], json.dumps(env_serv))
				env_counters[0] += 1
			if(env_serv["env"] == 'qa' and art_env_cnt == 'qa'):
				artifact_env_id[art_env_cnt]['servers'] += '''"%s":%s,''' % (env_serv["hostname"], json.dumps(env_serv))
				env_counters[1] += 1

			if(env_serv["env"] == 'stage' and art_env_cnt == 'stage'):
				artifact_env_id[art_env_cnt]['servers'] += '''"%s":%s,''' % (env_serv["hostname"], json.dumps(env_serv))
				env_counters[2] += 1

			if(env_serv["env"] == 'prod' and art_env_cnt == 'prod'):
				artifact_env_id[art_env_cnt]['servers'] += '''"%s":%s,''' % (env_serv["hostname"], json.dumps(env_serv))
				env_counters[3] += 1
			
		artifact_env_id[art_env_cnt]['servers'] = artifact_env_id[art_env_cnt]['servers'][:-1] + '''}}'''


	all_envs = '''"envs":{'''
	if (env_counters[0] > 0):
		all_envs += artifact_env_id['dev']['servers']  + ","
	if (env_counters[1] > 0):
		all_envs += artifact_env_id['qa']['servers']  + ","
	if (env_counters[2] > 0):
		all_envs += artifact_env_id['stage']['servers']  + ","
	if (env_counters[3] > 0):
		all_envs += artifact_env_id['prod']['servers'] 

	all_envs += "},"

	res = []
	for tmp_item in unfiltered_results:
		res_temp = []
		for tmp_item_2 in tmp_item:
			if (tmp_item_2 is None):
				res_temp.append("null")
			else:
				res_temp.append(tmp_item_2)
		res.append(res_temp)


	response.content_type = 'application/json'

	return template("artifact_json_view", default_servers=default_servers, all_envs=all_envs, res=res)



@route('/dashboard/artifacts')
@route('/dashboard/artifacts/')
def get_current_deployments():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()
	curr.execute(query_dict['dash_artifacts'])
	unfiltered_results = curr.fetchall()
	results = []
	field_names = curr.description
	for c_item in unfiltered_results:
		dash_dict_deps = dict()
		dash_dict_deps["id"] = str(c_item[12]).replace('None', 'null')
		dash_dict_deps["name"] = str(c_item[1]).replace('None', 'null')
		dash_dict_deps["serverType"] = str(c_item[2]).replace('None', 'null')
		dash_dict_deps["groupId"] = str(c_item[3]).replace('None', 'null')
		dash_dict_deps["artifactId"] = str(c_item[4]).replace('None', 'null')
		dash_dict_deps["artifactType"] = str(c_item[5]).replace('None', 'null')
		dash_dict_deps["mavenVersion"] = str(c_item[6]).replace('None', 'null')
		dash_dict_deps["deployName"] = str(c_item[7]).replace('None', 'null')
		dash_dict_deps["versionUrlSuffix"] = str(c_item[8]).replace('None', 'null')
		dash_dict_deps["statusUrlSuffix"] = str(c_item[9]).replace('None', 'null')
		dash_dict_deps["alwaysAllowSnapshots"] = str(c_item[10]).replace('None', 'null')
		dash_dict_deps["notes"] = str(c_item[11]).replace('None', 'null')
		dash_dict_deps["artifactName"] = str(c_item[13]).replace('None', 'null')
		dash_dict_deps["env"] = str(c_item[14]).replace('None', 'null')
		dash_dict_deps["isLoadBalanced"] = str(c_item[15]).replace('None', 'null')
		dash_dict_deps["serverData"] = str(c_item[16]).replace('None', 'null')
		dash_dict_deps["lastDeployedDate"] = str(c_item[17]).replace('None', 'null')
		dash_dict_deps["lastDeployedVersion"] = str(c_item[18]).replace('None', 'null')
		dash_dict_deps["isDeploying"] = str(c_item[19]).replace('None', 'null')
		dash_dict_deps["lastDeployedUser"] = str(c_item[20]).replace('None', 'null')
		dash_dict_deps["currentUser"] = str(c_item[21]).replace('None', 'null')
		dash_dict_deps["currentUniqueId"] = str(c_item[22]).replace('None', 'null')

		results.append( '''{"id":"%s","name":"%s","serverType":"%s","groupId":"%s","artifactId":"%s","artifactType":"%s","mavenVersion":"%s","deployName":"%s","versionUrlSuffix":"%s","statusUrlSuffix":"%s","alwaysAllowSnapshots":"%s","notes":"%s","artifactName":"%s","env":"%s","isLoadBalanced":"%s","serverData":"%s","lastDeployedDate":"%s","lastDeployedVersion":"%s","isDeploying":"%s","lastDeployedUser":"%s","currentUser":"%s","currentUniqueId":"%s"}''' % (
		dash_dict_deps["id"],
		dash_dict_deps["name"],
		dash_dict_deps["serverType"],
		dash_dict_deps["groupId"],
		dash_dict_deps["artifactId"],
		dash_dict_deps["artifactType"],
		dash_dict_deps["mavenVersion"],
		dash_dict_deps["deployName"],
		dash_dict_deps["versionUrlSuffix"],
		dash_dict_deps["statusUrlSuffix"],
		dash_dict_deps["alwaysAllowSnapshots"],
		dash_dict_deps["notes"],
		dash_dict_deps["artifactName"],
		dash_dict_deps["env"],
		dash_dict_deps["isLoadBalanced"],
		dash_dict_deps["serverData"],
		dash_dict_deps["lastDeployedDate"],
		dash_dict_deps["lastDeployedVersion"],
		dash_dict_deps["isDeploying"],
		dash_dict_deps["lastDeployedUser"],
		dash_dict_deps["currentUser"],
		dash_dict_deps["currentUniqueId"] ) )

	response.content_type = 'application/json, text/javascript'
 			
	return "[" + ",".join(results) + "]"



@route('/artifacts')
@get('/artifacts/<view_path>')
@get('/artifacts/<view_path>/')
def artifacts(view_path="", artifact_name=""):
	app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
		if( view_path == "view_old" ):
			artifact_env_query = """SELECT `artifacts`.*, `artifactEnvironments`.`id` AS `artifactEnvId`, `artifactEnvironments`.`env`, `artifactEnvironments`.`isLoadBalanced`, `artifactEnvironments`.`serverData`, `artifactEnvironments`.`lastDeployedDate`, `artifactEnvironments`.`lastDeployedVersion`, `artifactEnvironments`.`lastDeployedUser`, `artifactEnvironments`.`isDeploying`, `artifactEnvironments`.`currentUser`, `artifactEnvironments`.`currentUniqueId` FROM `artifacts`
			INNER JOIN `artifactEnvironments` ON artifacts.name = artifactEnvironments.artifactName WHERE (artifacts.name = artifactEnvironments.artifactName) AND (artifacts.name = '%s') ORDER BY Field(artifactEnvironments.env, 'dev','qa','stage','prod') """ % ( request.query["name"] )

			conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
			curr = conn.cursor()

			curr.execute(artifact_env_query)

			artifact_fields = curr.description

			#print str(artifact_fields), len(artifact_fields)

			unfiltered_results = curr.fetchall()

			#print str(unfiltered_results[0][13])

			artifact_env_id = { 'qa': {}, 'dev': {}, 'prod': {}, 'stage': {}}

			
			current_envs = []
			for env_ctr_item in unfiltered_results:
				current_envs.append(env_ctr_item[13])

			#print current_envs

			server_data = []

			for item_line in unfiltered_results:
				server_data.append(''.join(item_line[15].split()).rstrip())
				artifact_env_id[item_line[13]]['isLoadBalanced'] = str(item_line[14])
				artifact_env_id[item_line[13]]['isDeploying'] = str(item_line[19])
				artifact_env_id[item_line[13]]['lastDeployedDate'] = str(item_line[16])
				artifact_env_id[item_line[13]]['lastDeployedUser'] = str(item_line[18])
				artifact_env_id[item_line[13]]['lastDeployedVersion'] = str(item_line[17])
							
			#print str(artifact_env_id.keys())

			
			#print configs['deployer']
			#print configs['deployer'].find("skipSiteLoadingHostsRegexp")
			
			blacklisted_subtypes = get_config_values(configs['deployer'], "skipSiteLoadingHosts")

			default_servers = ""
			servers = []
			for server_line in server_data:
				for env_line in server_line.split(','):
					ib_env_query = ""
					for ib_env_line in env_line.split('|'):
						if ( "ib_system_type" in ib_env_line ):
							ib_env_query += """ib_system_type = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
						elif ( "ib_system_subtype" in ib_env_line ):
							ib_env_query += """ AND ib_system_subtype = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
						elif ( "ib_partner_group" in ib_env_line):
							ib_env_query += """ AND ib_partner_group = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
						elif ( "ib_env" in ib_env_line ):
							ib_env_query += """ AND ib_env = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
						elif ( "hostname" in ib_env_line):
							ib_env_query += """hostname = '%s' """ % ( ib_env_line[(ib_env_line.find('.eq.') + 4):] )
						else:
							ib_env_query += ""
					#print ib_env_query
					curr.execute("""select hostname,ib_datacenter,ib_env,ib_dc,ib_dc_room,ib_partner_group,ib_system_subtype,ib_system_type from hostinfo where %s""" % ( ib_env_query ) )
					for srv_host in curr.fetchall():
						if (srv_host[6] not in blacklisted_subtypes ):
							server_hosts = {}
							server_hosts["_config"] = {}
							server_hosts["dataCenter"] = srv_host[1]
							server_hosts["dc"] = srv_host[3]
							server_hosts["dcRoom"] = srv_host[4]
							server_hosts["domain"] = srv_host[1] + "ib-" + srv_host[2] + ".com"
							server_hosts["env"] = srv_host[2]
							server_hosts["fqdn"] = srv_host[0] + "." + srv_host[1] + ".ib-" + srv_host[2] + ".com"
							server_hosts["hostname"] = srv_host[0]
							server_hosts["isLoadBalanced"] = artifact_env_id[srv_host[2]]['isLoadBalanced']
							server_hosts["lastDeploymentStatus"] = "null"
							server_hosts["lastDeploymentVersion"] = artifact_env_id[srv_host[2]]['lastDeployedVersion']
							server_hosts["loadBalancers"] = []
							server_hosts["monitoringEnabled"] = "true"
							server_hosts["partnerGroup"] = srv_host[5]
							server_hosts["productionState"] = "null"
							server_hosts["sshEnabled"] = "true"
							server_hosts["systemSubtype"] = srv_host[6]
							server_hosts["systemType"] = srv_host[7]
							servers.append(server_hosts)													
			
			default_servers = '''"defaultServers":{'''
			for def_serv in servers:
				if (def_serv["env"] == "prod"):
					default_servers += '''"%s":%s,''' % (def_serv["fqdn"], json.dumps(def_serv))
			default_servers = default_servers[:-1]
			default_servers += "},"

			env_counters = [0,0,0,0]

			env_server = {}

			for art_env_cnt in current_envs:
			
				artifact_env_id[art_env_cnt]['servers'] = '''"%s":{"currentUniqueId":"%s","currentUser":"%s","isDeploying":"%s","isLoadBalanced":"%s","lastDeployedDate":"%s","lastDeployedUser":"%s","lastDeployedVersion":"%s","servers":{''' % ( art_env_cnt, "",user_stats['user_name'], artifact_env_id[art_env_cnt]['isDeploying'],artifact_env_id[art_env_cnt]['isLoadBalanced'],artifact_env_id[art_env_cnt]['lastDeployedDate'],artifact_env_id[art_env_cnt]['lastDeployedUser'],artifact_env_id[art_env_cnt]['lastDeployedVersion'] )

				for env_serv in servers:
					if(env_serv["env"] == 'dev' and art_env_cnt == 'dev'):
						artifact_env_id[art_env_cnt]['servers'] += '''"%s":%s,''' % (env_serv["hostname"], json.dumps(env_serv))
						env_counters[0] += 1

					if(env_serv["env"] == 'qa' and art_env_cnt == 'qa'):
						artifact_env_id[art_env_cnt]['servers'] += '''"%s":%s,''' % (env_serv["hostname"], json.dumps(env_serv))
						env_counters[1] += 1

					if(env_serv["env"] == 'stage' and art_env_cnt == 'stage'):
						artifact_env_id[art_env_cnt]['servers'] += '''"%s":%s,''' % (env_serv["hostname"], json.dumps(env_serv))
						env_counters[2] += 1

					if(env_serv["env"] == 'prod' and art_env_cnt == 'prod'):
						artifact_env_id[art_env_cnt]['servers'] += '''"%s":%s,''' % (env_serv["hostname"], json.dumps(env_serv))
						env_counters[3] += 1
			
				artifact_env_id[art_env_cnt]['servers'] = artifact_env_id[art_env_cnt]['servers'][:-1] + '''}}'''


			all_envs = '''"envs":{'''
			if (env_counters[0] > 0):
				all_envs += artifact_env_id['dev']['servers']  + ","
			if (env_counters[1] > 0):
				all_envs += artifact_env_id['qa']['servers']  + ","
			if (env_counters[2] > 0):
				all_envs += artifact_env_id['stage']['servers']  + ","
			if (env_counters[3] > 0):
				all_envs += artifact_env_id['prod']['servers'] 

			all_envs += "},"

			res = []
			for tmp_item in unfiltered_results:
				res_temp = []
				for tmp_item_2 in tmp_item:
					if (tmp_item_2 is None):
						res_temp.append("null")
					else:
						res_temp.append(tmp_item_2)
				res.append(res_temp)

			#print res

			response.content_type = 'application/json'

			return template("artifact_json_view", default_servers=default_servers, all_envs=all_envs, res=res)




		elif ( view_path == "get-nexus-versions-old2"):
			if( request.query.get("artifactName") and request.query.get("artifactEnv") ):
				conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
				curr = conn.cursor()
			

				artifact_env = request.query["artifactEnv"]

				get_nexus_query = """select nexus_metadata.* from nexus_metadata inner join artifacts on nexus_metadata.artifactId = artifacts.artifactId where artifacts.name = '%s';""" % ( request.query["artifactName"] )
				curr.execute(get_nexus_query)
				nexus_meta_data = curr.fetchone()

				versions = [] 
				
				for item in nexus_meta_data[3].split('|'):
					if ( (artifact_env == 'qa' or artifact_env == 'prod' or artifact_env == 'stage') and 'SNAPSHOT' not in item):
						versions.append('''"%s"''' % ( item ) )
					elif ( artifact_env == 'dev' ):
						versions.append('''"%s"''' % ( item ) )

				response.content_type = 'application/json'

				return "[" + ",".join(reversed(versions)) + "]"

				#response.content_type = 'application/json'

				#return '''["''' + '","'.join(reversed(nexus_meta_data[3].split('|'))) + '''"]'''

		elif ( view_path == "get-nexus-versions-old"):
			if( request.query.get("artifactName") and request.query.get("artifactEnv") ):
				conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
				curr = conn.cursor()

				artifact_env = request.query["artifactEnv"]

				get_deploy_name = """SELECT `artifactId` FROM `artifacts` WHERE (name = '%s') LIMIT 1""" % ( request.query["artifactName"] )
				curr.execute(get_deploy_name)
				deployed_name = str(curr.fetchone()[0])			

				print deployed_name, artifact_env
				c = pycurl.Curl()			
				buf = cStringIO.StringIO()

				c.setopt(c.URL, nexus_url + deployed_name + "/maven-metadata.xml")
				c.setopt(c.WRITEFUNCTION, buf.write)
				c.perform()

				xmldoc = minidom.parseString(buf.getvalue())
				item_list = xmldoc.getElementsByTagName('version')
				versions = []
				for item in item_list:
					if ( (artifact_env == 'qa' or artifact_env == 'prod') and 'SNAPSHOT' not in item.childNodes[0].nodeValue):
						versions.append('''"%s"''' % (item.childNodes[0].nodeValue) )
					elif ( artifact_env == 'dev' ):
						versions.append('''"%s"''' % (item.childNodes[0].nodeValue) )

				response.content_type = 'application/json'

				return "[" + ",".join(reversed(versions)) + "]"
				
				

		else:
			return template('artifacts', user=user_stats)
	redirect('/login')



@route('/history')
def history():
	app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
	 	return template('history', user=user_stats)
	redirect('/login')

@route('/admin/<admin_path>')
def admin(admin_path):
	app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
		if ( admin_path == "Artifacts" ):
		 	return template('admin_artifacts', user=user_stats)
		if ( admin_path == "permissions" ):
		 	return template('admin_permissions', user=user_stats)
		if ( admin_path == "prune" ):
		 	return template('admin_prune', user=user_stats)
		if ( admin_path == "reports" ):
		 	return template('admin_reports', user=user_stats)

	redirect('/login')

@post('/admin/artifacts/create-artifact-service-dependency-mapping')
def create_artifact_service_dependancy_mapping():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	query = '''INSERT INTO artifactServiceDependencyMapping VALUES (0,'%s', '%s', '%s', %s);''' % ( request.forms['artifactName'], request.forms['env'], request.forms['dependency_mapping'], request.forms['deployPriority'])
	try:
		curr.execute(query)
		conn.commit()
	except MySQLdb.error, e:
		return 

@post('/admin/artifacts/update-artifact-service-dependency-mapping')
def update_artifact_service_dependancy_mapping():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	query = '''UPDATE artifactServiceDependencyMapping SET artifactName = '%s', env = '%s', dependency_mapping = '%s', deployPriority = %s WHERE artifactName = '%s' and env = '%s';''' % ( request.forms['artifactName'], request.forms['env'], request.forms['dependency_mapping'], request.forms['deployPriority'], request.forms['artifactName'], request.forms['env'])
	print query
	curr.execute(query)
	conn.commit()


@post('/admin/artifacts/delete-artifact-service-dependency-mapping')
def delete_artifact_service_dependancy_mapping():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	query = '''DELETE FROM  artifactServiceDependencyMapping WHERE artifactName = '%s' and env = '%s';''' % ( request.forms['artifactName'], request.forms['env'] )

	curr.execute(query)
	conn.commit()

@route('/admin/artifacts/get-artifact-service-dependency-mapping')
def get_artifact_service_dependancy_mapping():
	conn = MySQLdb.connect(db_connect[0], db_connect[1], db_connect[2], db_connect[3])
	curr = conn.cursor()

	get_service_deps_query = ""

	artifact_name = ""

	output_str = ""

	result_list = []
	filter_list = {}

	if ( request.query['filter[filters][0][value]'] ):
		
		for req_key in request.query:
			if ( 'value' in req_key ):
				artifact_name = request.query[req_key]
				print artifact_name
				get_service_deps_query = '''SELECT * from artifactServiceDependencyMapping WHERE artifactName = '%s';''' % ( request.query[req_key] )
				
			else:
				filter_list[req_key] = request.query[req_key]
		

		get_service_deps_query = '''SELECT * from artifactServiceDependencyMapping WHERE artifactName = '%s';''' % ( request.query['filter[filters][0][value]']  )
		if ( curr.execute(get_service_deps_query) ):
			for result_item in curr.fetchall():
				result_list.append('''{"id":"%s","artifactName":"%s","env":"%s","dependency_mapping":"%s","deployPriority":"%s"}''' % ( result_item[0], result_item[1], result_item[2], result_item[3], result_item[4] ) )
	

		output_str = '''{"data":[%s],"total":"0","params":{"module":"admin", "controller":"artifacts","action":"get-artifact-service-dependency-mapping","take":"%s","skip":"%s","page":"%s","pageSize":"%s","filter":{"logic":"and","filters":[{"field":"artifactName","operator":"eq","value":"%s"}]}}}''' % ( ','.join(result_list), filter_list['take'], filter_list['skip'], filter_list['page'],filter_list['pageSize'], artifact_name )
		response.content_type = 'application/json'

	return output_str


	
@route('/monitor/status')
def monitor():
	app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
		return template('monitor_status')
	redirect('/login')

@route('/help')
def deployer_help():
	app_session = request.environ.get('beaker.session')
	if ( 'logged_in' in app_session.keys() and len(user_stats['user_name']) > 0):
		return template('help', user=user_stats)
	redirect('/login')


# Static Routes
@get('/js/<filename:re:.*\.js>')
def javascript(filename):
	return static_file(filename, root='public/js')

@get('/css/<filename:re:.*\.css>')
def stylesheets(filename):
	return static_file(filename, root='public/css')

@get('/css/ib-brand/images/<filename:re:.*\.(png|jpg|gif|jpeg|ico)>')
def ibcustom(filename):
	return static_file(filename, root='public/css/ib-brand/images/')

@get('/css/smoothness/images/<filename:re:.*\.(png|jpg|gif|jpeg|ico)>')
def ibcustom(filename):
	return static_file(filename, root='public/css/ib-brand/images/')

@get('/images/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
	return static_file(filename, root='public/images')

@get('/bootstrap/img/<filename:re:.*\.(png|jpg|gif|jpeg)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/img')

@get('/bootstrap/js/<filename:re:.*\.(js)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/js')

@get('/bootstrap/css/<filename:re:.*\.(css)>')
def bootstrap(filename):
	return static_file(filename, root='public/bootstrap/css')

@get('/cache/<filename:re:.*\.(js|css)>')
def cache(filename):
	return static_file(filename, root='public/cache')

@get('/screenshots/<unique_id>/<filename>')
def screenshots(unique_id, filename):
	return static_file(filename, root='public/screenshots/' + unique_id )

@error(404)
def error404(error):
	return "Oops!<br>Unable to find page<br>"



debug(True)
run(app=app_middleware, host='localhost', port=18080, server='flup', reloader=True)
