function initArtifactsView() {
    $(".server-version").each(function() {
        var artifactId = $(this).attr('alt');
        var url = $(this).attr('title');
        var serverVersion = $(this);

        serverVersion.html('<img src="/images/ajax-loaders/indicator-trans.gif"/>');

        $.getJSON('/artifacts/get-live-version/',
        {
            artifactId: artifactId,
            url: url
        },
        function(data) {
            if (data != 'UNKNOWN') {
                serverVersion.html('<a href="' + url.replace('/xml', '') + '">' + data + "</a>");
            } else {
                serverVersion.html('<img src="/images/16X16/help.png" alt="UNKNOWN" title="UNKNOWN"/>');
            }

            serverVersion.children('a').colorbox({
                iframe: true,
                width: "40%",
                height: "40%"
            });
        });

    });

    $(".service-status-wrapper").each(function() {
        var artifactId = $(this).attr('alt');
        var url = $(this).attr('title');
        var serviceStatusWrapper = $(this);

        serviceStatusWrapper.html('<img src="/images/ajax-loaders/flower-trans.gif"/>');

        $.getJSON('/artifacts/get-service-status/',
            {
                artifactId: artifactId,
                url: url
            },
            function(data) {
                switch (data) {
                    case 'n/a':
                        serviceStatusWrapper.html('<a class="btn btn-mini btn-warning service-status" href="#"><i class="icon-ban-circle icon-white"></i></a>');
                        break;
                    case 1:
                        serviceStatusWrapper.html('<a class="btn btn-mini btn-success service-status" href="' + url + '"><i class="icon-thumbs-up icon-white"></i></a>');
                        break;
                    case 0:
                        serviceStatusWrapper.html('<a class="btn btn-mini btn-danger service-status" href="' + url + '"><i class="icon-thumbs-down icon-white"></i></a>');
                        break;
                }

                if (data != 'n/a') {
                    serviceStatusWrapper.children('.service-status').colorbox({
                        iframe: true,
                        width: "50%",
                        height: "50%"
                    });
                }

            });

    });

    $(".host-graphs").colorbox({
        iframe:true, 
        width:"90%", 
        height:"90%"
    });


    var dtOptions = {
        "sScrollY": "350px",
        "bPaginate": false,
        "bScrollCollapse": true,
        "bDestroy": true,
        "bFilter": false,
        "bSort": false,
        "bInfo": false
    };

    $('.artifact-servers').dataTable(dtOptions);

    // refresh datatable on tab selection
    $('a[data-toggle="tab"]').on('shown', function (e) {
        //e.target // activated tab
        //e.relatedTarget // previous tab
        $(e.target.hash).find('table').dataTable(dtOptions);
    });
}