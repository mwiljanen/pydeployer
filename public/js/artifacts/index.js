function initArtifactsIndex() {

    // datatable
    $('#artifacts-datatable').dataTable({
        "bStateSave": true,
        "bJQueryUI": true,
        "sScrollY": "425px",
        "bPaginate": false,
        "bScrollCollapse": true,
        "aoColumns": [
            /* name */          { "sWidth": "170px" },
            /* artifactId */    { "bSearchable": true,
                                "bVisible":    false },
            /* deployName */    { "bSearchable": true,
                                "bVisible":    false },
            /* environment */   null
        ]
    });

    // tooltips
    $('.last-deployed-tooltip').tooltip();
    $('.deploying-tooltip').tooltip();
}