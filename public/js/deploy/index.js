// on page load, initialize form and load saved deployments
function initDeployIndex() {
    bindDeploymentSaveFunctions();
    bindDeployFormSubmit();

    $('#clear-form').click(function() {
        clearForm(false);
    });

    getUsername();
    getRoles();

    $('#deployWell').prepend('<div id="artifactsLoader" class="alert alert-info"><img src="/images/ajax-loaders/snake-trans.gif" style="vertical-align: middle;"/> <strong>Loading artifacts...</strong>');

    // get artifacts (this needs to be a synchronous request in order for things to load properly)
    var request = $.ajax({
        async: false,
        url: '/deploy/get-artifacts',
        dataType: "json",
        success: function(data) {
            artifacts = data;

            $.each(artifacts, function(name) {
                artifactNames.push(name);
            });


            // check for saved selections and build form using those, otherwise initialize form with one artifact selector
            var artifactsToDeploy = JSON.parse(localStorage.getItem('artifactsToDeploy'));

            if (artifactsToDeploy === null || artifactsToDeploy.length == 0) {
                addArtifactsSelector();
            } else {
                loadSavedArtifacts(artifactsToDeploy);
            }

            $('#artifactsLoader').fadeOut();
            $('#deployForm').fadeIn(1000);

            // enable #addArtifact button
            $('#addArtifact').click(addArtifactsSelector);
        }
    });

    request.fail(function(jqXHR) {
        $('#artifactsLoader').fadeOut();

        $('#saveDeployment').fadeOut();

        $('#deployWell').prepend('<div class="alert alert-warning fade in"><button type="button" class="close" data-dismiss="alert">×</button>' +
            '<h4>' + jqXHR.responseText + '</h4></div>');
    });

    // restore deploy options
    loadSavedOptions();

    getSavedDeployments();

    //bind form-saving function
    $(window).bind('unload', function() {
        saveDeployment();
    });
}


/**
 * Get roles
 */
function getRoles() {
    $.getJSON(
        '/deploy/get-roles',
        function(data) {
            roles = data;
        }
    );
}


/**
 * Get username
 */
function getUsername() {
    $.getJSON(
        '/deploy/get-username',
        function(data) {
            username = data['username'];
        }
    );
}


/* FORM ELEMENTS */

/** ARTIFACT SELECTOR **/

/**
 * Delete an artifact from the form
 */
function bindArtifactDelete() {
    $('.artifact-delete').unbind('click').click(function() {
        $(this).parent().remove();
    });
}


/**
 * Add an artifact selector to the form
 */
function addArtifactsSelector() {
    artifactCount++;

    $('#artifactsToDeploy').append(generateArtifactSelectorHTML(artifactCount, null));

    $('#' + artifactCount).find('.artifact-name').focus();

    bindArtifactNameChange();
    bindArtifactDelete();
}


/**
 * Generate HTML for an artifact selector
 *
 * @param artifactCount
 * @param artifactName
 * @return {String}
 */
function generateArtifactSelectorHTML(artifactCount, artifactName) {
    tabindex++;

    var artifactRow = '<div id="' + artifactCount + '" class="artifact controls controls-row">';

    if (Object.getOwnPropertyNames(artifacts).length > 10) {
         artifactRow +=
            '<button class="artifact-delete btn btn-danger" type="button"><i class="icon-minus icon-white"></i></button>' +
            '<input type="text" class="artifact-name span3" autocomplete="off" tabindex="' + tabindex + '" name="artifacts[' + artifactCount + '][name]" ';

        if (artifactName !== null) {
            artifactRow += 'value="' + artifactName + '"';
        }

        artifactRow += '></div>';
    } else {
        artifactRow +=
            '<button class="artifact-delete btn btn-danger" type="button"><i class="icon-minus icon-white"></i></button>' +
            '<select class="artifact-name span3" tabindex="' + tabindex + '" name="artifacts[' + artifactCount + '][name]">';

        if (artifactName == null) {
            artifactRow += "<option selected></option>";
        }

        $.each(artifacts, function(name) {
            if (name == artifactName) {
                artifactRow += '<option selected value="' + name + '">' + name + '</option>';
            } else {
                artifactRow += '<option value="' + name + '">' + name + '</option>';
            }

        });

        artifactRow += '</select></div>';
    }


    return artifactRow;
}



/** ENVIRONMENT SELECTOR **/

/**
 * Add an environment selector when the artifact is chosen
 */
function bindArtifactNameChange() {
    var artifactNameSelector = $('.artifact-name');

    artifactNameSelector.typeahead({source: artifactNames});

    artifactNameSelector.unbind('change').change(function() {

        var artifactName = $(this).val();
        var artifactNum = $(this).parent().attr('id');

        // if other fields already exist, remove them
        if ($(this).next().is('.artifact-env')) {
            $(this).next().remove(); // environment
            $(this).next().remove(); // artifact info button
            $(this).next().remove(); // artifact info modal
            $(this).next().remove(); // versionToDeploy
            $(this).next().remove(); // jiraIssueKey
        }

        if (artifactName != '' && $.inArray(artifactName, artifactNames) !== -1) {
            $(this).parent().append(generateArtifactEnvSelectorHTML(artifactNum, artifactName, null));
        }

        bindArtifactEnvChange();
    });
}


/**
 * Generate HTML for an environment selector
 *
 * @param artifactNum
 * @param artifactName
 * @param artifactEnv
 * @return {String}
 */
function generateArtifactEnvSelectorHTML(artifactNum, artifactName, artifactEnv) {
    tabindex++;

    // note the space before the select tag
    var envSelector = ' <select class="artifact-env span2" tabindex="' + tabindex + '" name="artifacts[' + artifactNum + '][env]">';

    if (artifactEnv == null) {
        envSelector += "<option selected></option>";
    }

    if (artifactName != null) {
        $.each(artifacts[artifactName].envs, function(index,env) {
            if (env == artifactEnv) {
                envSelector += '<option selected value="' + env + '">' + env + '</option>';
            } else {
                envSelector += '<option value="' + env + '">' + env + '</option>';
            }
        });
    }

    envSelector += '</select>';

    return envSelector;
}



/** VERSION SELECTOR & JIRA ISSUE KEY **/

/**
 * When the environment is chosen, add a version selector and artifact info/server selector button,
 */
function bindArtifactEnvChange() {
    $('.artifact-env').unbind('change').change(function() {

        var artifactNum = $(this).parent().attr('id');
        var artifactName = $(this).prev().val();
        var env = $(this).val();

        // if other fields already exist, remove them
        if ($(this).next().is('.artifact-info')) {
            $(this).next().remove(); // artifact info button
            $(this).next().remove(); // artifact info modal
            $(this).next().remove(); // versionToDeploy
            $(this).next().remove(); // jiraIssueKey
        }

        if (env != '') {

            // add 'show info' button
            $(this).parent().append(generateShowInfoButtonHTML(artifactName, env));
            generateArtifactInfo(artifactNum, artifactName, env);
            $('.artifactInfoModal').draggable( { handle: "h3" } );

            $(this).parent().append(generateVersionLoaderHTML(artifactNum));
            generateVersionSelectorAndJiraIssueKeyHTML(artifactNum, artifactName, env, null, null);

        }
    });
}


/**
 * Generate HTML for an artifact info/server selection button (modal trigger)
 *
 * @param artifactName
 * @param artifactEnv
 * @return {String}
 */
function generateShowInfoButtonHTML(artifactName, artifactEnv) {
    return '<a href="#' + artifactName + '-' + artifactEnv + '-info" role="button" id="' + artifactName + '-' + artifactEnv + '-info-button" class="artifact-info btn btn-info" data-toggle="modal"><i class="icon-eye-open icon-white"></i></a>'
        + '<div class="artifactInfoModal modal" id="' + artifactName + '-' + artifactEnv + '-info" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="' + artifactName + '-' + artifactEnv + '-infoModalLabel" aria-hidden="true" style="display: none; ">'
            + '<div class="modal-header">'
                + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'
                + '<h3 id="' + artifactName + '-' + artifactEnv + '-infoModalLabel"></h3>'
            + '</div>'
            + '<div class="modal-body"></div>'
        + '</div>';
}


/**
 * Generate HTML for an artifact info/server selection modal
 *
 * @param artifactNum
 * @param artifactName
 * @param artifactEnv
 */
function generateArtifactInfo(artifactNum, artifactName, artifactEnv) {

    var infoModalLabel = $('#' + artifactName + '-' + artifactEnv + '-infoModalLabel');
    var modalBody = $('#' + artifactName + '-' + artifactEnv + '-info .modal-body');

    infoModalLabel.html(artifactName.replace(/_/g, ' ') + ' : ' + artifactEnv.toUpperCase());

    modalBody.html('<img src="images/ajax-loaders/radar.png" alt="Retrieving artifact data..."/> Retrieving artifact data...');

    $.getJSON(
        '/artifacts/view',

        {
            name: artifactName,
            ajax: true
        },

        function(artifact) {

            var isAdmin = !!($.inArray('admin', roles) != -1);

            var isPowerUser = !!($.inArray('powerUser', roles) != -1);

            var isLoadBalanced = artifact['envs'][artifactEnv]['isLoadBalanced'];
            if (isLoadBalanced == 1) {
                infoModalLabel.append('<img alt="Load Balanced" title="Load Balanced" src="/images/32x32/scales-blue.png" style="margin-left: 10px;"/>')
            }

            var modalBodyHtml = '';

            var lastDeployedDate = Date.parse(artifact['envs'][artifactEnv]['lastDeployedDate']);
            if (lastDeployedDate !== null) {
                modalBodyHtml = '<p><strong>Last deployment:</strong>  Version <span class="label label-info">' +
                    artifact['envs'][artifactEnv]['lastDeployedVersion'] + '</span>' +
                    ' by <span class="label label-info">' + artifact['envs'][artifactEnv]['lastDeployedUser'] + '</span>' +
                    ' on ' + lastDeployedDate.toString('ddd, MMM d, yyyy @ h:mm tt') + '</p>';
            }

            var artifactNotes = artifact['notes'] != null ? artifact['notes'] : '';

            modalBodyHtml += '<table class="table table-striped table-bordered table-condensed" style="width: auto;"><tbody>' +
                '<tr><th>serverType</th><td>' + artifact['serverType'] + '</td></tr>' +
                '<tr><th>groupId</th><td>' + artifact['groupId'] + '</td></tr>' +
                '<tr><th>artifactId</th><td>' + artifact['artifactId'] + '</td></tr>' +
                '<tr><th>artifactType</th><td>' + artifact['artifactType'] + '</td></tr>' +
                '<tr><th>deployName</th><td>' + artifact['deployName'] + '</td></tr>' +
                '<tr><th>Notes</th><td>' + artifactNotes + '</td></tr>' +
                '</tbody></table>' +
                '<table summary="Servers" class="table table-striped table-bordered table-condensed table-hover" style="width: auto;">' +
                '<thead><tr><th style="text-align: center;">Hostname</th>';

            if (isAdmin == 1 || isPowerUser == 1) {
                modalBodyHtml += '<th style="text-align: center;">Actions</th>';
            }

            modalBodyHtml += '</tr></thead><tbody>';


            var servers = artifact['envs'][artifactEnv]['servers'];

            $.each(servers, function(key) {

                modalBodyHtml += '<tr><td style="text-align: right;"><strong>' + key + '</strong></td>';

                if (isAdmin == 1 || isPowerUser == 1) {

                    //todo: handle includes for 'basic' serverType (BasicServer class)

                    modalBodyHtml += '<td><label class="checkbox inline"><input type="checkbox" id="' + artifactName + '_' + artifactEnv + '_' + key + '_exclude" name="artifacts[' + artifactNum + '][serverExcludes][]" class="' + artifactName + '-' + artifactEnv + '-serverExclude serverExclude" value="' + key + '">Exclude</label>' +
                        '<label class="checkbox inline"><input type="checkbox" id="' + artifactName + '_' + artifactEnv + '_' + key + '_include" name="artifacts[' + artifactNum + '][serverIncludes][]" class="' + artifactName + '-' + artifactEnv + '-serverInclude serverInclude" value="' + key + '">Include</label></td>';


                }

                modalBodyHtml += '</tr>';

            });

            modalBodyHtml += '</tbody></table>';

            modalBody.hide().html(modalBodyHtml).fadeIn(500);


            // restore server exclude and include checkboxes
            $('.serverExclude').each(function() {
                var state = JSON.parse( localStorage.getItem('deploy_option_checkbox_'  + this.id) );

                if (state) this.checked = state.checked;
            });

            $('.serverInclude').each(function() {
                var state = JSON.parse( localStorage.getItem('deploy_option_checkbox_'  + this.id) );

                if (state) this.checked = state.checked;
            });

            handleServerSelection(artifactName, artifactEnv);

            // server exclude/include checkbox behavior
            $('.' + artifactName + '-' + artifactEnv + '-serverExclude').click(function() {
                handleServerSelection(artifactName, artifactEnv);
            });

            $('.' + artifactName + '-' + artifactEnv + '-serverInclude').click(function() {
                handleServerSelection(artifactName, artifactEnv);
            });
        }
    )
}


/**
 * Manipulate artifact info button and include/exclude checkboxes based on server selections
 *
 * @param artifactName
 * @param artifactEnv
 */
function handleServerSelection(artifactName,artifactEnv) {

    var infoButtonSelector = $('#' + artifactName + '-' + artifactEnv + '-info-button');

    var serverExcludeCount = countChecked('.' + artifactName + '-' + artifactEnv + '-serverExclude');
    var serverIncludeCount = countChecked('.' + artifactName + '-' + artifactEnv + '-serverInclude');

    var serverExcludeSelector = $('.' + artifactName + '-' + artifactEnv + '-serverExclude');
    var serverIncludeSelector = $('.' + artifactName + '-' + artifactEnv + '-serverInclude');

    if (serverExcludeCount == 0) {
        serverIncludeSelector.removeAttr('disabled');
    } else {
        serverIncludeSelector.attr('disabled', 'disabled');
    }

    if (serverIncludeCount == 0) {
        serverExcludeSelector.removeAttr('disabled');
    } else {
        serverExcludeSelector.attr('disabled', 'disabled');
    }

    if (serverExcludeCount != 0 || serverIncludeCount != 0) {
        infoButtonSelector.removeClass('btn-info');
        infoButtonSelector.addClass('btn-warning');
        infoButtonSelector.find('i').removeClass('icon-eye-open');
        infoButtonSelector.find('i').addClass('icon-wrench');
        infoButtonSelector.tooltip('destroy').tooltip({title: 'Edit Server Selections'});
    } else {
        infoButtonSelector.removeClass('btn-warning');
        infoButtonSelector.addClass('btn-info');
        infoButtonSelector.find('i').removeClass('icon-wrench');
        infoButtonSelector.find('i').addClass('icon-eye-open');
        infoButtonSelector.tooltip('destroy').tooltip({title: 'Show Info'});

    }

    $('.artifact-info').tooltip();
}


/**
 * Count checked checkboxes of a selector
 *
 * @param selector
 * @return {Number}
 */
function countChecked(selector) {

    var count = 0;

    $(selector).each(function() {
        if ($(this).attr('checked') == 'checked') {
            count++;
        }
    });

    return count;
}


/**
 * Generate HTML for version retrieval animated icon
 *
 * @param artifactNum
 * @return {String}
 */
function generateVersionLoaderHTML(artifactNum) {
    return '<img id="'+ artifactNum +'-version-loading' +'" src="images/ajax-loaders/radar.png" class="version-loader" title="Retrieving versions..." alt="Retrieving versions..."/>';
}


/**
 * Generate HTML for the version selector and JIRA issue key field
 *
 * @param artifactNum
 * @param artifactName
 * @param artifactEnv
 * @param versionToDeploy
 * @param jiraIssueKey
 */
function generateVersionSelectorAndJiraIssueKeyHTML(artifactNum, artifactName, artifactEnv, versionToDeploy, jiraIssueKey) {
    tabindex++;

    var versionSelectId = artifactNum +'-getVersion';

    var versionSelector = '';

    // get versions for this artifact
    $.ajax({
        //async: false,
        url: '/artifacts/get-nexus-versions/',
        data: {
            artifactName: artifactName,
            artifactEnv: artifactEnv
        },
        dataType: "json",
        success: function(data) {

            $('#' + artifactNum + '-version-loading').remove(); // remove the "loading" image

            if (data.length > 0) {
                // note the space before the select tag
                versionSelector = ' <select id="'+ versionSelectId +'" class="version-to-deploy span2" tabindex="' + tabindex + '" name="artifacts[' + artifactNum + '][versionToDeploy]">';

                if (versionToDeploy === null) {
                    versionSelector += '<option selected value="latest">latest</option>';
                }

                $.each(data, function(index,version) {
                    if (version == versionToDeploy) {
                        versionSelector += '<option selected value="' + version + '">' + version + '</option>';
                    } else {
                        versionSelector += '<option value="' + version + '">' + version + '</option>';
                    }

                });

                versionSelector += '</select>';
            } else {
                versionSelector = ' <span class="version-to-deploy control-group error"><input class="span2" type="text" value="No versions found!" disabled/></span>';
            }


            tabindex++;

            // note the space before the input tag
            var jiraIssueKeyField = ' <input type="text" class="jira-issue-key span2" tabindex="' + tabindex + '" name="artifacts[' + artifactNum + '][jiraIssueKey]"';

            if (jiraIssueKey !== null && jiraIssueKey !== 'undefined') {
                jiraIssueKeyField += ' value="' + jiraIssueKey + '"';
            }

            jiraIssueKeyField += '/>';

            $('#' + artifactNum).append(versionSelector+jiraIssueKeyField);

            $('#' + versionSelectId).focus();
        }
    });
}


/**
 * Clear all form selections
 *
 * @param skipNew
 */
function clearForm(skipNew) {
    $('#artifactsToDeploy').html('');

    $("input[type=checkbox]:checked").removeAttr('checked');

    $('.servers').val('');
    $('.artifactInfoModal').remove();

    $('#puppetAgent0').attr('checked', 'checked');
    $('#manageService0').attr('checked', 'checked');

    $('#run1').attr('checked', 'checked');
    $('#run').buttonset('refresh');

    localStorage.clear();

    $('.alert').alert('close');

    if (skipNew == false) {
        addArtifactsSelector();
    }

    $('#share').removeClass('active');
    $('#deploymentNickname').val('');
}


/** FORM VALIDATION **/
function bindDeployFormSubmit() {
    $('#deployForm').submit(function() {

        var errors = [];

        // validate artifact selections

        var artifacts = $('.artifact');

        if (artifacts.length !== 0) {
            artifacts.each(function() {
                var name = $(this).find('.artifact-name').val();
                if (name == '') {
                    errors.push("Artifact fields cannot be empty!");
                } else {
                    var env = $(this).find('.artifact-env').val();
                    if (env == '') {
                        errors.push('The environment field for "' + name + '" cannot be empty!');
                    }
                }
            });
        } else {
            errors.push('No artifacts were selected!');
        }


        if (errors.length > 0) {
            var message = '';

            $.each(errors, function(key,value) {
                message += value + '<br/>';
            });

            $('#deployWell').prepend('<div class="alert alert-error fade in"><button type="button" class="close" data-dismiss="alert">×</button>' +
                '<h4>The following error(s) occurred when validating the deployment form:</h4>' + message + '</div>');

            return false;
        } else {
            return true;
        }

    });
}
