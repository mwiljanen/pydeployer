/* STORED SELECTIONS */
/**
 * Load previously selected artifacts
 *
 * @param artifacts
 */
function loadSavedArtifacts(artifacts) {

    $.each(artifacts, function(index, artifact) {
        if (artifact.name != '' && $.inArray(artifact.name, artifactNames) !== -1) {
            artifactCount = index;
            $('#artifactsToDeploy').append(generateArtifactSelectorHTML(artifactCount, artifact.name));
            var artifactRow = $('#' + artifactCount); // only valid after artifactSelector HTML is appended to #artifactsToDeploy
            artifactRow.append(generateArtifactEnvSelectorHTML(artifactCount, artifact.name, artifact.env));
            artifactRow.append(generateShowInfoButtonHTML(artifact.name, artifact.env));
            generateArtifactInfo(artifactCount, artifact.name, artifact.env);
            artifactRow.append(generateVersionLoaderHTML(artifactCount));
            generateVersionSelectorAndJiraIssueKeyHTML(artifactCount, artifact.name, artifact.env, artifact.versionToDeploy, artifact.jiraIssueKey);
        }
    });

    bindArtifactNameChange();
    bindArtifactDelete();
    bindArtifactEnvChange();
    $('.artifactInfoModal').draggable( { handle: 'h3' } );
}


/**
 * Load previous selected deployment options
 */
function loadSavedOptions() {
    $('.servers').each(function() {
        var state = JSON.parse( localStorage.getItem('deploy_option_' + this.id) );

        if (state) $(this).val(state.val);

    });

    $('input[type=checkbox]').each(function() {
        var state = JSON.parse( localStorage.getItem('deploy_option_checkbox_'  + this.id) );

        if (state) this.checked = state.checked;
    });

    $('input[type=radio]').each(function() {
        var state = JSON.parse( localStorage.getItem('deploy_option_radio_'  + this.id) );

        if (state) this.checked = state.checked;
    });
}


/**
 * Load saved deployments (populate modal)
 */
function getSavedDeployments() {

    var savedDeploymentsTableBodySelector = $('#savedDeployments').find('.modal-body table tbody');
    savedDeploymentsTableBodySelector.html('Loading...');

    $.getJSON(
        '/deploy/get-saved-deployments',
        function(data) {
            savedDeploymentsTableBodySelector.html('');

            $.each(data, function(index, deployment) {
                var row = '<tr><td>' + deployment.username + '</td><td>' + deployment.deploymentNickname ;

                if (deployment.share == 1) {
                    row += ' <i title="Shared" class="icon-gift"></i>';
                }

                row += '</td><td><button value="' + deployment.id + '" class="deployLoader btn btn-mini btn-primary"><i class="icon-download-alt icon-white"></i> Load</button>';

                if (deployment.username == username) {
                    row += ' <button value="' + deployment.id + '" class="deployTrash btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></button>';
                }

                row += '</td></tr>';

                savedDeploymentsTableBodySelector.append(row);
            });

            $('.deployLoader').click(function() {
                clearForm(true);

                var deploymentId = $(this).val();

                $.getJSON(
                    '/deploy/get-saved-deployment',
                    { id: deploymentId },
                    function(data) {
                        var deploymentSelections = JSON.parse(data.deploymentSelections);

                        localStorage.clear();
                        $.each(deploymentSelections, function(key, value) {
                            localStorage.setItem(key, value);
                        });

                        var artifactsToDeploy = JSON.parse(localStorage.getItem('artifactsToDeploy'));

                        if (artifactsToDeploy.length > 0) {
                            loadSavedArtifacts(artifactsToDeploy);
                            loadSavedOptions();

                            $('#deploymentNickname').val(data.deploymentNickname);

                            if (data.share == 1) {
                                $('#share').addClass('active');
                            } else {
                                $('#share').removeClass('active');
                            }
                        }
                    }
                )
            });

            $('.deployTrash').click(function() {

                if (window.confirm('Are you sure you want to delete this deployment?')) {
                    var deploymentId = $(this).val();

                    $.getJSON(
                        '/deploy/delete-saved-deployment',
                        { id: deploymentId },
                        function() {
                            getSavedDeployments();
                        }
                    );
                }

            });
        }
    );
}


/**
 * Save deployment selections in local storage
 */
function saveDeployment() {
    // Artifacts
    var artifacts = [];

    $('.artifact').each(function() {
        var name = $(this).find('.artifact-name').val();
        if (name == '') return; // skip if value is empty

        var env = $(this).find('.artifact-env').val();
        if (env == '') return; // skip if value is empty

        var versionToDeploy = $(this).find('.version-to-deploy').val();
        if (versionToDeploy == '') return; // skip if value is empty

        var jiraIssueKey = $(this).find('.jira-issue-key').val();

        var artifact = {
            name: name,
            env: env,
            versionToDeploy: versionToDeploy,
            jiraIssueKey: jiraIssueKey
        };

        artifacts.push(artifact);
    });

    localStorage.setItem('artifactsToDeploy', JSON.stringify(artifacts));

    // Server includes/excludes
    $('.servers').each(function() {
        localStorage.setItem('deploy_option_' + this.id, JSON.stringify( { val: $(this).val() } ));
    });

    // Checkboxes
    $('input[type=checkbox]').each(function() {
        localStorage.setItem('deploy_option_checkbox_' + this.id, JSON.stringify( { checked: this.checked } ));
    });

    // Radio buttons (except for safe mode)
    $('.radio.inline input[type=radio]').each(function() {
        localStorage.setItem('deploy_option_radio_' + this.id, JSON.stringify( { checked: this.checked } ));
    });
}


/**
 * Bind deployment saving functions to appropriate buttons
 */
function bindDeploymentSaveFunctions() {
    $('#share').tooltip().click(function() {
       if (!$(this).hasClass('active')) {
            $(this).addClass('active');
        } else {
            $(this).removeClass('active');
        }
    });

    $('#save').click(
        function() {
            saveDeployment();

            var nicknameSelector = $('#deploymentNickname');
            var nickname = nicknameSelector.val();
            var shareSelector = $('#share');

            if (nickname != '') {
                var request = $.ajax({
                    type: 'POST',
                    url: '/deploy/save',
                    data: {
                        deploymentNickname: nickname,
                        deploymentSelections: JSON.stringify(localStorage),
                        share: shareSelector.hasClass('active')
                    }
                });

                request.done(function() {
                    $('#deployWell').prepend('<div class="alert alert-success fade in"><button type="button" class="close" data-dismiss="alert">×</button>' +
                        '<h4>Your deployment named "' + nickname + '" was saved.</h4></div>');

                    nicknameSelector.val('');

                    if (shareSelector.hasClass('active')) {
                        shareSelector.removeClass('active');
                    }
                });

                request.fail(function(jqXHR) {
                    $('#deployWell').prepend('<div class="alert alert-error fade in"><button type="button" class="close" data-dismiss="alert">×</button>' +
                        '<h4>An error occurred trying to save your deployment named "' + nickname + '":</h4>' + jqXHR.responseText + '</div>');
                });
            } else {
                $('#deployWell').prepend('<div class="alert alert-error fade in"><button type="button" class="close" data-dismiss="alert">×</button>' +
                    '<h4>Please enter a nickname for your deployment</h4></div>');
            }
        }
    );

    $('#refreshSaved').click(getSavedDeployments);

    $('#savedDeployments').draggable( { handle: "h3" } );
}