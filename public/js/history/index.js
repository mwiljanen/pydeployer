function initHistoryIndex() {

    var logsTable = $('#logs-datatable').dataTable({
        "bStateSave": true,
        
        "bJQueryUI": true,
        "sPaginationType": "full_numbers",
        
        "iDisplayLength": 10,
        "aLengthMenu": [5, 10, 15, 20, 25, 50, 100, 200],

        "aaSorting": [[ 1, "desc" ]],
        
        "oLanguage": {
            "sSearch": "Search all columns:"
        },
        
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": '/history/listlogs/'
    });

    var deploymentsTable = $('#deployments-datatable').dataTable({
        "bStateSave": true,

        "bJQueryUI": true,
        "sPaginationType": "full_numbers",

        "iDisplayLength": 10,
        "aLengthMenu": [10, 15, 20, 25, 50, 100, 200],

        "aaSorting": [[ 3, "desc" ]],

        "oLanguage": {
            "sSearch": "Search all columns:"
        },

        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": '/history/listdeployments/'
    });

    var logsDatatableTfootInputSelector = $("#logs-datatable").find('tfoot input');

    // logs table searching
    logsDatatableTfootInputSelector.keyup( function () {
        /* Filter on the column (the index) of this element */
        logsTable.fnFilter( this.value, logsDatatableTfootInputSelector.index(this) );
    } );
     
     
    /*
     * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
     * the footer
     */
    logsDatatableTfootInputSelector.each( function (i) {
        asLogsInitVals[i] = this.value;
    } );

    logsDatatableTfootInputSelector.focus( function () {
        if ( this.className == "search_init" )
        {
            this.className = "";
            this.value = "";
        }
    } );

    logsDatatableTfootInputSelector.blur( function (i) {
        if ( this.value == "" )
        {
            this.className = "search_init";
            this.value = asLogsInitVals[logsDatatableTfootInputSelector.index(this)];
        }
    } );


    var deploymentsDatatableTfootInputSelector = $("#deployments-datatable").find('tfoot input');

    // deployments table searching
    deploymentsDatatableTfootInputSelector.keyup( function () {
        /* Filter on the column (the index) of this element */
        deploymentsTable.fnFilter( this.value, deploymentsDatatableTfootInputSelector.index(this) );
    } );


    /*
     * Support functions to provide a little bit of 'user friendlyness' to the textboxes in
     * the footer
     */
    deploymentsDatatableTfootInputSelector.each( function (i) {
        asDeploymentsInitVals[i] = this.value;
    } );

    deploymentsDatatableTfootInputSelector.focus( function () {
        if ( this.className == "search_init" )
        {
            this.className = "";
            this.value = "";
        }
    } );

    deploymentsDatatableTfootInputSelector.blur( function (i) {
        if ( this.value == "" )
        {
            this.className = "search_init";
            this.value = asDeploymentsInitVals[deploymentsDatatableTfootInputSelector.index(this)];
        }
    } );
}