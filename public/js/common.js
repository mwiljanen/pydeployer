$(document).ready(function() {

	// Closing Divs - used on Notification Boxes
	$(".canhide").click(function() {
		
		$(this).fadeOut(700);
	});

});


function displayAlert(priority, message) {
    var header = '';

    switch (priority) {
        case 'warn':
            header = 'Warning!';
            break;
        case 'error':
            header = 'Error!';
            break;
        case 'success':
            header = 'Success!';
            break;
        case 'info':
            header = 'Heads up!';
            break;
    }

    $('#alerts').append('<div class="alert alert-' + priority + '"><button class="close" data-dismiss="alert" type="button">×</button>' +
        '<h4 class="alert-heading">' + header + '</h4>' +
        message);

    $(".alert").alert();
}