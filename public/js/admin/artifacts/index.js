var serverTypes = [];
var artifactTypes = [];

$(document).ready(function() {

    // get server types
    $.getJSON('/admin/artifacts/get-server-types', function(data) {
        serverTypes = data;
    });

    // get artifact types
    $.getJSON('/admin/artifacts/get-artifact-types', function(data) {
        artifactTypes = data;
    });


    var element = $("#artifacts").kendoGrid({
        dataSource: {
            transport: {
                create: {
                    url: "/admin/artifacts/create-artifact",
                    type: "POST"
                },
                read: "/admin/artifacts/get-artifacts",
                update: {
                    url: "/admin/artifacts/update-artifact",
                    type: "POST"
                },
                destroy: {
                    url: "/admin/artifacts/delete-artifact",
                    type: "POST"
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverSorting: true,
            sort: { field: "name", dir: "asc" },
            serverFiltering: true,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        name: { validation: { required: true } },
                        serverType: { validation: { required: true } },
                        groupId: { validation: { required: true } },
                        artifactId: { validation: { required: true } },
                        artifactType: { validation: { required: true } },
                        deployName: { validation: { required: true } },
                        versionUrlSuffix: { nullable: true },
                        statusUrlSuffix: { nullable: true }
                    }
                }
            }
        },

        height: 525,
        resizable: true,
        pageable: {
            refresh: true,
            pageSizes: true
        },
        sortable: {
            mode: "multiple", // enables multi-column sorting
            allowUnsort: true
        },
        filterable: true,
        editable: {
            update: true,
            destroy: true,
            confirmation: "Are you sure you want to delete this artifact and all its environments?"
        },
        navigatable: true,  // enables keyboard navigation in the grid
        toolbar: [
            { name: "create", text: "Add a new artifact"},
            { name: "save" },
            { name: "cancel" }
        ],

        columns: [
            { field: "name", title: "Name" },
            { field: "serverType", title: "Server Type", width: 110, editor: serverTypeDropDownEditor },
            { field: "groupId", width: 100 },
            { field: "artifactId" },
            { field: "artifactType", title: "Artifact Type", width: 105, editor: artifactTypeDropDownEditor },
            { field: "deployName", title: "Deploy Name" },
            { field: "versionUrlSuffix", title: "Version URL Suffix", width: 140, sortable: false },
            { field: "statusUrlSuffix", title: "Status URL Suffix", width: 140, sortable: false },
            { command: "destroy", title: " ", width: 100 }
        ],

        detailTemplate: kendo.template($("#artifacts-template").html()),
        detailInit: detailInit,
        dataBound: function() {
            this.expandRow(this.tbody.find("tr.k-master-row").first());
        },
        edit: function(e) {
            // collapse detail during editing
            this.collapseRow(this.tbody.find(e.container + ":parent"));
        }
    });
});


function detailInit(e) {

    var detailRow = e.detailRow;

    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: {
                duration: 150,
                effects: "fadeIn"
            }
        }
    });


    /*
     * Environments
     */
    detailRow.find(".envs").kendoGrid({
        dataSource: {
            transport: {
                create: {
                    url: "/admin/artifacts/create-artifact-env",
                    type: "POST"
                },
                read: "/admin/artifacts/get-artifact-envs",
                update: {
                    url: "/admin/artifacts/update-artifact-env",
                    type: "POST"
                },
                destroy: {
                    url: "/admin/artifacts/delete-artifact-env",
                    type: "POST"
                }

            },
            schema: {
                data: "data",
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        artifactName: { editable: false, defaultValue: e.data.name },
                        env: { validation: { required: true } },
                        isLoadBalanced: { type: "boolean" },
                        serverData: { validation: { required: true } }
                    }
                }
            },
            serverSorting: true,
            serverFiltering: true,
            filter: { field: "artifactName", operator: "eq", value: e.data.name }
        },
        resizable: true,
        scrollable: false,
        sortable: true,

        editable: {
            update: true,
            destroy: true,
            confirmation: "Are you sure you want to delete this environment?"
        },
        navigatable: true,  // enables keyboard navigation in the grid
        toolbar: [
            { name: "create", text: "Add a new environment"},
            { name: "save" },
            { name: "cancel" }
        ],

        columns: [
            { field: "env", title: "Environment Name", width: 130 },
            { field: "isLoadBalanced", title: "Load Balanced?", width: 110, sortable: false},
            { field: "serverData", title: "Servers (JSON/hostinfo)", sortable: false },
            { command: "destroy", title: " ", width: 100 }
        ]

    });


    /*
     * Other Details
     */
    detailRow.find(".other-details").kendoGrid({
        dataSource: {
            transport: {
                read: "/admin/artifacts/get-artifacts",
                update: {
                    url: "/admin/artifacts/update-artifact",
                    type: "POST"
                }
            },
            schema: {
                data: "data",
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        mavenVersion: {},
                        alwaysAllowSnapshots: { type: "boolean" },
                        notes: { nullable: true }
                    }
                }
            },
            serverFiltering: true,
            filter: { field: "name", operator: "eq", value: e.data.name }
        },
        resizable: true,
        scrollable: false,
        editable: {
            update: true,
            destroy: false
        },
        navigatable: true,
        toolbar: [ "save", "cancel" ],

        columns: [
            { field: "mavenVersion", title: "Maven Version", width: 100 },
            { field: "alwaysAllowSnapshots", title: "Always Allow Snapshots?", width: 160 },
            { field: "notes", title: "Notes" }
        ]
    });


    /*
     * Dependencies
     */
    detailRow.find(".dependencies").kendoGrid({
        dataSource: {
            transport: {
                create: {
                    url: "/admin/artifacts/create-artifact-dependency",
                    type: "POST"
                },
                read: "/admin/artifacts/get-artifact-dependencies",
                update: {
                    url: "/admin/artifacts/update-artifact-dependency",
                    type: "POST"
                },
                destroy: {
                    url: "/admin/artifacts/delete-artifact-dependency",
                    type: "POST"
                }

            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        artifactName: { editable: false, defaultValue: e.data.name },
                        env: { validation: { required: true } },
                        dependency: { validation: { required: true }  },
                        deployPriority: { defaultValue: 0 }
                    }
                }
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            filter: { field: "artifactName", operator: "eq", value: e.data.name }
        },
        resizable: true,
        pageable: {
            refresh: true,
            pageSizes: true
        },
        sortable: true,
        filterable: false,
        scrollable: false,
        editable: {
            update: true,
            destroy: true,
            confirmation: "Are you sure you want to delete this dependency?"
        },
        navigatable: true,  // enables keyboard navigation in the grid
        toolbar: [
            { name: "create", text: "Add a new dependency"},
            { name: "save" },
            { name: "cancel" }
        ],

        //height: 300,

        columns: [
            { field: "env", title: "Environment", width: 110 },
            { field: "dependency", title: "Dependency" },
            { field: "deployPriority", title: "Deploy Priority", width: 110 },
            { command: "destroy", title: " ", width: 100 }
        ]

    });


    /*
     * Sites
     */
    detailRow.find(".sites").kendoGrid({
        dataSource: {
            transport: {
                create: {
                    url: "/admin/artifacts/create-artifact-site",
                    type: "POST"
                },
                read: "/admin/artifacts/get-artifact-sites",
                update: {
                    url: "/admin/artifacts/update-artifact-site",
                    type: "POST"
                },
                destroy: {
                    url: "/admin/artifacts/delete-artifact-site",
                    type: "POST"
                }

            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        artifactName: { editable: false, defaultValue: e.data.name },
                        hostname: { validation: { required: true } },
                        isPrefix: { type: "boolean", defaultValue: 'true' },
                        priority: { validation: { required: true }, defaultValue: 1 },
                        isEnabled: { type: "boolean" },
                        alwaysLoad: { type: "boolean" }
                    }
                }
            },
            pageSize: 5,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            filter: { field: "artifactName", operator: "eq", value: e.data.name }
        },
        resizable: true,
        pageable: {
            refresh: true,
            pageSizes: true
        },
        sortable: true,
        filterable: false,
        scrollable: false,
        editable: {
            update: true,
            destroy: true,
            confirmation: "Are you sure you want to delete this site?"
        },
        navigatable: true,  // enables keyboard navigation in the grid
        toolbar: [
            { name: "create", text: "Add a new site"},
            { name: "save" },
            { name: "cancel" }
        ],

        //height: 300,

        columns: [
            { field: "hostname", title: "Hostname" },
            { field: "isPrefix", title: "Is Prefix?", width: 110 },
            { field: "priority", title: "Priority", width: 110 },
            { field: "isEnabled", title: "Is Enabled?", width: 110  },
            { field: "alwaysLoad", title: "Always Load?", width: 110 },
            { command: "destroy", title: " ", width: 100 }
        ]

    });

    /*
     * Haproxy Proxies
     */
    detailRow.find(".haproxyProxies").kendoGrid({
        dataSource: {
            transport: {
                create: {
                    url: "/admin/artifacts/create-artifact-haproxy-proxy",
                    type: "POST"
                },
                read: "/admin/artifacts/get-artifact-haproxy-proxies",
                update: {
                    url: "/admin/artifacts/update-artifact-haproxy-proxy",
                    type: "POST"
                },
                destroy: {
                    url: "/admin/artifacts/delete-artifact-haproxy-proxy",
                    type: "POST"
                }

            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        artifactName: { editable: false, defaultValue: e.data.name },
                        systemType: { validation: { required: true } },
                        systemSubType: { nullable: true },
                        partnerGroup: { nullable: true },
                        proxyName: { validation: { required: true } }
                    }
                }
            },
            pageSize: 6,
            serverPaging: true,
            serverSorting: true,
            serverFiltering: true,
            filter: { field: "artifactName", operator: "eq", value: e.data.name }
        },
        resizable: true,
        pageable: {
            refresh: true,
            pageSizes: true
        },
        sortable: true,
        filterable: false,
        scrollable: false,
        editable: {
            update: true,
            destroy: true,
            confirmation: "Are you sure you want to delete this proxy?"
        },
        navigatable: true,  // enables keyboard navigation in the grid
        toolbar: [
            { name: "create", text: "Add a new proxy"},
            { name: "save" },
            { name: "cancel" }
        ],

        //height: 300,

        columns: [
            { field: "systemType", title: "System Type" },
            { field: "systemSubType", title: "System Sub Type" },
            { field: "partnerGroup", title: "Partner Group" },
            { field: "proxyName", title: "Proxy Name" },
            { command: "destroy", title: " ", width: 100 }
        ]

    });
}



function serverTypeDropDownEditor(container, options) {
    $('<input data-text-field="text" data-value-field="value" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            autoBind: false,
            dataSource: serverTypes,
            dataTextField: "text",
            dataValueField: "value"
        });
}



function artifactTypeDropDownEditor(container, options) {
    $('<input data-text-field="text" data-value-field="value" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            autoBind: false,
            dataSource: artifactTypes,
            dataTextField: "text",
            dataValueField: "value"
        });
}


