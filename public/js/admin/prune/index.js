$(document).ready(function() {
    $(".datetimepicker").datetimepicker({
        showButtonPanel: true,
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        timeFormat: 'hh:mm:ss',
        showSecond: true
    });
});

// prune by date/time
$(document).ready(function() {
    $("#prune-by-dt").click(function() {

        var pruneByDtForm = $("#prune-by-dt-form").kendoValidator().data("kendoValidator");

        // validate
        if (pruneByDtForm.validate()) {
            $.getJSON(
                '/admin/prune/prune-by-date-time',

                $('#prune-by-dt-form').serialize(),

                function(data) {

                    if (data.errors.length > 0) {

                        var errors = '';

                        for (var error in data.errors) {
                            errors += data.errors[error] + '<br/>';
                        }

                        displayAlert('error', errors);
                    } else {
                        displayAlert('success', 'Logs and screenshots between ' + $('#startDT').attr('value') + ' and ' + $('#endDT').attr('value') + ' have been removed.');
                        clearDtForm();
                    }
                }
            );
        }

    });
});


function clearDtForm() {
    $(".datetimepicker").attr('value', '');
}

$(document).ready(function() {
    $("#clear-dt-form").click(function() {
        clearDtForm();
    });
});


// prune by uniqueId
$(document).ready(function() {
    $("#prune-by-id").click(function() {

        var pruneByIdForm = $("#prune-by-id-form").kendoValidator().data("kendoValidator");

        // validate
        if (pruneByIdForm.validate()) {
            $.getJSON(
                '/admin/prune/prune-by-id',

                $('#prune-by-id-form').serialize(),

                function(data) {

                    if (data.errors.length > 0) {

                        var errors = '';

                        for (var error in data.errors) {
                            errors += data.errors[error] + '<br/>';
                        }

                        displayAlert('error', errors);
                    } else {
                        displayAlert('success', 'Logs and screenshots for deployment id ' + $('#uniqueId').attr('value') + ' have been removed.');
                        clearDtForm();
                    }
                }
            );
        }

    });
});


function clearIdForm() {
    $("#uniqueId").attr('value', '');
}

$(document).ready(function() {
    $("#clear-id-form").click(function() {
        clearIdForm();
    });
});