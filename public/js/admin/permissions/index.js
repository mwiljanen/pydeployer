$(document).ready(function() {
    var element = $("#userPermissions").kendoGrid({
        dataSource: {
            transport: {
                create: {
                    url: "/admin/permissions/create-user-perm",
                    type: "POST"
                },
                read: "/admin/permissions/get-user-perms",
                update: {
                    url: "/admin/permissions/update-user-perm",
                    type: "POST"
                },
                destroy: {
                    url: "/admin/permissions/delete-user-perm",
                    type: "POST"
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverSorting: true,
            sort: { field: "artifactEnvId", dir: "asc" },
            serverFiltering: true,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        artifactName: { validation: { required: true } },
                        env: { validation: { required: true } },
                        username: { validation: { required: true } },
                        permission: { type: "boolean" }
                    }
                }
            },
            error: function(e) {
                alert(e.responseText);
            }
        },

        //height: 525,
        resizable: true,
        pageable: {
            refresh: true,
            pageSizes: true
        },
        sortable: {
            mode: "multiple", // enables multi-column sorting
            allowUnsort: true
        },
        filterable: true,
        editable: {
            update: true,
            destroy: true,
            confirmation: "Are you sure you want to delete this user's permissions?"
        },
        navigatable: true,  // enables keyboard navigation in the grid
        toolbar: [
            { name: "create", text: "Add new user permission"},
            { name: "save" },
            { name: "cancel" }
        ],

        columns: [
            { field: "artifactName", title: "Artifact Name" ,editor: artifactNameEditor },
            { field: "env", title: "Environment", width: 100 },
            { field: "username", title: "Username" },
            { field: "permission", title: "Perm. Enabled?", width: 125 },
            { command: "destroy", title: " ", width: 100 }
        ]

    });
});


$(document).ready(function() {
    var element = $("#groupPermissions").kendoGrid({
        dataSource: {
            transport: {
                create: {
                    url: "/admin/permissions/create-group-perm",
                    type: "POST"
                },
                read: "/admin/permissions/get-group-perms",
                update: {
                    url: "/admin/permissions/update-group-perm",
                    type: "POST"
                },
                destroy: {
                    url: "/admin/permissions/delete-group-perm",
                    type: "POST"
                }
            },
            pageSize: 10,
            serverPaging: true,
            serverSorting: true,
            sort: { field: "artifactEnvId", dir: "asc" },
            serverFiltering: true,
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "id",
                    fields: {
                        id: { editable: false, nullable: true },
                        artifactName: { validation: { required: true } },
                        env: { validation: { required: true } },
                        groupDn: { validation: { required: true } },
                        permission: { type: "boolean" }
                    }
                }
            },
            error: function(e) {
                alert(e.responseText);
            }
        },

        //height: 525,
        resizable: true,
        pageable: {
            refresh: true,
            pageSizes: true
        },
        sortable: {
            mode: "multiple", // enables multi-column sorting
            allowUnsort: true
        },
        filterable: true,
        editable: {
            update: true,
            destroy: true,
            confirmation: "Are you sure you want to delete this group's permissions?"
        },
        navigatable: true,  // enables keyboard navigation in the grid
        toolbar: [
            { name: "create", text: "Add new group permission"},
            { name: "save" },
            { name: "cancel" }
        ],

        columns: [
            { field: "artifactName", title: "Artifact Name" ,editor: artifactNameEditor },
            { field: "env", title: "Environment", width: 100 },
            { field: "groupDn", title: "Group DN" },
            { field: "permission", title: "Perm. Enabled?", width: 125 },
            { command: "destroy", title: " ", width: 100 }
        ]

    });
});


function artifactNameEditor(container, options) {
    $('<input data-text-field="name" data-value-field="name" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoDropDownList({
            autoBind: false,
            dataSource: {
                transport: {
                    read: "/admin/permissions/get-artifacts"
                }
            },
            dataTextField: "name",
            dataValueField: "name"
        });
}


$(document).ready(function() {
    $("#tabstrip").kendoTabStrip({
        animation:	{
            open: {
                effects: "fadeIn"
            }
        }

    });
});