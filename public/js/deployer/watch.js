/**
 * Start watch
 * @param uniqueId
 */
function initDeployWatch(uniqueId)
{
    bindAbortButton();
    bindLogToggle();
    $('#screenshot-thumbs').jcarousel({});
    $("#progressbar").css('width', '0%');
    $("#abort").show();
    pollForNotifications(uniqueId);
    pollForProgressBarStatus(uniqueId);
}



/*
 * Notifications
 */

/**
 * Poll for notifications
 * @param uniqueId
 */
function pollForNotifications(uniqueId) {
    var notificationsTimer = setTimeout(function() {
        $.getJSON("/deployer/notifications/",
            {
                uniqueId: uniqueId
            },
            function(data) {

                if (data instanceof Array) {

                    $(data).each(function() {
                        if (this.type == 'failure' && this.message == '#abort#') {
                            handleAbortion();
                            clearTimeout(notificationsTimer);
                            clearTimeout(pbStatusTimer);
                        } else if (this.type == 'server') {
                            updateServersCompleted(this.message, this.context);
                        } else if (this.type == 'screenshots') {
                            updateScreenshots($.parseJSON(this.message));
                        } else if (!document.getElementById("note" + this.id)) {
                            displayPulledNotification(this);
                        }
                    });

                    pollForNotifications(uniqueId);
                } else {
                    clearTimeout(notificationsTimer);
                }
            });
    }, 2000);
}


/**
 * Display pulled notification
 * @param note
 */
function displayPulledNotification(note)
{
    var type = 'ATTENTION';

    if (note.type != 'lightbulb') {
        type = note.type.toUpperCase();
    }

    var notification = '<div id="note'+ note.id + '" class="notification ' + note.type + ' canhide"><p><strong>' + type + ': </strong>' + note.message + '</p></div>';
    $("#notifications").append(notification);

    if (note.type == 'failure') {
        var help = $("#help").html();

        $("#help-link").colorbox({
            html:help
        });
    } else {
        $(".canhide").click(function() {
            $(this).fadeOut(700);
        });
    }

    $("#notifications-wrapper").scrollTo('max', 500);
}


/**
 * Display notification
 * @todo merge with displayPulledNotification()
 * @param note
 */
function Notification(note) {
    notificationCount++;

    if (!note.id) {
        note.id = notificationCount;
    }

    var type = 'ATTENTION';

    if (note.type != 'lightbulb') {
        type = note.type.toUpperCase();
    }

    var notification = '<div id="note'+ note.id + '" class="notification ' + note.type + ' canhide"><p><strong>' + type + ': </strong>' + note.msg + '</p></div>';
    $("#notifications").append(notification);

    if (note.type == 'failure') {
        var help = $("#help").html();

        $("#help-link").colorbox({
            html:help
        });
    } else {
        $(".canhide").click(function() {
            $(this).fadeOut(500);
        });
    }

    if (note.msg == 'Abort requested') {
        handleAbortion();
    }

    $("#notifications-wrapper").scrollTo('max', 500);
}



/*
 * Progress Bar
 */

function updateProgressBarStatus(data)
{
    if (data.value > 1) {
        $("#progressbar").css('width', data.value + '%');
    }

    $("#progress").text(data.message);
}


function pollForProgressBarStatus(uniqueId) {
    pbStatusTimer = setTimeout(function() {
        $.getJSON("/deployer/progress/",
            {
                uniqueId: uniqueId
            },
            function(data) {

                if (data.value != 100) {
                    updateProgressBarStatus(data);
                    pollForProgressBarStatus(uniqueId);
                } else {
                    $("#progress").hide();
                    $("div.progress").removeClass('active').hide();
                    $("#abort").hide();
                    endLogtail();
                    clearTimeout(pbStatusTimer);
                }
            });
    }, 2000);
}



/*
 * Servers Completed
 */

function updateServersCompleted(server, context) {

    var contextCssId = context.replace('@', '_'); // replace '@' with '_' in artifactGroupName

    var serversCompletedSelector = $("#servers-completed");

    if (!document.getElementById('artifactGroup-' + contextCssId)) {
        serversCompletedSelector.append('<p id="artifactGroup-' + contextCssId + '"><strong>[' + context + ']:</strong>&nbsp;</p>')
    }

    if (serversCompleted.indexOf(server+context) == -1) {
        serversCompleted.push(server + context);

        var artifactGroupSelector = $("#artifactGroup-" + contextCssId );

        if (artifactGroupSelector.html() == '<strong>[' + context + ']:</strong>&nbsp;') {
            artifactGroupSelector.append(server);
        } else {
            artifactGroupSelector.append(', ' + server);
        }
    }
}



/*
 * Screenshots
 */

function updateScreenshots(screenshotFiles)
{
    var filenamePrefix = screenshotFiles.filenamePrefix;
    var filename = screenshotFiles.filename;
    var thumbnailFilename = screenshotFiles.thumbnailFilename;

    var spanId = 'ss-' + filenamePrefix;

    var uniqueId = $("#uniqueId");

    var img = '<span id="' + spanId + '">'
        +'<!--suppress HtmlUnknownTarget --><a class="screenshot" href="/screenshots/' + uniqueId.text() + '/' + filename + '">'
        + '<img title="' + filenamePrefix + '" alt="' + filenamePrefix + '" src="/screenshots/' + uniqueId.text() + '/' + thumbnailFilename + '"/></a></span>';

    var screenshotsWrapper = $("#screenshots-wrapper");

    if (screenshotsWrapper.is(":hidden")) {
        screenshotsWrapper.fadeIn(500);
    }

    if (!document.getElementById(spanId)) {
        screenshotsCount++;
        $("#screenshot-thumbs").jcarousel('add', screenshotsCount, img);
    }

    $('a.screenshot').colorbox();
}



/*
 * abort button
 */
function bindAbortButton() {
    $("#abort").click( function() {
        var uniqueId = $("#uniqueId").text();

        $.getJSON(
            '/deployer/abort/',
            {
                uniqueId: uniqueId
            }
        );
    });
}


function handleAbortion() {
    var note = {
        id: '-aborted',
        type:'failure',
        msg:'Deployment aborted.  Need <a id="help-link" name="help-link" href="#">help</a>?'
    };
    if (!document.getElementById("note-aborted")) {
        Notification(note);
    }
    $("#abort").hide();
    endLogtail();
}



/*
 * Log
 */

// toggle
function bindLogToggle() {
    $('#logtoggle').click(function() {

        $('#log').toggle();

        var logtoggle = $('#logtoggle');

        if (logtoggle.html() == 'Show Log') {
            logtoggle.html('Hide Log');
        } else {
            logtoggle.html('Show Log');
        }
    });
}


function endLogtail() {
    //noinspection JSUnresolvedFunction
    document.getElementById('logtail').contentWindow.updateLog();
    document.getElementById('logtail').contentWindow.stop();
}