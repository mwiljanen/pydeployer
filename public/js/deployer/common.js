function initDeployerCommon() {

    // deployer options
    $('#dep-op-view').click(function() {

        $('#deployment-options-wrapper').toggle();

        if ($('#deployment-options-wrapper:hidden')) {
            $('.help-spacer').hide();
            switchDepOpHelp('hide');
        }

        var depOpView = $('#dep-op-view');

        if (depOpView.html() == '<i class="icon-circle-arrow-down"></i>') {
            depOpView.html('<i class="icon-circle-arrow-up"></i>');
        } else {
            depOpView.html('<i class="icon-circle-arrow-down"></i>');
        }

    });

    var serverExcludesSelector = $('#serverExcludes');

    // deployer options help
    $('.deployer-option').tooltip();
    $('.has-tooltip').tooltip();

    serverExcludesSelector.popover({
        trigger: 'manual',
        placement: 'top'
    });

    $('#options-label').popover({
        trigger: 'manual',
        placement: 'top'
    });

    $('#manage-puppet-agent-label').popover({
       trigger: 'manual',
       placement: 'right'
    });

    $('#manage-service-label').popover({
        trigger: 'manual',
        placement: 'bottom'
    });

    // deployer options help
    $('#dep-op-help').click(function() {
        if ($('#deployment-options-wrapper').is(':visible')) {
            $('.help-spacer').toggle();
            switchDepOpHelp('toggle');
        }
    });


    // server excludes
    serverExcludesSelector.blur(function() {
        // remove line breaks
        $(this).val($(this).val().replace(/(\r\n|\n|\r)/gm,""));
    });

    // automatically check 'resetZenossProdState' when starting services from a stopped state
    $('#manageService1').click(function() {
        $('#resetZenossProdState').attr('checked', null);
    });

    $('#manageService2').click(function() {
        $('#resetZenossProdState').attr('checked', 'checked');
    });

    // safe mode
    $('#run').buttonset();
}


function switchDepOpHelp(state) {
    $('.control-label').popover(state);
    $('.servers').popover(state);
}