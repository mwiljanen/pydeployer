/*
 * Artifacts
 */
var artifactsPoll;

function pollForDeployingArtifacts() {
    artifactsPoll = setTimeout("getDeployingArtifacts()", 10000);
}


function getDeployingArtifacts() {

    $('#artifacts_deploying').find('li').remove();

    var artifactsCount = $('#artifacts_count');

    artifactsCount.html('0');

    $.getJSON("/dashboard/artifacts",
        {},
        function(data) {

            if (data instanceof Array) {

                artifactsCount.html(data.length);

                $(data).each(function() {
                    var name = this.name;
                    var env = this.env.toUpperCase();

                    var li = '<li><a href="/artifacts/view/name/' + name + '">' + name + '</a> : ' + env + '</li>';

                    $('#artifacts_deploying').append(li);
                });

            } else {
                artifactsCount.html('0');
            }
        }
    );

    pollForDeployingArtifacts();
}