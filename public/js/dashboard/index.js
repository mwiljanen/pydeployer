/*
 * Deployments
 */
var deploymentsPoll;

function pollForCurrentDeployments() {
    deploymentsPoll = setTimeout("getCurrentDeployments()", 10000);
}

function getCurrentDeployments() {

    var depsCountSelector = $('#deps_count');

    $('#current_deployments').find('.deployment').remove();
    depsCountSelector.html('0');

    $.getJSON("/dashboard/currentdeps/",
        {},
        function(data) {

            if (data instanceof Array) {

                depsCountSelector.html(data.length);

                $(data).each(function() {
                    var uniqueId = this.uniqueId;
                    var datetime = this.datetime;
                    var lastProgressBarValue = this.lastProgressBarValue;
                    var lastProgressBarMessage = this.lastProgressBarMessage;
                    var artifactsSelectedJson = $.parseJSON(this.artifactsSelected);
                    var artifactsSelected = '';
                    $.each(artifactsSelectedJson, function(index, artifact) {
                        artifactsSelected += artifact.name + ':' + artifact.env + ':' + artifact.versionToDeploy + ':' + artifact.jiraIssueKey + '<br/>';
                    });

                    var row = '<tr class="deployment"><td><a href="/deployer/watch/uniqueId/' + uniqueId + '">' + uniqueId + '</a></td>' +
                        '<td>' + datetime + '</td>' +
                        '<td>' + artifactsSelected + '</td>' +
                        '<td>' + lastProgressBarMessage + ' <strong>(' + lastProgressBarValue + '%)</strong>' + '</td>' +
                        '</tr>';

                    $('#current_deployments').append(row);
                });

            } else {
                depsCountSelector.html('0');
            }
        }
    );

    pollForCurrentDeployments();
}



/*
 * Servers
 */
var serversPoll;

function pollForDeployingServers() {
    serversPoll = setTimeout("getDeployingServers()", 10000);
}


function getDeployingServers() {

    var serversCountSelector = $('#servers_count');

    $('#servers_deploying').find('li').remove();
    serversCountSelector.html('0');

    $.getJSON("/dashboard/servers",
        {},
        function(data) {

            if (data instanceof Array) {

                serversCountSelector.html(data.length);

                $(data).each(function() {
                    var hostname = this.hostname;

                    var li = '<li>' + hostname + '</a></li>';

                    $('#servers_deploying').append(li);
                });

            } else {
                serversCountSelector.html('0');
            }
        }
    );

    pollForDeployingServers();
}