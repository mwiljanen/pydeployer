<?php

// require composer autoloader
require_once(__DIR__ . '/../vendor/autoload.php');

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));


/* If the APPLICATION_ENV environment variable is not set,
  define the application environment based on hostname.
  This is a custom addition to the default ZF index.php */
if (!getenv('APPLICATION_ENV')) {
    $localHostname = gethostname(); // only works on PHP 5.3
    
    if (preg_match('/dev/', $localHostname)) {
        define('APPLICATION_ENV', 'development');
    } elseif (preg_match('/qa/', $localHostname)) {
        define('APPLICATION_ENV', 'qa');
    } else {
        define('APPLICATION_ENV', 'production');
    }
}


// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));


// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

$configFiles = array(
	'application.ini',
	'classes.ini',
	'deployer.ini',
	'db.ini',
	'ldap.ini',
	'services.ini',
);

define('SYSTEM_CONFIG_PATH', '/etc/ibconf/deployer/');

$systemConfigFiles = scandir(SYSTEM_CONFIG_PATH);

foreach ($configFiles as $configFile) {

	if (in_array($configFile, $systemConfigFiles)) {
		// use system-level config file
		$config[] = SYSTEM_CONFIG_PATH . $configFile;
	} else {
		// use default config file
		$config[] = APPLICATION_PATH . '/configs/' . $configFile;
	}
}

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
	array('config' => $config)
);

$application->bootstrap();

if (PHP_SAPI != 'cli') // customization...only run application if not in CLI
{
    $application->run(); // only run if not on command line
}